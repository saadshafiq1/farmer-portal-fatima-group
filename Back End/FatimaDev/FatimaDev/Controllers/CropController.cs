﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Models;
using Infrastructure.OtherModels;
using Microsoft.AspNetCore.Authorization;
using Infrastructure.ViewModels;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Farmer")]
    public class CropController : ControllerBase
    {
        private readonly FFContext _context;
        ApiResponse response = new ApiResponse();
        public CropController(FFContext context)
        {
            _context = context;
        }

        // GET: api/TbCropTypeCodes
        [HttpGet]
        public ApiResponse GetCrops()
        {
            response.data = _context.TbCropTypeCode;
            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            return response;
        }

        // GET: api/TbCropTypeCodes/5
        [HttpGet("{id}")]
        public async Task<ApiResponse> GetCrops([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var tbCropTypeCode = await _context.TbCropTypeCode.FindAsync(id);

            if (tbCropTypeCode == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found";
                return response;
            }

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbCropTypeCode;
            return response;
        }

        [HttpGet("CropsOfBlock/{id}")]
        public async Task<ApiResponse> GetCropsOfBlock([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var cropsOfBlock = await _context.TbBlockCrops.Where(b => b.FarmBlockId == id).ToListAsync();

            if (cropsOfBlock == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found";
                return response;
            }

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = cropsOfBlock;
            return response;
        }

        // PUT: api/TbCropTypeCodes/5
        [HttpPut("{id}")]
        public async Task<ApiResponse> PutCrops([FromRoute] int id, [FromBody] TbCropTypeCode tbCropTypeCode)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            if (id != tbCropTypeCode.CropCode)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found";
                return response;
            }

            var cropsWithThisName = _context.TbCropTypeCode.Where(t => t.CropName == tbCropTypeCode.CropName);
            _context.Entry(tbCropTypeCode).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TbCropTypeCodeExists(id))
                {
                    response.responseCode = (int)Common.Common.ResponseCode.Failure;
                    response.responseMessage = "Not Found";
                    return response;
                }
                else
                {
                    throw;
                }
            }

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = cropsWithThisName;
            return response;
        }

        // POST: api/TbCropTypeCodes
        [HttpPost]
        public async Task<ApiResponse> PostCrops([FromBody] TbCropTypeCode tbCropTypeCode)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var temp = _context.TbCropTypeCode.Where(c => c.CropName == tbCropTypeCode.CropName).ToList();

            if (temp.Count == 0)
            {
                _context.TbCropTypeCode.Add(tbCropTypeCode);
                await _context.SaveChangesAsync();


                response.responseCode = (int)Common.Common.ResponseCode.Success;
                response.responseMessage = "Action Completed.";
                response.data = tbCropTypeCode;
                return response;
                // return CreatedAtAction("GetTbCropTypeCode", new { id = tbCropTypeCode.CropCode }, tbCropTypeCode);
            }
            else
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Record Already Exists.";
                response.data = tbCropTypeCode;
                return response;

            }
        }

        // DELETE: api/TbCropTypeCodes/5
        [HttpDelete("{id}")]
        public async Task<ApiResponse> DeleteCrops([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var tbCropTypeCode = await _context.TbCropTypeCode.FindAsync(id);
            if (tbCropTypeCode == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found";
                return response;
            }

            _context.TbCropTypeCode.Remove(tbCropTypeCode);
            await _context.SaveChangesAsync();

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbCropTypeCode;
            return response;
        }

        private bool TbCropTypeCodeExists(int id)
        {
            return _context.TbCropTypeCode.Any(e => e.CropCode == id);
        }

        [HttpPost("RegisterFarmPlan")]
        public ApiResponse RegisterFarmerPlan([FromBody]RegFarmerModel model)
        {

            ApiResponse response = new ApiResponse();

            try
            {
                var plan = GetGeneralCropPlan(model.cropId, model.sowingDate.Day, model.sowingDate.Month);

                TbFarmerCropPlan fcp = new TbFarmerCropPlan();
                fcp.ActiveStatus = "1";
                fcp.CropCaseCode = plan[0].CaseId;
                fcp.FarmerId = model.farmerId;
                fcp.SowingDate = model.sowingDate;

                FarmerCropPlan fplan = new FarmerCropPlan();
                fplan.Plan = plan;
                fplan.UnitIds = model.farmUnitIds;
                fplan.FarmId = model.farmId;

                //_context.Add(fcp);
                //_context.SaveChanges();

                List<TbFarmerCropPlanUnit> cpus = new List<TbFarmerCropPlanUnit>();
                foreach (var a in model.farmUnitIds)
                {
                    var cpu = new TbFarmerCropPlanUnit();
                    // cpu.FarmerCropPlanId = fcp.FarmerCropPlanId;
                    cpu.FarmUnitId = a;
                    cpu.ActiveStatus = "1";
                    cpus.Add(cpu);

                    fcp.TbFarmerCropPlanUnit.Add(cpu);
                }

                // _context.TbFarmerCropPlanUnit.AddRange(cpus);
                _context.TbFarmerCropPlan.Add(fcp);
                _context.SaveChanges();

                response.responseMessage = "Data Saved Successfully";
                response.responseCode = (int)Common.Common.ResponseCode.Success;
                response.data = fplan;

                return response;

            }
            catch (Exception ex)
            {
                response.responseMessage = "Error occured while saving";
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.data = null;

                return response;

            }

        }
        [HttpGet("Abc")]
        public RegFarmerModel GetAbc()
        {
            RegFarmerModel rm = new RegFarmerModel();
            rm.cropId = 1;

            rm.farmerId = 4;
            rm.farmUnitIds = new int[] { 1, 2, 3 };


            return rm;
        }

        [HttpGet("CropsOfFarmer/{farmerId}")]
        public ApiResponse CropsOfFarmer(int farmerId)
        {
            ApiResponse response = new ApiResponse();

            try
            {
                var cropPlans = _context.TbFarmerCropPlan.Include(c => c.CropCaseCodeNavigation).ThenInclude(c => c.CropCodeNavigation).Where(c => c.FarmerId == farmerId && c.ActiveStatus == "1").ToList().OrderBy(a => a.FarmerCropPlanId);
                List<FarmerCropsModel> farmerCrops = new List<FarmerCropsModel>();
                int i = 0;
                foreach (var cp in cropPlans)
                {
                    i++;
                    var fc = new FarmerCropsModel();
                    fc.CropName = cp.CropCaseCodeNavigation.CropCodeNavigation.CropName;
                    fc.CropImage = cp.CropCaseCodeNavigation.CropCodeNavigation.CropLogo;
                    fc.CropId = cp.CropCaseCodeNavigation.CropCode.Value;
                    fc.FarmerCropPlanId = cp.FarmerCropPlanId;
                    fc.SowingDate = cp.SowingDate;
                    fc.CropPlanNumber = i;
                    farmerCrops.Add(fc);
                }

                response.data = farmerCrops;
                response.responseMessage = "Data retrieved successfully.";
                response.responseCode = (int)Common.Common.ResponseCode.Success;

                return response;
            }
            catch (Exception ex)
            {
                response.data = null;
                response.responseMessage = "Error occured during executuion";
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                return response;
            }

        }

        //General Crop Plan

        [HttpGet("GeneralCropPlan")]
        public List<GeneralCropPlan> GetGeneralCropPlan(int cropid, int day, int month, int cropplanid = -1)
        {
            
                List<GeneralCropPlan> result = new List<GeneralCropPlan>();
                //find case between these dates against cropid
                var CropCase = _context.TbCropCaseMain.Where(c => c.CropCode == cropid).OrderBy(o => o.SowingBeginDate).ToList();
                TbCropCaseMain ourCase = null;

                var firstCase = CropCase[0];
                var lastCase = CropCase[CropCase.Count - 1];
                foreach (var c in CropCase)
                {
                    //date comparison without considering year

                    int beginMonth = c.SowingBeginDate.Month;
                    int endMonth = c.SowingEndDate.Month;

                    if (endMonth < beginMonth)
                    {
                        endMonth = endMonth + beginMonth;
                    }

                    if (month >= beginMonth && month <= endMonth)
                    {
                        if (month == beginMonth && month == endMonth)
                        {
                            if (day >= c.SowingBeginDate.Day && day <= c.SowingEndDate.Day)
                            {
                                ourCase = c;
                            }
                        }
                        else
                        if (month == c.SowingBeginDate.Month)
                        {

                            if (day >= c.SowingBeginDate.Day)
                            {
                                ourCase = c;
                            }

                        }
                        else if (month == c.SowingEndDate.Month)

                        {
                            if (day <= c.SowingEndDate.Day)
                            {
                                ourCase = c;
                            }
                        }

                    }
                }

                //checking if the sowing date is outside the boundaries of our cases, in which case the nearest one will be fetched.
                if (ourCase == null)
                {
                    int temp1 = firstCase.SowingBeginDate.Month - month;
                    if (temp1 < 0)
                    {
                        temp1 = temp1 + 12;
                    }

                    int temp2 = month - lastCase.SowingBeginDate.Month;

                    if (temp2 < 0)
                    {
                        temp2 = temp2 + 12;
                    }

                    if (temp1 <= temp2)
                    {
                        ourCase = firstCase;
                    }
                    else
                    {
                        ourCase = lastCase;
                    }

                    //if (month <= firstCase.SowingBeginDate.Month)
                    //{
                    //    ourCase = firstCase;
                    //}
                    //else { ourCase = lastCase; }
                }
                if (ourCase != null)
                {
                    var cropPlan = _context.TbCropCalenderMain.Where(c => c.CropCaseCode == ourCase.CropCaseCode).Include(a => a.CropStepCodeNavigation).ThenInclude(b => b.CropCodeNavigation).ToList();

                    foreach (var a in cropPlan)
                    {
                        GeneralCropPlan gp = new GeneralCropPlan();
                        gp.CropId = a.CropStepCodeNavigation.CropCodeNavigation.CropCode;
                        gp.AudioUrl = a.CropStepCodeNavigation.AudioUrl;
                        gp.DaysafterpreviousStep = a.DaysafterpreviousStep;
                        gp.VideoUrl = a.CropStepCodeNavigation.VideoUrl;
                        gp.StepNumber = a.CropStepCodeNavigation.StepNumber;
                        gp.StepImage = a.CropStepCodeNavigation.StepImage;
                        gp.CropName =  a.CropStepCodeNavigation.CropCodeNavigation.CropName;
                        gp.StepName= a.CropStepCodeNavigation.StepNameUrder;
                        gp.StartDate = a.StepStartDate;
                        gp.EndDate = a.StepEndDate;
                        gp.StepDetail = a.CropStepCodeNavigation.StepDetail;
                        gp.CaseId = a.CropCaseCode.Value;
                        var stepActions = _context.TbCropStepAction.Where(q => q.CropStepCode == a.CropStepCode).ToList();

                        List<CropActions> cropStepActions = new List<CropActions>();

                        foreach (var cs in stepActions)
                        {
                            var temp = new CropActions();

                            temp.Detail = cs.Detail;
                            temp.IsAlert = cs.IsAlert;
                            cropStepActions.Add(temp);
                        }

                        gp.StepActions = cropStepActions;
                        result.Add(gp);
                    }

                }

                return result;
            }

        [HttpGet("CropPlan/{cropPlanId}")]
        public ApiResponse GetCropPlanWithId(int cropPlanId)
        {

            ApiResponse response = new ApiResponse();

            try
            {
                FarmerCropPlan fcp = new FarmerCropPlan();

                List<GeneralCropPlan> result = new List<GeneralCropPlan>();
                var units = _context.TbFarmerCropPlanUnit.Where(a => a.FarmerCropPlanId == cropPlanId).ToList();
                var cropCase = _context.TbFarmerCropPlan.Where(a => a.FarmerCropPlanId == cropPlanId).FirstOrDefault();
                int cropCaseCode = cropCase.CropCaseCode.Value;
                //fcp.FarmId = (int)cropCase.FarmId.Value;
                fcp.UnitIds = new int[units.Count()];
                int i = 0;
                foreach (var a in units)
                {
                    fcp.UnitIds[i] = (int)a.FarmUnitId.Value;
                    i++;
                }
                var cropPlan = _context.TbCropCalenderMain.Where(c => c.CropCaseCode == cropCaseCode).Include(a => a.CropStepCodeNavigation).ThenInclude(b => b.CropCodeNavigation).ToList();

                DateTime sowingDate = cropCase.SowingDate.Value;
                var year = sowingDate.Year;

                DateTime previousDate = new DateTime();
                foreach (var a in cropPlan)
                {
                    GeneralCropPlan gp = new GeneralCropPlan();


                    var stepNumber =a.CropStepCodeNavigation.StepNumber;

                    if (stepNumber == 1)
                    {
                        gp.StartDate = cropCase.SowingDate;
                        DateTime edate = Convert.ToDateTime(gp.StartDate);
                        edate=edate.AddDays(a.StepMarginDays.Value-1);
                        gp.EndDate = edate;

                        previousDate = edate;
                    }
                    else
                    {
                        gp.StartDate = previousDate.AddDays(a.DaysafterpreviousStep.Value+1);//add 1
                        DateTime edate = Convert.ToDateTime(gp.StartDate);
                        edate=edate.AddDays(a.StepMarginDays.Value-1);
                        gp.EndDate = edate;
                        previousDate = edate;

                    }
                    gp.CropId = a.CropStepCodeNavigation.CropCodeNavigation.CropCode;
                    gp.AudioUrl = a.CropStepCodeNavigation.AudioUrl;

                    gp.DaysafterpreviousStep = a.DaysafterpreviousStep;

                    gp.VideoUrl = a.CropStepCodeNavigation.VideoUrl;
                    gp.StepNumber = a.CropStepCodeNavigation.StepNumber;
                    gp.StepName = a.CropStepCodeNavigation.StepNameUrder;
                    gp.StepImage = a.CropStepCodeNavigation.StepImage;
                    gp.CropName = a.CropStepCodeNavigation.CropCodeNavigation.CropName;

                    //gp.StartDate = a.StepStartDate;
                   // gp.EndDate = a.StepEndDate;

                    gp.StepDetail = a.CropStepCodeNavigation.StepDetail;
                    gp.CaseId = a.CropCaseCode.Value;
                    var stepActions = _context.TbCropStepAction.Where(q => q.CropStepCode == a.CropStepCode).ToList();

                    List<CropActions> cropStepActions = new List<CropActions>();

                    foreach (var cs in stepActions)
                    {
                        var temp = new CropActions();

                        temp.Detail = cs.Detail;
                        temp.IsAlert = cs.IsAlert;
                        cropStepActions.Add(temp);
                    }

                    gp.StepActions = cropStepActions;
                    result.Add(gp);
                }

                fcp.Plan = result;

                response.data = fcp;
                response.responseCode = (int)Common.Common.ResponseCode.Success;
                response.responseMessage = "Data retrieved successfully.";
                return response;
            } catch (Exception ex)
            {
                response.data = null;
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Error occured while retrieving data.";
                return response;
            }
        }

    }
    //General Crop Plan

    //Get crop plan with crop ip

 

}



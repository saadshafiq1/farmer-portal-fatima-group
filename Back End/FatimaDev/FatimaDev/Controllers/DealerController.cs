﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Models;
using Infrastructure.OtherModels;
using Microsoft.AspNetCore.Authorization;
using Infrastructure.ViewModels;
using Core.Interfaces;
using AutoMapper;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Farmer")]

    public class DealerController : ControllerBase
    {
        private readonly FFContext _Context;
        private readonly IMapper _Mapper;

        ApiResponse response = new ApiResponse();
        public DealerController(FFContext _context, IMapper _mapper)
        {
            _Context = _context;
            _Mapper = _mapper;
        }

        // GET: api/tbDealerInfo
        [HttpGet]
        public ApiResponse GetDealers()
        {
            response.data = _Context.TbDealerInfo;
            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            return response;
        }

        // GET: api/tbDealerInfo/5
        [HttpGet("{id}")]
        public async Task<ApiResponse> GetDealers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var tbDealerType = await _Context.TbDealerInfo.FindAsync(id);

            if (tbDealerType == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found";
                return response;
            }

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbDealerType;
            return response;
        }

        // GET: api/GetSeedDealers?seedID=5&districtCode=5
        [HttpGet("GetSeedDealers")]
        public ApiResponse GetSeedDealers(int seedID, int districtCode)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var tbDealerDetail = _Context.TbDealerSeedDetails.Where(d => d.AvailableSeedId == (seedID == 0 ? d.AvailableSeedId : seedID) && d.Dealer.DistrictCode == (districtCode==0? d.Dealer.DistrictCode: districtCode));

            if (tbDealerDetail == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found";
                return response;
            }
            var dealerIDs = tbDealerDetail.Select(a => a.DealerId).ToList();
            var tbDealerInfo = _Context.TbDealerInfo.Where(d => dealerIDs.Contains(d.DealerId)).Include(d => d.DistrictCodeNavigation);
            var result = _Mapper.Map<List<DealerInfoModel>>(tbDealerInfo);
            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = result;
            return response;
        }

        // GET: api/GetFertilizerDealers?fertilizerID=5&districtCode=5
        [HttpGet("GetFertilizerDealers")]
        public ApiResponse GetFertilizerDealers(int fertilizerID, int districtCode)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }


            var tbDealerDetail = _Context.TbDealerFertilizerDetails.Where(d => d.AvailableFertilizerId == (fertilizerID == 0 ? d.AvailableFertilizerId : fertilizerID) && d.Dealer.DistrictCode == (districtCode == 0 ? d.Dealer.DistrictCode : districtCode));
          
            if (tbDealerDetail == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found";
                return response;
            }

            var dealerIDs = tbDealerDetail.Select(a => a.DealerId).ToList();
            var tbDealerInfo = _Context.TbDealerInfo.Where(d => dealerIDs.Contains(d.DealerId)).Include(d=>d.DistrictCodeNavigation);
            var result = _Mapper.Map<List<DealerInfoModel>>(tbDealerInfo);
            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = result;
            return response;
        }

        // PUT: api/tbDealerInfo/5
        [HttpPut("{id}")]
        public async Task<ApiResponse> PutDealers([FromRoute] int id, [FromBody] TbDealerInfo tbDealerType)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            if (id != tbDealerType.DealerId)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found";
                return response;
            }

            _Context.Entry(tbDealerType).State = EntityState.Modified;

            try
            {
                await _Context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tbDealerTypeExists(id))
                {
                    response.responseCode = (int)Common.Common.ResponseCode.Failure;
                    response.responseMessage = "Not Found";
                    return response;
                }
                else
                {
                    throw;
                }
            }

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbDealerType;
            return response;
        }

        // POST: api/tbDealerInfo
        [HttpPost]
        public async Task<ApiResponse> PostDealers([FromBody] TbDealerInfo tbDealerType)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var temp = _Context.TbDealerInfo.Where(c => c.DealerName == tbDealerType.DealerName).ToList();

            if (temp.Count == 0)
            {
                _Context.TbDealerInfo.Add(tbDealerType);
                await _Context.SaveChangesAsync();


                response.responseCode = (int)Common.Common.ResponseCode.Success;
                response.responseMessage = "Action Completed.";
                response.data = tbDealerType;
                return response;
            }
            else
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Record Already Exists.";
                response.data = tbDealerType;
                return response;

            }
        }

        // DELETE: api/tbDealerInfo/5
        [HttpDelete("{id}")]
        public async Task<ApiResponse> DeleteDealers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var tbDealerInfo = await _Context.TbDealerInfo.FindAsync(id);
            if (tbDealerInfo == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found";
                return response;
            }

            _Context.TbDealerInfo.Remove(tbDealerInfo);
            await _Context.SaveChangesAsync();

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbDealerInfo;
            return response;
        }

        private bool tbDealerTypeExists(int id)
        {
            return _Context.TbDealerInfo.Any(e => e.DealerId == id);
        }

    }

}



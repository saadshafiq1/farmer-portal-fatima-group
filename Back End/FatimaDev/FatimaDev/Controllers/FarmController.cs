﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Models;
using Infrastructure.OtherModels;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;
using Infrastructure.ViewModels;

namespace API.Controllers
{
   
    [Route("api/[controller]")]
    [ApiController]
    public class FarmController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly FFContext _context;
        ApiResponse response = new ApiResponse();
        public FarmController(FFContext context,IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/TbFarmInfoes
        [HttpGet]
        public ApiResponse GetTbFarmInfo()
        {

            response.data = _context.TbFarmInfo;
            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Data Retrieved";
            return response;
        }

        // GET: api/TbFarmInfoes/5


        [HttpGet("FarmsOfFarmer/{id}")]
        public async Task<ApiResponse> FarmsOfFarmer([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var tbFarmInfo = await _context.TbFarmInfo.Include(c=>c.TbFarmBlock).Where(f=>f.FarmerId==id).ToListAsync();
            
            var result = _mapper.Map<List<FarmModel>>(tbFarmInfo);

            foreach (var a in result)
            {
                a.TbFarmBlock = _mapper.Map<List<FarmBlockModel>>(a.TbFarmBlock); ;
                a.FarmType = _context.TbFarmTypeCode.Find(a.FarmTypeCode==null?1:a.FarmTypeCode).TypeName;
                a.TotalUnits = _context.TbFarmUnit.Where(i => i.FarmId == a.FarmId).ToList().Count();
            }
            if (tbFarmInfo == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found";
                return response;
            }

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = result;
            return response;
        }

        [HttpGet("{id}")]
        public async Task<ApiResponse> GetTbFarmInfo([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }
            
            var tbFarmInfo = await _context.TbFarmInfo.Include(c=>c.TbFarmBlock).FirstOrDefaultAsync(i=>i.FarmId==id);
            

            if (tbFarmInfo == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found.";
                return response;
            }

            //int count = 0;

            tbFarmInfo.TbFarmBlock = await _context.TbFarmBlock.Where(i => i.FarmId == tbFarmInfo.FarmId).ToListAsync();

            foreach (var a in tbFarmInfo.TbFarmBlock)
            {
                a.Farm = null;
                a.TbFarmBlockUnit = null;
            }


            // tbFarmInfo.TotalUnits = _context.TbFarmUnit.Where(f => f.FarmId == id).ToList().Count;
            //tbFarmInfo.TbFarmUnit = null;

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbFarmInfo;
            return response;
            
        }



        // PUT: api/TbFarmInfoes/5
        [HttpPut("{id}")]
        public async Task<ApiResponse> PutFarmInfo([FromRoute] long id, [FromBody] TbFarmInfo tbFarmInfo)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            if (id != tbFarmInfo.FarmId)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not found.";
                return response;
            }

            _context.Entry(tbFarmInfo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();

                response.responseCode = (int)Common.Common.ResponseCode.Success;
                response.responseMessage = "Action Completed.";
                response.data = tbFarmInfo;
            }
            catch (Exception ex)
            {
                if (!TbFarmInfoExists(id))
                {
                    response.responseCode = (int)Common.Common.ResponseCode.Failure;
                    response.responseMessage = "Could not save.";
                    return response;
                }
                else
                {
                    throw;
                }
            }

            return response;


        }

        [HttpPut("FarmUnit/{id}")]
        public async Task<ApiResponse> PutFarmUnit([FromRoute] long id, [FromBody] TbFarmUnit unitInfo)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            if (id != unitInfo.FarmUnitId)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not found.";
                return response;
            }

            _context.Entry(unitInfo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();

                response.responseCode = (int)Common.Common.ResponseCode.Success;
                response.responseMessage = "Action Completed.";
                response.data = unitInfo;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TbFarmInfoExists(id))
                {
                    response.responseCode = (int)Common.Common.ResponseCode.Failure;
                    response.responseMessage = "Could not save.";
                    return response;
                }
                else
                {
                    throw;
                }
            }

            return response;

        }

        [HttpPut("BlockUnit/{id}")]
        public async Task<ApiResponse> PutBlockUnit([FromRoute] long id, [FromBody] TbFarmBlockUnit unitInfo)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            if (id != unitInfo.FarmBlockUnitId)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not found.";
                return response;
            }

            _context.Entry(unitInfo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();

                response.responseCode = (int)Common.Common.ResponseCode.Success;
                response.responseMessage = "Action Completed.";
                response.data = unitInfo;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TbFarmInfoExists(id))
                {
                    response.responseCode = (int)Common.Common.ResponseCode.Failure;
                    response.responseMessage = "Could not save.";
                    return response;
                }
                else
                {
                    throw;
                }
            }

            return response;

        }

        // POST: api/TbFarmInfoes
        [HttpPost]
        public async Task<ApiResponse> PostTbFarmInfo([FromBody] TbFarmInfo tbFarmInfo)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            try
            {
                _context.TbFarmInfo.Add(tbFarmInfo);
                await _context.SaveChangesAsync();

                response.responseCode = (int)Common.Common.ResponseCode.Success;
                response.responseMessage = "Action Completed.";
                response.data = tbFarmInfo;
            }
            catch (Exception ex)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Could not save.";
            }
            return response;

        }

        // DELETE: api/TbFarmInfoes/5
        [HttpDelete("{id}")]
        public async Task<ApiResponse> DeleteTbFarmInfo([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var tbFarmInfo = await _context.TbFarmInfo.FindAsync(id);
            if (tbFarmInfo == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found.";
                return response;
            }

            _context.TbFarmInfo.Remove(tbFarmInfo);
            await _context.SaveChangesAsync();
            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = id;
            return response;
        }

        private bool TbFarmInfoExists(long id)
        {
            return _context.TbFarmInfo.Any(e => e.FarmId == id);
        }


        [HttpGet("BlocksOfFarms/{id}")]
        public async Task<ApiResponse> BlocksOfFarms([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var tbFarmInfo = await _context.TbFarmBlock.Where(t => t.FarmId == id).ToListAsync();

            if (tbFarmInfo == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not found.";
                return response;
            }

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbFarmInfo;
            return response;
        }

        [HttpGet("UnitsOfFarms/{id}")]
        public async Task<ApiResponse> UnitsOfFarms([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var tbFarmInfo = await _context.TbFarmUnit.Where(t => t.FarmId == id).ToListAsync();

            if (tbFarmInfo == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not found.";
                return response;
            }

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbFarmInfo;
            return response;
        }

        [HttpGet("UnitsOfBlocks/{id}")]
        public async Task<ApiResponse> UnitsOfBlocks([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var tbFarmInfo = await _context.TbFarmBlockUnit.Where(t => t.FarmBlockId == id).ToListAsync();

            if (tbFarmInfo == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not found.";
                return response;
            }

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbFarmInfo;
            return response;
        }

        [HttpPost("FarmUnit")]
        public async Task<ApiResponse> PostFarmBlock([FromBody] TbFarmUnit tbfarmunit)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
            }

            try
            {
                _context.TbFarmUnit.Add(tbfarmunit);
                await _context.SaveChangesAsync();

                response.responseCode = (int)Common.Common.ResponseCode.Success;
                response.responseMessage = "Action Completed.";
                response.data = tbfarmunit;
            }
            catch (Exception ex)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = ex.ToString();
            }
            return response;
        }

        [HttpPost("BlockUnit")]
        public async Task<ApiResponse> PostBlockUnit([FromBody] TbFarmBlockUnit tbfarmblockunit)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
            }

            try
            {
                _context.TbFarmBlockUnit.Add(tbfarmblockunit);
                await _context.SaveChangesAsync();

                response.responseCode = (int)Common.Common.ResponseCode.Success;
                response.responseMessage = "Action Completed.";
                response.data = tbfarmblockunit;
            }
            catch (Exception ex)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = ex.ToString();
            }
            return response;
        }

        [HttpPut("Block/{id}")]
        public async Task<ApiResponse> PutBlock([FromRoute] long id, [FromBody] TbFarmBlock unitInfo)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            if (id != unitInfo.FarmBlockId)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not found.";
                return response;
            }

            _context.Entry(unitInfo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();

                response.responseCode = (int)Common.Common.ResponseCode.Success;
                response.responseMessage = "Action Completed.";
                response.data = unitInfo;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TbFarmInfoExists(id))
                {
                    response.responseCode = (int)Common.Common.ResponseCode.Failure;
                    response.responseMessage = "Could not save.";
                    return response;
                }
                else
                {
                    throw;
                }
            }

            return response;

        }

        [HttpPost("FarmBlock")]
        public async Task<ApiResponse> PostFarmBlock([FromBody] TbFarmBlock tbfarmBlock)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
            }

            try
            {
                _context.TbFarmBlock.Add(tbfarmBlock);
                await _context.SaveChangesAsync();

                response.responseCode = (int)Common.Common.ResponseCode.Success;
                response.responseMessage = "Action Completed.";
                response.data = tbfarmBlock;
            }
            catch (Exception ex)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = ex.ToString();
            }
            return response;
        }

        [HttpGet("TypesOfFarm")]
        public async Task<ApiResponse> TypesOfFarm()
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var tbFarmInfo = await _context.TbFarmTypeCode.ToListAsync();

            if (tbFarmInfo == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Farm Not Found.";
            }

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbFarmInfo;
            return response;
        }

    }
}
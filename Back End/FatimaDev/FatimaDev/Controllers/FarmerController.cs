﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Models;
using Microsoft.AspNetCore.Authorization;
using API.Services;
using Infrastructure.OtherModels;
using API.Common;
using System.Net.Http;

namespace API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class FarmerController : ControllerBase
    {
        private readonly FFContext _context;
        private readonly IUserService _userService;
        ApiResponse response = new ApiResponse();

        public FarmerController(FFContext context,IUserService userService)
        {
            _context = context;
            _userService = userService;
        }

        // GET: api/TbFarmerInfoes
        [HttpGet]
        public ApiResponse GetFarmer()
        {

            try
            {
                response.data = _context.TbFarmerInfo;
                response.responseCode = (int)Common.Common.ResponseCode.Success;
                response.responseMessage = "Action Completed.";
            }
            catch (Exception ex)
            {
                response.responseCode= (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = ex.ToString();
            }
            return response;
        }

        // GET: api/TbFarmerInfoes/5
        [HttpGet("{id}")]
        public async Task<ApiResponse> GetFarmer([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not validated.";
                return response;
            }

            try
            {
                var tbFarmerInfo = await _context.TbFarmerInfo.Where(i=>i.FarmerId==id).FirstOrDefaultAsync();
              //  var tbFarmerInfo = await _context.TbFarmerInfo.FindAsync(id);
                //var tbFarms = _context.TbFarmerInfo.Where(a => a.FarmerId == id);
                //tbFarmerInfo.TbFarmInfo = tbFarms;


                if (tbFarmerInfo == null)
                {
                    response.responseCode = (int)Common.Common.ResponseCode.Failure;
                    response.responseMessage = "Not Found.";

                    return response;

                }
                response.responseCode = (int)Common.Common.ResponseCode.Success;
                response.responseMessage = "Action Completed.";
                response.data = tbFarmerInfo;
                return response;

            }
            catch (Exception ex)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = ex.ToString();

                return response;
            }


        }

        // PUT: api/TbFarmerInfoes/5
        [HttpPut("{id}")]
        public async Task<ApiResponse> PutFarmer([FromRoute] long id, [FromBody] TbFarmerInfo tbFarmerInfo)
        {
           
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage ="Model State Validation Failed.";
                return response;
            }

            if (id != tbFarmerInfo.FarmerId)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "id is not equal to farmer object id";
                return response;
            }
            _context.Entry<TbFarmerInfo>(tbFarmerInfo).State = EntityState.Modified;

            // var tbfarmerold = _context.TbFarmerInfo.Where(i => i.FarmerId == id).FirstOrDefault();
            //_context.Set<TbFarmerInfo>.SetTryUpdateModelAsync(tbFarmerInfo);
            //TbUserLogin tempuser = new TbUserLogin();

            try
            {
                var tempUser = _context.TbUserLogin.FirstOrDefault(i => i.UserId == tbFarmerInfo.UserGuid);

                if (tempUser.CellPhone.ToString() != tbFarmerInfo.CellPhone.ToString())
                    if (tempUser != null)
                    {
                        //check if the cellphone number already exists
                        if (_context.TbUserLogin.Where(c => c.CellPhone.Contains(tbFarmerInfo.CellPhone)).ToList().Count >= 1)
                        {
                            response.responseCode = (int)Common.Common.ResponseCode.Failure;
                            response.responseMessage = "Cellphone number already registered.Please provide unique phone number.";
                            response.data = null;

                            return response;
                        }

                        tempUser.CellPhone = tbFarmerInfo.CellPhone;
                        var updatedEntity = _context.Set<TbUserLogin>().Update(tempUser);

                        //updatedEntity.State = EntityState.Modified;
                       // _context.SaveChanges();

                    }
                await _context.SaveChangesAsync();
               
                response.responseCode = (int)Common.Common.ResponseCode.Success;
                response.responseMessage = "Action Completed.";
                response.data= tbFarmerInfo;
            }
            catch (Exception ex)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                if (!TbFarmerInfoExists(id))
                {
                   
                    response.responseMessage = "ID doesn't exist.";
                    return response;
                }
                else
                {
                    response.responseMessage = "DB ERROR";
                }
            }

            return response;
        }

        // POST: api/TbFarmerInfoes
        [AllowAnonymous]
        [HttpPost]
        public async Task<ApiResponse> PostFarmer([FromBody] TbFarmerInfo tbFarmerInfo)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State Validation Failed.";
                return response;
            }

            try
            {
                //check if same cellphone already exists
                var temp = _context.TbFarmerInfo.Where(x => x.CellPhone == tbFarmerInfo.CellPhone).FirstOrDefault();

                if (temp != null)
                {
                    response.responseCode = (int)Common.Common.ResponseCode.Failure;
                    response.responseMessage = "phone number already registered.";
                    return response;
                }
                var guid = Guid.NewGuid();
                tbFarmerInfo.UserGuid = guid;

                _context.TbFarmerInfo.Add(tbFarmerInfo);
               // await _context.SaveChangesAsync();

                Random generator = new Random();
                string r = generator.Next(0, 99999).ToString("D5");

                TbUserLogin userLogin = new TbUserLogin();
                userLogin.CellPhone = tbFarmerInfo.CellPhone;
                userLogin.Passkey = r;
                userLogin.InsertionDate = DateTime.Now;
                userLogin.ActiveStatus = "1";
                userLogin.UserRoleCode = _context.TbUserRoleTypeCode.Where(x => x.RoleName == "Farmer").FirstOrDefault().UserRoleCode;
                userLogin.UserId = guid;
                _context.TbUserLogin.Add(userLogin);

                await _context.SaveChangesAsync();

                //string token = _userService.Authenticate(userLogin.CellPhone, userLogin.Passkey);

                response.responseCode = (int)Common.Common.ResponseCode.Success;
                response.responseMessage = "Action Completed:username:" + userLogin.CellPhone+ " password:"+ userLogin.Passkey;
                response.data = tbFarmerInfo;

                //HttpClient client = new HttpClient();
                //string uri = String.Format("https://sendpk.com/api/sms.php?username=923238782529&password=7526&sender=Masking&mobile={0}&message={1}", userLogin.CellPhone, userLogin.Passkey);
                ////client.DefaultRequestHeaders.Add("Authorization", "token ADD YOUR OAUTH TOKEN");
                //client.DefaultRequestHeaders.Add("User-Agent", "WebAPi");
                //var content = client.GetStringAsync(uri);

            } catch (Exception ex)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = ex.ToString();
            }
            //Call Message Sending API here

            return response;
             //return Ok(new {responseCode="1",responseMessage=token, data=userLogin });
            //return CreatedAtAction("GetTbFarmerInfo", new { id = tbFarmerInfo.FarmerId }, userLogin);
        }

        // DELETE: api/TbFarmerInfoes/5
        [HttpDelete("{id}")]
        public async Task<ApiResponse> DeleteFarmer([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State Validation Failed.";
            }

            var tbFarmerInfo = await _context.TbFarmerInfo.FindAsync(id);
            if (tbFarmerInfo == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State Validation Failed.";
            }

            try
            {
                _context.TbFarmerInfo.Remove(tbFarmerInfo);
                await _context.SaveChangesAsync();
                response.responseCode = (int)Common.Common.ResponseCode.Success;
                response.responseMessage = "Action Completed.";
                response.data = id;
            }
            catch (Exception ex)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = ex.ToString();
            }
            return response;
        }

        private bool TbFarmerInfoExists(long id)
        {
            return _context.TbFarmerInfo.Any(e => e.FarmerId == id);
        }
    }
}
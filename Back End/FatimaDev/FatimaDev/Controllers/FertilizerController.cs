﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Models;
using Infrastructure.OtherModels;
using Microsoft.AspNetCore.Authorization;
using Infrastructure.ViewModels;
using Core.Interfaces;
using AutoMapper;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Farmer")]

    public class FertilizerController : ControllerBase
    {
        private readonly FFContext _Context;
        private readonly IMapper _Mapper;

        ApiResponse response = new ApiResponse();
        public FertilizerController(FFContext _context, IMapper _mapper)
        {
            _Context = _context;
            _Mapper = _mapper;
        }

        // GET: api/tbFertilizerTypes
        [HttpGet]
        public ApiResponse GetFertilizers()
        {
            var tbFertilizerTypes = _Context.TbFertilizerTypes.Include(f => f.TbFertilizerTypeDetails);
            var result = _Mapper.Map<List<FertilizerTypesModel>>(tbFertilizerTypes);
            response.data = result;
            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            return response;
        }

        // GET: api/tbFertilizerTypes/5
        [HttpGet("{id}")]
        public async Task<ApiResponse> GetFertilizers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var tbFertilizerType = await _Context.TbFertilizerTypes.FindAsync(id);

            if (tbFertilizerType == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found";
                return response;
            }

            var result = _Mapper.Map<List<FertilizerTypesModel>>(tbFertilizerType);
            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = result;
            return response;
        }

        // PUT: api/tbFertilizerTypes/5
        [HttpPut("{id}")]
        public async Task<ApiResponse> PutFertilizers([FromRoute] int id, [FromBody] TbFertilizerTypes tbFertilizerType)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            if (id != tbFertilizerType.FertilizerId)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found";
                return response;
            }

            _Context.Entry(tbFertilizerType).State = EntityState.Modified;

            try
            {
                await _Context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tbFertilizerTypeExists(id))
                {
                    response.responseCode = (int)Common.Common.ResponseCode.Failure;
                    response.responseMessage = "Not Found";
                    return response;
                }
                else
                {
                    throw;
                }
            }

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbFertilizerType;
            return response;
        }

        // POST: api/tbFertilizerTypes
        [HttpPost]
        public async Task<ApiResponse> PostFertilizers([FromBody] TbFertilizerTypes tbFertilizerType)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var temp = _Context.TbFertilizerTypes.Where(c => c.FertilizerName == tbFertilizerType.FertilizerName).ToList();

            if (temp.Count == 0)
            {
                _Context.TbFertilizerTypes.Add(tbFertilizerType);
                await _Context.SaveChangesAsync();


                response.responseCode = (int)Common.Common.ResponseCode.Success;
                response.responseMessage = "Action Completed.";
                response.data = tbFertilizerType;
                return response;
            }
            else
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Record Already Exists.";
                response.data = tbFertilizerType;
                return response;

            }
        }

        // DELETE: api/tbFertilizerTypes/5
        [HttpDelete("{id}")]
        public async Task<ApiResponse> DeleteFertilizers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var tbFertilizerTypes = await _Context.TbFertilizerTypes.FindAsync(id);
            if (tbFertilizerTypes == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found";
                return response;
            }

            _Context.TbFertilizerTypes.Remove(tbFertilizerTypes);
            await _Context.SaveChangesAsync();

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbFertilizerTypes;
            return response;
        }

        private bool tbFertilizerTypeExists(int id)
        {
            return _Context.TbFertilizerTypes.Any(e => e.FertilizerId == id);
        }

    }

}



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure.Models;
using Infrastructure.OtherModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]

    public class LookupController : ControllerBase
    {

        private readonly FFContext _context;
        ApiResponse response = new ApiResponse();

        public LookupController(FFContext context)
        {
            _context = context;
        }

        // GET: api/TbFarmerInfoes
        [HttpGet("Province")]
        public ApiResponse GetProvinceInfo()
        {
            response.data = _context.TbProvinceCode;
            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Data Retrieved";
            return response;
        }

        //GET: api/TbFarmerInfoes/5
        [HttpGet("Province/{id}")]
        public async Task<ApiResponse> GetProvinceInfo([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var tbProvinceInfo = await _context.TbProvinceCode.FindAsync(id);

            if (tbProvinceInfo == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found";
                return response;
            }

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbProvinceInfo;
            return response;
        }

        //GET: api/TbFarmerInfoes/5
        [HttpGet("DistrictsOfProvince/{id}")]
        public async Task<ApiResponse> GetDistrictsOfProvince([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var tbDistrictInfo = await _context.TbDistrictCode.Where(p=>p.ProvinceCode==id).ToListAsync();

            if (tbDistrictInfo == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found";
                return response;
            }

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbDistrictInfo;
            return response;
        }

        [HttpGet("TehsilsOfDistrict/{id}")]
        public async Task<ApiResponse> GetTehsilsOfDistrict([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var tbTehsilInfo = await _context.TbTehsilCode.Where(p => p.DistrictCode == id).ToListAsync();

            if (tbTehsilInfo == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found";
                return response;
            }

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbTehsilInfo;
            return response;
        }

        [HttpGet("Tehsil/{id}")]
        public async Task<ApiResponse> GetTehsilInfo([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var tbTehsilInfo = await _context.TbTehsilCode.Where(t => t.TehsilCode == id).FirstOrDefaultAsync();

            if (tbTehsilInfo == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found";
                return response;
            }


            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbTehsilInfo;
            return response;
        }

        [HttpGet("District/{id}")]
        public async Task<ApiResponse> GetDistrictInfo([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }


            var tbDistInfo = await _context.TbDistrictCode.FindAsync(id);

            if (tbDistInfo == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found";
                return response;
            }


            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbDistInfo;
            return response;
        }


        [HttpGet("FarmShape/{id}")]
        public async Task<ApiResponse> GetFarmShape([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }


            var tbDistInfo =  _context.TbFarmShapes.FirstOrDefault(i=>i.FarmId==id);

            if (tbDistInfo == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found";
                return response;
            }


            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbDistInfo;
            return response;
        }

        [HttpPost("FarmShape")]
        public async Task<ApiResponse> CreateFarmShape([FromBody] TbFarmShapes farmshape)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            try
            {

                _context.TbFarmShapes.Add(farmshape);
                await _context.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Error occured while saving.";
                response.data = null;
                return response;
            }
        

            //var tbDistInfo = await _context.TbFarmShapes.FindAsync(id);

            //if (tbDistInfo == null)
            //{
            //    response.responseCode = (int)Common.Common.ResponseCode.Failure;
            //    response.responseMessage = "Not Found";
            //    return response;
            //}


            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = farmshape;
            return response;
        }

        [HttpPost("UnitShape")]
        public async Task<ApiResponse> CreateUnitShape([FromBody] TbUnitShapes unitShape)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            try
            {

                _context.TbUnitShapes.Add(unitShape);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Error occured while saving.";
                response.data = null;
                return response;
            }


            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = unitShape;
            return response;
        }


        [HttpGet("UnitShape/{id}")]
        public async Task<ApiResponse> FarmUnitShapes([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }


            var tbDistInfo = await _context.TbUnitShapes.Include(c=>c.Unit).Where(i=>i.Unit.FarmId==id).ToListAsync();

            if (tbDistInfo == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found";
                return response;
            }

            foreach (var unit in tbDistInfo)
            {
                unit.Unit = null;
            }

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbDistInfo;
            return response;
        }

    }
}
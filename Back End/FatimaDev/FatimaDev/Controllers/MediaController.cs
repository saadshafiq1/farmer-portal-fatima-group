﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Models;
using Infrastructure.OtherModels;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MediaController : ControllerBase
    {
        private readonly FFContext _context;
        public ApiResponse response=new ApiResponse();
        public MediaController(FFContext context)
        {
            _context = context;
        }

        // GET: api/TbMedia
        [HttpGet]
        public IEnumerable<TbMedia> GetTbMedia()
        {
            response.data = _context.TbMedia.OrderByDescending(o=>o.InsertionDate);
            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Data Retrieved";
            return _context.TbMedia;
        }

        // GET: api/TbMedia/5
        [HttpGet("{id}")]
        public async Task<ApiResponse> GetTbMedia([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.data = null;
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Invalid Model.";
                return response;
            }

            var tbMedia = await _context.TbMedia.FindAsync(id);

            if (tbMedia == null)
            {
                response.data = null;
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found.";

            }

            response.data = tbMedia;
            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Data Retrieved.";
            return response;
        }

        //pagination to be applied
        [HttpGet("MediaOfFarmer/{id}")]
        public async Task<ApiResponse> GetMediaOfFarmer([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.data = null;
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Invalid Model.";
                return response;
            }

            var tbMedia = await _context.TbFarmerMedia.Where(i=>i.FarmerId==id).ToListAsync();

            if (tbMedia == null)
            {
                response.data = null;
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found.";

            }

            response.data = tbMedia;
            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Data Retrieved.";
            return response;
        }

        //pagination to be applied
        [HttpGet("MediaByDistrict/{mediaTypeId}/{id}")]
        public async Task<ApiResponse> GetMediaByDistrict([FromRoute] long id,long mediaTypeId)
        {
            if (!ModelState.IsValid)
            {
                response.data = null;
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Invalid Model.";
                return response;
            }

            var tbMedia = await _context.TbMedia.Where(i=>i.DistrictCode==id&&i.MediaTypeId==mediaTypeId).OrderByDescending(o=>o.InsertionDate).ToListAsync();

            if (tbMedia == null)
            {
                response.data = null;
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Does not exist.";
                return response;
            }

            response.data = tbMedia;
            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Data Retrieved.";
            return response;
        }

        // PUT: api/TbMedia/5
        [HttpPut("{id}")]
        public async Task<ApiResponse> PutTbMedia([FromRoute] long id, [FromBody] TbMedia tbMedia)
        {
            if (!ModelState.IsValid)
            {
                response.data = null;
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Invalid Model.";
                return response;
            }

            if (id != tbMedia.MediaId)
            {
                response.data = null;
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Id not same as entity.";
                return response;
            }

            _context.Entry(tbMedia).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TbMediaExists(id))
                {
                    response.data = null;
                    response.responseCode = (int)Common.Common.ResponseCode.Failure;
                    response.responseMessage = "Not Found.";
                    return response;
                }
                else
                {
                    throw;
                }
            }
            response.data = tbMedia;
            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Data Saved.";
            return response;
        }


        // POST: api/TbMedia
        [HttpPost]
        public async Task<ApiResponse> PostTbMedia([FromBody] TbMedia tbMedia)
        {
            if (!ModelState.IsValid)
            {
                response.data = null;
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Invalid Model.";
                return response;
            }

            _context.TbMedia.Add(tbMedia);
            await _context.SaveChangesAsync();

            response.data = tbMedia;
            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Data Saved Successfully.";
            return response;
        }

        // DELETE: api/TbMedia/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTbMedia([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tbMedia = await _context.TbMedia.FindAsync(id);
            if (tbMedia == null)
            {
                return NotFound();
            }

            _context.TbMedia.Remove(tbMedia);
            await _context.SaveChangesAsync();

            return Ok(tbMedia);
        }

        private bool TbMediaExists(long id)
        {
            return _context.TbMedia.Any(e => e.MediaId == id);
        }
    }
}
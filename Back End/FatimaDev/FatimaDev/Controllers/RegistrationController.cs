﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core;
using Core.Interfaces;
using Infrastructure.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FatimaDev.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class RegistrationController : ControllerBase
    {
        public IFarmerRepository farmerRepository;
        public RegistrationController(IFarmerRepository farmerRepository)
        {
             this.farmerRepository=farmerRepository ;
        }
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            var a = Request.Headers;
            return new string[] { "value1", "value2" };
        }

        [Route("~/api/RegisterFarmer")]
        [HttpPost]
        public ActionResult<string> RegisterFarmer([FromBody] farmerModel farmer)
        {
            TbFarmerInfo farmr = new TbFarmerInfo() {FarmerName=farmer.FarmerName,Cnic=farmer.Cnic };
            var result = farmerRepository.RegisterFarmer(farmr);

            return result;
        }
        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

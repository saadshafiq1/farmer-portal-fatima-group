﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Models;
using Infrastructure.OtherModels;
using Microsoft.AspNetCore.Authorization;
using Infrastructure.ViewModels;
using Core.Interfaces;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Farmer")]

    public class SeedController : ControllerBase
    {
        private readonly FFContext _Context;
      
        ApiResponse response = new ApiResponse();
        public SeedController(FFContext _context)
        {
            _Context = _context;
        }

        // GET: api/tbSeedTypes
        [HttpGet]
        public ApiResponse GetSeeds()
        {
            response.data = _Context.TbSeedTypes;
            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            return response;
        }

        // GET: api/tbSeedTypes/5
        [HttpGet("{id}")]
        public async Task<ApiResponse> GetSeeds([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var tbSeedType = await _Context.TbSeedTypes.FindAsync(id);

            if (tbSeedType == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found";
                return response;
            }

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbSeedType;
            return response;
        }

        // PUT: api/tbSeedTypes/5
        [HttpPut("{id}")]
        public async Task<ApiResponse> PutSeeds([FromRoute] int id, [FromBody] TbSeedTypes tbSeedType)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            if (id != tbSeedType.SeedId)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found";
                return response;
            }
          
            _Context.Entry(tbSeedType).State = EntityState.Modified;

            try
            {
                await _Context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tbSeedTypeExists(id))
                {
                    response.responseCode = (int)Common.Common.ResponseCode.Failure;
                    response.responseMessage = "Not Found";
                    return response;
                }
                else
                {
                    throw;
                }
            }

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbSeedType;
            return response;
        }

        // POST: api/tbSeedTypes
        [HttpPost]
        public async Task<ApiResponse> PostSeeds([FromBody] TbSeedTypes tbSeedType)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var temp = _Context.TbSeedTypes.Where(c => c.SeedName == tbSeedType.SeedName).ToList();

            if (temp.Count == 0)
            {
                _Context.TbSeedTypes.Add(tbSeedType);
                await _Context.SaveChangesAsync();


                response.responseCode = (int)Common.Common.ResponseCode.Success;
                response.responseMessage = "Action Completed.";
                response.data = tbSeedType;
                return response;
            }
            else
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Record Already Exists.";
                response.data = tbSeedType;
                return response;

            }
        }

        // DELETE: api/tbSeedTypes/5
        [HttpDelete("{id}")]
        public async Task<ApiResponse> DeleteSeeds([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not valid.";
                return response;
            }

            var tbSeedTypes = await _Context.TbSeedTypes.FindAsync(id);
            if (tbSeedTypes == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found";
                return response;
            }

            _Context.TbSeedTypes.Remove(tbSeedTypes);
            await _Context.SaveChangesAsync();

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbSeedTypes;
            return response;
        }

        private bool tbSeedTypeExists(int id)
        {
            return _Context.TbSeedTypes.Any(e => e.SeedId == id);
        }
        
    }

}



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Models;
using API.Services;
using Microsoft.AspNetCore.Authorization;
using Infrastructure.OtherModels;
using System.Net.Http;
using Twilio.Rest.Api.V2010.Account;
using Twilio;
using System.Security.Claims;

namespace API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly FFContext _context;
        private readonly IUserService _userService;
        ApiResponse response = new ApiResponse();
        public UserController(FFContext context, IUserService userService)
        {
            _context = context;
            _userService = userService;
        }

        // GET: api/User
        [HttpGet]
        public ApiResponse GetUser()
        {
            response.data = _context.TbUserLogin;
            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            return response;
        }

        [HttpGet("UserProfile")]
        public ApiResponse GetUserProfile()
        {
            if (User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value == "Farmer")
            {
                string  cellno = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value.TrimStart('0');
                var farmer = _context.TbFarmerInfo.Include(d=>d.TehsilCodeNavigation).Where(i => i.CellPhone.Contains(cellno)).FirstOrDefault();

                response.data = farmer;
                if (farmer.TehsilCodeNavigation != null)
                {
                    farmer.TehsilCodeNavigation.TbFarmerInfo = null;
                }
                    response.responseCode = (int)Common.Common.ResponseCode.Success;
                response.responseMessage = "Action Completed.";
                return response;
            }
            else
            {
                response.data = null;
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Only Farmer Info Available at the moment.";
                return response;
            }
           
        }


        [AllowAnonymous]
        [HttpGet("SendPassword")]
        public ApiResponse ForgetPassword(string userName)
        {
            var user = _context.TbUserLogin.Where(c => c.CellPhone == userName).ToList();

            if(user.Count()!=0)
            {
                var temp = user.FirstOrDefault();
                //HttpClient client = new HttpClient();

                //string uri =String.Format("https://sendpk.com/api/sms.php?username=923238782529&password=7526&sender=Masking&mobile={0}&message={1}",temp.CellPhone , temp.Passkey);
                ////client.DefaultRequestHeaders.Add("Authorization", "token ADD YOUR OAUTH TOKEN");
                //client.DefaultRequestHeaders.Add("User-Agent", "WebAPi");
                //var content = client.GetStringAsync(uri);

                //const string accountSid = "AC511468507e9aebb5e088f3f2e18b6eac";
                //const string authToken = "40f18012c3baad95cb225210bbfc29a0";

            
                //TwilioClient.Init(accountSid, authToken);

                //var message = MessageResource.Create(
                //    body: "UserName: "+temp.CellPhone+" Password: "+ temp.Passkey,
                //    from: new Twilio.Types.PhoneNumber("+17193565480"),
                //    to: new Twilio.Types.PhoneNumber("+923238782529")
                //);

            response.responseCode = 0;
            response.responseMessage = "UserName: "+temp.CellPhone+", Password:"+temp.Passkey;
            response.data = "Message will be send when messaging API is available";
            return response;
            } else
            {
                response.responseCode = 1;
                response.responseMessage = "User Name Doesn't exist";
                response.data = null;
                return response;
            }
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public  ApiResponse Authenticate([FromBody] TbUserLogin tbUserLogin)
        {
            try
            {
                string token = _userService.Authenticate(tbUserLogin.CellPhone, tbUserLogin.Passkey);

                if (token == null)
                {
                    response.responseCode = (int)Common.Common.ResponseCode.Failure;
                    response.responseMessage = "token is Null.";
                    response.data = null;
                    return response;

                }
                response.responseCode = (int)Common.Common.ResponseCode.Success;
                response.responseMessage = "Action Completed.";
                response.data = token;
                return response;

            }
            catch (Exception ex)
            {

                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = ex.ToString() ;
                response.data = null;
                return response;
            }
        }
        [AllowAnonymous]
        [HttpGet("roles")]
        public ApiResponse GetRoles()
        {
            response.data = _context.TbUserRoleTypeCode;
            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            return response;
        }


        // GET: api/User/5
        [HttpGet("{id}")]
        public async Task<ApiResponse> GetUser([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not validated.";
                return response;
            }

                var tbUserLogin = await _context.TbUserLogin.FindAsync(id);

            if (tbUserLogin == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found.";

                return response;
            }

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbUserLogin;
            return response;
        }

        // PUT: api/User/5
        [HttpPut("{id}")]
        public async Task<ApiResponse> PutUser([FromRoute] string id, [FromBody] TbUserLogin tbUserLogin)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not validated.";
                return response;
            }

            if (id != tbUserLogin.CellPhone)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Id is not equal userlogin object id.";
                return response;
            }

            _context.Entry(tbUserLogin).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TbUserLoginExists(id))
                {
                    response.responseCode = (int)Common.Common.ResponseCode.Failure;
                    response.responseMessage = "Not Found.";

                    return response;
                }
                else
                {
                    throw;
                }
            }

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbUserLogin;
            return response;
        }

        // POST: api/User
        [AllowAnonymous]
        [HttpPost]
        public async Task<ApiResponse> PostUser([FromBody] TbUserLogin tbUserLogin)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not validated.";
                return response;
            }

            _context.TbUserLogin.Add(tbUserLogin);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TbUserLoginExists(tbUserLogin.CellPhone))
                {
                    response.responseCode = (int)Common.Common.ResponseCode.Failure;
                    response.responseMessage = "Already Exists.";
                    return response;
                }
                else
                {
                    throw;
                }
            }



            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = tbUserLogin;
            return response;
        }

        // DELETE: api/User/5
        [HttpDelete("{id}")]
        public async Task<ApiResponse> DeleteUser([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Model State not validated.";
                return response;
            }

            var tbUserLogin = await _context.TbUserLogin.FindAsync(id);
            if (tbUserLogin == null)
            {
                response.responseCode = (int)Common.Common.ResponseCode.Failure;
                response.responseMessage = "Not Found.";

                return response;
            }

            _context.TbUserLogin.Remove(tbUserLogin);
            await _context.SaveChangesAsync();

            response.responseCode = (int)Common.Common.ResponseCode.Success;
            response.responseMessage = "Action Completed.";
            response.data = id;
            return response;
        }
   

        private bool TbUserLoginExists(string id)
        {
            return _context.TbUserLogin.Any(e => e.CellPhone == id);
        }
    }
}
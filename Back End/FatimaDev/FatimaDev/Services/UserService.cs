﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace API.Services
{
    public class UserService : IUserService
    {
        private readonly FFContext _context;
        private readonly IConfiguration _configuration;
        public UserService(FFContext context,IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }
        public string Authenticate(string username, string password)
        {
            var user = _context.TbUserLogin.Include(a=>a.UserRoleCodeNavigation).Where(x => x.CellPhone == username && x.Passkey == password).FirstOrDefault();

            // return null if user not found
            if (user == null)
                return null;


            DateTime expireDateTime;
            if (user.UserRoleCodeNavigation.RoleName == "Farmer")
            {
                expireDateTime = DateTime.UtcNow.AddYears(10);
            }
            else
            {
                expireDateTime = DateTime.UtcNow.AddDays(1);
            }
                // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("this is a great key for the algorithm long and secure");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.CellPhone.ToString()),
                    new Claim(ClaimTypes.Role,user.UserRoleCodeNavigation.RoleName)
                }),
                Expires = expireDateTime,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            string tokenString = tokenHandler.WriteToken(token);

            // remove password before returning
            user.Passkey = null;

            return tokenString;
        }
    }
}

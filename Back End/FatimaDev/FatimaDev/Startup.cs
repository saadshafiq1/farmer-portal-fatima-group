﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Services;
using AutoMapper;
using Core.Concrete;
using Core.Interfaces;
using Infrastructure.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace FatimaDev
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddAutoMapper();

            services.Configure<IISOptions>(options =>
            {
                options.AutomaticAuthentication = false;
            });
            //string secretKey = Configuration.GetSection("AppSettings:Secret").ToString();

            var key = System.Text.Encoding.ASCII.GetBytes("this is a great key for the algorithm long and secure");
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            // configure DI for application services
            services.AddScoped<IUserService, UserService>();


            //var connection = Configuration.GetConnectionString("DBConnection");
            //debug
            //var connection = "Server=LHRLT-1365\\;Database=FF;Persist Security Info=False;User ID=sa;Password=Musab@123;";

            //release
            //var connection = "Server=WIN2K12-VM2\\;Database=FF;Persist Security Info=False;User ID=ff;Password=Musab@1234;";
            // var connection = "Server=TS-FFT-21\\;Database=FF;Persist Security Info=False;User ID=ff;Password=Musab@1234;";
          var connection = "Server=119.63.133.166\\;Database=FF;Persist Security Info=False;User ID=ff;Password=Musab@1234;";


            services.AddDbContext<FFContext>(options => options.UseSqlServer(connection));
            services.AddTransient<IFarmerRepository, FarmerRepository>();
            //services.AddScoped<IFarmerRepository, FarmerRepository>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

                            app.UseCors(builder =>
                             builder.AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            .AllowCredentials());

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}

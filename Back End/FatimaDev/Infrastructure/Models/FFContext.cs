﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Infrastructure.Models
{
    public partial class FFContext : DbContext
    {
        public FFContext()
        {
        }

        public FFContext(DbContextOptions<FFContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TbBlockCrops> TbBlockCrops { get; set; }
        public virtual DbSet<TbCropCalenderMain> TbCropCalenderMain { get; set; }
        public virtual DbSet<TbCropCaseMain> TbCropCaseMain { get; set; }
        public virtual DbSet<TbCropStepAction> TbCropStepAction { get; set; }
        public virtual DbSet<TbCropSteps> TbCropSteps { get; set; }
        public virtual DbSet<TbCropTypeCode> TbCropTypeCode { get; set; }
        public virtual DbSet<TbDealerFertilizerDetails> TbDealerFertilizerDetails { get; set; }
        public virtual DbSet<TbDealerInfo> TbDealerInfo { get; set; }
        public virtual DbSet<TbDealerSeedDetails> TbDealerSeedDetails { get; set; }
        public virtual DbSet<TbDealerTypeCode> TbDealerTypeCode { get; set; }
        public virtual DbSet<TbDistrictCode> TbDistrictCode { get; set; }
        public virtual DbSet<TbEducationLevelCode> TbEducationLevelCode { get; set; }
        public virtual DbSet<TbFarmBlock> TbFarmBlock { get; set; }
        public virtual DbSet<TbFarmBlockUnit> TbFarmBlockUnit { get; set; }
        public virtual DbSet<TbFarmerCropCalender> TbFarmerCropCalender { get; set; }
        public virtual DbSet<TbFarmerCropCalenderDetails> TbFarmerCropCalenderDetails { get; set; }
        public virtual DbSet<TbFarmerCropPlan> TbFarmerCropPlan { get; set; }
        public virtual DbSet<TbFarmerCropPlanUnit> TbFarmerCropPlanUnit { get; set; }
        public virtual DbSet<TbFarmerInfo> TbFarmerInfo { get; set; }
        public virtual DbSet<TbFarmerMedia> TbFarmerMedia { get; set; }
        public virtual DbSet<TbFarmInfo> TbFarmInfo { get; set; }
        public virtual DbSet<TbFarmShapes> TbFarmShapes { get; set; }
        public virtual DbSet<TbFarmTypeCode> TbFarmTypeCode { get; set; }
        public virtual DbSet<TbFarmUnit> TbFarmUnit { get; set; }
        public virtual DbSet<TbFertilizerTypeDetails> TbFertilizerTypeDetails { get; set; }
        public virtual DbSet<TbFertilizerTypes> TbFertilizerTypes { get; set; }
        public virtual DbSet<TbMarketInfo> TbMarketInfo { get; set; }
        public virtual DbSet<TbMarketRate> TbMarketRate { get; set; }
        public virtual DbSet<TbMedia> TbMedia { get; set; }
        public virtual DbSet<TbMediaType> TbMediaType { get; set; }
        public virtual DbSet<TbPortfolioTypeCode> TbPortfolioTypeCode { get; set; }
        public virtual DbSet<TbProvinceCode> TbProvinceCode { get; set; }
        public virtual DbSet<TbRegionCode> TbRegionCode { get; set; }
        public virtual DbSet<TbRequestHeader> TbRequestHeader { get; set; }
        public virtual DbSet<TbRequestStatusCode> TbRequestStatusCode { get; set; }
        public virtual DbSet<TbRequestTypeCode> TbRequestTypeCode { get; set; }
        public virtual DbSet<TbSalepointCode> TbSalepointCode { get; set; }
        public virtual DbSet<TbSeedTypes> TbSeedTypes { get; set; }
        public virtual DbSet<TbTehsilCode> TbTehsilCode { get; set; }
        public virtual DbSet<TbUnitShapes> TbUnitShapes { get; set; }
        public virtual DbSet<TbUserLogin> TbUserLogin { get; set; }
        public virtual DbSet<TbUserRoleTypeCode> TbUserRoleTypeCode { get; set; }
        public virtual DbSet<TbWaterSoilResult> TbWaterSoilResult { get; set; }

        // Unable to generate entity type for table 'dbo.TB_REQUEST_DETAILS'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=119.63.133.166\\;Database=FF;Persist Security Info=False;User ID=ff;Password=Musab@1234;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TbBlockCrops>(entity =>
            {
                entity.HasKey(e => e.FarmCropId);

                entity.ToTable("TB_BLOCK_CROPS");

                entity.Property(e => e.FarmCropId).HasColumnName("FarmCropID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.CropArea).HasColumnType("decimal(8, 2)");

                entity.Property(e => e.FarmBlockId).HasColumnName("FarmBlockID");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.WaterSoilResultId).HasColumnName("WaterSoilResultID");

                entity.HasOne(d => d.CropCodeNavigation)
                    .WithMany(p => p.TbBlockCrops)
                    .HasForeignKey(d => d.CropCode)
                    .HasConstraintName("FK__TB_BLOCK___CropC__282DF8C2");

                entity.HasOne(d => d.FarmBlock)
                    .WithMany(p => p.TbBlockCrops)
                    .HasForeignKey(d => d.FarmBlockId)
                    .HasConstraintName("FK__TB_BLOCK___FarmB__29221CFB");
            });

            modelBuilder.Entity<TbCropCalenderMain>(entity =>
            {
                entity.HasKey(e => e.CalenderCode);

                entity.ToTable("TB_CROP_CALENDER_MAIN");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.InsertionDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDateTime).HasColumnType("datetime");

                entity.Property(e => e.StepEndDate).HasColumnType("datetime");

                entity.Property(e => e.StepStartDate).HasColumnType("datetime");

                entity.HasOne(d => d.CropCaseCodeNavigation)
                    .WithMany(p => p.TbCropCalenderMain)
                    .HasForeignKey(d => d.CropCaseCode)
                    .HasConstraintName("FK__TB_CROP_C__CropC__2A164134");

                entity.HasOne(d => d.CropStepCodeNavigation)
                    .WithMany(p => p.TbCropCalenderMain)
                    .HasForeignKey(d => d.CropStepCode)
                    .HasConstraintName("FK__TB_CROP_C__CropS__2B0A656D");
            });

            modelBuilder.Entity<TbCropCaseMain>(entity =>
            {
                entity.HasKey(e => e.CropCaseCode);

                entity.ToTable("TB_CROP_CASE_MAIN");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.InsertionDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.SowingBeginDate).HasColumnType("datetime");

                entity.Property(e => e.SowingEndDate).HasColumnType("datetime");

                entity.HasOne(d => d.CropCodeNavigation)
                    .WithMany(p => p.TbCropCaseMain)
                    .HasForeignKey(d => d.CropCode)
                    .HasConstraintName("FK__TB_CROP_C__CropC__2BFE89A6");
            });

            modelBuilder.Entity<TbCropStepAction>(entity =>
            {
                entity.HasKey(e => e.CropStepActionCode);

                entity.ToTable("TB_CROP_STEP_ACTION");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Detail).HasMaxLength(200);

                entity.Property(e => e.InsertionDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.HasOne(d => d.CropStepCodeNavigation)
                    .WithMany(p => p.TbCropStepAction)
                    .HasForeignKey(d => d.CropStepCode)
                    .HasConstraintName("FK__TB_CROP_S__CropS__2CF2ADDF");
            });

            modelBuilder.Entity<TbCropSteps>(entity =>
            {
                entity.HasKey(e => e.CropStepCode);

                entity.ToTable("TB_CROP_STEPS");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.AudioUrl).HasMaxLength(100);

                entity.Property(e => e.InsertionDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.StepName).HasMaxLength(50);

                entity.Property(e => e.StepNameUrder).HasMaxLength(50);

                entity.Property(e => e.VideoUrl).HasMaxLength(100);

                entity.HasOne(d => d.CropCodeNavigation)
                    .WithMany(p => p.TbCropSteps)
                    .HasForeignKey(d => d.CropCode)
                    .HasConstraintName("FK__TB_CROP_S__CropC__2DE6D218");
            });

            modelBuilder.Entity<TbCropTypeCode>(entity =>
            {
                entity.HasKey(e => e.CropCode);

                entity.ToTable("TB_CROP_TYPE_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.CropName).HasMaxLength(500);

                entity.Property(e => e.CropNameUrdu).HasMaxLength(500);

                entity.Property(e => e.CropSeason).HasMaxLength(20);

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<TbDealerFertilizerDetails>(entity =>
            {
                entity.HasKey(e => e.DealerDetailId);

                entity.ToTable("TB_DEALER_FERTILIZER_DETAILS");

                entity.Property(e => e.DealerDetailId).HasColumnName("DealerDetailID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.AvailableFertilizerId).HasColumnName("AvailableFertilizerID");

                entity.Property(e => e.DealerId).HasColumnName("DealerID");

                entity.Property(e => e.DealerTypeCodeId).HasColumnName("DealerTypeCodeID");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.AvailableFertilizer)
                    .WithMany(p => p.TbDealerFertilizerDetails)
                    .HasForeignKey(d => d.AvailableFertilizerId)
                    .HasConstraintName("FK_TB_DEALER_FERTILIZER_DETAILS_TB_FERTILIZER_TYPES");

                entity.HasOne(d => d.Dealer)
                    .WithMany(p => p.TbDealerFertilizerDetails)
                    .HasForeignKey(d => d.DealerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_DEALER_FERTILIZER_DETAILS_TB_DEALER_INFO");

                entity.HasOne(d => d.DealerTypeCode)
                    .WithMany(p => p.TbDealerFertilizerDetails)
                    .HasForeignKey(d => d.DealerTypeCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_DEALER_FERTILIZER_DETAILS_TB_DEALER_TYPE_CODE");
            });

            modelBuilder.Entity<TbDealerInfo>(entity =>
            {
                entity.HasKey(e => e.DealerId);

                entity.ToTable("TB_DEALER_INFO");

                entity.Property(e => e.DealerId).HasColumnName("DealerID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.Address).HasMaxLength(1000);

                entity.Property(e => e.CellPhone).HasMaxLength(20);

                entity.Property(e => e.Cnic)
                    .HasColumnName("CNIC")
                    .HasMaxLength(20);

                entity.Property(e => e.DealerName).HasMaxLength(500);

                entity.Property(e => e.DealerNameUrdu).HasMaxLength(500);

                entity.Property(e => e.Gender).HasMaxLength(20);

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.DistrictCodeNavigation)
                    .WithMany(p => p.TbDealerInfo)
                    .HasForeignKey(d => d.DistrictCode)
                    .HasConstraintName("FK_TB_DEALER_INFO_TB_DISTRICT_CODE");

                entity.HasOne(d => d.TehsilCodeNavigation)
                    .WithMany(p => p.TbDealerInfo)
                    .HasForeignKey(d => d.TehsilCode)
                    .HasConstraintName("FK_TB_DEALER_INFO_TB_TEHSIL_CODE");
            });

            modelBuilder.Entity<TbDealerSeedDetails>(entity =>
            {
                entity.HasKey(e => e.DealerDetailId);

                entity.ToTable("TB_DEALER_SEED_DETAILS");

                entity.Property(e => e.DealerDetailId).HasColumnName("DealerDetailID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.AvailableSeedId).HasColumnName("AvailableSeedID");

                entity.Property(e => e.DealerId).HasColumnName("DealerID");

                entity.Property(e => e.DealerTypeCodeId).HasColumnName("DealerTypeCodeID");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.AvailableSeed)
                    .WithMany(p => p.TbDealerSeedDetails)
                    .HasForeignKey(d => d.AvailableSeedId)
                    .HasConstraintName("FK_TB_DEALER_SEED_DETAILS_TB_SEED_TYPES");

                entity.HasOne(d => d.Dealer)
                    .WithMany(p => p.TbDealerSeedDetails)
                    .HasForeignKey(d => d.DealerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_DEALER_SEED_DETAILS_TB_DEALER_INFO");

                entity.HasOne(d => d.DealerTypeCode)
                    .WithMany(p => p.TbDealerSeedDetails)
                    .HasForeignKey(d => d.DealerTypeCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_DEALER_SEED_DETAILS_TB_DEALER_TYPE_CODE");
            });

            modelBuilder.Entity<TbDealerTypeCode>(entity =>
            {
                entity.HasKey(e => e.DealerTypeCodeId);

                entity.ToTable("TB_DEALER_TYPE_CODE");

                entity.Property(e => e.DealerTypeCodeId).HasColumnName("DealerTypeCodeID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.TypeName).HasMaxLength(500);

                entity.Property(e => e.TypeNameUrdu).HasMaxLength(500);
            });

            modelBuilder.Entity<TbDistrictCode>(entity =>
            {
                entity.HasKey(e => e.DistrictCode);

                entity.ToTable("TB_DISTRICT_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.DistrictName).HasMaxLength(500);

                entity.Property(e => e.DistrictNameUrdu).HasMaxLength(500);

                entity.Property(e => e.GovernmentDistrict)
                    .HasMaxLength(1)
                    .HasDefaultValueSql("('Y')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.SaleDistrict)
                    .HasMaxLength(1)
                    .HasDefaultValueSql("('Y')");

                entity.HasOne(d => d.ProvinceCodeNavigation)
                    .WithMany(p => p.TbDistrictCode)
                    .HasForeignKey(d => d.ProvinceCode)
                    .HasConstraintName("FK__TB_DISTRI__Provi__2EDAF651");
            });

            modelBuilder.Entity<TbEducationLevelCode>(entity =>
            {
                entity.HasKey(e => e.EducationCode);

                entity.ToTable("TB_EDUCATION_LEVEL_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.EducationLevelName).HasMaxLength(500);

                entity.Property(e => e.EducationLevelNameUrdu).HasMaxLength(500);

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<TbFarmBlock>(entity =>
            {
                entity.HasKey(e => e.FarmBlockId);

                entity.ToTable("TB_FARM_BLOCK");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.BlockArea).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BlockName).HasMaxLength(100);

                entity.Property(e => e.FarmGis).HasColumnName("FarmGIS");

                entity.Property(e => e.InsertionDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDateTime).HasColumnType("datetime");

                entity.Property(e => e.WaterSoilResultId).HasColumnName("WaterSoilResultID");

                entity.HasOne(d => d.Farm)
                    .WithMany(p => p.TbFarmBlock)
                    .HasForeignKey(d => d.FarmId)
                    .HasConstraintName("FK__TB_FARM_B__FarmI__2FCF1A8A");

                entity.HasOne(d => d.WaterSoilResult)
                    .WithMany(p => p.TbFarmBlock)
                    .HasForeignKey(d => d.WaterSoilResultId)
                    .HasConstraintName("FK__TB_FARM_B__Water__30C33EC3");
            });

            modelBuilder.Entity<TbFarmBlockUnit>(entity =>
            {
                entity.HasKey(e => e.FarmBlockUnitId);

                entity.ToTable("TB_FARM_BLOCK_UNIT");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.InsertionDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDateTime).HasColumnType("datetime");

                entity.HasOne(d => d.FarmBlock)
                    .WithMany(p => p.TbFarmBlockUnit)
                    .HasForeignKey(d => d.FarmBlockId)
                    .HasConstraintName("FK__TB_FARM_B__FarmB__31B762FC");

                entity.HasOne(d => d.FarmUnit)
                    .WithMany(p => p.TbFarmBlockUnit)
                    .HasForeignKey(d => d.FarmUnitId)
                    .HasConstraintName("FK__TB_FARM_B__FarmU__32AB8735");
            });

            modelBuilder.Entity<TbFarmerCropCalender>(entity =>
            {
                entity.HasKey(e => e.FarmerCropCalenderCode);

                entity.ToTable("TB_FARMER_CROP_CALENDER");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.FarmId).HasColumnName("FarmID");

                entity.Property(e => e.FarmerId).HasColumnName("FarmerID");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.SowingDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TbFarmerCropCalenderDetails>(entity =>
            {
                entity.HasKey(e => e.FarmerCropCalenderId);

                entity.ToTable("TB_FARMER_CROP_CALENDER_DETAILS");

                entity.Property(e => e.FarmerCropCalenderId).HasColumnName("FarmerCropCalenderID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.FarmId).HasColumnName("FarmID");

                entity.Property(e => e.FarmerId).HasColumnName("FarmerID");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PhaseDetails).HasMaxLength(1000);

                entity.Property(e => e.PhaseName).HasMaxLength(500);
            });

            modelBuilder.Entity<TbFarmerCropPlan>(entity =>
            {
                entity.HasKey(e => e.FarmerCropPlanId);

                entity.ToTable("TB_FARMER_CROP_PLAN");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.InsertionDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.SowingDate).HasColumnType("datetime");

                entity.HasOne(d => d.CropCaseCodeNavigation)
                    .WithMany(p => p.TbFarmerCropPlan)
                    .HasForeignKey(d => d.CropCaseCode)
                    .HasConstraintName("FK_TB_FARMER_CROP_PLAN_TB_CROP_CASE_MAIN");
            });

            modelBuilder.Entity<TbFarmerCropPlanUnit>(entity =>
            {
                entity.HasKey(e => e.FarmerCropPlanUnitId);

                entity.ToTable("TB_FARMER_CROP_PLAN_UNIT");

                entity.Property(e => e.ActiveStatus)
                    .HasColumnName("ActiveSTatus")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.FarmUnit)
                    .WithMany(p => p.TbFarmerCropPlanUnit)
                    .HasForeignKey(d => d.FarmUnitId)
                    .HasConstraintName("FK_TB_FARMER_CROP_PLAN_UNIT_TB_FARM_UNIT");

                entity.HasOne(d => d.FarmerCropPlan)
                    .WithMany(p => p.TbFarmerCropPlanUnit)
                    .HasForeignKey(d => d.FarmerCropPlanId)
                    .HasConstraintName("FK_TB_FARMER_CROP_PLAN_UNIT_TB_FARMER_CROP_PLAN");
            });

            modelBuilder.Entity<TbFarmerInfo>(entity =>
            {
                entity.HasKey(e => e.FarmerId);

                entity.ToTable("TB_FARMER_INFO");

                entity.Property(e => e.FarmerId).HasColumnName("FarmerID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.AlternateCellPhoneNo).HasMaxLength(20);

                entity.Property(e => e.AlternateName).HasMaxLength(500);

                entity.Property(e => e.AlternateRelationshipwithFarmer).HasMaxLength(500);

                entity.Property(e => e.CellPhone).HasMaxLength(20);

                entity.Property(e => e.Cnic)
                    .HasColumnName("CNIC")
                    .HasMaxLength(20);

                entity.Property(e => e.FarmerName).HasMaxLength(500);

                entity.Property(e => e.FatherHusbandName).HasMaxLength(500);

                entity.Property(e => e.Gender).HasMaxLength(20);

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MozaName).HasMaxLength(500);

                entity.Property(e => e.OtherImplement).HasDefaultValueSql("((0))");

                entity.Property(e => e.PermanentAddress).HasMaxLength(1000);

                entity.Property(e => e.PresentAddress).HasMaxLength(1000);

                entity.Property(e => e.Tractor).HasDefaultValueSql("((0))");

                entity.Property(e => e.UnionCouncil).HasMaxLength(500);

                entity.HasOne(d => d.TehsilCodeNavigation)
                    .WithMany(p => p.TbFarmerInfo)
                    .HasForeignKey(d => d.TehsilCode)
                    .HasConstraintName("FK__TB_FARMER__Tehsi__395884C4");
            });

            modelBuilder.Entity<TbFarmerMedia>(entity =>
            {
                entity.HasKey(e => e.FavouriteId);

                entity.ToTable("TB_FARMER_MEDIA");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Farmer)
                    .WithMany(p => p.TbFarmerMedia)
                    .HasForeignKey(d => d.FarmerId)
                    .HasConstraintName("FK_TB_FARMER_MEDIA_TB_FARMER_INFO");

                entity.HasOne(d => d.Media)
                    .WithMany(p => p.TbFarmerMedia)
                    .HasForeignKey(d => d.MediaId)
                    .HasConstraintName("FK_TB_FARMER_MEDIA_TB_MEDIA");
            });

            modelBuilder.Entity<TbFarmInfo>(entity =>
            {
                entity.HasKey(e => e.FarmId);

                entity.ToTable("TB_FARM_INFO");

                entity.Property(e => e.FarmId).HasColumnName("FarmID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.AreaonHeis)
                    .HasColumnName("AreaonHEIS")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.Canal)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('N')");

                entity.Property(e => e.FarmAddress).HasMaxLength(1000);

                entity.Property(e => e.FarmGis).HasColumnName("FarmGIS");

                entity.Property(e => e.FarmName).HasMaxLength(500);

                entity.Property(e => e.FarmerId).HasColumnName("FarmerID");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.OwnerTubeWell)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('N')");

                entity.Property(e => e.RegionId).HasColumnName("RegionID");

                entity.Property(e => e.TotalAreaAcres).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.WaterCourseType)
                    .HasMaxLength(20)
                    .HasDefaultValueSql("('Pacca')");

                entity.HasOne(d => d.FarmTypeCodeNavigation)
                    .WithMany(p => p.TbFarmInfo)
                    .HasForeignKey(d => d.FarmTypeCode)
                    .HasConstraintName("FK__TB_FARM_I__FarmT__3493CFA7");

                entity.HasOne(d => d.Farmer)
                    .WithMany(p => p.TbFarmInfo)
                    .HasForeignKey(d => d.FarmerId)
                    .HasConstraintName("FK__TB_FARM_I__Farme__339FAB6E");
            });

            modelBuilder.Entity<TbFarmShapes>(entity =>
            {
                entity.HasKey(e => e.FarmShapeId);

                entity.ToTable("TB_FARM_SHAPES");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.InsertionDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDateTime).HasColumnType("datetime");

                entity.HasOne(d => d.Farm)
                    .WithMany(p => p.TbFarmShapes)
                    .HasForeignKey(d => d.FarmId)
                    .HasConstraintName("FK_TB_FARM_SHAPES_TB_FARM_INFO");
            });

            modelBuilder.Entity<TbFarmTypeCode>(entity =>
            {
                entity.HasKey(e => e.FarmTypeCode);

                entity.ToTable("TB_FARM_TYPE_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.TypeName).HasMaxLength(500);

                entity.Property(e => e.TypeNameUrdu).HasMaxLength(500);
            });

            modelBuilder.Entity<TbFarmUnit>(entity =>
            {
                entity.HasKey(e => e.FarmUnitId);

                entity.ToTable("TB_FARM_UNIT");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.Gis).HasColumnName("GIS");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UnitName).HasMaxLength(50);

                entity.HasOne(d => d.Farm)
                    .WithMany(p => p.TbFarmUnit)
                    .HasForeignKey(d => d.FarmId)
                    .HasConstraintName("FK_TB_FARM_UNIT_TB_FARM_INFO");
            });

            modelBuilder.Entity<TbFertilizerTypeDetails>(entity =>
            {
                entity.HasKey(e => e.DetailId);

                entity.ToTable("TB_FERTILIZER_TYPE_DETAILS");

                entity.Property(e => e.DetailId).HasColumnName("DetailID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.FertilizerId).HasColumnName("FertilizerID");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Fertilizer)
                    .WithMany(p => p.TbFertilizerTypeDetails)
                    .HasForeignKey(d => d.FertilizerId)
                    .HasConstraintName("FK_TB_FERTILIZER_TYPE_DETAILS_TB_FERTILIZER_TYPES");
            });

            modelBuilder.Entity<TbFertilizerTypes>(entity =>
            {
                entity.HasKey(e => e.FertilizerId);

                entity.ToTable("TB_FERTILIZER_TYPES");

                entity.Property(e => e.FertilizerId).HasColumnName("FertilizerID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.FertilizerName).HasMaxLength(500);

                entity.Property(e => e.FertilizerNameUrdu).HasMaxLength(500);

                entity.Property(e => e.FertilizerSeason).HasMaxLength(20);

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<TbMarketInfo>(entity =>
            {
                entity.HasKey(e => e.MarketId);

                entity.ToTable("TB_MARKET_INFO");

                entity.Property(e => e.MarketId).HasColumnName("MarketID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MarketName).HasMaxLength(500);

                entity.Property(e => e.MarketNameUrdu).HasMaxLength(500);

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RoleName).HasMaxLength(500);

                entity.Property(e => e.RoleNameUrdu).HasMaxLength(500);
            });

            modelBuilder.Entity<TbMarketRate>(entity =>
            {
                entity.HasKey(e => e.MarketRateId);

                entity.ToTable("TB_MARKET_RATE");

                entity.Property(e => e.MarketRateId).HasColumnName("MarketRateID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.CropId).HasColumnName("CropID");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MarketId).HasColumnName("MarketID");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Rate).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.RateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TbMedia>(entity =>
            {
                entity.HasKey(e => e.MediaId);

                entity.ToTable("TB_MEDIA");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.Description).HasMaxLength(50);

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Url)
                    .HasColumnName("URL")
                    .HasMaxLength(100);

                entity.HasOne(d => d.DistrictCodeNavigation)
                    .WithMany(p => p.TbMedia)
                    .HasForeignKey(d => d.DistrictCode)
                    .HasConstraintName("FK_TB_MEDIA_TB_DISTRICT_CODE");

                entity.HasOne(d => d.MediaType)
                    .WithMany(p => p.TbMedia)
                    .HasForeignKey(d => d.MediaTypeId)
                    .HasConstraintName("FK_TB_MEDIA_TB_MEDIA_TYPE");
            });

            modelBuilder.Entity<TbMediaType>(entity =>
            {
                entity.HasKey(e => e.MediaTypeId);

                entity.ToTable("TB_MEDIA_TYPE");

                entity.Property(e => e.Description).HasMaxLength(50);

                entity.Property(e => e.MediaType).HasMaxLength(50);
            });

            modelBuilder.Entity<TbPortfolioTypeCode>(entity =>
            {
                entity.HasKey(e => e.PortfolioCode);

                entity.ToTable("TB_PORTFOLIO_TYPE_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PortfolioName).HasMaxLength(500);

                entity.Property(e => e.PortfolioNameUrdu).HasMaxLength(500);
            });

            modelBuilder.Entity<TbProvinceCode>(entity =>
            {
                entity.HasKey(e => e.ProvinceCode);

                entity.ToTable("TB_PROVINCE_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ProvinceName).HasMaxLength(500);

                entity.Property(e => e.ProvinceNameUrdu).HasMaxLength(500);
            });

            modelBuilder.Entity<TbRegionCode>(entity =>
            {
                entity.HasKey(e => e.RegionCode);

                entity.ToTable("TB_REGION_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RegionName).HasMaxLength(500);

                entity.Property(e => e.RegionNameUrdu).HasMaxLength(500);
            });

            modelBuilder.Entity<TbRequestHeader>(entity =>
            {
                entity.HasKey(e => e.RequestId);

                entity.ToTable("TB_REQUEST_HEADER");

                entity.Property(e => e.RequestId).HasColumnName("RequestID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.Details).HasMaxLength(1000);

                entity.Property(e => e.FarmId).HasColumnName("FarmID");

                entity.Property(e => e.FarmerId).HasColumnName("FarmerID");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<TbRequestStatusCode>(entity =>
            {
                entity.HasKey(e => e.RequestStatusCode);

                entity.ToTable("TB_REQUEST_STATUS_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RequestStatusName).HasMaxLength(500);

                entity.Property(e => e.RequestStatusNameUrdu).HasMaxLength(500);
            });

            modelBuilder.Entity<TbRequestTypeCode>(entity =>
            {
                entity.HasKey(e => e.RequestTypeCode);

                entity.ToTable("TB_REQUEST_TYPE_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RequestTypeName).HasMaxLength(500);

                entity.Property(e => e.RequestTypeNameUrdu).HasMaxLength(500);
            });

            modelBuilder.Entity<TbSalepointCode>(entity =>
            {
                entity.HasKey(e => e.SalePointCode);

                entity.ToTable("TB_SALEPOINT_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.SalePointName).HasMaxLength(500);

                entity.Property(e => e.SalePointNameUrdu).HasMaxLength(500);
            });

            modelBuilder.Entity<TbSeedTypes>(entity =>
            {
                entity.HasKey(e => e.SeedId);

                entity.ToTable("TB_SEED_TYPES");

                entity.Property(e => e.SeedId).HasColumnName("SeedID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.SeedName).HasMaxLength(500);

                entity.Property(e => e.SeedNameUrdu).HasMaxLength(500);

                entity.Property(e => e.SeedSeason).HasMaxLength(20);
            });

            modelBuilder.Entity<TbTehsilCode>(entity =>
            {
                entity.HasKey(e => e.TehsilCode);

                entity.ToTable("TB_TEHSIL_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.TehsilName).HasMaxLength(500);

                entity.Property(e => e.TehsilNameUrdu).HasMaxLength(500);

                entity.HasOne(d => d.DistrictCodeNavigation)
                    .WithMany(p => p.TbTehsilCode)
                    .HasForeignKey(d => d.DistrictCode)
                    .HasConstraintName("FK_TB_TEHSIL_CODE_TB_DISTRICT_CODE");
            });

            modelBuilder.Entity<TbUnitShapes>(entity =>
            {
                entity.HasKey(e => e.UnitShapeId);

                entity.ToTable("TB_UNIT_SHAPES");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.InsertionDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDateTime).HasColumnType("datetime");

                entity.HasOne(d => d.Unit)
                    .WithMany(p => p.TbUnitShapes)
                    .HasForeignKey(d => d.UnitId)
                    .HasConstraintName("FK_TB_UNIT_SHAPES_TB_FARM_UNIT");
            });

            modelBuilder.Entity<TbUserLogin>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("TB_USER_LOGIN");

                entity.Property(e => e.UserId).ValueGeneratedNever();

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CellPhone)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.InsertionDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDateTime).HasColumnType("datetime");

                entity.Property(e => e.Passkey)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.HasOne(d => d.UserRoleCodeNavigation)
                    .WithMany(p => p.TbUserLogin)
                    .HasForeignKey(d => d.UserRoleCode)
                    .HasConstraintName("FK__TB_USER_L__UserR__3F115E1A");
            });

            modelBuilder.Entity<TbUserRoleTypeCode>(entity =>
            {
                entity.HasKey(e => e.UserRoleCode);

                entity.ToTable("TB_USER_ROLE_TYPE_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RoleName).HasMaxLength(500);

                entity.Property(e => e.RoleNameUrdu).HasMaxLength(500);
            });

            modelBuilder.Entity<TbWaterSoilResult>(entity =>
            {
                entity.HasKey(e => e.WaterSoilResultId);

                entity.ToTable("TB_WATER_SOIL_RESULT");

                entity.Property(e => e.WaterSoilResultId).HasColumnName("WaterSoilResultID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.Details).HasMaxLength(1000);

                entity.Property(e => e.FarmCropId).HasColumnName("FarmCropID");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ResultDate).HasColumnType("datetime");

                entity.Property(e => e.TestDate).HasColumnType("datetime");
            });
        }
    }
}

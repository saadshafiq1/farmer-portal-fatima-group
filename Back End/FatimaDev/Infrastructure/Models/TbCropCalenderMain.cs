﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbCropCalenderMain
    {
        public int CalenderCode { get; set; }
        public int? CropCaseCode { get; set; }
        public int? CropStepCode { get; set; }
        public DateTime? StepStartDate { get; set; }
        public DateTime? StepEndDate { get; set; }
        public int? DaysafterpreviousStep { get; set; }
        public int? StepMarginDays { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public TbCropCaseMain CropCaseCodeNavigation { get; set; }
        public TbCropSteps CropStepCodeNavigation { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbCropCaseMain
    {
        public TbCropCaseMain()
        {
            TbCropCalenderMain = new HashSet<TbCropCalenderMain>();
            TbFarmerCropPlan = new HashSet<TbFarmerCropPlan>();
        }

        public int CropCaseCode { get; set; }
        public int? CropCode { get; set; }
        public int? ProvinceCode { get; set; }
        public DateTime SowingBeginDate { get; set; }
        public DateTime SowingEndDate { get; set; }
        public int? MarginDays { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public TbCropTypeCode CropCodeNavigation { get; set; }
        public ICollection<TbCropCalenderMain> TbCropCalenderMain { get; set; }
        public ICollection<TbFarmerCropPlan> TbFarmerCropPlan { get; set; }
    }
}

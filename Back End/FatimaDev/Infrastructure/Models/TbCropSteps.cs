﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbCropSteps
    {
        public TbCropSteps()
        {
            TbCropCalenderMain = new HashSet<TbCropCalenderMain>();
            TbCropStepAction = new HashSet<TbCropStepAction>();
        }

        public int CropStepCode { get; set; }
        public int? CropCode { get; set; }
        public int StepNumber { get; set; }
        public string StepName { get; set; }
        public string StepNameUrder { get; set; }
        public string StepDetail { get; set; }
        public string StepImage { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
        public string VideoUrl { get; set; }
        public string AudioUrl { get; set; }

        public TbCropTypeCode CropCodeNavigation { get; set; }
        public ICollection<TbCropCalenderMain> TbCropCalenderMain { get; set; }
        public ICollection<TbCropStepAction> TbCropStepAction { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbDealerFertilizerDetails
    {
        public int DealerDetailId { get; set; }
        public int DealerId { get; set; }
        public int DealerTypeCodeId { get; set; }
        public int? AvailableFertilizerId { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public TbFertilizerTypes AvailableFertilizer { get; set; }
        public TbDealerInfo Dealer { get; set; }
        public TbDealerTypeCode DealerTypeCode { get; set; }
    }
}

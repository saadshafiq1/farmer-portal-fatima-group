﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbDealerInfo
    {
        public TbDealerInfo()
        {
            TbDealerFertilizerDetails = new HashSet<TbDealerFertilizerDetails>();
            TbDealerSeedDetails = new HashSet<TbDealerSeedDetails>();
        }

        public int DealerId { get; set; }
        public string DealerName { get; set; }
        public string DealerNameUrdu { get; set; }
        public string Cnic { get; set; }
        public string Gender { get; set; }
        public string CellPhone { get; set; }
        public string Address { get; set; }
        public string DealerImage { get; set; }
        public int? DistrictCode { get; set; }
        public int? TehsilCode { get; set; }
        public string Geometry { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public TbDistrictCode DistrictCodeNavigation { get; set; }
        public TbTehsilCode TehsilCodeNavigation { get; set; }
        public ICollection<TbDealerFertilizerDetails> TbDealerFertilizerDetails { get; set; }
        public ICollection<TbDealerSeedDetails> TbDealerSeedDetails { get; set; }
    }
}

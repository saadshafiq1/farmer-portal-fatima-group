﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbDealerTypeCode
    {
        public TbDealerTypeCode()
        {
            TbDealerFertilizerDetails = new HashSet<TbDealerFertilizerDetails>();
            TbDealerSeedDetails = new HashSet<TbDealerSeedDetails>();
        }

        public int DealerTypeCodeId { get; set; }
        public string TypeName { get; set; }
        public string TypeNameUrdu { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public ICollection<TbDealerFertilizerDetails> TbDealerFertilizerDetails { get; set; }
        public ICollection<TbDealerSeedDetails> TbDealerSeedDetails { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbDistrictCode
    {
        public TbDistrictCode()
        {
            TbDealerInfo = new HashSet<TbDealerInfo>();
            TbMedia = new HashSet<TbMedia>();
            TbTehsilCode = new HashSet<TbTehsilCode>();
        }

        public int DistrictCode { get; set; }
        public string DistrictName { get; set; }
        public string DistrictNameUrdu { get; set; }
        public int? ProvinceCode { get; set; }
        public int? RegionCode { get; set; }
        public string SaleDistrict { get; set; }
        public string GovernmentDistrict { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public TbProvinceCode ProvinceCodeNavigation { get; set; }
        public ICollection<TbDealerInfo> TbDealerInfo { get; set; }
        public ICollection<TbMedia> TbMedia { get; set; }
        public ICollection<TbTehsilCode> TbTehsilCode { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbFarmBlock
    {
        public TbFarmBlock()
        {
            TbBlockCrops = new HashSet<TbBlockCrops>();
            TbFarmBlockUnit = new HashSet<TbFarmBlockUnit>();
        }

        public long FarmBlockId { get; set; }
        public long? FarmId { get; set; }
        public long? WaterSoilResultId { get; set; }
        public int? ModifiedBy { get; set; }
        public long? FarmGis { get; set; }
        public string BlockName { get; set; }
        public decimal? BlockArea { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public TbFarmInfo Farm { get; set; }
        public TbWaterSoilResult WaterSoilResult { get; set; }
        public ICollection<TbBlockCrops> TbBlockCrops { get; set; }
        public ICollection<TbFarmBlockUnit> TbFarmBlockUnit { get; set; }
    }
}

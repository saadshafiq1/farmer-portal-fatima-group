﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbFarmCrops
    {
        public long FarmCropId { get; set; }
        public long? FarmBlockId { get; set; }
        public int? CropCode { get; set; }
        public decimal? CropArea { get; set; }
        public long? WaterSoilResultId { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public TbCropTypeCode CropCodeNavigation { get; set; }
        public TbFarmBlock FarmBlock { get; set; }
    }
}

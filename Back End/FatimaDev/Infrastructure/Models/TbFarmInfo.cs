﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbFarmInfo
    {
        public TbFarmInfo()
        {
            TbFarmBlock = new HashSet<TbFarmBlock>();
            TbFarmShapes = new HashSet<TbFarmShapes>();
            TbFarmUnit = new HashSet<TbFarmUnit>();
        }

        public long FarmId { get; set; }
        public long? FarmerId { get; set; }
        public int? FarmTypeCode { get; set; }
        public string Canal { get; set; }
        public string OwnerTubeWell { get; set; }
        public decimal? AreaonHeis { get; set; }
        public string WaterCourseType { get; set; }
        public decimal? TotalAreaAcres { get; set; }
        public string FarmName { get; set; }
        public string FarmAddress { get; set; }
        public long? FarmGis { get; set; }
        public int? ProvinceCode { get; set; }
        public int? TehsilCode { get; set; }
        public int? DistrictCode { get; set; }
        public long? RegionId { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public TbFarmTypeCode FarmTypeCodeNavigation { get; set; }
        public TbFarmerInfo Farmer { get; set; }
        public ICollection<TbFarmBlock> TbFarmBlock { get; set; }
        public ICollection<TbFarmShapes> TbFarmShapes { get; set; }
        public ICollection<TbFarmUnit> TbFarmUnit { get; set; }
    }
}

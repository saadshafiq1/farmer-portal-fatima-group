﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbFarmUnit
    {
        public TbFarmUnit()
        {
            TbFarmBlockUnit = new HashSet<TbFarmBlockUnit>();
            TbFarmerCropPlanUnit = new HashSet<TbFarmerCropPlanUnit>();
            TbUnitShapes = new HashSet<TbUnitShapes>();
        }

        public long FarmUnitId { get; set; }
        public long? FarmId { get; set; }
        public int? AreaInAcres { get; set; }
        public long? Gis { get; set; }
        public int? UnitNumber { get; set; }
        public string UnitName { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
        public bool? IsCropped { get; set; }

        public TbFarmInfo Farm { get; set; }
        public ICollection<TbFarmBlockUnit> TbFarmBlockUnit { get; set; }
        public ICollection<TbFarmerCropPlanUnit> TbFarmerCropPlanUnit { get; set; }
        public ICollection<TbUnitShapes> TbUnitShapes { get; set; }
    }
}

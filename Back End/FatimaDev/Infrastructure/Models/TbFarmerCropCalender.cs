﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbFarmerCropCalender
    {
        public long FarmerCropCalenderCode { get; set; }
        public int? CropCode { get; set; }
        public DateTime? SowingDate { get; set; }
        public long? FarmerId { get; set; }
        public long? FarmId { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbFarmerCropCalenderDetails
    {
        public long FarmerCropCalenderId { get; set; }
        public int? CropCode { get; set; }
        public long? FarmerId { get; set; }
        public long? FarmId { get; set; }
        public string PhaseLogo { get; set; }
        public int? PhaseSequence { get; set; }
        public string PhaseDetails { get; set; }
        public string PhaseName { get; set; }
        public int? DaysafterpreviousPhase { get; set; }
        public int? MarginDays { get; set; }
        public long? FarmerCropCalenderCode { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
    }
}

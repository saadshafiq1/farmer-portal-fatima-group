﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbFarmerCropPlan
    {
        public TbFarmerCropPlan()
        {
            TbFarmerCropPlanUnit = new HashSet<TbFarmerCropPlanUnit>();
        }

        public long FarmerCropPlanId { get; set; }
        public long? FarmerId { get; set; }
        public long? FarmId { get; set; }
        public int? CropCaseCode { get; set; }
        public int? SowingMonth { get; set; }
        public int? SowingDay { get; set; }
        public int? SowingYear { get; set; }
        public DateTime? SowingDate { get; set; }
        public string FarmUnits { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public TbCropCaseMain CropCaseCodeNavigation { get; set; }
        public ICollection<TbFarmerCropPlanUnit> TbFarmerCropPlanUnit { get; set; }
    }
}

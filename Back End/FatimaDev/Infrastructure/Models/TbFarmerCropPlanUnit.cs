﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbFarmerCropPlanUnit
    {
        public long FarmerCropPlanUnitId { get; set; }
        public long? FarmerCropPlanId { get; set; }
        public long? FarmUnitId { get; set; }
        public string ActiveStatus { get; set; }

        public TbFarmUnit FarmUnit { get; set; }
        public TbFarmerCropPlan FarmerCropPlan { get; set; }
    }
}

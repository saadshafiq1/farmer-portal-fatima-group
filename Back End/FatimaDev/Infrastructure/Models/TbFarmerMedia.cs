﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbFarmerMedia
    {
        public long FavouriteId { get; set; }
        public long? FarmerId { get; set; }
        public long? MediaId { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public TbFarmerInfo Farmer { get; set; }
        public TbMedia Media { get; set; }
    }
}

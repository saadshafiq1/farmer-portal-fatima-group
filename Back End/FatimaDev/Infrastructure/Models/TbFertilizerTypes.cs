﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbFertilizerTypes
    {
        public TbFertilizerTypes()
        {
            TbDealerFertilizerDetails = new HashSet<TbDealerFertilizerDetails>();
            TbFertilizerTypeDetails = new HashSet<TbFertilizerTypeDetails>();
        }

        public int FertilizerId { get; set; }
        public string FertilizerName { get; set; }
        public string FertilizerNameUrdu { get; set; }
        public string FertilizerDetailUrdu { get; set; }
        public string FertilizerSeason { get; set; }
        public string FertilizerLogo { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public ICollection<TbDealerFertilizerDetails> TbDealerFertilizerDetails { get; set; }
        public ICollection<TbFertilizerTypeDetails> TbFertilizerTypeDetails { get; set; }
    }
}

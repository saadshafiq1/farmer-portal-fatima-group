﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbMarketInfo
    {
        public int MarketId { get; set; }
        public string MarketName { get; set; }
        public string MarketNameUrdu { get; set; }
        public int? TehsilCode { get; set; }
        public int? DistrictCode { get; set; }
        public string RoleName { get; set; }
        public string RoleNameUrdu { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
    }
}

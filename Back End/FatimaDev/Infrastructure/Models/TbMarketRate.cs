﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbMarketRate
    {
        public long MarketRateId { get; set; }
        public int? MarketId { get; set; }
        public int? CropId { get; set; }
        public decimal? Rate { get; set; }
        public DateTime? RateDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbPortfolioTypeCode
    {
        public int PortfolioCode { get; set; }
        public string PortfolioName { get; set; }
        public string PortfolioNameUrdu { get; set; }
        public string PortfolioLogo { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
    }
}

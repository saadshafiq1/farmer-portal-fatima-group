﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbProvinceCode
    {
        public TbProvinceCode()
        {
            TbDistrictCode = new HashSet<TbDistrictCode>();
        }

        public int ProvinceCode { get; set; }
        public string ProvinceName { get; set; }
        public string ProvinceNameUrdu { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public ICollection<TbDistrictCode> TbDistrictCode { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbRegionCode
    {
        public int RegionCode { get; set; }
        public string RegionName { get; set; }
        public string RegionNameUrdu { get; set; }
        public int? ProvinceCode { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
    }
}

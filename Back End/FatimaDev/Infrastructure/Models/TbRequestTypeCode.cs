﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbRequestTypeCode
    {
        public int RequestTypeCode { get; set; }
        public string RequestTypeName { get; set; }
        public string RequestTypeNameUrdu { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
    }
}

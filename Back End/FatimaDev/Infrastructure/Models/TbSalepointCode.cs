﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbSalepointCode
    {
        public int SalePointCode { get; set; }
        public string SalePointName { get; set; }
        public string SalePointNameUrdu { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
    }
}

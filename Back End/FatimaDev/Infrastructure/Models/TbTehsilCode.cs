﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbTehsilCode
    {
        public TbTehsilCode()
        {
            TbDealerInfo = new HashSet<TbDealerInfo>();
            TbFarmerInfo = new HashSet<TbFarmerInfo>();
        }

        public int TehsilCode { get; set; }
        public string TehsilName { get; set; }
        public string TehsilNameUrdu { get; set; }
        public int? DistrictCode { get; set; }
        public int? RegionCode { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public TbDistrictCode DistrictCodeNavigation { get; set; }
        public ICollection<TbDealerInfo> TbDealerInfo { get; set; }
        public ICollection<TbFarmerInfo> TbFarmerInfo { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbUserRoleTypeCode
    {
        public TbUserRoleTypeCode()
        {
            TbUserLogin = new HashSet<TbUserLogin>();
        }

        public int UserRoleCode { get; set; }
        public string RoleName { get; set; }
        public string RoleNameUrdu { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public ICollection<TbUserLogin> TbUserLogin { get; set; }
    }
}

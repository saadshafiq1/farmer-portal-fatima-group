﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbWaterSoilResult
    {
        public TbWaterSoilResult()
        {
            TbFarmBlock = new HashSet<TbFarmBlock>();
        }

        public long WaterSoilResultId { get; set; }
        public long? FarmCropId { get; set; }
        public DateTime? TestDate { get; set; }
        public DateTime? ResultDate { get; set; }
        public string Details { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public ICollection<TbFarmBlock> TbFarmBlock { get; set; }
    }
}

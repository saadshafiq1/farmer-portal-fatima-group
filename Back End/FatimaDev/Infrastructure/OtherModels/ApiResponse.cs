﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.OtherModels
{
    public class ApiResponse
    {
        public int responseCode { get; set; }
        public object responseMessage { get; set; }

        public object data { get; set; }
    }
}

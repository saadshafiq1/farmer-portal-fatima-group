﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
   public class CropActions
    {
        public bool? IsAlert { get; set; }
        public string Detail { get; set; }
    }
}

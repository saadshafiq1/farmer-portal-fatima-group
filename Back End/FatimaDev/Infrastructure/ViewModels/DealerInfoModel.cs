﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
    public class DealerInfoModel
    {
        public int DealerId { get; set; }
        public string DealerName { get; set; }
        public string DealerNameUrdu { get; set; }
        public string Cnic { get; set; }
        public string Gender { get; set; }
        public string CellPhone { get; set; }
        public string Address { get; set; }
        public string DealerImage { get; set; }
        public int? TehsilCode { get; set; }
        public string Geometry { get; set; }
        public DistrictCodeModel DistrictCodeNavigation { get; set; }
    }
}

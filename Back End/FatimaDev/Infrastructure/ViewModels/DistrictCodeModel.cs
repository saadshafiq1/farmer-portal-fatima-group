﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
    public class DistrictCodeModel
    {
        public int DistrictCode { get; set; }
        public string DistrictName { get; set; }
        public string DistrictNameUrdu { get; set; }
    }
}

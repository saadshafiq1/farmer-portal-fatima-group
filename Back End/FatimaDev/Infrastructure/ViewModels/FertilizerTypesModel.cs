﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
    public class FertilizerTypesModel
    {
        public int FertilizerId { get; set; }
        public string FertilizerName { get; set; }
        public string FertilizerNameUrdu { get; set; }
        public string FertilizerDetailUrdu { get; set; }
        public string FertilizerSeason { get; set; }
        public string FertilizerLogo { get; set; }

        public ICollection<FertilizerTypeDetailsModel> TbFertilizerTypeDetails { get; set; }
    }
}

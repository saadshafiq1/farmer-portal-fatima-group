﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
  public  class GeneralCropPlan
    {
        public int CropId { get; set; }
        public int CaseId { get; set; }
        public int? DaysafterpreviousStep { get; set; }
        public string StepImage { get; set; }
        public string StepName { get; set; }
        public string StepDetail { get; set; }
        public string CropName { get; set; }
        public int StepNumber { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string VideoUrl { get; set; }
        public string AudioUrl { get; set; }

        public List<CropActions> StepActions { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Windows.Forms;
using System.Threading;
using System.Web;
using System.Data.Entity;

namespace AIMS
{
    class AIMSCities:Form
    {
        private static string aimsSiteURL = "";
        private static string commodityCityPage = "";     
        public AIMSCities()
        {
            aimsSiteURL = ConfigurationManager.AppSettings["SiteURL"];
            commodityCityPage = ConfigurationManager.AppSettings["CommodityCity"];
            ShowInTaskbar = false;
            WindowState = FormWindowState.Minimized;
            Load += new EventHandler(Window_Load);
        }  
        void Window_Load(object sender, EventArgs e)
        {
            WebBrowser wb = new WebBrowser();
            wb.AllowNavigation = true;
            wb.DocumentCompleted += LoadCities_Completed;
            wb.Navigate(aimsSiteURL + commodityCityPage);
        }
        private void LoadCities_Completed(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            WebBrowser webBrowser = sender as WebBrowser;
            if (webBrowser.ReadyState == WebBrowserReadyState.Complete)
            {
                HtmlElement tbCities = webBrowser.Document.GetElementById("TABLE1");
                HtmlElementCollection rowsInCityTable = tbCities.GetElementsByTagName("tr");
                foreach (HtmlElement cityRow in rowsInCityTable)
                {
                    HtmlElementCollection cellsInCityRow = cityRow.GetElementsByTagName("td");
                    foreach (HtmlElement cityCell in cellsInCityRow)
                    {
                        HtmlElementCollection anchorsInCityCell = cityCell.GetElementsByTagName("a");
                        foreach (HtmlElement cityAnchor in anchorsInCityCell)
                        {
                            string strCityName = cityAnchor.InnerText;
                            Uri cityUri = new Uri(cityAnchor.GetAttribute("href"));
                            int commodityId = Convert.ToInt32(HttpUtility.ParseQueryString(cityUri.Query).Get("commodityId"));
                            AddUpdateCities(commodityId, strCityName);
                        }
                    }
                }
            }
            Application.Exit();
        }
        private void AddUpdateCities(int cityID,string cityName)
        {
            using (FFEntities ffContext = new FFEntities())
            {
                TB_AIMS_COMMODITY_CITY CommodityCity = ffContext.TB_AIMS_COMMODITY_CITY.FirstOrDefault(c => c.CityID == cityID);
                if (CommodityCity != null)
                {
                    CommodityCity.CityName = cityName.Trim();
                    CommodityCity.DateUpdated = DateTime.Now;
                    ffContext.Entry(CommodityCity).State = EntityState.Modified;
                }
                else
                {
                    CommodityCity = new TB_AIMS_COMMODITY_CITY();
                    CommodityCity.CityID = cityID;
                    CommodityCity.CityName = cityName.Trim();
                    ffContext.TB_AIMS_COMMODITY_CITY.Add(CommodityCity);
                }
                ffContext.SaveChanges();
            }
        }
    }
}

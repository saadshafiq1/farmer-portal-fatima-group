﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Windows.Forms;
using System.Threading;
using System.Web;
using System.Data.Entity;

namespace AIMS
{
    class AIMSCommodities : Form
    {
        private static string aimsSiteURL = "";
        private static string searchByCity = "";
        private static int cityID = 0;
        public AIMSCommodities()
        {
            aimsSiteURL = ConfigurationManager.AppSettings["SiteURL"];
            searchByCity = ConfigurationManager.AppSettings["SearchType1"];
            ShowInTaskbar = false;
            WindowState = FormWindowState.Minimized;
            Load += new EventHandler(Window_Load);
        }
        void Window_Load(object sender, EventArgs e)
        {
            WebBrowser wb = new WebBrowser();
            wb.AllowNavigation = true;
            wb.DocumentCompleted += LoadCommodity_Completed;

            FFEntities ffContext = new FFEntities();
            var commodityCities = ffContext.TB_AIMS_COMMODITY_CITY.ToList();
            ffContext.Dispose();

            foreach (var commodityCity in commodityCities)
            {
                cityID = Convert.ToInt32(commodityCity.CityID);
                wb.Navigate(aimsSiteURL + searchByCity + cityID);

                while (wb.ReadyState != WebBrowserReadyState.Complete)
                {
                    Application.DoEvents();
                }
            }
            DeleteOldCommodities();
            Application.Exit();
        }
        private void LoadCommodity_Completed(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            WebBrowser webBrowser = sender as WebBrowser;
            if (webBrowser.ReadyState == WebBrowserReadyState.Complete)
            {
                HtmlElement divOfCommodityPrices = webBrowser.Document.GetElementById("amis_prices");
                HtmlElementCollection tableOfCommodityPrices = divOfCommodityPrices.GetElementsByTagName("table");
                foreach (HtmlElement tableOfCommodity in tableOfCommodityPrices)
                {
                    HtmlElementCollection rowsOfCommodity = tableOfCommodity.GetElementsByTagName("tr");
                    foreach (HtmlElement rowOfCommodity in rowsOfCommodity)
                    {
                        HtmlElementCollection cellOfCommodityName = rowOfCommodity.GetElementsByTagName("th");
                        if (cellOfCommodityName.Count > 0)
                        {
                            string commodityName = "", minPrice = "", maxPrice = "", fqp = "";
                            int commodityID = 0;

                            HtmlElementCollection commodityNameAnchor = cellOfCommodityName[0].GetElementsByTagName("a");
                            if (commodityNameAnchor.Count > 0)
                            {
                                commodityName = commodityNameAnchor[0].InnerText;
                                Uri commodityUri = new Uri(commodityNameAnchor[0].GetAttribute("href"));
                                commodityID = Convert.ToInt32(HttpUtility.ParseQueryString(commodityUri.Query).Get("commodityId"));
                            }

                            HtmlElementCollection cellOfCommodityPrices = rowOfCommodity.GetElementsByTagName("td");
                            minPrice = cellOfCommodityPrices[1].InnerText;
                            maxPrice = cellOfCommodityPrices[2].InnerText;
                            fqp = cellOfCommodityPrices[3].InnerText;
                            Console.WriteLine(commodityName + " " + minPrice + " " + maxPrice + " " + fqp);
                            AddUpdateCommodities(cityID, commodityID, commodityName, minPrice, maxPrice, fqp);
                        }
                    }
                }
            }
        }
        private void AddUpdateCommodities(int cityID, int commodityID, string commodityName, string minPrice, string maxPrice, string FQP)
        {
            DateTime dateInserted = DateTime.Now.Date;
            using (FFEntities ffContext = new FFEntities())
            {
                TB_AIMS_COMMODITY commodity = ffContext.TB_AIMS_COMMODITY.FirstOrDefault(c => c.DateInserted == dateInserted && c.CommodityID == commodityID && c.CommodityCityID == cityID);
                if (commodity != null)
                {
                    commodity.CommodityCityID = cityID;
                    commodity.CommodityID = commodityID;
                    commodity.CommodityName = commodityName.Trim();
                    commodity.MIN = minPrice.Trim(); ;
                    commodity.MAX = maxPrice.Trim(); ;
                    commodity.FQP = FQP.Trim();
                    commodity.DateUpdated = DateTime.Now;
                    ffContext.Entry(commodity).State = EntityState.Modified;
                }
                else
                {
                    commodity = new TB_AIMS_COMMODITY();
                    commodity.CommodityCityID = cityID;
                    commodity.CommodityID = commodityID;
                    commodity.CommodityName = commodityName.Trim(); ;
                    commodity.MIN = minPrice.Trim();
                    commodity.MAX = maxPrice.Trim();
                    commodity.FQP = FQP.Trim();
                    ffContext.TB_AIMS_COMMODITY.Add(commodity);
                }
                ffContext.SaveChanges();
            }
        }
        private void DeleteOldCommodities()
        {
            using (FFEntities ffContext = new FFEntities())
            {
                DateTime prevDate= DateTime.Now.AddDays(-64);
                var LastTenDays = ffContext.TB_AIMS_COMMODITY.GroupBy(c => c.DateInserted).OrderByDescending(c => c.Key).Take(5).Select(c => c.Key).ToList();
                var oldCommodities = ffContext.TB_AIMS_COMMODITY.Where(c => !LastTenDays.Contains(c.DateInserted)).ToList();

                foreach (TB_AIMS_COMMODITY com in oldCommodities)
                {
                    ffContext.TB_AIMS_COMMODITY.Remove(com);                  
                }
               
                ffContext.SaveChanges();
            }
        }
    }
}

﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AIMS
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class FFEntities : DbContext
    {
        public FFEntities()
            : base("name=FFEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<TB_AIMS_COMMODITY_CITY> TB_AIMS_COMMODITY_CITY { get; set; }
        public virtual DbSet<TB_AIMS_COMMODITY> TB_AIMS_COMMODITY { get; set; }
    }
}

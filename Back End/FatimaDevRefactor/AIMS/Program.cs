﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AIMS
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            // Application.Run(new AIMSCities());
            DateTime StartTime = DateTime.Now;
            Console.WriteLine("Started On " + StartTime);

            Application.Run(new AIMSCommodities());

            DateTime EndTime = DateTime.Now;
            Console.WriteLine("Completed On " + EndTime);

            TimeSpan ts = StartTime - EndTime;
            Console.WriteLine("Time Laps " + ts);           
        }
    }
}

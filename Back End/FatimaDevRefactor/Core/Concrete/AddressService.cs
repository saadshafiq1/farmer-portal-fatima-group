﻿using Core.Interfaces;
using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Core.Concrete
{
    public class AddressService : IAddressService
    {

        IAddressRepository addressRepository;
        public AddressService(IAddressRepository addressRepository)
        {
            this.addressRepository = addressRepository;
        }
        public async Task<IEnumerable<TbProvinceCode>> GetAllProvinces()
        {
            return await addressRepository.GetAllProvincesAsync();                    
        }
        public async Task<TbDistrictCode> GetDistrictById(int districtid)
        {
            return await addressRepository.GetDistrictById(districtid);
        }
        public async Task<IEnumerable<TbDistrictCode>> GetDistrictsOfProvince(long provinceid)
        {
            return await addressRepository.GetDistrictsOfProvince(provinceid);
        }
        public async Task<IEnumerable<TbDistrictCode>> GetDistrictsByRegion(long regionid)
        {
            return await addressRepository.GetDistrictsByRegion(regionid);
        }
        
        public async Task<TbProvinceCode> GetProvinceById(long id)
        {
            return await addressRepository.GetProvinceByIdAsync(id);
        }
        public async Task<TbTehsilCode> GetTehsilById(int tehsilid)
        {
            return await addressRepository.GetTehsilById(tehsilid);
        }
        public async Task<IEnumerable<TbTehsilCode>> GetTehsilsOfDistrict(long districtid)
        {
            return await addressRepository.GetTehsilsOfDistrict(districtid);
        }

        public async Task<IEnumerable<TbRegionCode>> GetCitiesOfProvince(int provinceid)
        {
            return await addressRepository.GetCitiesOfProvince(provinceid);
        }
        public async Task<TbRegionCode> GetRegionById(int regionid)
        {
            return await addressRepository.GetRegionById(regionid);
        }

        public async Task<IEnumerable<TbRegionCode>> GetALLRegion()
        {
            return await addressRepository.GetAllRegion();
        }

        public async Task<IEnumerable<TbDistrictCode>> GetAllDistricts()
        {
            return await addressRepository.GetAllDistricts();
        }

        public async Task<IEnumerable<TbSalepointCode>> GetAllSalesPoint()
        {
            return await addressRepository.GetAllSalesPoint();
        }
    }
}

﻿using Core.Interfaces;
using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core.Concrete
{
    public class CallCenterAgentService : ICallCenterAgentService
    {
        ICallCenterAgentRepository callCenterAgentRepository;
        public CallCenterAgentService(ICallCenterAgentRepository callCenterAgentRepository)
        {
            this.callCenterAgentRepository = callCenterAgentRepository;
        }
        public async Task<TbCallCenterAgentInfo> GetAgentByUserName(Guid userName)
        {
            var includes = new Expression<Func<TbCallCenterAgentInfo, object>>[2];
            includes[0] = f => f.DistrictCodeNavigation;
            includes[1] = f => f.TehsilCodeNavigation;


            return await callCenterAgentRepository.GetAgentByUserName(f => f.UserGuid == userName,includes);
        }

        public async Task<TbCallCenterAgentInfo> GetAgentById(long id=0)
        {
            //var includes = new Expression<Func<TbCallCenterAgentInfo, object>>[2];
            //includes[0] = f => f.DistrictCodeNavigation;
            //includes[1] = f => f.TehsilCodeNavigation;

            return await callCenterAgentRepository.GetAgentById(f => f.CallCenterAgentId == id);
        }

        public async Task<IEnumerable<TbCallCenterAgentInfo>> GetAllAgents()
        {
            return await callCenterAgentRepository.GetAllAgentsAsync();
        }

    }
}

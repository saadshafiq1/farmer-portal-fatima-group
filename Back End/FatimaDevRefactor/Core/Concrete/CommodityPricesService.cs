﻿using Core.Interfaces;
using Infrastructure.Interface;
using Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Core.Concrete
{
    public class CommodityPricesService : ICommodityPricesService
    {
        IAIMSCommodityRepository AIMSCommodityRepository;
        IAIMSCommodityCityRepository AIMSCommodityCityRepository;
        public CommodityPricesService(IAIMSCommodityRepository aIMSCommodityRepository, IAIMSCommodityCityRepository aIMSCommodityCityRepository)
        {
            this.AIMSCommodityRepository = aIMSCommodityRepository;
            this.AIMSCommodityCityRepository = aIMSCommodityCityRepository;
        }
        public async Task<IEnumerable<TbAimsCommodity>> GetTodaysCommodityPricesByCity(string CityName, DateTime DateToday)
        {
            var CommodityCity = await AIMSCommodityCityRepository.GetCommodityCityByCondition(c => c.CityName == CityName);
            if (CommodityCity.Count() > 0)
            {
                int CommodityCityID = Convert.ToInt32(CommodityCity.FirstOrDefault().CityId);
                var CommodityPrices = await AIMSCommodityRepository.GetCommodityByCondition(c => c.CommodityCityId== CommodityCityID && c.DateInserted== DateToday);
                return CommodityPrices.OrderBy(c=>c.CommodityName);
            }
            else
                return null;
        }
        public async Task<IEnumerable<TbAimsCommodity>> GetTodaysCommodityPricesByCity(string CityName)
        {
            var CommodityCity = await AIMSCommodityCityRepository.GetCommodityCityByCondition(c => c.CityName == CityName);
            if (CommodityCity.Count() > 0)
            {
                int CommodityCityID = Convert.ToInt32(CommodityCity.FirstOrDefault().CityId);
                var CommodityPrices = await AIMSCommodityRepository.GetCommodityByCondition(c => c.CommodityCityId == CommodityCityID);
                return CommodityPrices.OrderBy(c => c.CommodityName);
            }
            else
                return null;
        }
        public async Task<IEnumerable<TbAimsCommodityCity>> GetAllCommodityCities()
        {
            return await AIMSCommodityCityRepository.GetAllCommodityCitysAsync();
        }

    }
}

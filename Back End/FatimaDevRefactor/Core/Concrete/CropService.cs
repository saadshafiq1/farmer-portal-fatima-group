﻿using Core.Interfaces;
using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Core.Concrete
{
    public class CropService : ICropService
    {
        ICropRepository cropRepository;
        IBlockCropRepository blockCropRepository;
        IFarmerCropPlanRepository farmerCropPlanRepository;
        ICropCaseMainRepository cropCaseMainRepository;
        ICropCalenderMainRepository cropCalenderMainRepository;
        ICropStepActionRepository cropStepActionRepository;
        IFarmerCropPlanUnitRepository farmerCropPlanUnitRepository;
        public CropService(ICropRepository cropRepository, IBlockCropRepository blockCropRepository, IFarmerCropPlanRepository farmerCropPlanRepository
            , ICropCaseMainRepository cropCaseMainRepository, ICropCalenderMainRepository cropCalenderMainRepository, ICropStepActionRepository cropStepActionRepository
            , IFarmerCropPlanUnitRepository farmerCropPlanUnitRepository)
        {
            this.cropRepository = cropRepository;
            this.blockCropRepository = blockCropRepository;
            this.farmerCropPlanRepository = farmerCropPlanRepository;
            this.cropCaseMainRepository = cropCaseMainRepository;
            this.cropCalenderMainRepository = cropCalenderMainRepository;
            this.cropStepActionRepository = cropStepActionRepository;
            this.farmerCropPlanUnitRepository = farmerCropPlanUnitRepository;
        }
        public async Task CreateCrop(TbCropTypeCode Crop)
        {
            await cropRepository.CreateCropAsync(Crop);
        }
        public async Task DeleteCrop(TbCropTypeCode Crop)
        {
            await cropRepository.DeleteCropAsync(Crop);
        }
        public async Task<IEnumerable<TbCropTypeCode>> GetAllCrop()
        {
            return await cropRepository.GetAllCropsAsync();
        }
        public async Task<TbCropTypeCode> GetCropById(long id)
        {
            return await cropRepository.GetCropByIdAsync(id);
        }
        public async Task<IEnumerable<TbBlockCrops>> GetCropsByBlockId(long id)
        {
            return await blockCropRepository.GetBlockCropByCondition(b => b.FarmBlockId == id);
        }
        public async Task UpdateCrop(TbCropTypeCode Crop)
        {
            await cropRepository.UpdateCropAsync(Crop);
        }
        public async Task CreateFarmerCropPlan(TbFarmerCropPlan farmerCropPlan)
        {
            await farmerCropPlanRepository.CreateFarmerCropPlanAsync(farmerCropPlan);
        }
        public async Task UpdateFarmerCropPlan(TbFarmerCropPlan farmerCropPlan)
        {
            await farmerCropPlanRepository.UpdateFarmerCropPlanAsync(farmerCropPlan);
        }
        public async Task<IEnumerable<TbFarmerCropPlan>> GetFarmerCropPlanByFarmerID(long farmerId)
        {
            var includes = new Expression<Func<TbFarmerCropPlan, object>>[2];
            includes[0] = c => c.CropCaseCodeNavigation;
            includes[1] = c => c.CropCaseCodeNavigation.CropCodeNavigation;

            var farmerCropPlan = await farmerCropPlanRepository.GetFarmerCropPlanByCondition(c => c.FarmerId == farmerId && c.ActiveStatus == "A", includes);
            return farmerCropPlan.OrderBy(c => c.FarmerCropPlanId);
        }
        public async Task<IEnumerable<TbFarmerCropPlan>> GetFarmerCropPlanByCropPlanId(long cropPlanId)
        {
            return await farmerCropPlanRepository.GetFarmerCropPlanByCondition(c => c.FarmerCropPlanId == cropPlanId);
        }
        public async Task<IEnumerable<TbCropCaseMain>> GetCorpCaseMainByCropID(long cropId)
        {
            var cropCaseMain = await cropCaseMainRepository.GetCropCaseMainByCondition(c => c.CropCode == cropId);
            return cropCaseMain.OrderBy(c => c.SowingBeginDate);
        }
        public async Task<IEnumerable<TbCropCalenderMain>> GetCropCalenderMainByCropCaseCode(long cropCaseCode)
        {
            var includes = new Expression<Func<TbCropCalenderMain, object>>[2];
            includes[0] = c => c.CropStepCodeNavigation;
            includes[1] = c => c.CropStepCodeNavigation.CropCodeNavigation;
            return await cropCalenderMainRepository.GetCropCalenderMainByCondition(c => c.CropCaseCode == cropCaseCode, includes);
        }
        public async Task<IEnumerable<TbCropStepAction>> GetCropStepActionByCropStepCode(int cropStepCode)
        {
            return await cropStepActionRepository.GetCropStepActionByCondition(c => c.CropStepCode == cropStepCode);
        }
        public async Task<IEnumerable<TbFarmerCropPlanUnit>> GetFarmerCropPlanUnitByCropPlanId(long cropPlanId)
        {
            return await farmerCropPlanUnitRepository.GetFarmerCropPlanUnitByCondition(c => c.FarmerCropPlanId == cropPlanId);
        }
    }
}

﻿using Core.Interfaces;
using Infrastructure.Interface;
using Infrastructure.Models;
using Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Concrete
{
    public class DealerService : IDealerService
    {
        IDealerInfoRepository DealerInfoRepository;
        IDealerSeedDetailRepository DealerSeedDetailRepository;
        IDealerFertilizerDetailRepository DealerFertilizerDetailRepository;
        IDealerStatusRepository DealerStatusRepository;
        IDealerCategoryRepository DealerCategoryRepository;

        public DealerService(IDealerInfoRepository dealerInfoRepository, IDealerFertilizerDetailRepository dealerFertilizerDetailRepository, IDealerSeedDetailRepository dealerSeedDetailRepository, IDealerStatusRepository DealerStatusRepository, IDealerCategoryRepository DealerCategoryRepository)
        {
            this.DealerInfoRepository = dealerInfoRepository;
            this.DealerSeedDetailRepository = dealerSeedDetailRepository;
            this.DealerFertilizerDetailRepository = dealerFertilizerDetailRepository;
            this.DealerStatusRepository = DealerStatusRepository;
            this.DealerCategoryRepository = DealerCategoryRepository;

        }
        public async Task CreateDealer(TbDealerInfo dealerInfo)
        {
            await DealerInfoRepository.CreateDealerInfoAsync(dealerInfo);
        }
        public async Task DeleteDealer(TbDealerInfo dealerInfo)
        {
            await DealerInfoRepository.DeleteDealerInfoAsync(dealerInfo);
        }
        public async Task<IEnumerable<TbDealerInfo>> GetAllDealers(int pageNumber, int pageSize)
        {
            return await DealerInfoRepository.GetDealerInfoByCondition(d=>d.ActiveStatus=="I", pageNumber, pageSize);
        }
        public async Task<IEnumerable<TbDealerInfo>> GetAllDealersCount()
        {
            return await DealerInfoRepository.GetAllDealersCountAsync();
        }
        public async Task<IEnumerable<TbDealerInfo>> GetAllDealersCountByFilter(string filter = "", string value = "")
        {
            return await DealerInfoRepository.GetAllDealersCountAsync(filter, value);
        }

        public async Task<IEnumerable<TbDealerInfo>> GetAllDealers(int pageNumber, int pageSize, string filter="",string value="")
        {
            return await DealerInfoRepository.GetAllDealerInfoAsync(pageNumber, pageSize, filter,value);
        }
       
        public async Task<IEnumerable<TbDealerInfo>> GetDealerByDealerName(string dealerName, int pageNumber, int pageSize)
        {
            return await DealerInfoRepository.GetDealerInfoByCondition(i => i.DealerName == dealerName, pageNumber, pageSize);
        }
        public async Task<IEnumerable<TbDealerInfo>> GetDealersById(List<int> dealerIds, int pageNumber, int pageSize)
        {
            return await DealerInfoRepository.GetDealerInfoByCondition(d => dealerIds.Contains(d.DealerId), pageNumber, pageSize);
        }
        public async Task<TbDealerInfo> GetDealerById(int id)
        {
            return await DealerInfoRepository.GetDealerInfoByIdAsync(id);
        }
      /*  public async Task<IEnumerable<TbDealerSeedDetails>> GetDealerBySeed(int pageNumber, int pageSize, int seedID, int districtCode)
        {
            return await DealerSeedDetailRepository.GetDealerSeedDetailByCondition(d => d.AvailableSeedId == (seedID == 0 ? d.AvailableSeedId : seedID) && d.Dealer.DistrictCode == (districtCode == 0 ? d.Dealer.DistrictCode : districtCode), pageNumber, pageSize);
        }
        public async Task<IEnumerable<TbDealerFertilizerDetails>> GetDealerByFertilizer(int fertilizerID, int districtCode)
        {
            return await DealerFertilizerDetailRepository.GetDealerFertilizerDetailByCondition(d => d.AvailableFertilizerId == (fertilizerID == 0 ? d.AvailableFertilizerId : fertilizerID) && d.Dealer.DistrictCode == (districtCode == 0 ? d.Dealer.DistrictCode : districtCode));
        }*/
        public async Task UpdateDealer(TbDealerInfo dealerInfo)
        {
            await DealerInfoRepository.UpdateDealerInfoAsync(dealerInfo);
        }

        public async Task<IEnumerable<TbDealerStatus>> GetDealerStatus()
        {
            return await DealerStatusRepository.GetDealerStatus();
        }

        public async Task<IEnumerable<TbDealerCategory>> GetDealerCategories()
        {
            return await DealerCategoryRepository.GetAllDealerCategoryAsync();
        }
        public async Task<IEnumerable<TbDealerInfo>> GetDistrictDealerByType(int DistrictCode, int DealerTypeID, int pageNumber, int pageSize,float latitude, float longitude)
        {
            return await DealerInfoRepository.GetDealerInfoByConditionLatLon(d => d.DealerTypeCode == DealerTypeID && d.DistrictCode == (DistrictCode == 0 ? d.DistrictCode : DistrictCode), pageNumber, pageSize, latitude, longitude);
        }
    }
}

﻿using Core.Interfaces;
using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Core.Concrete
{
    public class FarmService : IFarmService
    {
        IFarmRepository farmRepository;
        IFarmBlockRepository farmBlockRepository;
        IFarmUnitRepository farmUnitRepository;
        IFarmTypeRepository farmTypeRepository;
        IFarmBlockUnitRepository farmBlockUnitRepository;
        IFarmShapeRepository farmShapeRepository;
        IUnitShapeRepository unitShapeRepository;
        public FarmService(IFarmRepository farmRepository, IFarmBlockRepository farmBlockRepository,
            IFarmUnitRepository farmUnitRepository, IFarmTypeRepository farmTypeRepository, IFarmBlockUnitRepository farmBlockUnitRepository,
            IFarmShapeRepository farmShapeRepository, IUnitShapeRepository unitShapeRepository)
        {
            this.farmShapeRepository = farmShapeRepository;
            this.unitShapeRepository = unitShapeRepository;
            this.farmRepository = farmRepository;
            this.farmTypeRepository = farmTypeRepository;
            this.farmUnitRepository = farmUnitRepository;
            this.farmBlockRepository = farmBlockRepository;
            this.farmBlockUnitRepository = farmBlockUnitRepository;
        }
        public async Task CreateFarm(TbFarmInfo farm)
        {
            await farmRepository.CreateFarmAsync(farm);
        }
        public async Task CreateFarmBlock(TbFarmBlock farm)
        {
            await farmBlockRepository.CreateFarmBlockAsync(farm);
        }
        public async Task CreateFarmBlockUnit(TbFarmBlockUnit farm)
        {
            await farmBlockUnitRepository.CreateFarmBlockUnitAsync(farm);
        }
        public async Task CreateFarmType(TbFarmTypeCode farm)
        {
            await farmTypeRepository.CreateFarmTypeAsync(farm);
        }
        public async Task CreateShapeFarm(TbFarmShapes farm)
        {
            await farmShapeRepository.CreateFarmShapeAsync(farm);
        }
        public async Task CreateShapeUnit(TbUnitShapes farm)
        {
            await unitShapeRepository.CreateUnitShapeAsync(farm);
        }
        public async Task CreateUnitFarm(TbFarmUnit farm)
        {
            await farmUnitRepository.CreateFarmUnitAsync(farm);
        }
        public async Task DeleteFarm(TbFarmInfo farm)
        {
            await farmRepository.DeleteFarmAsync(farm);
        }
        public async Task DeleteFarmBlock(TbFarmBlock farm)
        {
            await farmBlockRepository.DeleteFarmBlockAsync(farm);
        }
        public async Task DeleteFarmBlockUnit(TbFarmBlockUnit farm)
        {
            await farmBlockUnitRepository.DeleteFarmBlockUnitAsync(farm);
        }
        public async Task DeleteFarmShape(TbFarmShapes farm)
        {
            await farmShapeRepository.DeleteFarmShapeAsync(farm);

        }
        public async Task DeleteFarmType(TbFarmTypeCode farm)
        {
            await farmTypeRepository.DeleteFarmTypeAsync(farm);
        }
        public async Task DeleteFarmUnit(TbFarmUnit farm)
        {
            await farmUnitRepository.DeleteFarmUnitAsync(farm);
        }
        public async Task DeleteUnitShape(TbUnitShapes farm)
        {
            await unitShapeRepository.DeleteUnitShapeAsync(farm);
        }
        public async Task<IEnumerable<TbFarmInfo>> GetAllFarm()
        {
            return await farmRepository.GetAllFarmsAsync();
        }
        public async Task<IEnumerable<TbFarmBlock>> GetAllFarmBlock()
        {
            return await farmBlockRepository.GetAllFarmBlocksAsync();
        }
        public async Task<IEnumerable<TbFarmBlockUnit>> GetAllFarmBlockUnit()
        {
            return await farmBlockUnitRepository.GetAllFarmBlockUnitsAsync();
        }
        public async Task<IEnumerable<TbFarmShapes>> GetAllFarmShapes()
        {
            return await farmShapeRepository.GetAllFarmShapesAsync();

        }
        public async Task<IEnumerable<TbFarmTypeCode>> GetAllFarmType()
        {
            return await farmTypeRepository.GetAllFarmTypesAsync();
        }
        public async Task<IEnumerable<TbFarmUnit>> GetAllFarmUnits()
        {
            return await farmUnitRepository.GetAllFarmUnitsAsync();
        }
        public async Task<IEnumerable<TbUnitShapes>> GetAllUnitShapes()
        {
            return await unitShapeRepository.GetAllUnitShapesAsync();

        }
        public async Task<IEnumerable<TbFarmBlockUnit>> GetBlockUnitsWithBlockId(int blockid)
        {
            return await farmBlockUnitRepository.GetFarmBlockUnitByCondition(b => b.FarmBlockId == blockid);

        }
        public async Task<IEnumerable<TbFarmBlock>> GetFarmBlockByFarmId(int farmid)

        {
            return await farmBlockRepository.GetFarmBlockByCondition(f => f.FarmId == farmid);
        }
        public async Task<TbFarmBlock> GetFarmBlockByIdAsync(long id)
        {
            return await farmBlockRepository.GetFarmBlockByIdAsync(id);
        }
        public async Task<TbFarmBlockUnit> GetFarmBlockUnitById(long id)
        {
            return await farmBlockUnitRepository.GetFarmBlockUnitByIdAsync(id);
        }
        public async Task<TbFarmInfo> GetFarmById(int id)
        {
            return await farmRepository.GetFarmByIdAsync(id);
        }
        public async Task<IEnumerable<TbFarmShapes>> GetFarmShapesByFarmIdAsync(long id)
        {
            var a = await farmShapeRepository.GetFarmShapeByCondition(f => f.FarmId == id);
            return a;
        }
        public async Task<TbFarmShapes> GetFarmShapeByIdAsync(long id)
        {
            return await farmShapeRepository.GetFarmShapeByIdAsync(id);
        }
        public async Task<IEnumerable<TbFarmInfo>> GetFarmsOfFarmer(int farmerid)
        {
            return await farmRepository.GetFarmByCondition(f => f.FarmerId == farmerid && f.ActiveStatus == "A");
        }

        public async Task<IEnumerable<TbFarmInfo>> GetFarmsOfFarmerWeb(int farmerid)
        {

            return await farmRepository.GetFarmsByConditionForWeb(f => f.FarmerId == farmerid && f.ActiveStatus == "A");
        }

        public async Task<TbFarmTypeCode> GetFarmTypeByIdAsync(long id)
        {
            return await farmTypeRepository.GetFarmTypeByIdAsync(id);
        }
        public async Task<TbFarmUnit> GetFarmUnitByIdAsync(long id)
        {
            return await farmUnitRepository.GetFarmUnitByIdAsync(id);
        }
        public async Task<IEnumerable<TbFarmUnit>> GetFarmUnitsByFarmId(int farmid)
        {
            return await farmUnitRepository.GetFarmUnitByCondition(f => f.FarmId == farmid);
        }
        public async Task<TbUnitShapes> GetUnitShapeByIdAsync(long id)
        {
            return await unitShapeRepository.GetUnitShapeByIdAsync(id);
        }
        public async Task<IEnumerable<TbUnitShapes>> GetUnitShapesByUnitId(long id)
        {
            var units = await unitShapeRepository.GetUnitShapeByCondition(f => f.UnitId == id);
            return units;
        }
        public async Task<IEnumerable<TbUnitShapes>> GetUnitShapesByFarmId(long id)
        {
            var includes = new Expression<Func<TbUnitShapes, object>>[1];
            includes[0] = u => u.Unit;
            return await unitShapeRepository.GetUnitShapeByCondition(f => f.Unit.FarmId == id, includes);
        }
        public async Task UpdateFarm(TbFarmInfo farm)
        {
            await farmRepository.UpdateFarmAsync(farm);
        }
        public async Task UpdateFarmBlock(TbFarmBlock block)
        {
            await farmBlockRepository.UpdateFarmBlockAsync(block);
        }
        public async Task UpdateFarmBlockUnit(TbFarmBlockUnit farm)
        {
            await farmBlockUnitRepository.UpdateFarmBlockUnitAsync(farm);
        }
        public async Task UpdateFarmType(TbFarmTypeCode farm)
        {
            await farmTypeRepository.UpdateFarmTypeAsync(farm);
        }
        public async Task UpdateShapeFarm(TbFarmShapes farm)
        {
            await farmShapeRepository.UpdateFarmShapeAsync(farm);
        }
        public async Task UpdateShapeUnit(TbUnitShapes farm)
        {
            await unitShapeRepository.UpdateUnitShapeAsync(farm);
        }
        public async Task UpdateUnitFarm(TbFarmUnit farm)
        {
            await farmUnitRepository.UpdateFarmUnitAsync(farm);
        }

        public async Task<IEnumerable<TbTubewellStatus>> TubeWell()
        {
            return await farmUnitRepository.TubeWell();
        }
        public async Task<IEnumerable<TbTubewellTypeInfo>> TubeWellType()
        {
            return await farmUnitRepository.TubeWellType();
        }
        public async Task<IEnumerable<TbWatercourseType>> WaterCourse()
        {
            return await farmUnitRepository.WaterCourse();
        }
        public async Task<IEnumerable<TbInputPurchaseInfo>> InputPurchase()
        {
            return await farmUnitRepository.InputPurchase();
        }
        public async Task<IEnumerable<TbOutputExportInfo>> OutputExport()
        {
            return await farmUnitRepository.OutputExport();
        }
        public async Task<IEnumerable<TbTunnelTypeInfo>> Tunnel()
        {
           return await farmUnitRepository.Tunnel();
        }

    }
}

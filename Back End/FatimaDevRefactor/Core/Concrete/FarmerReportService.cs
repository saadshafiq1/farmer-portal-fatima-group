﻿using Core.Interfaces;
using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Concrete
{
    public class FarmerReportService : IFarmerReportService
    {
        IFarmerReportsRepository FarmerReportsRepository;
        public FarmerReportService(IFarmerReportsRepository FarmerReportsRepository)
        {
            this.FarmerReportsRepository = FarmerReportsRepository;
        }
        public async Task CreateFarmerReport(TbFarmerReports FarmerReport)
        {
            await FarmerReportsRepository.CreateFarmerReportAsync(FarmerReport);
        }
        public async Task DeleteFarmerReport(TbFarmerReports FarmerReport)
        {
            await FarmerReportsRepository.DeleteFarmerReportAsync(FarmerReport);
        }
        public async Task<IEnumerable<TbFarmerReports>> GetAllFarmerReports(int pageNumber, int pageSize)
        {
            return await FarmerReportsRepository.GetAllFarmerReportsAsync(pageNumber,pageSize);           
        }
        public async Task<IEnumerable<TbFarmerReports>> GetFarmerReportByFarmerID(int FarmerID)
        {
            return await FarmerReportsRepository.GetFarmerReportByCondition(i => i.FarmerId == FarmerID && i.ActiveStatus=="A");
        }
        public async Task<TbFarmerReports> GetFarmerReportByID(int id)
        {
            return await FarmerReportsRepository.GetFarmerReportByIdAsync(id);
        }
        public async Task UpdateFarmerReport(TbFarmerReports FarmerReport)
        {
            await FarmerReportsRepository.UpdateFarmerReportAsync(FarmerReport);
        }
    }
}

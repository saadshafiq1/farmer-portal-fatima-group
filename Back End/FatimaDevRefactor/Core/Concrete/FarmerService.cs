﻿using Core.Interfaces;
using Infrastructure.Interface;
using Infrastructure.Models;
using Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Core.Concrete
{
    public class FarmerService : IFarmerService
    {
        IFarmererRepository farmerRepository;
        public FarmerService(IFarmererRepository farmerRepository)
        {
            this.farmerRepository = farmerRepository;
        }
        public async Task CreateFarmer(TbFarmerInfo Farmer)
        {
            await farmerRepository.CreateFarmerAsync(Farmer);
        }
        public async Task DeleteFarmer(TbFarmerInfo Farmer)
        {
            await farmerRepository.DeleteFarmerAsync(Farmer);
        }
        public async Task<IEnumerable<TbFarmerInfo>> GetAllFarmer()
        {
            return await farmerRepository.GetFarmerByCondition(f => f.ActiveStatus == "A");
        }
        public async Task<TbFarmerInfo> GetFarmerById(long id)
        {
            //return await farmerRepository.GetFarmerByIdAsync(id);
            var obj = await farmerRepository.GetFarmerByCondition(f => f.FarmerId == id);
            return obj.FirstOrDefault();
        }
        public async Task<TbFarmerInfo> GetFarmerByCellPhone(string phone)
        {
            var includes = new Expression<Func<TbFarmerInfo, object>>[2];
            includes[0] = f => f.DistrictCodeNavigation;
            includes[1] = f => f.TehsilCodeNavigation;
                

            return await farmerRepository.GetFarmerByCellPhone(f => f.CellPhone.Contains(phone), includes);
        }
        public async Task UpdateFarmer(TbFarmerInfo Farmer)
        {
            await farmerRepository.UpdateFarmerAsync(Farmer);
        }

        public async Task<IEnumerable<TbFarmerInfo>> GetAllFarmers(int pageNumber, int pageSize, string filter = "", string value = "")
        {
            return await farmerRepository.GetAllFarmerInfoAsync(pageNumber, pageSize, filter, value);
        }

        public async Task<IEnumerable<TbFarmerInfo>> GetAllFarmersCountByFilter(string filter = "", string value = "")
        {
            return await farmerRepository.GetAllFarmersCountAsync(filter, value);
        }

        public async Task<IEnumerable<TbFarmerInfo>> GetFarmerByFilter(string cnic, string phone)
        {
            if (!string.IsNullOrEmpty(cnic))
            {
                var includes = new Expression<Func<TbFarmerInfo, object>>[2];
                includes[0] = f => f.DistrictCodeNavigation;
                includes[1] = f => f.TehsilCodeNavigation;

                return await farmerRepository.GetFarmerByCondition(f => f.Cnic.Equals(cnic), includes);
            }
            else
            {
                var includes = new Expression<Func<TbFarmerInfo, object>>[2];
                includes[0] = f => f.DistrictCodeNavigation;
                includes[1] = f => f.TehsilCodeNavigation;

                return await farmerRepository.GetFarmerByCondition(f => f.CellPhone.Equals(phone), includes);
            }
            
        }
        public async Task<IEnumerable<TbFarmerStatus>> GetFarmerStatues()
        {          
               return await farmerRepository.GetFarmerStatuses();
          
        }
    }
}

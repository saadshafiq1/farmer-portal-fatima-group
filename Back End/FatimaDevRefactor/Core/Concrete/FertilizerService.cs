﻿using Core.Interfaces;
using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Concrete
{
    public class FertilizerService : IFertilizerService
    {
        IFertilizerTypesRepository FertilizerTypesRepository;
        public FertilizerService(IFertilizerTypesRepository fertilizerTypesRepository)
        {
            this.FertilizerTypesRepository = fertilizerTypesRepository;
        }
        public async Task CreateFertilizer(TbFertilizerTypes fertilizerType)
        {
            await FertilizerTypesRepository.CreateFertilizerTypeAsync(fertilizerType);
        }
        public async Task DeleteFertilizer(TbFertilizerTypes fertilizerType)
        {
            await FertilizerTypesRepository.DeleteFertilizerTypeAsync(fertilizerType);
        }
        public async Task<IEnumerable<TbFertilizerTypes>> GetAllFertilizers()
        {
            return await FertilizerTypesRepository.GetAllFertilizerTypesAsync();
        }
        public async Task<IEnumerable<TbFertilizerTypes>> GetFertilizerByFertilizerName(string fertilizerName)
        {
            return await FertilizerTypesRepository.GetFertilizerTypeByCondition(i => i.FertilizerName == fertilizerName);
        }
        public async Task<TbFertilizerTypes> GetFertilizerById(int id)
        {
            return await FertilizerTypesRepository.GetFertilizerTypeByIdAsync(id);
        }
        public async Task UpdateFertilizer(TbFertilizerTypes fertilizerType)
        {
            await FertilizerTypesRepository.UpdateFertilizerTypeAsync(fertilizerType);
        }
    }
}

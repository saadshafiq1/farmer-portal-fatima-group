﻿using Core.Interfaces;
using Infrastructure.Interface;
using Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Core.Concrete
{
    public class MediaService : IMediaService
    {
        IMediaRepository MediaRepository;
        IFarmerMediaRepository farmerMediaRepository;
        public MediaService(IMediaRepository mediaRepository, IFarmerMediaRepository farmerMediaRepository)
        {
            this.MediaRepository = mediaRepository;
            this.farmerMediaRepository = farmerMediaRepository;
        }
        public async Task<IEnumerable<TbMedia>> GetAllMedia()
        {
            var media= await MediaRepository.GetAllTbMediasAsync();           
            return media.OrderByDescending(x => x.InsertionDate);
        }
        public async Task<IEnumerable<TbMedia>> GetAllMedia(int pageNumber, int pageSize, string filter = "", string value = "")
        {
            return await MediaRepository.GetAllTbMediasAsync(pageNumber, pageSize, filter, value);
        }
        public async Task<IEnumerable<TbMedia>> GetAllMedia(string filter = "", string value = "")
        {
            return await MediaRepository.GetAllTbMediasAsync(filter, value);
        }
        public async Task<TbMedia> GetMediaById(long id)
        {
            return await MediaRepository.GetMediaByIdAsync(id);
        }
        public async Task<IEnumerable<TbFarmerMedia>> GetMediaOfFarmer(long id)
        {
            var farmerMedia = await farmerMediaRepository.GetFarmerMediaByCondition(x => x.FarmerId == id);
            return farmerMedia;
        }
        public async Task<IEnumerable<TbMedia>> GetMediaByDistrictAndMediaType(long MediaTypeId,long districtId)
        {
            var media= await MediaRepository.GetMediaByCondition(i => i.DistrictCode == districtId && i.MediaTypeId == MediaTypeId);
            return media.OrderByDescending(m => m.InsertionDate);
        }
        public async Task<IEnumerable<TbMedia>> GetMediaByMediaType(string MediaType)
        {
            var media = await MediaRepository.GetMediaByCondition(m=>m.MediaType.MediaType == MediaType);
            return media.OrderByDescending(m => m.InsertionDate);
        }
        public async Task<IEnumerable<TbMedia>> GetMediaByMediaType(long MediaTypeId)
        {
            var media = await MediaRepository.GetMediaByCondition(m => m.MediaTypeId == MediaTypeId);
            return media.OrderByDescending(m => m.InsertionDate);
        }
        public async Task UpdateMedia(TbMedia media)
        {
            await MediaRepository.UpdateMediaAsync(media);
        }
        public async Task CreateMedia(TbMedia media)
        {
            await MediaRepository.CreateMediaAsync(media);
        }
        public async Task DeleteMedia(TbMedia media)
        {
            await MediaRepository.DeleteMediaAsync(media);
        }
    }
}

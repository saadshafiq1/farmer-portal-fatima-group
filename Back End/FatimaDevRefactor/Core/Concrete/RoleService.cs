﻿using Core.Interfaces;
using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Concrete
{
    public class RoleService : IRoleService
    {
        IRoleRepository RoleRepository;
        public RoleService(IRoleRepository RoleRepository)
        {
            this.RoleRepository = RoleRepository;
        }
        public async Task CreateRole(TbUserRoleTypeCode Role)
        {
            await RoleRepository.CreateRoleAsync(Role);
        }
        public async Task DeleteRole(TbUserRoleTypeCode Role)
        {
            await RoleRepository.DeleteRoleAsync(Role);
        }
        public async Task<IEnumerable<TbUserRoleTypeCode>> GetAllRoles()
        {
            return await RoleRepository.GetAllRolesAsync();
        }
        public async Task<IEnumerable<TbUserRoleTypeCode>> GetRoleByRoleName(string role)
        {
            return await RoleRepository.GetRoleByCondition(i => i.RoleName == role);
        }
        public async Task<TbUserRoleTypeCode> GetRoleById(int id)
        {
            return await RoleRepository.GetRoleByIdAsync(id);
        }
        public async Task UpdateRole(TbUserRoleTypeCode Role)
        {
            await RoleRepository.UpdateRoleAsync(Role);
        }
    }
}

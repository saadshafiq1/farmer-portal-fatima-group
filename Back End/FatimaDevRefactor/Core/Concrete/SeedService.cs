﻿using Core.Interfaces;
using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Concrete
{
    public class SeedService : ISeedService
    {
        ISeedTypesRepository SeedTypesRepository;
        public SeedService(ISeedTypesRepository seedTypesRepository)
        {
            this.SeedTypesRepository = seedTypesRepository;
        }
        public async Task CreateSeed(TbSeedTypes seedType)
        {
            await SeedTypesRepository.CreateSeedTypeAsync(seedType);
        }
        public async Task DeleteSeed(TbSeedTypes seedType)
        {
            await SeedTypesRepository.DeleteSeedTypeAsync(seedType);
        }
        public async Task<IEnumerable<TbSeedTypes>> GetAllSeeds()
        {
            return await SeedTypesRepository.GetSeedTypeByCondition(i => i.ActiveStatus == "A");
        }
        public async Task<IEnumerable<TbSeedTypes>> GetSeedBySeedName(string seedName)
        {
            return await SeedTypesRepository.GetSeedTypeByCondition(i => i.SeedName == seedName);
        }
        public async Task<TbSeedTypes> GetSeedById(int id)
        {
            return await SeedTypesRepository.GetSeedTypeByIdAsync(id);
        }
        public async Task UpdateSeed(TbSeedTypes seedType)
        {
            await SeedTypesRepository.UpdateSeedTypeAsync(seedType);
        }
    }
}

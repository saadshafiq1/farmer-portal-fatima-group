﻿using Core.Interfaces;
using Infrastructure.Interface;
using Infrastructure.Models;
using Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Core.Concrete
{
    public class TicketService : ITicketService
    {
        ITicketRepository ticketRepository;
        ITicketTypeRepository ticketTypeRepository;
        ITicketTypeStatusRepository TicketTypeStatusRepository;
        ITicketActivityRepository ticketActivityRepository;
        public TicketService(ITicketRepository ticketRepository, ITicketTypeRepository ticketTypeRepository, ITicketTypeStatusRepository TicketTypeStatusRepository, ITicketActivityRepository ticketActivityRepository)
        {
            this.ticketTypeRepository = ticketTypeRepository;
            this.TicketTypeStatusRepository = TicketTypeStatusRepository;
            this.ticketRepository = ticketRepository;
            this.ticketActivityRepository = ticketActivityRepository;
        }
        public async Task CreateTicket(TbTicketInfo ticket)
        {
            await ticketRepository.CreateTicketAsync(ticket);
        }

        public async Task DeleteTicket(TbTicketInfo ticket)
        {
            await ticketRepository.DeleteTicketAsync(ticket);
        }

        public async Task<IEnumerable<TbTicketInfo>> GetAllTickets()
        {
            return await ticketRepository.GetAllTicketsAsync();
        }
        public async Task<IEnumerable<TbTicketInfo>> GetAllActiveTickets(int pageNumber, int pageSize, string filter = "", string value = "")
        {
            return await ticketRepository.GetAllTicketInfoAsync(pageNumber, pageSize, filter, value);
        }
        public async Task<IEnumerable<TbTicketInfo>> GetAllFarmerTickets(int pageNumber, int pageSize,int farmerid, string filter = "", string value = "")
        {           
            return await ticketRepository.GetFarmerTicketInfoAsync(pageNumber, pageSize,farmerid, filter, value);
        }
        public async Task<IEnumerable<TbTicketInfo>> GetAllFarmerTicketsCount(int farmerid , string filter = "", string value = "")
        {
            return await ticketRepository.GetFarmerTicketCountAsync(farmerid,filter, value);
        }
        public async Task<IEnumerable<TbTicketInfo>> GetAllTSOTickets(int pageNumber, int pageSize, int tsoID, string filter = "", string value = "")
        {
            return await ticketRepository.GetTSOTicketInfoAsync(pageNumber, pageSize, tsoID, filter, value);
        }
        public async Task<IEnumerable<TbTicketInfo>> GetAllTSOTicketsCount(int tsoID, string filter = "", string value = "")
        {
            return await ticketRepository.GetTSOTicketCountAsync(tsoID, filter, value);
        }

        public async Task<TbTicketInfo> GetTicketById(long id)
        {
            return await ticketRepository.GetTicketInfoById(id);
        }

        public async Task<IEnumerable<TbTicketInfo>> GetCurrentTicketById(long id)
        {
            var includes = new Expression<Func<TbTicketInfo, object>>[1];
            includes[0] = f => f.TicketType;

            return await ticketRepository.GetTicketByCondition(f => f.FarmerId == id && (f.StatusId == 1 || f.StatusId == 2),includes);
        }
        public async Task<IEnumerable<TbTicketInfo>> GetPreviousTicketById(long id)
        {
            var includes = new Expression<Func<TbTicketInfo, object>>[1];
            includes[0] = f => f.TicketType;

            return await ticketRepository.GetTicketByCondition(f => f.FarmerId == id && f.StatusId == 3, includes);
        }
        public async Task<IEnumerable<TbTicketInfo>> GetFarmerTicketsByTicketTypeId(long id,int tickettypeid)
        {
            var includes = new Expression<Func<TbTicketInfo, object>>[1];
            includes[0] = f => f.TicketType;

            return await ticketRepository.GetTicketByCondition(f => f.FarmerId == id && f.TicketTypeId ==tickettypeid, includes);
        }
        public async Task UpdateTicket(TbTicketInfo ticket)
        {
            await ticketRepository.UpdateTicketAsync(ticket);
        }

        public async Task<IEnumerable<TbTicketsTypes>> GetTicketTypes()
        {
            return await ticketTypeRepository.GetTicketTypes();
        }

        public async Task<IEnumerable<TbTicketsStatus>> GetTicketStatuses()
        {
            return await TicketTypeStatusRepository.GetAllTicketsStautsAsync();
        }
        public async Task<TbTicketsStatus> GetTicketsStatusByID(int ID)
        {
            return await TicketTypeStatusRepository.GetTicketsStatusByID(ID);
        }


        public async Task<IEnumerable<TbTicketInfo>> GetAllTicketsCount(string filter = "", string value = "")
        {
            return await ticketRepository.GetAllTicketCountAsync(filter, value);
        }


        public async Task<IEnumerable<TbTicketInfo>> GetTicketsStatusesCount(int id)
        {          
            return await ticketRepository.GetTicketByCondition(f => f.StatusId == id);
        }

        public async Task<IEnumerable<TbTicketInfo>> GetTicketsStatusesCountForFarmer(int id,int farmerid)
        {
            return await ticketRepository.GetTicketByCondition(f => f.StatusId == id && f.FarmerId == farmerid);
        }

        public async Task CreateTicketActivity(TbTicketActivity ticket)
        {
            await ticketActivityRepository.CreateTicketAsync(ticket);
        }
    }
}

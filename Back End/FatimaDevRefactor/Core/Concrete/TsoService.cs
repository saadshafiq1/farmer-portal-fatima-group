﻿using Core.Interfaces;
using Infrastructure.Interface;
using Infrastructure.Models;
using Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Core.Concrete
{
    public class TsoService : ITsoService
    { 
        ITsoRepository tsoRepository;
        public TsoService(ITsoRepository tsoRepository)
        {
            this.tsoRepository = tsoRepository;
        }
        public async Task<TbTsoInfo> GetTSOById(long id = 0)
        {
            return await tsoRepository.GetTSOById(f => f.Tsoid == id);
        }
        public async Task<IEnumerable<TbTsoInfo>> GetAllTSOs()
        {

            var includes = new Expression<Func<TbTsoInfo, object>>[2];
            includes[0] = f => f.SalePointCodeNavigation;
            includes[1] = f => f.RegionCodeNavigation;

            return await tsoRepository.GetAllTSOsAsync(includes);
        }
        public async Task<TbTsoInfo> GetTSOByCellPhone(string phone)
        {
            var includes = new Expression<Func<TbTsoInfo, object>>[2];
            includes[0] = f => f.SalePointCodeNavigation;
            includes[1] = f => f.RegionCodeNavigation;


            return await tsoRepository.GetTSOByCellPhone(f => f.CellPhone.Contains(phone) || f.AlternateCellPhone.Contains(phone), includes);
        }
    }

 }


﻿using Core.Interfaces;
using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Concrete
{
    public class UserLoginService : IUserLoginService
    {
        IUserLoginRepository UserLoginRepository;
        IUserRoleTypeCodeRepository UserRoleTypeCodeRepository;
        public UserLoginService(IUserLoginRepository UserLoginRepository, IUserRoleTypeCodeRepository UserRoleTypeCodeRepository)
        {
            this.UserLoginRepository = UserLoginRepository;
            this.UserRoleTypeCodeRepository = UserRoleTypeCodeRepository;
        }
        public async Task CreateUserLogin(TbUserLogin UserLogin)
        {
            await UserLoginRepository.CreateUserAsync(UserLogin);
        }
        public async Task DeleteUserLogin(TbUserLogin UserLogin)
        {
            await UserLoginRepository.DeleteUserAsync(UserLogin);
        }
        public async Task<IEnumerable<TbUserLogin>> GetAllUserLogin()
        {
            return await UserLoginRepository.GetAllUsersAsync();
        }
        public async Task<IEnumerable<TbUserLogin>> GetUserByCellPhone(string phone)
        {
            return await UserLoginRepository.GetUserByCondition(i => i.CellPhone.Contains(phone));
        }
        public async Task<IEnumerable<TbUserLogin>> GetUserByUserName(string username)
        {
            return await UserLoginRepository.GetUserByCondition(i => i.UserName.Contains(username));
        }
        public async Task<TbUserLogin> GetUserLoginById(Guid id)
        {
            return await UserLoginRepository.GetUserByIdAsync(id);
        }
        public async Task UpdateUserLogin(TbUserLogin UserLogin)
        {
            await UserLoginRepository.UpdateUserAsync(UserLogin);
        }
        public async Task<IEnumerable<TbUserRoleTypeCode>> GetAllUserRoleTypeCodeAsync()
        {
            return await UserRoleTypeCodeRepository.GetAllUserRoleTypeCodesAsync();
        }
    }
}

﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IAddressService
    {
        Task<IEnumerable<TbProvinceCode>> GetAllProvinces();
        Task<TbProvinceCode> GetProvinceById(long id);
        Task<IEnumerable<TbDistrictCode>> GetDistrictsOfProvince(long provinceid);
        Task<IEnumerable<TbDistrictCode>> GetDistrictsByRegion(long regionid);
        Task<IEnumerable<TbTehsilCode>> GetTehsilsOfDistrict(long districtid);
        Task<TbTehsilCode> GetTehsilById(int tehsilid);
        Task<TbDistrictCode> GetDistrictById(int districtid);
        Task<IEnumerable<TbRegionCode>> GetCitiesOfProvince(int provinceid);
        Task<TbRegionCode> GetRegionById(int regionid);
        Task<IEnumerable<TbRegionCode>> GetALLRegion();
        Task<IEnumerable<TbDistrictCode>> GetAllDistricts();
        Task<IEnumerable<TbSalepointCode>> GetAllSalesPoint();
    }
}

﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface ICallCenterAgentService
    {
        Task<TbCallCenterAgentInfo> GetAgentByUserName(Guid userName);
        Task<TbCallCenterAgentInfo> GetAgentById(long id);
        Task<IEnumerable<TbCallCenterAgentInfo>> GetAllAgents();
    }
}

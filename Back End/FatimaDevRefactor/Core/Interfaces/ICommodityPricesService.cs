﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface ICommodityPricesService
    {
        Task<IEnumerable<TbAimsCommodity>> GetTodaysCommodityPricesByCity(string CityName, DateTime DateToday);
        Task<IEnumerable<TbAimsCommodity>> GetTodaysCommodityPricesByCity(string CityName);
        Task<IEnumerable<TbAimsCommodityCity>> GetAllCommodityCities();
    }
}

﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface ICropService
    {
        Task<IEnumerable<TbCropTypeCode>> GetAllCrop();
        Task<TbCropTypeCode> GetCropById(long id);
        Task UpdateCrop(TbCropTypeCode Crop);
        Task CreateCrop(TbCropTypeCode Crop);
        Task DeleteCrop(TbCropTypeCode Crop);
        Task<IEnumerable<TbBlockCrops>> GetCropsByBlockId(long id);
        Task CreateFarmerCropPlan(TbFarmerCropPlan farmerCropPlan);
        Task UpdateFarmerCropPlan(TbFarmerCropPlan farmerCropPlan);
        Task<IEnumerable<TbFarmerCropPlan>> GetFarmerCropPlanByFarmerID(long farmerId);
        Task<IEnumerable<TbFarmerCropPlan>> GetFarmerCropPlanByCropPlanId(long cropPlanId);
        Task<IEnumerable<TbCropCaseMain>> GetCorpCaseMainByCropID(long cropId);
        Task<IEnumerable<TbCropCalenderMain>> GetCropCalenderMainByCropCaseCode(long cropCaseCode);
        Task<IEnumerable<TbCropStepAction>> GetCropStepActionByCropStepCode(int cropStepCode);
        Task<IEnumerable<TbFarmerCropPlanUnit>> GetFarmerCropPlanUnitByCropPlanId(long cropPlanId);
    }
}

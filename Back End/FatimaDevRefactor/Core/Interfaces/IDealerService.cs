﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IDealerService
    {
        Task<IEnumerable<TbDealerInfo>> GetAllDealers(int pageNumber, int pageSize);
        Task<IEnumerable<TbDealerInfo>> GetAllDealersCount();
        Task<IEnumerable<TbDealerStatus>> GetDealerStatus();
        Task<IEnumerable<TbDealerCategory>> GetDealerCategories();
        Task<IEnumerable<TbDealerInfo>> GetAllDealersCountByFilter(string filer = "", string value = "");
        Task<IEnumerable<TbDealerInfo>> GetAllDealers(int pageNumber, int pageSize, string filer="",string value="");
        Task<TbDealerInfo> GetDealerById(int id);
      /*  Task<IEnumerable<TbDealerSeedDetails>> GetDealerBySeed(int pageNumber, int pageSize, int seedID, int districtCode);
        Task<IEnumerable<TbDealerFertilizerDetails>> GetDealerByFertilizer(int seedID, int districtCode);*/
        Task<IEnumerable<TbDealerInfo>> GetDealersById(List<int> dealerIds, int pageNumber, int pageSize);
        Task<IEnumerable<TbDealerInfo>> GetDealerByDealerName(string dealerName, int pageNumber, int pageSize);
        Task UpdateDealer(TbDealerInfo dealerType);
        Task CreateDealer(TbDealerInfo dealerType);
        Task DeleteDealer(TbDealerInfo dealerType);
        Task<IEnumerable<TbDealerInfo>> GetDistrictDealerByType(int DistrictCode, int DealerTypeID, int pageNumber, int pageSize, float latitude, float longitude);

    }
}

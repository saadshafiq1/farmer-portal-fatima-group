﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IFarmService
    {
        Task<IEnumerable<TbFarmInfo>> GetAllFarm();
        Task<TbFarmInfo> GetFarmById(int id);
        Task<IEnumerable<TbFarmInfo>> GetFarmsOfFarmer(int farmerid);
         Task<IEnumerable<TbFarmInfo>> GetFarmsOfFarmerWeb(int farmerid);
        Task UpdateFarm(TbFarmInfo farm);
        Task CreateFarm(TbFarmInfo farm);
        Task DeleteFarm(TbFarmInfo farm);
        Task<IEnumerable<TbFarmBlock>> GetAllFarmBlock();
        Task<TbFarmBlock> GetFarmBlockByIdAsync(long id);
        Task UpdateFarmBlock(TbFarmBlock farm);
        Task CreateFarmBlock(TbFarmBlock farm);
        Task DeleteFarmBlock(TbFarmBlock farm);
        Task<IEnumerable<TbFarmTypeCode>> GetAllFarmType();
        Task<TbFarmTypeCode> GetFarmTypeByIdAsync(long id);
        Task UpdateFarmType(TbFarmTypeCode farm);
        Task CreateFarmType(TbFarmTypeCode farm);
        Task DeleteFarmType(TbFarmTypeCode farm);
        Task<IEnumerable<TbFarmBlock>> GetFarmBlockByFarmId(int farmid);
        Task<IEnumerable<TbFarmUnit>> GetFarmUnitsByFarmId(int farmid);
        Task<IEnumerable<TbFarmBlockUnit>> GetBlockUnitsWithBlockId(int blockid);
        Task<IEnumerable<TbFarmBlockUnit>> GetAllFarmBlockUnit();
        Task<TbFarmBlockUnit> GetFarmBlockUnitById(long id);
        Task UpdateFarmBlockUnit(TbFarmBlockUnit farm);
        Task CreateFarmBlockUnit(TbFarmBlockUnit farm);
        Task DeleteFarmBlockUnit(TbFarmBlockUnit farm);
        Task<IEnumerable<TbFarmUnit>> GetAllFarmUnits();
        Task<TbFarmUnit> GetFarmUnitByIdAsync(long id);
        Task UpdateUnitFarm(TbFarmUnit farm);
        Task CreateUnitFarm(TbFarmUnit farm);
        Task DeleteFarmUnit(TbFarmUnit farm);
        Task<IEnumerable<TbFarmShapes>> GetAllFarmShapes();
        Task<TbFarmShapes> GetFarmShapeByIdAsync(long id);
        Task<IEnumerable<TbFarmShapes>> GetFarmShapesByFarmIdAsync(long id);
        Task UpdateShapeFarm(TbFarmShapes farm);
        Task CreateShapeFarm(TbFarmShapes farm);
        Task DeleteFarmShape(TbFarmShapes farm);
        Task<IEnumerable<TbUnitShapes>> GetAllUnitShapes();
        Task<IEnumerable<TbUnitShapes>> GetUnitShapesByUnitId(long id);
        Task<IEnumerable<TbUnitShapes>> GetUnitShapesByFarmId(long id);
        Task<TbUnitShapes> GetUnitShapeByIdAsync(long id);
        Task UpdateShapeUnit(TbUnitShapes farm);
        Task CreateShapeUnit(TbUnitShapes farm);
        Task DeleteUnitShape(TbUnitShapes farm);

        Task<IEnumerable<TbTubewellStatus>> TubeWell();
        Task<IEnumerable<TbTubewellTypeInfo>> TubeWellType();
        Task<IEnumerable<TbWatercourseType>> WaterCourse();
        Task<IEnumerable<TbTunnelTypeInfo>> Tunnel();
        Task<IEnumerable<TbInputPurchaseInfo>> InputPurchase();
        Task<IEnumerable<TbOutputExportInfo>> OutputExport();

    }
}

﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IFarmerReportService
    {
        Task<IEnumerable<TbFarmerReports>> GetAllFarmerReports(int pageNumber, int pageSize);
        Task<TbFarmerReports> GetFarmerReportByID(int id);
        Task<IEnumerable<TbFarmerReports>> GetFarmerReportByFarmerID(int FarmerID);
        Task UpdateFarmerReport(TbFarmerReports FarmerReport);
        Task CreateFarmerReport(TbFarmerReports FarmerReport);
        Task DeleteFarmerReport(TbFarmerReports FarmerReport);
    }
}

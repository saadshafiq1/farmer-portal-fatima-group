﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IFarmerService
    {

        Task<IEnumerable<TbFarmerInfo>> GetAllFarmer();
        Task<IEnumerable<TbFarmerStatus>> GetFarmerStatues();
        Task<IEnumerable<TbFarmerInfo>> GetAllFarmers(int pageNumber, int pageSize, string filter = "", string value = "");
        Task<TbFarmerInfo> GetFarmerById(long id);
        Task UpdateFarmer(TbFarmerInfo Farmer);
        Task CreateFarmer(TbFarmerInfo Farmer);
        Task DeleteFarmer(TbFarmerInfo Farmer);
        Task<TbFarmerInfo> GetFarmerByCellPhone(string phone);
        Task<IEnumerable<TbFarmerInfo>> GetAllFarmersCountByFilter(string filter = "", string value = "");
        Task<IEnumerable<TbFarmerInfo>> GetFarmerByFilter(string cnic, string phone);




    }
}

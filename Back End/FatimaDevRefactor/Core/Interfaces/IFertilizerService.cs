﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
   public interface IFertilizerService
    {
        Task<IEnumerable<TbFertilizerTypes>> GetAllFertilizers();
        Task<TbFertilizerTypes> GetFertilizerById(int id);
        Task<IEnumerable<TbFertilizerTypes>> GetFertilizerByFertilizerName(string fertilizerName);    
        Task UpdateFertilizer(TbFertilizerTypes fertilizerType);
        Task CreateFertilizer(TbFertilizerTypes fertilizerType);
        Task DeleteFertilizer(TbFertilizerTypes fertilizerType);
    }
}

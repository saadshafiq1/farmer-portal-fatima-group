﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IMediaService
    {
        Task<IEnumerable<TbMedia>> GetAllMedia();
        Task<IEnumerable<TbMedia>> GetAllMedia(int pageNumber, int pageSize, string filer = "", string value = "");
        Task<IEnumerable<TbMedia>> GetAllMedia(string filer = "", string value = "");
        Task<TbMedia> GetMediaById(long id);
        Task<IEnumerable<TbFarmerMedia>> GetMediaOfFarmer(long id);
        Task<IEnumerable<TbMedia>> GetMediaByDistrictAndMediaType(long MediaTypeId, long districtId);
        Task<IEnumerable<TbMedia>> GetMediaByMediaType(string MediaType);
        Task<IEnumerable<TbMedia>> GetMediaByMediaType(long MediaTypeId);
        Task UpdateMedia(TbMedia media);
        Task CreateMedia(TbMedia media);
        Task DeleteMedia(TbMedia media);
    }
}

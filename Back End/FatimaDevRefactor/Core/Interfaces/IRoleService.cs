﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
   public interface IRoleService
    {
        Task<IEnumerable<TbUserRoleTypeCode>> GetAllRoles();
        Task<TbUserRoleTypeCode> GetRoleById(int id);
        Task<IEnumerable<TbUserRoleTypeCode>> GetRoleByRoleName(string role);       
        Task UpdateRole(TbUserRoleTypeCode UserLogin);
        Task CreateRole(TbUserRoleTypeCode UserLogin);
        Task DeleteRole(TbUserRoleTypeCode UserLogin);
    }
}

﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface ISeedService
    {
        Task<IEnumerable<TbSeedTypes>> GetAllSeeds();
        Task<TbSeedTypes> GetSeedById(int id);
        Task<IEnumerable<TbSeedTypes>> GetSeedBySeedName(string seedName);
        Task UpdateSeed(TbSeedTypes seedType);
        Task CreateSeed(TbSeedTypes seedType);
        Task DeleteSeed(TbSeedTypes seedType);
    }
}

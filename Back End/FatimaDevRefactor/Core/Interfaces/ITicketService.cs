﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace Core.Interfaces
{
    public interface ITicketService
    {
        Task<IEnumerable<TbTicketInfo>> GetAllTickets();
        Task<IEnumerable<TbTicketInfo>> GetAllActiveTickets(int pageNumber, int pageSize, string filter = "", string value = "");      
        Task<IEnumerable<TbTicketInfo>> GetAllTicketsCount(string filter = "", string value = "");
        Task<IEnumerable<TbTicketInfo>> GetAllFarmerTickets(int pageNumber, int pageSize, int farmerid, string filter = "", string value = "");
        Task<IEnumerable<TbTicketInfo>> GetAllFarmerTicketsCount(int farmerid,string filter = "", string value = "");
        Task<IEnumerable<TbTicketInfo>> GetAllTSOTickets(int pageNumber, int pageSize, int tsoID, string filter = "", string value = "");
        Task<IEnumerable<TbTicketInfo>> GetAllTSOTicketsCount(int tsoID, string filter = "", string value = "");
        Task<TbTicketInfo> GetTicketById(long id);
        Task UpdateTicket(TbTicketInfo ticket);
        Task CreateTicket(TbTicketInfo ticket);
        Task DeleteTicket(TbTicketInfo ticket);
        Task<IEnumerable<TbTicketInfo>> GetCurrentTicketById(long id);
        Task<IEnumerable<TbTicketInfo>> GetPreviousTicketById(long id);
        Task<IEnumerable<TbTicketsTypes>> GetTicketTypes();
        Task<IEnumerable<TbTicketsStatus>> GetTicketStatuses();
        Task<TbTicketsStatus> GetTicketsStatusByID(int ID);
        Task<IEnumerable<TbTicketInfo>> GetTicketsStatusesCount(int id);
        Task<IEnumerable<TbTicketInfo>> GetTicketsStatusesCountForFarmer(int id, int farmerid);
        Task CreateTicketActivity(TbTicketActivity ticket);
        Task<IEnumerable<TbTicketInfo>> GetFarmerTicketsByTicketTypeId(long id, int tickettypeid);


    }
}

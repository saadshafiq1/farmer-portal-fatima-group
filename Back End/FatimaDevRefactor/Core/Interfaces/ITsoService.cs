﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface ITsoService
    {
        Task<TbTsoInfo> GetTSOById(long id);
        Task<IEnumerable<TbTsoInfo>> GetAllTSOs();
        Task<TbTsoInfo> GetTSOByCellPhone(string phone);
    }
}

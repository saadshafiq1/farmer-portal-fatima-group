﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IUserLoginService
    {
        Task<IEnumerable<TbUserLogin>> GetAllUserLogin();
        Task<TbUserLogin> GetUserLoginById(Guid id);
        Task<IEnumerable<TbUserLogin>> GetUserByCellPhone(string phone);
        Task<IEnumerable<TbUserLogin>> GetUserByUserName(string username);
        Task UpdateUserLogin(TbUserLogin UserLogin);
        Task CreateUserLogin(TbUserLogin UserLogin);
        Task DeleteUserLogin(TbUserLogin UserLogin);
        Task<IEnumerable<TbUserRoleTypeCode>> GetAllUserRoleTypeCodeAsync();
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace API.Common
{
    public static class Common
    {
        public enum ResponseCode
        {
            Success = 0,
            Failure = 1
        }

        public static string UploadImage(string path, string image)
        {
            string filePath = Environment.CurrentDirectory + "\\" + path + ".jpg";

            File.WriteAllBytes(filePath, Convert.FromBase64String(image));

            return filePath;
        }
        public static bool sendEmail(string strSubject, string strBody, string strFrom, string strTo)
        {
            bool emailSent = false;
            try
            {
                var client = new SmtpClient("smtp.gmail.com", 587)
                {
                    Credentials = new NetworkCredential("MyEmail@gmail.com", "MyPassword"),
                    EnableSsl = true
                };
                client.Send(strFrom, strTo, strSubject, strBody);
                emailSent = true;
            }
            catch (Exception)
            {
              //  throw;
            }
            return emailSent;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Models;
using Infrastructure.OtherModels;
using Infrastructure.ViewModels;
using Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using System.Dynamic;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CommodityController : ControllerBase
    {
       // private readonly FFContext _context;
        public ApiResponse response=new ApiResponse();
        ICommodityPricesService _CommodityPricesService;
      
        private readonly IMapper _Mapper;
      
        public CommodityController(ICommodityPricesService CommodityPricesService, IMapper _mapper)
        {
            _CommodityPricesService = CommodityPricesService;          
            _Mapper = _mapper;         
        }

        // GET: api/TbAimsCommodity
        [HttpGet("TodaysPricesByCity")]
        public async Task<ApiResponse> GetTodaysPricesByCity(string City,DateTime DateToday)
        {   
           var tbAimsCommodities = await _CommodityPricesService.GetTodaysCommodityPricesByCity(City, DateToday);

            if (tbAimsCommodities.Count() < 1)
            {
                DateTime maxDataInserted = DateToday.AddDays(-1);
                try
                {
                    // Get last date when data pulled from AIMS                   
                    // maxDataInserted = Convert.ToDateTime((await _CommodityPricesService.GetTodaysCommodityPricesByCity(City)).OrderByDescending(c=>c.DateInserted).First().DateInserted);
                    maxDataInserted = Convert.ToDateTime((await _CommodityPricesService.GetTodaysCommodityPricesByCity(City)).GroupBy(c => c.DateInserted).OrderByDescending(c => c.Key).First().First().DateInserted);
                }
                catch(Exception ex)
                {
                    maxDataInserted = DateToday.AddDays(-1);
                }

                tbAimsCommodities = await _CommodityPricesService.GetTodaysCommodityPricesByCity(City, maxDataInserted);
            }
            response.Data = tbAimsCommodities;
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Data Retrieved";
            return response;
        }
        [HttpGet("Cities")]
        public async Task<ApiResponse> GetAllCommodityCities()
        {
            response.Data = await _CommodityPricesService.GetAllCommodityCities();
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Data Retrieved";
            return response;
        }
    }
}
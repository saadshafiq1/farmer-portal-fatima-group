﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Models;
using Infrastructure.OtherModels;
using Microsoft.AspNetCore.Authorization;
using Infrastructure.ViewModels;
using Core.Interfaces;
using System.Linq.Expressions;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Farmer, TSO, Call service agent")]

    public class CropController : ControllerBase
    {
        private readonly ICropService _cropService;
        ApiResponse response = new ApiResponse();
        public CropController(ICropService cropService, FFContext context)
        {
            _cropService = cropService;
        }

        // GET: api/TbCropTypeCodes
        [HttpGet]
        public async Task<ApiResponse> GetCrops()
        {
            response.Data = await _cropService.GetAllCrop();
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            return response;
        }

        // GET: api/TbCropTypeCodes/5
        [HttpGet("{id}")]
        public async Task<ApiResponse> GetCrops([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            var tbCropTypeCode = await _cropService.GetCropById(id);

            if (tbCropTypeCode == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbCropTypeCode;
            return response;
        }

        [HttpGet("CropsOfBlock/{id}")]
        public async Task<ApiResponse> GetCropsOfBlock([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            var cropsOfBlock = await _cropService.GetCropsByBlockId(id);

            if (cropsOfBlock == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = cropsOfBlock;
            return response;
        }

        // PUT: api/TbCropTypeCodes/5
        [HttpPut("{id}")]
        public async Task<ApiResponse> PutCrops([FromRoute] int id, [FromBody] TbCropTypeCode tbCropTypeCode)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            if (id != tbCropTypeCode.CropCode)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }

            try
            {
                await _cropService.UpdateCrop(tbCropTypeCode);
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = tbCropTypeCode;

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TbCropTypeCodeExists(id))
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Could not save.";
                    return response;
                }
                else
                {
                    throw;
                }
            }

            return response;
        }

        // POST: api/TbCropTypeCodes
        [HttpPost]
        public async Task<ApiResponse> PostCrops([FromBody] TbCropTypeCode tbCropTypeCode)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            try
            {
                await _cropService.CreateCrop(tbCropTypeCode);
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = tbCropTypeCode;
            }
            catch (Exception)
            {
                if (TbCropTypeCodeExists(tbCropTypeCode.CropCode))
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Record Already Exists.";
                    response.Data = tbCropTypeCode;
                }
                else
                {
                    throw;
                }
            }
            return response;
        }

        // DELETE: api/TbCropTypeCodes/5
        [HttpDelete("{id}")]
        public async Task<ApiResponse> DeleteCrops([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            var tbCropTypeCode = await _cropService.GetCropById(id);
            if (tbCropTypeCode == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }
            tbCropTypeCode.ActiveStatus = "I";
            await _cropService.UpdateCrop(tbCropTypeCode);
            // await _cropService.DeleteCrop(tbCropTypeCode);

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbCropTypeCode;
            return response;
        }

        // DELETE: api/TbFarmerCropPlan/5
        [HttpDelete("DeleteCropOfFarmer/{FarmerCropPlanId}")]
        public async Task<ApiResponse> DeleteCropOfFarmer([FromRoute] int farmerCropPlanId)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            var tbFarmerCropPlans = await _cropService.GetFarmerCropPlanByCropPlanId(farmerCropPlanId);
            if (tbFarmerCropPlans == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }
            var tbFarmerCropPlan = tbFarmerCropPlans.FirstOrDefault();
            tbFarmerCropPlan.ActiveStatus = "I";
            await _cropService.UpdateFarmerCropPlan(tbFarmerCropPlan);

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbFarmerCropPlan;
            return response;
        }

        private bool TbCropTypeCodeExists(int id)
        {
            return _cropService.GetCropById(id) == null ? false : true;// _context.TbCropTypeCode.Any(e => e.CropCode == id);
        }

        [HttpPost("RegisterFarmPlan")]
        public async Task<ApiResponse> RegisterFarmerPlan([FromBody]RegFarmerModel model)
        {

            ApiResponse response = new ApiResponse();

            try
            {
                var plan = GetGeneralCropPlan(model.cropId, model.sowingDate.Day, model.sowingDate.Month);

                var fcp = new TbFarmerCropPlan()
                {
                    ActiveStatus = "A",
                    CropCaseCode = plan.Result[0].CaseId,
                    FarmerId = model.farmerId,
                    SowingDate = model.sowingDate
                };

                var fplan = new FarmerCropPlan()
                {
                    Plan = plan.Result,
                    UnitIds = model.farmUnitIds,
                    FarmId = model.farmId
                };

                List<TbFarmerCropPlanUnit> fcpus = new List<TbFarmerCropPlanUnit>();
                foreach (var a in model.farmUnitIds)
                {
                    var fcpu = new TbFarmerCropPlanUnit()
                    {
                        FarmUnitId = a,
                        ActiveStatus = "A"
                    };
                    fcpus.Add(fcpu);

                    fcp.TbFarmerCropPlanUnit.Add(fcpu);
                }

                await _cropService.CreateFarmerCropPlan(fcp);
                fplan.CropPlanId = fcp.FarmerCropPlanId;
                response.ResponseMessage = "Data Saved Successfully";
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.Data = fplan;

                return response;

            }
            catch (Exception ex)
            {
                response.ResponseMessage = "Error occured while saving";
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.Data = null;

                return response;

            }

        }
        [HttpGet("Abc")]
        public RegFarmerModel GetAbc()
        {
            RegFarmerModel rm = new RegFarmerModel()
            {
                cropId = 1,
                farmerId = 4,
                farmUnitIds = new int[] { 1, 2, 3 }
            };


            return rm;
        }
        [HttpGet("CropsOfFarmer/{farmerId}")]
        public async Task<ApiResponse> CropsOfFarmer(int farmerId)
        {
            ApiResponse response = new ApiResponse();

            try
            {
                var cropPlans = await _cropService.GetFarmerCropPlanByFarmerID(farmerId);

                List<FarmerCropsModel> farmerCrops = new List<FarmerCropsModel>();
                int i = 0;
                foreach (var cp in cropPlans)
                {
                    i++;
                    var fc = new FarmerCropsModel()
                    {
                        CropName = cp.CropCaseCodeNavigation.CropCodeNavigation.CropName,
                        CropImage = cp.CropCaseCodeNavigation.CropCodeNavigation.CropLogo,
                        CropId = cp.CropCaseCodeNavigation.CropCode.Value,
                        FarmerCropPlanId = cp.FarmerCropPlanId,
                        SowingDate = cp.SowingDate,
                        CropPlanNumber = i,
                        CropNameUrdu = cp.CropCaseCodeNavigation.CropCodeNavigation.CropNameUrdu
                    };
                    farmerCrops.Add(fc);
                }

                response.Data = farmerCrops;
                response.ResponseMessage = "Data retrieved successfully.";
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;

                return response;
            }
            catch (Exception)
            {
                response.Data = null;
                response.ResponseMessage = "Error occured during executuion";
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                return response;
            }

        }

        //General Crop Plan
        [HttpGet("GeneralCropPlan")]
        public async Task<List<GeneralCropPlan>> GetGeneralCropPlan(int cropid, int day, int month, int cropplanid = -1)
        {

            List<GeneralCropPlan> result = new List<GeneralCropPlan>();
            //find case between these dates against cropid
            var CropCase = await _cropService.GetCorpCaseMainByCropID(cropid);

            TbCropCaseMain ourCase = null;

            var firstCase = CropCase.ToList()[0];
            var lastCase = CropCase.ToList()[CropCase.Count() - 1];
            foreach (var c in CropCase)
            {
                //date comparison without considering year

                int beginMonth = c.SowingBeginDate.Month;
                int endMonth = c.SowingEndDate.Month;

                if (endMonth < beginMonth)
                {
                    endMonth = endMonth + beginMonth;
                }

                if (month >= beginMonth && month <= endMonth)
                {
                    if (month == beginMonth && month == endMonth)
                    {
                        if (day >= c.SowingBeginDate.Day && day <= c.SowingEndDate.Day)
                        {
                            ourCase = c;
                        }
                    }
                    else
                    if (month == c.SowingBeginDate.Month)
                    {

                        if (day >= c.SowingBeginDate.Day)
                        {
                            ourCase = c;
                        }

                    }
                    else if (month == c.SowingEndDate.Month)

                    {
                        if (day <= c.SowingEndDate.Day)
                        {
                            ourCase = c;
                        }
                    }

                }
            }

            //checking if the sowing date is outside the boundaries of our cases, in which case the nearest one will be fetched.
            if (ourCase == null)
            {
                int temp1 = firstCase.SowingBeginDate.Month - month;
                if (temp1 < 0)
                {
                    temp1 = temp1 + 12;
                }

                int temp2 = month - lastCase.SowingBeginDate.Month;

                if (temp2 < 0)
                {
                    temp2 = temp2 + 12;
                }

                if (temp1 <= temp2)
                {
                    ourCase = firstCase;
                }
                else
                {
                    ourCase = lastCase;
                }

            }
            if (ourCase != null)
            {
                var cropPlan = await _cropService.GetCropCalenderMainByCropCaseCode(ourCase.CropCaseCode);

                string dateString = DateTime.Now.Year.ToString() + "-" + month.ToString("00") + "-" + day.ToString("00");
                DateTime planStepDate = DateTime.ParseExact(dateString, "yyyy-MM-dd", null);

                foreach (var a in cropPlan)
                {
                    int stepMargin = Convert.ToInt32(a.StepMarginDays) - 1;
                    int daysAfterPrevious = Convert.ToInt32(a.DaysafterpreviousStep);
                    daysAfterPrevious = (daysAfterPrevious == 0) ? 0 : daysAfterPrevious + 1;

                    planStepDate = planStepDate.AddDays(daysAfterPrevious);

                    GeneralCropPlan gp = new GeneralCropPlan()
                    {
                        CropId = a.CropStepCodeNavigation.CropCodeNavigation.CropCode,
                        DaysafterpreviousStep = a.DaysafterpreviousStep,
                        VideoUrl = a.CropStepCodeNavigation.VideoUrl,
                        StepNumber = a.CropStepCodeNavigation.StepNumber,
                        StepImage = a.CropStepCodeNavigation.StepImage,
                        CropName = a.CropStepCodeNavigation.CropCodeNavigation.CropName,
                        CropNameUrdu = a.CropStepCodeNavigation.CropCodeNavigation.CropNameUrdu,

                        StepName = a.CropStepCodeNavigation.StepNameUrder,
                        // StartDate = a.StepStartDate,
                        // EndDate = a.StepEndDate,
                        StartDate = planStepDate,
                        EndDate = planStepDate.AddDays(stepMargin),
                        StepDetail = a.CropStepCodeNavigation.StepDetail,
                        CaseId = a.CropCaseCode.Value
                    };
                    planStepDate = planStepDate.AddDays(stepMargin);

                    var stepActions = await _cropService.GetCropStepActionByCropStepCode(a.CropStepCode.Value);

                    List<CropActions> cropStepActions = new List<CropActions>();

                    foreach (var cs in stepActions)
                    {
                        var temp = new CropActions()
                        {
                            Detail = cs.Detail,
                            IsAlert = cs.IsAlert
                        };
                        cropStepActions.Add(temp);
                    }

                    gp.StepActions = cropStepActions;
                    result.Add(gp);
                }

            }

            return result;
        }

        [HttpGet("GeneralCropNotification")]
        public async Task<ApiResponse> GetGeneralCropNotification(int cropid, int day, int month)
        {
            ApiResponse response = new ApiResponse();

            try
            {
                var result = await GetGeneralCropPlan(cropid, day, month);
                response.Data = result;
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "";

            }
            catch (Exception)
            {
                response.Data = null;
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Error occured while retrieving data.";
            }
            return response;
        }
        [HttpGet("CropPlan/{cropPlanId}")]
        public async Task<ApiResponse> GetCropPlanWithId(int cropPlanId)
        {

            ApiResponse response = new ApiResponse();

            try
            {
                FarmerCropPlan fcp = new FarmerCropPlan();

                List<GeneralCropPlan> result = new List<GeneralCropPlan>();
                var units = await _cropService.GetFarmerCropPlanUnitByCropPlanId(cropPlanId);
                var cropCase = await _cropService.GetFarmerCropPlanByCropPlanId(cropPlanId);

                int cropCaseCode = cropCase.FirstOrDefault().CropCaseCode.Value;

                fcp.UnitIds = new int[units.Count()];
                int i = 0;
                foreach (var a in units)
                {
                    fcp.UnitIds[i] = (int)a.FarmUnitId.Value;
                    i++;
                }
                var cropPlan = await _cropService.GetCropCalenderMainByCropCaseCode(cropCaseCode);

                DateTime sowingDate = cropCase.FirstOrDefault().SowingDate.Value;
                var year = sowingDate.Year;

                DateTime previousDate = new DateTime();
                foreach (var a in cropPlan)
                {
                    GeneralCropPlan gp = new GeneralCropPlan();


                    var stepNumber = a.CropStepCodeNavigation.StepNumber;

                    if (stepNumber == 1)
                    {
                        gp.StartDate = cropCase.FirstOrDefault().SowingDate;
                        DateTime edate = Convert.ToDateTime(gp.StartDate);
                        edate = edate.AddDays(a.StepMarginDays.Value - 1);
                        gp.EndDate = edate;

                        previousDate = edate;
                    }
                    else
                    {
                        gp.StartDate = previousDate.AddDays(a.DaysafterpreviousStep.Value + 1);//add 1
                        DateTime edate = Convert.ToDateTime(gp.StartDate);
                        edate = edate.AddDays(a.StepMarginDays.Value - 1);
                        gp.EndDate = edate;
                        previousDate = edate;

                    }
                    gp.CropId = a.CropStepCodeNavigation.CropCodeNavigation.CropCode;
                    gp.AudioUrl = a.CropStepCodeNavigation.AudioUrl;

                    gp.DaysafterpreviousStep = a.DaysafterpreviousStep;

                    gp.VideoUrl = a.CropStepCodeNavigation.VideoUrl;
                    gp.StepNumber = a.CropStepCodeNavigation.StepNumber;
                    gp.StepName = a.CropStepCodeNavigation.StepNameUrder;
                    gp.StepImage = a.CropStepCodeNavigation.StepImage;
                    gp.CropName = a.CropStepCodeNavigation.CropCodeNavigation.CropName;
                    gp.CropNameUrdu = a.CropStepCodeNavigation.CropCodeNavigation.CropNameUrdu;

                    gp.StepDetail = a.CropStepCodeNavigation.StepDetail;
                    gp.CaseId = a.CropCaseCode.Value;
                    var stepActions = await _cropService.GetCropStepActionByCropStepCode(a.CropStepCode.Value);

                    List<CropActions> cropStepActions = new List<CropActions>();

                    foreach (var cs in stepActions)
                    {
                        var temp = new CropActions()
                        {
                            Detail = cs.Detail,
                            IsAlert = cs.IsAlert
                        };
                        cropStepActions.Add(temp);
                    }

                    gp.StepActions = cropStepActions;
                    result.Add(gp);
                }

                fcp.Plan = result;

                response.Data = fcp;
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Data retrieved successfully.";
                return response;
            }
            catch (Exception)
            {
                response.Data = null;
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Error occured while retrieving data.";
                return response;
            }
        }

        [HttpGet("CropPlanCutOffDates/{CropCode}")]
        public async Task<ApiResponse> GetCropPlanCutOffDates([FromRoute]int CropCode)
        {
            ApiResponse response = new ApiResponse();

            try
            {
                if (!ModelState.IsValid)
                {
                    response.Data = null;
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Model State not valid.";
                }

                var tbCropTypeCode = await _cropService.GetCorpCaseMainByCropID(CropCode);
                
                if (tbCropTypeCode == null)
                {
                    response.Data = null;
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Not Found";
                }
                else
                {
                    CropPlanCutOffDates cropPlanCutOffDates = new CropPlanCutOffDates();
                    cropPlanCutOffDates.MinDate = tbCropTypeCode.Min(c => c.SowingBeginDate);
                    cropPlanCutOffDates.MaxDate = tbCropTypeCode.Max(c => c.SowingEndDate);
                    cropPlanCutOffDates.CropID = Convert.ToInt32(tbCropTypeCode.FirstOrDefault().CropCode);

                    response.Data = cropPlanCutOffDates;
                    response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                    response.ResponseMessage = "";

                }  
            }
            catch (Exception)
            {
                response.Data = null;
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Error occured while retrieving data.";
            }
            return response;
        }
    }
}



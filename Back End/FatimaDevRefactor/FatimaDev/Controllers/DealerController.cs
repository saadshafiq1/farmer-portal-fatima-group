﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Models;
using Infrastructure.OtherModels;
using Microsoft.AspNetCore.Authorization;
using Infrastructure.ViewModels;
using Core.Interfaces;
using AutoMapper;
using System.Dynamic;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Farmer, TSO, Call service agent")]

    public class DealerController : ControllerBase
    {
       //  private readonly FFContext _Context;
        private readonly IDealerService _DealerService;
        private readonly IMapper _Mapper;
        ApiResponse response = new ApiResponse();
        public DealerController(IDealerService dealerService, FFContext _context, IMapper _mapper)
        {
            _DealerService = dealerService;
            _Mapper = _mapper;
           // _Context = _context;
        }

        // GET: api/tbDealerInfo
        [HttpGet]
        public async Task<ApiResponse> GetDealers()
        {
            var tbDealerInfo =await _DealerService.GetAllDealers(0,1000);
            var result = _Mapper.Map<List<DealerInfoModel>>(tbDealerInfo);       
            response.Data = result;
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            return response;
        }

        [HttpGet("AllDealers")]
        public async Task<ApiResponse> GetAllDealers(int pageNumber, int pageSize, string filter = "", string value = "")
        {
            var tbDealerDetail = await _DealerService.GetAllDealers(pageNumber, pageSize, filter.ToLower(),value);
           
            var result = _Mapper.Map<List<DealerDetailModel>>(tbDealerDetail);
            dynamic obj = new ExpandoObject();
            obj.data = result;
            
            if (String.IsNullOrEmpty(filter))
            {
                var count = await _DealerService.GetAllDealersCount();
                obj.totalCount = count.Count();
            }
            else
            {
                var count = await _DealerService.GetAllDealersCountByFilter(filter.ToLower(), value);
                 obj.totalCount = count.Count();
            }
            response.Data = obj;
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            return response;
        }

        // GET: api/tbDealerInfo/5
        [HttpGet("{id}")]
        public async Task<ApiResponse> GetDealers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            
            var tbDealerInfo = await _DealerService.GetDealerById(id);

            if (tbDealerInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }
            var result = _Mapper.Map<DealerDetailModel>(tbDealerInfo);
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = result;
            return response;
        }

        // GET: api/GetSeedDealers?seedID=5&districtCode=5
      /*  [HttpGet("GetSeedDealers")]
        public async Task<ApiResponse> GetSeedDealers(int pageNumber, int pageSize, int seedID, int districtCode, float lat = 0, float lon = 0)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            
            var tbDealerSeedDetails= await _DealerService.GetDealerBySeed(pageNumber, pageSize,seedID, districtCode);
            var dealerIDs = tbDealerSeedDetails.Select(a => a.DealerId).ToList();

            if (tbDealerSeedDetails == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }

            var tbDealerInfo = await _DealerService.GetDealersById(dealerIDs);

            List<TbDealerInfo> Data = new List<TbDealerInfo>();
            foreach (var dealer in tbDealerInfo)
            {
                if (!String.IsNullOrEmpty(dealer.Latitude) && !String.IsNullOrEmpty(dealer.Longitude))
                {
                    var latitude = Math.Pow(69.1 * (float.Parse(dealer.Latitude) - lat), 2);
                    var longitude = Math.Pow(69.1 * (lon - float.Parse(dealer.Longitude)) * Math.Cos(float.Parse(dealer.Latitude) / 57.3), 2);
                    var res = Math.Sqrt(latitude + longitude);

                    if (dealer.DistrictCodeNavigation != null)
                    {
                        dealer.DistrictCodeNavigation.TbDealerInfo = null;
                    }
                    if (dealer.TehsilCodeNavigation != null)
                    {
                        dealer.TehsilCodeNavigation.TbDealerInfo = null;
                    }
                    if (res < 25)
                    {
                        Data.Add(dealer);
                    }
                }
            }

            dynamic obj = new ExpandoObject();
            obj.data = _Mapper.Map<List<DealerInfoModel>>(Data);
            if (Data.Count() == 0)
            {
                obj.data = _Mapper.Map<List<DealerInfoModel>>(tbDealerInfo);

            }
            response.Data = obj.data;
           // var result = ;
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            //response.Data = result;
            return response;
        }

        // GET: api/GetFertilizerDealers?fertilizerID=5&districtCode=5
        [HttpGet("GetFertilizerDealers")]
        public async Task<ApiResponse> GetFertilizerDealers(int fertilizerID, int districtCode)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid."; 
                return response;
            }


            var tbDealerFertilizerDetails = await _DealerService.GetDealerByFertilizer(fertilizerID, districtCode);
            var dealerIDs = tbDealerFertilizerDetails.Select(a => a.DealerId).ToList();

            if (tbDealerFertilizerDetails == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }

            var tbDealerInfo = await _DealerService.GetDealersById(dealerIDs);
            var result = _Mapper.Map<List<DealerInfoModel>>(tbDealerInfo);
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = result;
            return response;
        }*/
        // PUT: api/tbDealerInfo/5
        [HttpPut("{id}")]
        public async Task<ApiResponse> PutDealers([FromRoute] int id, [FromBody] TbDealerInfo tbDealerInfo)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            if (id != tbDealerInfo.DealerId)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }

            try
            {
               await _DealerService.UpdateDealer(tbDealerInfo);
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = tbDealerInfo;

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DealerInfoExists(id))
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Could not save.";
                    return response;
                }
                else
                {
                    throw;
                }
            }

            return response;
        }

        // POST: api/tbDealerInfo
        [HttpPost]
        public async Task<ApiResponse> PostDealers([FromBody] TbDealerInfo tbDealerInfo)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }
          
            try
            {
                await _DealerService.CreateDealer(tbDealerInfo);
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = tbDealerInfo;
            }
            catch (Exception)
            {
                if (DealerInfoExists(tbDealerInfo.DealerId))
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Record Already Exists.";
                    response.Data = tbDealerInfo;
                }
                else
                {
                    throw;
                }
            }
            return response;
        }

        // DELETE: api/tbDealerInfo/5
        [HttpDelete("{id}")]
        public async Task<ApiResponse> DeleteDealers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }
            
            var tbDealerInfo = await _DealerService.GetDealerById(id);
            if (tbDealerInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }
           
            await _DealerService.DeleteDealer(tbDealerInfo);
           
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbDealerInfo;
            return response;
        }

        private bool DealerInfoExists(int id)
        {
            return _DealerService.GetDealerById(id) == null ? false : true;
        }

        // GET: api/GetAllDealersWithLatLong?lat=0&lon=0
        [HttpGet("GetAllDealersWithLatLong")]
        public async Task<ApiResponse> GetAllDealersWithLatLong(int pageNumber, int pageSize,float lat = 0, float lon = 0)
        {
            try {
           
            var tbDealerInfo = await _DealerService.GetAllDealers(pageNumber, pageSize);
                
            List<TbDealerInfo> Data= new List<TbDealerInfo>();
            foreach (var dealer in tbDealerInfo)
            {
                    if (!String.IsNullOrEmpty(dealer.Latitude) && !String.IsNullOrEmpty(dealer.Longitude))
                    {
                        var latitude = Math.Pow(69.1 * (float.Parse(dealer.Latitude) - lat), 2);
                        var longitude = Math.Pow(69.1 * (lon - float.Parse(dealer.Longitude)) * Math.Cos(float.Parse(dealer.Latitude) / 57.3), 2);
                        var res = Math.Sqrt(latitude + longitude);

                        if (dealer.DistrictCodeNavigation != null)
                        {
                            dealer.DistrictCodeNavigation.TbDealerInfo = null;
                        }
                        if (dealer.TehsilCodeNavigation != null)
                        {
                            dealer.TehsilCodeNavigation.TbDealerInfo = null;
                        }
                        if (res < 25)
                        {
                            Data.Add(dealer);
                        }
                    }
            }
            
            dynamic obj = new ExpandoObject();
            obj.data = Data;

          
            response.Data = obj;
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            return response;
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }

            return response;
        }
        [HttpGet("GetDistrictDealerByType")]
        public async Task<ApiResponse> GetDistrictDealerByType(int pageNumber, int pageSize, int DealerTypeID, int DistrictCode, float lat = 0, float lon = 0)
        {
            try
            {
                var tbDealerInfo = await _DealerService.GetDistrictDealerByType(DistrictCode, DealerTypeID, pageNumber, pageSize, lat, lon);

                var result = _Mapper.Map<List<DealerInfoModel>>(tbDealerInfo);

                response.Data = result;
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                return response;
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }

            return response;
        }

        [HttpGet("GetDealerStatusesAndCategory")]
        public async Task<ApiResponse> GetDealerStatus()
        {
            try
            {
                dynamic obj = new ExpandoObject();
                obj.status = await _DealerService.GetDealerStatus();
                obj.category = await _DealerService.GetDealerCategories();
                response.Data = obj;
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }
            return response;
        }

        [HttpGet("GetDealerCategory")]
        public async Task<ApiResponse> GetDealerCategory()
        {
            try
            {
                response.Data = await _DealerService.GetDealerCategories();
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }
            return response;
        }

    }

}



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Models;
using Infrastructure.OtherModels;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;
using Infrastructure.ViewModels;
using Core.Interfaces;
using System.Dynamic;

namespace API.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FarmController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IFarmService _farmService;
        private readonly IAddressService _addressService;

       ApiResponse response = new ApiResponse();
        public FarmController(IMapper mapper, IFarmService farmService ,IAddressService addressService)
        {
           
            _farmService = farmService;
            _mapper = mapper;
            _addressService = addressService;
        }

        // GET: api/TbFarmInfoes
        [HttpGet]
        public async Task<ApiResponse> GetTbFarmInfo()
        {

            response.Data = await _farmService.GetAllFarm();
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Data Retrieved";
            return response;
        }

        // GET: api/TbFarmInfoes/5


        [HttpGet("FarmsOfFarmer/{id}")]
        public async Task<ApiResponse> FarmsOfFarmer([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            var tbFarmInfo = await _farmService.GetFarmsOfFarmer((int)id);

            var result = _mapper.Map<List<FarmModel>>(tbFarmInfo);

            foreach (var a in result)
            {
                var farmType = await _farmService.GetFarmTypeByIdAsync((int)(a.FarmTypeCode == null ? 1 : a.FarmTypeCode));
                var farmBlock = await _farmService.GetFarmBlockByFarmId((int)a.FarmId);
                var farmUnits = await _farmService.GetFarmUnitsByFarmId((int)a.FarmId);

                a.TbFarmBlock = _mapper.Map<List<FarmBlockModel>>(farmBlock); ;
                a.FarmType = farmType.TypeName;
                a.TotalUnits = farmUnits.Count();
            }
            if (tbFarmInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = result;
            return response;
        }

        [HttpGet("FarmsOfFarmerWeb/{id}")]
        public async Task<ApiResponse> FarmsOfFarmerWeb([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            var tbFarmInfo = await _farmService.GetFarmsOfFarmerWeb((int)id);

            var result = _mapper.Map<List<FarmModel>>(tbFarmInfo);
           
            if (tbFarmInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }
            List<FarmModelWeb> Data = new List<FarmModelWeb>();
            foreach (var a in result)
            {
                FarmModelWeb obj = new FarmModelWeb();
                if (a.ProvinceCode > 0 && a.ProvinceCode != null)
                {
                    var ProvinceCodeNavigation = await _addressService.GetProvinceById((long)a.ProvinceCode);
                    obj.ProvinceCode = ProvinceCodeNavigation.ProvinceCode;
                    obj.ProvinceName= ProvinceCodeNavigation.ProvinceName;
                    obj.ProvinceNameUrdu = ProvinceCodeNavigation.ProvinceNameUrdu;
                }
                if (a.DistrictCode > 0 && a.DistrictCode != null)
                {                   
                     var DistrictCodeNavigation = await _addressService.GetDistrictById((int)a.DistrictCode);
                   
                    obj.DistrictCode = DistrictCodeNavigation.DistrictCode;
                    obj.DistrictName = DistrictCodeNavigation.DistrictName;
                    obj.DistrictNameUrdu= DistrictCodeNavigation.DistrictNameUrdu;
               
                }
                if (a.TehsilCode > 0 && a.TehsilCode != null)
                {
                    var TehsilCodeNavigation = await _addressService.GetTehsilById((int)a.TehsilCode);
                    obj.TehsilCode = TehsilCodeNavigation.TehsilCode;
                    obj.TehsilName = TehsilCodeNavigation.TehsilName;
                    obj.TehsilNameUrdu = TehsilCodeNavigation.TehsilNameUrdu;
                
                }
                if (a.RegionId > 0 && a.RegionId != null)
                {
                    var RegionCodeNavigation = await _addressService.GetRegionById((int)a.RegionId);
                    obj.RegionCode = RegionCodeNavigation.RegionCode;
                    obj.RegionName = RegionCodeNavigation.RegionName;
                    obj.RegionNameUrdu = RegionCodeNavigation.RegionNameUrdu;
                }
               
                obj.FarmModel = a;
           
                Data.Add(obj);
            }
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = Data;
            return response;
        }

        [HttpGet("{id}")]
        public async Task<ApiResponse> GetFarmInfo([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            var tbFarmInfo = await _farmService.GetFarmById(id);



            if (tbFarmInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found.";
                return response;
            }

            var block = await _farmService.GetFarmBlockByFarmId((int)id);
            tbFarmInfo.TbFarmBlock = (ICollection<TbFarmBlock>)block;
            
            foreach (var a in tbFarmInfo.TbFarmBlock)
            {
                a.Farm = null;
                a.TbFarmBlockUnit = null;
            }

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbFarmInfo;
            return response;

        }

        //// PUT: api/TbFarmInfoes/5
        [HttpPut("{id}")]
        public async Task<ApiResponse> PutFarmInfo([FromRoute] int id, [FromBody] TbFarmInfo tbFarmInfo)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            if (id != tbFarmInfo.FarmId)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not found.";
                return response;
            }

            try
            {              

              await  _farmService.UpdateFarm(tbFarmInfo);
               
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = tbFarmInfo;
            }
            catch (Exception)
            {
                if (!TbFarmInfoExists(id))
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Could not save.";
                    return response;
                }
                else
                {
                    throw;
                }
            }

            return response;


        }

        [HttpPut("FarmUnit/{id}")]
        public async Task<ApiResponse> PutFarmUnit([FromRoute] int id, [FromBody] TbFarmUnit unitInfo)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            if (id != unitInfo.FarmUnitId)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not found.";
                return response;
            }

            try
            {
             await   _farmService.UpdateUnitFarm(unitInfo);              

                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = unitInfo;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TbFarmInfoExists(id))
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Could not save.";
                    return response;
                }
                else
                {
                    throw;
                }
            }

            return response;

        }

        [HttpPut("BlockUnit/{id}")]
        public async Task<ApiResponse> PutBlockUnit([FromRoute] int id, [FromBody] TbFarmBlockUnit unitInfo)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            if (id != unitInfo.FarmBlockUnitId)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not found.";
                return response;
            }


            try
            {
                await _farmService.UpdateFarmBlockUnit(unitInfo);
              
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = unitInfo;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TbFarmInfoExists(id))
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Could not save.";
                    return response;
                }
                else
                {
                    throw;
                }
            }

            return response;

        }

        // POST: api/TbFarmInfoes
        [HttpPost]
        public async Task<ApiResponse> PostFarmInfo([FromBody] TbFarmInfo tbFarmInfo)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            try
            {
                await _farmService.CreateFarm(tbFarmInfo);

                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = tbFarmInfo;
            }
            catch (Exception)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Could not save.";
            }
            return response;

        }

        // DELETE: api/TbFarmInfoes/5
        [HttpDelete("{id}")]
        public async Task<ApiResponse> DeleteFarmInfo([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            var tbFarmInfo = await _farmService.GetFarmById(id);
          
            if (tbFarmInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found.";
                return response;
            }
            tbFarmInfo.ActiveStatus = "I";
            await _farmService.UpdateFarm(tbFarmInfo);
            //await _farmService.DeleteFarm(tbFarmInfo);
           
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbFarmInfo;
            return response;
        }

        private bool TbFarmInfoExists(int id)
        {
            return _farmService.GetFarmById(id) == null ? false : true;
        }


        [HttpGet("BlocksOfFarms/{id}")]
        public async Task<ApiResponse> BlocksOfFarms([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            var tbFarmInfo = await _farmService.GetFarmBlockByFarmId((int)id);

            if (tbFarmInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not found.";
                return response;
            }

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbFarmInfo;
            return response;
        }

        [HttpGet("UnitsOfFarms/{id}")]
        public async Task<ApiResponse> UnitsOfFarms([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            var tbFarmInfo = await _farmService.GetFarmUnitsByFarmId((int)id);

            if (tbFarmInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not found.";
                return response;
            }

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbFarmInfo;
            return response;
        }

        [HttpGet("UnitsOfBlocks/{id}")]
        public async Task<ApiResponse> UnitsOfBlocks([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            var tbFarmInfo = await _farmService.GetFarmUnitsByFarmId((int)id);

            if (tbFarmInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not found.";
                return response;
            }

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbFarmInfo;
            return response;
        }

        [HttpPost("FarmUnit")]
        public async Task<ApiResponse> PostFarmBlock([FromBody] TbFarmUnit tbfarmunit)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
            }

            try
            {
                await _farmService.CreateUnitFarm(tbfarmunit);
               
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = tbfarmunit;
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }
            return response;
        }

        [HttpPost("BlockUnit")]
        public async Task<ApiResponse> PostBlockUnit([FromBody] TbFarmBlockUnit tbfarmblockunit)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
            }

            try
            {
                await _farmService.CreateFarmBlockUnit(tbfarmblockunit);
              
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = tbfarmblockunit;
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }
            return response;
        }

        [HttpPut("Block/{id}")]
        public async Task<ApiResponse> PutBlock([FromRoute] int id, [FromBody] TbFarmBlock unitInfo)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            if (id != unitInfo.FarmBlockId)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not found.";
                return response;
            }

            try
            {
                           await   _farmService.UpdateFarmBlock(unitInfo);
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = unitInfo;
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TbFarmInfoExists(id))
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Could not save.";
                    return response;
                }
                else
                {
                    throw;
                }
            }

            return response;

        }

        [HttpPost("FarmBlock")]
        public async Task<ApiResponse> PostFarmBlock([FromBody] TbFarmBlock tbfarmBlock)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
            }

            try
            {
                await _farmService.CreateFarmBlock(tbfarmBlock);
                
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = tbfarmBlock;
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }
            return response;
        }

        [HttpGet("TypesOfFarm")]
        public async Task<ApiResponse> TypesOfFarm()
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            var tbFarmInfo = await _farmService.GetAllFarmType();

            if (tbFarmInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Farm Not Found.";
            }

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbFarmInfo;
            return response;
        }

        [HttpGet("FarmShapeByFarmId/{id}")]
        public async Task<ApiResponse> GetFarmShape([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }


            var tbDistInfo = await _farmService.GetFarmShapesByFarmIdAsync(id);

            if (tbDistInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }


            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbDistInfo;
            return response;
        }

        [HttpPost("FarmShape")]
        public async Task<ApiResponse> CreateFarmShape([FromBody] TbFarmShapes farmshape)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            try
            {
                await _farmService.CreateShapeFarm(farmshape);
            }
            catch (Exception)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Error occured while saving.";
                response.Data = null;
                return response;
            }

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = farmshape;
            return response;
        }

        [HttpPost("UnitShape")]
        public async Task<ApiResponse> CreateUnitShape([FromBody] TbUnitShapes unitShape)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            try
            {
                await _farmService.CreateShapeUnit(unitShape);               
            }
            catch (Exception)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Error occured while saving.";
                response.Data = null;
                return response;
            }


            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = unitShape;
            return response;
        }


        [HttpGet("UnitShapeByFarmId/{id}")]
        public async Task<ApiResponse> FarmUnitShapes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            var units = await _farmService.GetFarmUnitsByFarmId(id);

            var unitShapes = new List<UnitShapesModel>();
            foreach (var unit in units)
            {
                var tbUnitShapes = await _farmService.GetUnitShapesByUnitId(unit.FarmUnitId);
                var result = _mapper.Map<List<UnitShapesModel>>(tbUnitShapes);
                if (result.Count() > 0)
                    unitShapes.Add(result.FirstOrDefault());

            }

            if (unitShapes == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = unitShapes;
            return response;
        }


        [HttpGet("YieldCalculator")]
        public ApiResponse YieldCalculator(float soil,float phosporous , float proceeds, long farmerid , float farmid ,float blockid,float cropid )
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }
            float calculation = soil * phosporous * proceeds * farmerid * farmid * blockid * cropid;
 
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = calculation;
            return response;
        }


        [HttpGet("GetFarmRelevantDropDowns")]
        public async Task<ApiResponse> GetFarmRelevantDropDowns([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }        
                var tubewell = await _farmService.TubeWell();
                var tubewelltype = await _farmService.TubeWellType();
                var watercourse = await _farmService.WaterCourse();
                var inputPurchase = await _farmService.InputPurchase();
                var outputExport = await _farmService.OutputExport();
                var tunnel = await _farmService.Tunnel();

            dynamic obj = new ExpandoObject();
            obj.tubewell = tubewell;
            obj.tubewelltype = tubewelltype;
            obj.watercourse = watercourse;
            obj.inputPurchase = inputPurchase;
            obj.outputExport = outputExport;
            obj.tunnel = tunnel;
          

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = obj;
            return response;
        }


    }
}
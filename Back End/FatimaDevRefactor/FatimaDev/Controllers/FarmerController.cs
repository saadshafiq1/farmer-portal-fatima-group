﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Models;
using Microsoft.AspNetCore.Authorization;
using API.Services;
using Infrastructure.OtherModels;
using API.Common;
using System.Net.Http;
using Core.Interfaces;
using System.Dynamic;
using AutoMapper;
using Infrastructure.ViewModels;
using System.IO;
using Newtonsoft.Json;
using Microsoft.Extensions.Options;
using FatimaDev;
using Microsoft.AspNetCore.Hosting;

namespace API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class FarmerController : ControllerBase
    {
        //private readonly FFContext _context;
        private IFarmerService _farmerService;
        private readonly IAddressService _addressService;
        private IUserLoginService _userLoginService;
        private readonly IUserService _userService;
        private IRoleService _roleService;
        private IFarmerReportService _farmerReportService;
        private readonly IOptions<IpSettings> _iconfig;
        private IHostingEnvironment _hostingEnvironment;

        private readonly IMapper _Mapper;
        ApiResponse response = new ApiResponse();       

        public FarmerController(IRoleService roleService, IMapper mapper, IFarmerService farmerService, IUserService userService,
            IUserLoginService userLoginService, IAddressService addressService, IFarmerReportService farmerReportService,
            IOptions<IpSettings> iconfig, IHostingEnvironment hostingEnvironment)
        {
            _addressService = addressService;
            _roleService = roleService;
            _farmerService = farmerService;
            _userLoginService = userLoginService;
            _farmerReportService = farmerReportService;
            _userService = userService;
            _Mapper = mapper;
            _iconfig = iconfig;
            _hostingEnvironment = hostingEnvironment;
        }

        // GET: api/TbFarmerInfoes
        [HttpGet]
        public async Task<ApiResponse> GetFarmer()
        {

            try
            {
                var result = await _farmerService.GetAllFarmer();//_context.TbFarmerInfo;
                var result2 = _Mapper.Map<List<FarmerInfoModel>>(result);
                response.Data = result2;
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }
            return response;
        }

        // GET: api/TbFarmerInfoes/5
        [HttpGet("{id}")]
        public async Task<ApiResponse> GetFarmer([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not validated.";
                return response;
            }

            try
            {
                var tbFarmerInfo = await _farmerService.GetFarmerById(id);

                if (tbFarmerInfo == null)
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Not Found.";
                    return response;
                }
                if (tbFarmerInfo.DistrictCodeNavigation != null)
                {
                    tbFarmerInfo.DistrictCodeNavigation.TbFarmerInfoDistrictCodeNavigation = null;
                    tbFarmerInfo.DistrictCodeNavigation.TbFarmerInfoPresentDistrictCodeNavigation = null;
                    tbFarmerInfo.DistrictCodeNavigation.ProvinceCodeNavigation = null;
                }

                if (tbFarmerInfo.ProvinceCodeNavigation != null)
                {
                    tbFarmerInfo.ProvinceCodeNavigation.TbFarmerInfoProvinceCodeNavigation = null;
                    tbFarmerInfo.ProvinceCodeNavigation.TbFarmerInfoPresentProvinceCodeNavigation = null;
                    tbFarmerInfo.ProvinceCodeNavigation.TbDistrictCode = null;
                    tbFarmerInfo.ProvinceCodeNavigation.TbRegionCode = null;
                }
                if (tbFarmerInfo.TehsilCodeNavigation != null)
                {
                    tbFarmerInfo.TehsilCodeNavigation.TbFarmerInfoTehsilCodeNavigation = null;
                    tbFarmerInfo.TehsilCodeNavigation.TbFarmerInfoPresentTehsilCodeNavigation = null;
                    tbFarmerInfo.TehsilCodeNavigation.DistrictCodeNavigation = null;
                }
                if (tbFarmerInfo.RegionCodeNavigation != null)
                {
                    tbFarmerInfo.RegionCodeNavigation.ProvinceCodeNavigation = null;
                    tbFarmerInfo.RegionCodeNavigation.TbFarmerInfoPresentRegionCodeNavigation = null;
                    tbFarmerInfo.RegionCodeNavigation.TbFarmerInfoRegionCodeNavigation = null;
                }
                if (tbFarmerInfo.PresentDistrictCodeNavigation != null)
                {
                    tbFarmerInfo.PresentDistrictCodeNavigation.TbFarmerInfoDistrictCodeNavigation = null;
                    tbFarmerInfo.PresentDistrictCodeNavigation.TbFarmerInfoPresentDistrictCodeNavigation = null;
                    tbFarmerInfo.PresentDistrictCodeNavigation.ProvinceCodeNavigation = null;
                    tbFarmerInfo.PresentDistrictCodeNavigation.TbTehsilCode = null;
                }
                if (tbFarmerInfo.PresentRegionCodeNavigation != null)
                {
                    tbFarmerInfo.PresentRegionCodeNavigation.TbFarmerInfoPresentRegionCodeNavigation = null;
                    tbFarmerInfo.PresentRegionCodeNavigation.TbFarmerInfoRegionCodeNavigation = null;
                }
                if (tbFarmerInfo.PresentTehsilCodeNavigation != null)
                {
                    tbFarmerInfo.PresentTehsilCodeNavigation.DistrictCodeNavigation = null;
                    tbFarmerInfo.PresentTehsilCodeNavigation.TbFarmerInfoPresentTehsilCodeNavigation = null;
                }
                if (tbFarmerInfo.PresentProvinceCodeNavigation != null)
                {
                    tbFarmerInfo.PresentProvinceCodeNavigation.TbFarmerInfoProvinceCodeNavigation = null;
                    tbFarmerInfo.PresentProvinceCodeNavigation.TbFarmerInfoPresentProvinceCodeNavigation = null;
                    tbFarmerInfo.PresentProvinceCodeNavigation.TbDistrictCode = null;
                    tbFarmerInfo.PresentProvinceCodeNavigation.TbRegionCode = null;
                }
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = tbFarmerInfo;

                return response;

            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();

                return response;
            }


        }

        [HttpGet("GetAllFarmers")]
        public async Task<ApiResponse> GetAllFarmers(int pageNumber, int pageSize, string filter = "", string value = "")
        {
            var tbFarmerInfo = await _farmerService.GetAllFarmers(pageNumber, pageSize, filter.ToLower(), value);

            if (tbFarmerInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found.";

                return response;
            }
            List<TbFarmerInfo> farmerinfo = new List<TbFarmerInfo>();
            foreach (var j in tbFarmerInfo)
            {
                if (j.DistrictCodeNavigation != null)
                {
                    if (j.DistrictCodeNavigation.TbFarmerInfoDistrictCodeNavigation != null)
                    { j.DistrictCodeNavigation.TbFarmerInfoDistrictCodeNavigation = null; }
                }
                if (j.TehsilCodeNavigation != null)
                {
                    if (j.TehsilCodeNavigation != null)
                    {
                        j.TehsilCodeNavigation.TbFarmerInfoPresentTehsilCodeNavigation = null;
                        j.TehsilCodeNavigation.TbFarmerInfoTehsilCodeNavigation = null;
                    }
                }
                if (j.FarmerStatus != null)
                {
                    j.FarmerStatus.TbFarmerInfo = null;
                }
                decimal area = 0;
                foreach (var k in j.TbFarmInfo)
                {
                    if (k.TotalAreaAcres != null)
                    {
                        area = area + (decimal)k.TotalAreaAcres;
                    }
                }
                //Here Union Council is being used to calculate the total area. 
                j.UnionCouncil = area.ToString();
                farmerinfo.Add(j);
            }

            var result = _Mapper.Map<List<FarmerInfoModel>>(farmerinfo); // CHange the code here 
                                                                         // var result = tbFarmerInfo;
            dynamic obj = new ExpandoObject();
            obj.data = result;

            if (String.IsNullOrEmpty(filter))
            {
                var count = await _farmerService.GetAllFarmer();
                obj.totalCount = count.Count();
            }
            else
            {
                var count = await _farmerService.GetAllFarmersCountByFilter(filter.ToLower(), value);
                obj.totalCount = count.Count();
            }
            response.Data = obj;
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            return response;
        }


        //// PUT: api/TbFarmerInfoes/5
        [HttpPut("{id}")]
        public async Task<ApiResponse> PutFarmer([FromRoute] long id, [FromBody] TbFarmerInfo tbFarmerInfo)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State Validation Failed.";
                return response;
            }

            if (id != tbFarmerInfo.FarmerId)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "id is not equal to farmer object id";
                return response;
            }
            //_context.Entry<TbFarmerInfo>(tbFarmerInfo).State = EntityState.Modified;

            // var tbfarmerold = _context.TbFarmerInfo.Where(i => i.FarmerId == id).FirstOrDefault();
            //_context.Set<TbFarmerInfo>.SetTryUpdateModelAsync(tbFarmerInfo);
            //TbUserLogin tempuser = new TbUserLogin();

            try
            {
                var Guiid = (Guid)tbFarmerInfo.UserGuid;
                var tempUser = await _userLoginService.GetUserLoginById(Guiid);//_context.TbUserLogin.FirstOrDefault(i => i.UserId == tbFarmerInfo.UserGuid);
                var farmeid = tbFarmerInfo.FarmerId;
                var result = await _farmerService.GetFarmerById(farmeid);

                //TbFarmerInfo newobj = new TbFarmerInfo();
                //newobj = tbFarmerInfo;
                //if (tbFarmerInfo.Cnic != null)
                //    result.Cnic = tbFarmerInfo.Cnic;
                //if (tbFarmerInfo.FarmerName != null)
                //    result.FarmerName = tbFarmerInfo.FarmerName;
                //if (tbFarmerInfo.ReferalCode != null)
                //    result.ReferalCode = tbFarmerInfo.ReferalCode;


                if (tbFarmerInfo.FarmerName != null)
                { result.FarmerName = tbFarmerInfo.FarmerName; }
                if (tbFarmerInfo.FatherHusbandName != null)
                { result.FatherHusbandName = tbFarmerInfo.FatherHusbandName; }
                if (tbFarmerInfo.Cnic != null)
                { result.Cnic = tbFarmerInfo.Cnic; }
                if (tbFarmerInfo.DistrictCode != null)
                { result.DistrictCode = tbFarmerInfo.DistrictCode; }
                if (tbFarmerInfo.TehsilCode != null)
                { result.TehsilCode = tbFarmerInfo.TehsilCode; }
                if (tbFarmerInfo.Gender != null)
                { result.Gender = tbFarmerInfo.Gender; }
                if (tbFarmerInfo.CellPhone != null)
                { result.CellPhone = tbFarmerInfo.CellPhone; }
                if (tbFarmerInfo.PresentAddress != null)
                { result.PresentAddress = tbFarmerInfo.PresentAddress; }
                if (tbFarmerInfo.PermanentAddress != null)
                { result.PermanentAddress = tbFarmerInfo.PermanentAddress; }
                if (tbFarmerInfo.EducationCode != null)
                { result.EducationCode = tbFarmerInfo.EducationCode; }
                if (tbFarmerInfo.MaleDependant != null)
                { result.MaleDependant = tbFarmerInfo.MaleDependant; }
                if (tbFarmerInfo.FemaleDependant != null)
                { result.FemaleDependant = tbFarmerInfo.FemaleDependant; }
                if (tbFarmerInfo.AlternateName != null)
                { result.AlternateName = tbFarmerInfo.AlternateName; }
                if (tbFarmerInfo.AlternateCellPhoneNo != null)
                { result.AlternateCellPhoneNo = tbFarmerInfo.AlternateCellPhoneNo; }
                if (tbFarmerInfo.AlternateRelationshipwithFarmer != null)
                { result.AlternateRelationshipwithFarmer = tbFarmerInfo.AlternateRelationshipwithFarmer; }
                if (tbFarmerInfo.ReferalCode != null)
                { result.ReferalCode = tbFarmerInfo.ReferalCode; }
                if (tbFarmerInfo.FarmerImage != null)
                { result.FarmerImage = tbFarmerInfo.FarmerImage; }
                if (tbFarmerInfo.UnionCouncil != null)
                { result.UnionCouncil = tbFarmerInfo.UnionCouncil; }
                if (tbFarmerInfo.MozaName != null)
                { result.MozaName = tbFarmerInfo.MozaName; }
                if (tbFarmerInfo.SmallAnimals != null)
                { result.SmallAnimals = tbFarmerInfo.SmallAnimals; }
                if (tbFarmerInfo.BigAnimals != null)
                { result.BigAnimals = tbFarmerInfo.BigAnimals; }
                if (tbFarmerInfo.Tractor != null)
                { result.Tractor = tbFarmerInfo.Tractor; }
                if (tbFarmerInfo.OtherImplement != null)
                { result.OtherImplement = tbFarmerInfo.OtherImplement; }
                if (tbFarmerInfo.ModifiedBy != null)
                { result.ModifiedBy = tbFarmerInfo.ModifiedBy; }
                if (tbFarmerInfo.ModifiedDateTime != null)
                { result.ModifiedDateTime = tbFarmerInfo.ModifiedDateTime; }
                if (tbFarmerInfo.InsertionDate != null)
                { result.InsertionDate = tbFarmerInfo.InsertionDate; }
                if (tbFarmerInfo.ActiveStatus != null)
                { result.ActiveStatus = tbFarmerInfo.ActiveStatus; }
                if (tbFarmerInfo.DateOfBirth != null)
                { result.DateOfBirth = tbFarmerInfo.DateOfBirth; }
                if (tbFarmerInfo.ProvinceCode != null)
                { result.ProvinceCode = tbFarmerInfo.ProvinceCode; }
                if (tbFarmerInfo.Hobbies != null)
                { result.Hobbies = tbFarmerInfo.Hobbies; }
                if (tbFarmerInfo.PresentProvinceCode != null)
                { result.PresentProvinceCode = tbFarmerInfo.PresentProvinceCode; }
                if (tbFarmerInfo.PresentDistrictCode != null)
                { result.PresentDistrictCode = tbFarmerInfo.PresentDistrictCode; }
                if (tbFarmerInfo.PresentRegionCode != null)
                { result.PresentRegionCode = tbFarmerInfo.PresentRegionCode; }
                if (tbFarmerInfo.PresentTehsilCode != null)
                { result.PresentTehsilCode = tbFarmerInfo.PresentTehsilCode; }
                if (tbFarmerInfo.PresentUnionConcil != null)
                { result.PresentUnionConcil = tbFarmerInfo.PresentUnionConcil; }
                if (tbFarmerInfo.LandLine != null)
                { result.LandLine = tbFarmerInfo.LandLine; }
                if (tbFarmerInfo.AdjacentFarmerFriendName != null)
                { result.AdjacentFarmerFriendName = tbFarmerInfo.AdjacentFarmerFriendName; }
                if (tbFarmerInfo.AdjacentFarmerFriendCellPhone != null)
                { result.AdjacentFarmerFriendCellPhone = tbFarmerInfo.AdjacentFarmerFriendCellPhone; }
                if (tbFarmerInfo.RegionCode != null)
                { result.RegionCode = tbFarmerInfo.RegionCode; }
                if (tbFarmerInfo.FarmerStatus != null)
                { result.FarmerStatus = tbFarmerInfo.FarmerStatus; }
                if (tbFarmerInfo.FarmerStatusId != null)
                { result.FarmerStatusId = tbFarmerInfo.FarmerStatusId; }


                if (result != null)
                {
                    if (tempUser.CellPhone.ToString() != tbFarmerInfo.CellPhone.ToString())
                        if (tempUser != null)
                        {
                            //check if the cellphone number already exists
                            if (/*_context.TbUserLogin.Where(c => c.CellPhone.Contains(tbFarmerInfo.CellPhone))*/(await _userLoginService.GetUserByCellPhone(tbFarmerInfo.CellPhone)).ToList().Count >= 1)
                            {
                                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                                response.ResponseMessage = "Cellphone number already registered.Please provide unique phone number.";
                                response.Data = null;

                                return response;
                            }

                            tempUser.CellPhone = tbFarmerInfo.CellPhone;
                            await _userLoginService.UpdateUserLogin(tempUser);//_context.Set<TbUserLogin>().Update(tempUser);
                            var updatedEntity = tempUser;
                            //updatedEntity.State = EntityState.Modified;
                            // _context.SaveChanges();

                        }

                    await _farmerService.UpdateFarmer(result);
                    //await //_context.SaveChangesAsync();

                    response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                    response.ResponseMessage = "Action Completed.";
                    response.Data = _Mapper.Map<FarmerInfoModel>(tbFarmerInfo);
                }
                else
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "ID doesn't exist.";
                }
            }
            catch
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                if (!TbFarmerInfoExists(id))
                {

                    response.ResponseMessage = "ID doesn't exist.";
                    return response;
                }
                else
                {
                    response.ResponseMessage = "DB ERROR";
                }
            }

            return response;
        }

        //// POST: api/TbFarmerInfoes
        [AllowAnonymous]
        [HttpPost]
        public async Task<ApiResponse> PostFarmer([FromBody] TbFarmerInfo tbFarmerInfo)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State Validation Failed.";
                return response;
            }

            try
            {
                //check if same cellphone already exists
                var temp = await _farmerService.GetFarmerByCellPhone(tbFarmerInfo.CellPhone);//_context.TbFarmerInfo.Where(x => x.CellPhone == tbFarmerInfo.CellPhone).FirstOrDefault();

                if (temp != null)
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "phone number already registered.";
                    return response;
                }
                var guid = Guid.NewGuid();
                tbFarmerInfo.UserGuid = guid;

                //string path=Common.Common.UploadImage(guid.ToString(),tbFarmerInfo.FarmerImage);
                //tbFarmerInfo.FarmerImage = path;

                await _farmerService.CreateFarmer(tbFarmerInfo);
                //_context.TbFarmerInfo.Add(tbFarmerInfo);
                // await _context.SaveChangesAsync();

                Random generator = new Random();
                string r = generator.Next(0, 99999).ToString("D5");

                TbUserLogin userLogin = new TbUserLogin
                {
                    CellPhone = tbFarmerInfo.CellPhone,
                    Passkey = r,
                    InsertionDate = DateTime.Now,
                    ActiveStatus = "A",
                    UserRoleCode = (await _roleService.GetRoleByRoleName("Farmer")).FirstOrDefault().UserRoleCode,
                    UserId = guid
                };


                await _userLoginService.CreateUserLogin(userLogin);

                string httpResponse = _userService.SendOTP(userLogin.CellPhone, userLogin.Passkey);

                if (httpResponse != "0")
                {
                    response.ResponseMessage = httpResponse;
                    response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                }
                else
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Failure sending OTP";
                    response.Data = "UserName: " + userLogin.CellPhone + ", userLogin:" + userLogin.Passkey;
                }
                response.Data = tbFarmerInfo;

            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }

            return response;

        }

        // DELETE: api/TbFarmerInfoes/5
        [HttpDelete("{id}")]
        public async Task<ApiResponse> DeleteFarmer([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State Validation Failed.";
            }

            var tbFarmerInfo = await _farmerService.GetFarmerById(id);//await _context.TbFarmerInfo.FindAsync(id);
            if (tbFarmerInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State Validation Failed.";
            }

            try
            {
                await _farmerService.DeleteFarmer(tbFarmerInfo);
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = id;
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }
            return response;
        }
        private bool TbFarmerInfoExists(long id)
        {
            var farmer = _farmerService.GetFarmerById(id);
            return farmer == null ? false : true;//_context.TbFarmerInfo.Any(e => e.FarmerId == id);
        }

        [HttpGet("GetFarmerByFilter")]
        public async Task<ApiResponse> GetFarmerByFilter(string cnic = "", string phone = "")
        {
            if (!string.IsNullOrEmpty(cnic) || !string.IsNullOrEmpty(phone))
            {
                var tbFarmerInfo = await _farmerService.GetFarmerByFilter(cnic, phone);

                if (tbFarmerInfo == null)
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Not Found.";

                    return response;
                }

                var result = _Mapper.Map<List<FarmerInfoModel>>(tbFarmerInfo);

                response.Data = result;
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                return response;
            }
            else
            {
                response.Data = "Invalid Request";
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Successful";
                return response;
            }
        }

        [HttpGet("GetFarmerStatuses")]
        public async Task<ApiResponse> GetFarmerStatuses()
        {
            try
            {
                var tbFarmerInfo = await _farmerService.GetFarmerStatues();

                if (tbFarmerInfo == null)
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Not Found.";

                    return response;
                }



                response.Data = tbFarmerInfo;
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                return response;
            }
            catch
            {
                response.Data = "Invalid Request";
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Successful";
                return response;
            }
        }

        #region FarmerReports

        [HttpPost("AddFarmerReport")]
        public async Task<ApiResponse> PostFarmerReport()
        {
            StreamReader reader = new StreamReader(Request.Body);
            string requestFromPost = reader.ReadToEnd();

            var obj = JsonConvert.DeserializeObject<ReportUploadAPI>(requestFromPost);
            TbFarmerReports tbFarmerReports = new TbFarmerReports();
            tbFarmerReports.FarmerId =  obj.farmerId;
            tbFarmerReports.ReportName = obj.reportName;           

            if (obj.pdf != null)
            {               
                string pdfName = Guid.NewGuid().ToString()+".pdf";
                var value = _iconfig.Value.LocalIp;
                string path = Path.Combine(_hostingEnvironment.ContentRootPath + "\\Reports\\", pdfName);

                byte[] pdfBytes = Convert.FromBase64String(obj.pdf.Split(',')[0]);

                System.IO.File.WriteAllBytes(path, pdfBytes);
                tbFarmerReports.Url = Path.Combine(value + "/Reports/", pdfName);
            }
            if (!ModelState.IsValid)
            {
                response.Data = null;
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Invalid Model.";
                return response;
            }

            tbFarmerReports.ModifiedDateTime = DateTime.Now;
            tbFarmerReports.InsertionDate = DateTime.Now;
            tbFarmerReports.ReportDate = Convert.ToDateTime(obj.reportDate);
            await _farmerReportService.CreateFarmerReport(tbFarmerReports);

            response.Data = tbFarmerReports;
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Data Saved Successfully.";
            return response;
        }

        [HttpDelete("DeleteFarmerReport/{reportID}")]
        public async Task<ApiResponse> DeleteFarmerReport([FromRoute] int reportID)
        {
            var tbFarmerReports = await _farmerReportService.GetFarmerReportByID(reportID);
            try
            {
                tbFarmerReports.ActiveStatus = "I";
                tbFarmerReports.ModifiedDateTime = DateTime.Now;
                await _farmerReportService.UpdateFarmerReport(tbFarmerReports);

                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = reportID;
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }
            return response;
        }

        [HttpGet("GetFarmerReportByFarmerID/{farmerID}")]
        public async Task<ApiResponse> GetFarmerReportByFarmerID([FromRoute] int farmerID)
        {
            try
            {
                var result = await _farmerReportService.GetFarmerReportByFarmerID(farmerID);
                var tbFarmerReports = _Mapper.Map<List<FarmerReportsModel>>(result);

                if (tbFarmerReports == null)
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Not Found.";

                    return response;
                }

                response.Data = tbFarmerReports;
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                return response;
            }
            catch
            {
                response.Data = "Invalid Request";
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Successful";
                return response;
            }
        }

        [HttpGet("GetFarmerReportByReportID/{reportID}")]
        public async Task<ApiResponse> GetFarmerReportByReportID([FromRoute] int reportID)
        {
            try
            {
                var result = await _farmerReportService.GetFarmerReportByID(reportID);
                var tbFarmerReports = _Mapper.Map<List<FarmerReportsModel>>(result);

                if (tbFarmerReports == null)
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Not Found.";

                    return response;
                }

                response.Data = tbFarmerReports;
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                return response;
            }
            catch
            {
                response.Data = "Invalid Request";
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Successful";
                return response;
            }
        }

        [HttpGet("GetAllFarmerReports")]
        public async Task<ApiResponse> GetAllFarmerReports(int pageNumber, int pageSize)
        {
            try
            {
                var result = await _farmerReportService.GetAllFarmerReports(pageNumber,pageSize);              
                var tbFarmerReports = _Mapper.Map<List<FarmerReportsModel>>(result);

                if (tbFarmerReports == null)
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Not Found.";

                    return response;
                }

                response.Data = tbFarmerReports;
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                return response;
            }
            catch
            {
                response.Data = "Invalid Request";
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Successful";
                return response;
            }
        }

        #endregion
    }
    public class ReportUploadAPI
    {
        public int farmerId { get; set; }
        public string pdf { get; set; }
        public string reportName { get; set; }
        public string reportDate { get; set; }

    }
}
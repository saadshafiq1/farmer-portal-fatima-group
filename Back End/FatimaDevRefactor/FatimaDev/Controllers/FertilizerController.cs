﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Models;
using Infrastructure.OtherModels;
using Microsoft.AspNetCore.Authorization;
using Infrastructure.ViewModels;
using Core.Interfaces;
using AutoMapper;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Farmer")]

    public class FertilizerController : ControllerBase
    {
        // private readonly FFContext _context;
        private readonly IFertilizerService _FertilizerService;
        private readonly IMapper _Mapper;

        ApiResponse response = new ApiResponse();
        public FertilizerController(IFertilizerService FertilizerService, IMapper Mapper)
        {
            _FertilizerService = FertilizerService;
            _Mapper = Mapper;
        }

        // GET: api/tbFertilizerTypes
        [HttpGet]
        public async Task<ApiResponse> GetFertilizers()
        {
            var tbFertilizerTypes = await _FertilizerService.GetAllFertilizers();
            var result = _Mapper.Map<List<FertilizerTypesModel>>(tbFertilizerTypes);
            response.Data = result;
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            return response;
        }

        // GET: api/tbFertilizerTypes/5
        [HttpGet("{id}")]
        public async Task<ApiResponse> GetFertilizers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            var tbFertilizerType = await _FertilizerService.GetFertilizerById(id);

            if (tbFertilizerType == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }

            var result = _Mapper.Map<List<FertilizerTypesModel>>(tbFertilizerType);
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = result;
            return response;
        }
       
        // PUT: api/tbFertilizerTypes/5
        [HttpPut("{id}")]
        public async Task<ApiResponse> PutFertilizers([FromRoute] int id, [FromBody] TbFertilizerTypes tbFertilizerType)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            if (id != tbFertilizerType.FertilizerId)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }

            try
            {
                await _FertilizerService.UpdateFertilizer(tbFertilizerType);
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = tbFertilizerType;

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FertilizerTypeExists(id))
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Could not save.";
                    return response;
                }
                else
                {
                    throw;
                }
            }

            return response;
        }

        // POST: api/tbFertilizerTypes
        [HttpPost]
        public async Task<ApiResponse> PostFertilizers([FromBody] TbFertilizerTypes tbFertilizerType)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }
          
            try
            {
                await _FertilizerService.CreateFertilizer(tbFertilizerType);
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = tbFertilizerType;
            }
            catch (Exception)
            {
                if (FertilizerTypeExists(tbFertilizerType.FertilizerId))
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Record Already Exists.";
                    response.Data = tbFertilizerType;
                }
                else
                {
                    throw;
                }
            }
            return response;
        }

        // DELETE: api/tbFertilizerTypes/5
        [HttpDelete("{id}")]
        public async Task<ApiResponse> DeleteFertilizers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }
            
            var tbFertilizerType = await _FertilizerService.GetFertilizerById(id);
            if (tbFertilizerType == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }
           
            await _FertilizerService.DeleteFertilizer(tbFertilizerType);
           
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbFertilizerType;
            return response;
        }

        private bool FertilizerTypeExists(int id)
        {
            return _FertilizerService.GetFertilizerById(id) == null ? false : true;
        }             
              
    }

}



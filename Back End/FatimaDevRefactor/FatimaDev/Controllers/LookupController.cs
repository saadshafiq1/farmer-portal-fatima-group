﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Interfaces;
using Infrastructure.Models;
using Infrastructure.OtherModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using AutoMapper;
using Infrastructure.ViewModels;

namespace API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]

    public class LookupController : ControllerBase
    {
        IAddressService _addressService;
        private IFarmerService _farmerService;
        IFarmService _farmService;
        private readonly IMapper _Mapper;

        ApiResponse response = new ApiResponse();

        public LookupController(IAddressService addressService, IFarmService farmService, IMapper mapper,IFarmerService farmerService)
        {
            _addressService = addressService;
            _farmService = farmService;
            _Mapper = mapper;
            _farmerService = farmerService;
        }

        // GET: api/TbFarmerInfoes
        [HttpGet("Province")]
        public async Task<ApiResponse> GetProvinceInfo()
        {
            response.Data = await _addressService.GetAllProvinces();
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Data Retrieved";
            return response;
        }

        //GET: api/TbFarmerInfoes/5
        [HttpGet("Province/{id}")]
        public async Task<ApiResponse> GetProvinceInfo([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            var tbProvinceInfo = await _addressService.GetProvinceById(id);

            if (tbProvinceInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbProvinceInfo;
            return response;
        }

        ////GET: api/TbFarmerInfoes/5
        [HttpGet("CitiesOfProvince/{id}")]
        public async Task<ApiResponse> GetCitiesOfProvince([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            var tbDistrictInfo = await _addressService.GetCitiesOfProvince(id);

            if (tbDistrictInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbDistrictInfo;
            return response;
        }


        ////GET: api/TbFarmerInfoes/5
        [HttpGet("DistrictsOfProvince/{id}")]
        public async Task<ApiResponse> GetDistrictsOfProvince([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            var tbDistrictInfo = await _addressService.GetDistrictsOfProvince(id);

            if (tbDistrictInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbDistrictInfo;
            return response;
        }


        [HttpGet("GetDistrictsByRegion/{id}")]
        public async Task<ApiResponse> GetDistrictsByRegion([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            var tbDistrictInfo = await _addressService.GetDistrictsByRegion(id);

            if (tbDistrictInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbDistrictInfo;
            return response;
        }


        [HttpGet("TehsilsOfDistrict/{id}")]
        public async Task<ApiResponse> GetTehsilsOfDistrict([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            var tbTehsilInfo = await _addressService.GetTehsilsOfDistrict(id);

            if (tbTehsilInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbTehsilInfo;
            return response;
        }

        [HttpGet("Region/{id}")]
        public async Task<ApiResponse> GetRegionInfo([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            var tbRegionInfo = await _addressService.GetRegionById(id);

            if (tbRegionInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }


            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbRegionInfo;
            return response;
        }

        [HttpGet("GetAllCities")]
        public async Task<ApiResponse> GetRegions()
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            var tbRegionInfo = await _addressService.GetALLRegion();
          
            if (tbRegionInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }
            var result = _Mapper.Map<List<RegionCodeModel>>(tbRegionInfo);

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = result;
            return response;
        }



        [HttpGet("Tehsil/{id}")]
        public async Task<ApiResponse> GetTehsilInfo([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            var tbTehsilInfo = await _addressService.GetTehsilById(id);

            if (tbTehsilInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }


            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbTehsilInfo;
            return response;
        }

        [HttpGet("District/{id}")]
        public async Task<ApiResponse> GetDistrictInfo([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            
            var tbDistInfo = await _addressService.GetDistrictById(id);

            if (tbDistInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }


            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbDistInfo;
            return response;
        }

        [HttpGet("FarmShape/{id}")]
        public async Task<ApiResponse> GetFarmShape([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }
            
            var tbDistInfo = await _farmService.GetFarmShapesByFarmIdAsync(id);

            if (tbDistInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }


            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbDistInfo;
            return response;
        }

        [HttpPost("FarmShape")]
        public async Task<ApiResponse> CreateFarmShape([FromBody] TbFarmShapes farmshape)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            try
            {
                await _farmService.CreateShapeFarm(farmshape);
            }
            catch (Exception)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Error occured while saving.";
                response.Data = null;
                return response;
            }

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = farmshape;
            return response;
        }

        [HttpPost("UnitShape")]
        public async Task<ApiResponse> CreateUnitShape([FromBody] TbUnitShapes unitShape)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            try
            {
                await _farmService.CreateShapeUnit(unitShape);               
            }
            catch (Exception)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Error occured while saving.";
                response.Data = null;
                return response;
            }


            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = unitShape;
            return response;
        }


        [HttpGet("UnitShape/{id}")]
        public async Task<ApiResponse> FarmUnitShapes([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }
            
            var tbUnitShapes = await _farmService.GetUnitShapesByFarmId(id);
           

            if (tbUnitShapes == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }

            foreach (var unit in tbUnitShapes)
            {
                unit.Unit = null;
            }

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbUnitShapes;
            return response;
        }


        [HttpGet("GetAllCitiesOfFarmer")]
        public async Task<ApiResponse> GetAllCitiesOfFarmer(long farmerid)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }
            List<DistrictCodeModel> districtData = new List<DistrictCodeModel>();
            var tbDistrictInfo = await _addressService.GetAllDistricts();
            var tbFarmerFarmsInfo = await _farmService.GetFarmsOfFarmer((int)farmerid);
            if (tbFarmerFarmsInfo != null)
            {
                foreach (var item in tbFarmerFarmsInfo)
                {
                    var districts = tbDistrictInfo.Where(x => x.DistrictCode == item.DistrictCode).FirstOrDefault();
                    districtData.Add(_Mapper.Map<DistrictCodeModel>(districts));
                }
            }

            var tbFarmerInfo = await _farmerService.GetFarmerById((int)farmerid);
            if (tbFarmerInfo != null && tbFarmerInfo.DistrictCode != null)
            {
                var districts = tbDistrictInfo.Where(x => x.DistrictCode == tbFarmerInfo.DistrictCode).FirstOrDefault();
                districtData.Add(_Mapper.Map<DistrictCodeModel>(districts));
            }

            if (tbDistrictInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }

            //var result = _Mapper.Map<List<RegionCodeModel>>(tbRegionInfo);

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data =  districtData.GroupBy(i => i.DistrictCode) .Select(g => g.First()).ToList();
            return response;
        }


        [HttpGet("GetAllDistricts")]
        public async Task<ApiResponse> GetAllDistricts()
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }
            var tbDistrictInfo = await _addressService.GetAllDistricts();

            var result = _Mapper.Map<List<DistrictCodeModel>>(tbDistrictInfo);
            
            if (tbDistrictInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = result;
            return response;
        }
        [HttpGet("GetAllSalesPoint")]
        public async Task<ApiResponse> GetAllSalesPoint()
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }
            var tbsalePointInfo = await _addressService.GetAllSalesPoint();

            var result = _Mapper.Map<List<SalePointModel>>(tbsalePointInfo);

            if (tbsalePointInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = result;
            return response;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Models;
using Infrastructure.OtherModels;
using Infrastructure.ViewModels;
using Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using System.Dynamic;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using AutoMapper.Configuration;
using Microsoft.Extensions.Options;
using FatimaDev;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MediaController : ControllerBase
    {
       // private readonly FFContext _context;
        public ApiResponse response=new ApiResponse();
        IMediaService _MediaService;
        IAddressService _AddressService;
        private readonly IMapper _Mapper;
        private IHostingEnvironment _hostingEnvironment;
        private readonly IOptions<IpSettings> _iconfig;
        public MediaController(IMediaService mediaService, IAddressService addressService, IMapper _mapper, IHostingEnvironment hostingEnvironment, IOptions<IpSettings> iconfig)
        {
            _AddressService = addressService;
            _MediaService = mediaService;
            _Mapper = _mapper;
            _hostingEnvironment = hostingEnvironment;
            _iconfig = iconfig;
        }

        // GET: api/TbMedia
        [HttpGet]
        public async Task<ApiResponse> GetTbMedia()
        {           
            response.Data = await _MediaService.GetAllMedia();
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Data Retrieved";
            return response;
        }
        [HttpGet("AllMedia")]
        public async Task<ApiResponse> GetTbMedia(int pageNumber, int pageSize, string filter = "", string value = "")
        {
            var tbMedias = await _MediaService.GetAllMedia(pageNumber, pageSize, filter.ToLower(), value);

            var result = _Mapper.Map<List<MediaModel>>(tbMedias);    

            dynamic obj = new ExpandoObject();
            obj.data = result;

            if (String.IsNullOrEmpty(filter))
            {
                var count = await _MediaService.GetMediaByMediaType("News");
                obj.totalCount = count.Count();
            }
            else
            {
                var count = await _MediaService.GetAllMedia(filter.ToLower(), value);
                obj.totalCount = count.Count();
            }
            response.Data = obj;
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Data Retrieved";
            return response;
        }

        //// GET: api/TbMedia/5
        [HttpGet("{id}")]
        public async Task<ApiResponse> GetTbMedia([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.Data = null;
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Invalid Model.";
                return response;
            }

            var tbMedia = await _MediaService.GetMediaById(id); 
          
            if (tbMedia == null)
            {
                response.Data = null;
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found.";

            }

            response.Data = tbMedia;
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Data Retrieved.";
            return response;
        }

        ////pagination to be applied
        [HttpGet("MediaOfFarmer/{id}")]
        public async Task<ApiResponse> GetMediaOfFarmer([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.Data = null;
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Invalid Model.";
                return response;
            }

            var tbMedia = await _MediaService.GetMediaOfFarmer(id);//await _context.TbFarmerMedia.Where(i => i.FarmerId == id).ToListAsync();

            if (tbMedia == null)
            {
                response.Data = null;
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found.";

            }

            response.Data = tbMedia;
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Data Retrieved.";
            return response;
        }

        ////pagination to be applied
        [HttpGet("MediaByDistrict/{mediaTypeId}/{districtId}")]
        public async Task<ApiResponse> GetMediaByDistrict([FromRoute] long mediaTypeId, [FromRoute] long districtId)
        {
            if (!ModelState.IsValid)
            {
                response.Data = null;
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Invalid Model.";
                return response;
            }

            var tbMedia = await _MediaService.GetMediaByDistrictAndMediaType(mediaTypeId, districtId);           
            if (tbMedia == null)
            {
                response.Data = null;
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Does not exist.";
                return response;
            }

            response.Data = tbMedia;
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Data Retrieved.";
            return response;
        }
        [HttpGet("MediaByMediaType/{mediaTypeId}")]
        public async Task<ApiResponse> GetMediaByMediaType([FromRoute] long mediaTypeId)
        {
            if (!ModelState.IsValid)
            {
                response.Data = null;
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Invalid Model.";
                return response;
            }

            var tbMedia = await _MediaService.GetMediaByMediaType(mediaTypeId);
            if (tbMedia == null)
            {
                response.Data = null;
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Does not exist.";
                return response;
            }

            response.Data = tbMedia;
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Data Retrieved.";
            return response;
        }

        //// PUT: api/TbMedia/5
        [HttpPut("{id}")]
        public async Task<ApiResponse> PutTbMedia([FromRoute] long id)
        {
            StreamReader reader = new StreamReader(Request.Body);
            string requestFromPost = reader.ReadToEnd();

            if (!ModelState.IsValid)
            {
                response.Data = null;
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Invalid Model.";
                return response;
            }
            var obj = JsonConvert.DeserializeObject<MediaUploadAPI>(requestFromPost);
            TbMedia tbMedia = JsonConvert.DeserializeObject<TbMedia>(obj.tbMedia);

            if (id != tbMedia.MediaId)
            {
                response.Data = null;
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Id not same as entity.";
                return response;
            }
            if (obj.image != null && obj.image != tbMedia.Url)
            {
                string imgName = DateTime.Now.Ticks + "." + obj.image.Split(";")[0].Split("/")[1];
                var value = _iconfig.Value.LocalIp;
                string path = Path.Combine(_hostingEnvironment.ContentRootPath + "\\Source-Images\\", imgName);

                byte[] imageBytes = Convert.FromBase64String(obj.image.Split(',')[1]);              

                System.IO.File.WriteAllBytes(path, imageBytes);
                tbMedia.Url = Path.Combine(value + "\\Source-Images\\", imgName);
            }
            // _context.Entry(tbMedia).State = EntityState.Modified;

            try
            {
                tbMedia.ModifiedDateTime = DateTime.Now;
                await _MediaService.UpdateMedia(tbMedia);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TbMediaExists(id))
                {
                    response.Data = null;
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Not Found.";
                    return response;
                }
                else
                {
                    throw;
                }
            }
            response.Data = tbMedia;
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Data Saved.";
            return response;
        }


        //// POST: api/TbMedia
        [HttpPost, DisableRequestSizeLimit]
        public async Task<ApiResponse> PostTbMedia()
        {
            StreamReader reader = new StreamReader(Request.Body);
            string requestFromPost = reader.ReadToEnd();

            var obj = JsonConvert.DeserializeObject<MediaUploadAPI>(requestFromPost);
            TbMedia myobj = JsonConvert.DeserializeObject<TbMedia>(obj.tbMedia);
            //Convert.FromBase64String(obj.image)
            if (obj.image != null)
            {
                
                string imgName = DateTime.Now.Ticks + "." +obj.image.Split(";")[0].Split("/")[1];
                var value = _iconfig.Value.LocalIp;
                string path = Path.Combine(_hostingEnvironment.ContentRootPath + "\\Source-Images\\", imgName);
                
                byte[] imageBytes = Convert.FromBase64String(obj.image.Split(',')[1]);
      
                System.IO.File.WriteAllBytes(path, imageBytes);
                myobj.Url = Path.Combine(value + "\\Source-Images\\", imgName);
            }
            if (!ModelState.IsValid)
            {
                response.Data = null;
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Invalid Model.";
                return response;
            }

            myobj.ModifiedDateTime = DateTime.Now;
            myobj.InsertionDate = DateTime.Now;
            await _MediaService.CreateMedia(myobj);

            //_context.TbMedia.Add(tbMedia);
            //await _context.SaveChangesAsync();

            response.Data = myobj;
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Data Saved Successfully.";
            return response;
        }

        //// DELETE: api/TbMedia/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTbMedia([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tbMedia = await _MediaService.GetMediaById(id);//await _context.TbMedia.FindAsync(id);
            if (tbMedia == null)
            {
                return NotFound();
            }

            await _MediaService.DeleteMedia(tbMedia);
            //_context.TbMedia.Remove(tbMedia);
            //await _context.SaveChangesAsync();

            return Ok(tbMedia);
        }

        private bool TbMediaExists(long id)
        {
            return  _MediaService.GetMediaById(id).ToAsyncEnumerable().ToList()!=null?true:false;
            //return _context.TbMedia.Any(e => e.MediaId == id);
        }



    }

    public class MediaUploadAPI
    {
        public string image { get; set; }
        public string tbMedia { get; set; }
    }
}
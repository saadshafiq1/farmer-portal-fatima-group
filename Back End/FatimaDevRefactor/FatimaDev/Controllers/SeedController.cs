﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Models;
using Infrastructure.OtherModels;
using Microsoft.AspNetCore.Authorization;
using Infrastructure.ViewModels;
using Core.Interfaces;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Farmer, TSO, Call service agent")]

    public class SeedController : ControllerBase
    {
        // private readonly FFContext _context;
        private readonly ISeedService _SeedService;
        ApiResponse response = new ApiResponse();
        public SeedController(ISeedService seedService)
        {
            _SeedService = seedService;
        }

        // GET: api/tbSeedTypes
        [HttpGet]
        public async Task<ApiResponse> GetSeeds()
        {
            response.Data = await _SeedService.GetAllSeeds();
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            return response;
        }

        // GET: api/tbSeedTypes/5
        [HttpGet("{id}")]
        public async Task<ApiResponse> GetSeeds([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            var tbSeedType = await _SeedService.GetSeedById(id);

            if (tbSeedType == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbSeedType;
            return response;
        }
       
        // PUT: api/tbSeedTypes/5
        [HttpPut("{id}")]
        public async Task<ApiResponse> PutSeeds([FromRoute] int id, [FromBody] TbSeedTypes tbSeedType)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }

            if (id != tbSeedType.SeedId)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }

            try
            {
                await _SeedService.UpdateSeed(tbSeedType);
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = tbSeedType;

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SeedTypeExists(id))
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Could not save.";
                    return response;
                }
                else
                {
                    throw;
                }
            }

            return response;
        }

        // POST: api/tbSeedTypes
        [HttpPost]
        public async Task<ApiResponse> PostSeeds([FromBody] TbSeedTypes tbSeedType)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }
          
            try
            {
                await _SeedService.CreateSeed(tbSeedType);
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = tbSeedType;
            }
            catch (Exception)
            {
                if (SeedTypeExists(tbSeedType.SeedId))
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Record Already Exists.";
                    response.Data = tbSeedType;
                }
                else
                {
                    throw;
                }
            }
            return response;
        }

        // DELETE: api/tbSeedTypes/5
        [HttpDelete("{id}")]
        public async Task<ApiResponse> DeleteSeeds([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not valid.";
                return response;
            }
            
            var tbSeedType = await _SeedService.GetSeedById(id);
            if (tbSeedType == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found";
                return response;
            }
           
            await _SeedService.DeleteSeed(tbSeedType);
           
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbSeedType;
            return response;
        }

        private bool SeedTypeExists(int id)
        {
            return _SeedService.GetSeedById(id) == null ? false : true;
        }             
              
    }

}



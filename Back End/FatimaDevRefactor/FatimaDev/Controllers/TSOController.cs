﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Core.Interfaces;
using Infrastructure.Models;
using Infrastructure.OtherModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Infrastructure.ViewModels;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Farmer, TSO, Call service agent")]
    public class TSOController : ControllerBase
    {
        private readonly ITsoService _TSOService;
        private readonly IMapper _Mapper;
        ApiResponse response = new ApiResponse();
        public TSOController(ITsoService TSOService, IMapper _mapper)
        {
            _TSOService = TSOService;
            _Mapper = _mapper;
        }
        public async Task<ApiResponse> GetTSOs()
        {
            var tbTsoInfos = await _TSOService.GetAllTSOs();
            var result = _Mapper.Map<List<TsoInfoModel>>(tbTsoInfos);
            response.Data = result;
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            return response;
        }
        [HttpGet("{cellphone}")]
        public async Task<ApiResponse> GetTSO([FromRoute] string cellphone)
        {
            var tbTso = await _TSOService.GetTSOByCellPhone(cellphone);
            var result = _Mapper.Map<TsoInfoModel>(tbTso);
            response.Data = result;
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            return response;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Models;
using Infrastructure.OtherModels;
using Microsoft.AspNetCore.Authorization;
using Infrastructure.ViewModels;
using Core.Interfaces;
using System.Dynamic;
using AutoMapper;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Farmer, TSO, Call service agent")]
    public class TicketController : ControllerBase
    {
        private readonly ITicketService _iTicketService;
        private readonly IFarmerService _iFarmerService;
        private readonly IFarmService _farmService;
        private readonly IMapper _Mapper;
        private readonly ITsoService _TsoService;
        private readonly ICallCenterAgentService _CallCenterAgentService;
        private readonly IAddressService _addressService;
       
        ApiResponse response = new ApiResponse();

        public TicketController(ITicketService ticketService,IFarmService farmService, IFarmerService farmerService, IMapper _mapper, 
            ITsoService TsoService, IAddressService addressService, ICallCenterAgentService CallCenterAgentService)
        {
            _iTicketService = ticketService;
            _farmService= farmService;
            _iFarmerService = farmerService;
            _Mapper = _mapper;
            _addressService = addressService;
            _TsoService = TsoService;
            _CallCenterAgentService = CallCenterAgentService;
        }
        // GET: api/TbTicketInfoes
        [HttpGet("GetAllActiveTickets")]
        public async Task<ApiResponse> GetAllActiveTickets(int pageNumber, int pageSize, string filter = "", string value = "")
        {
            try
            {
                var ticketInfos = await _iTicketService.GetAllActiveTickets(pageNumber, pageSize, filter.ToLower(), value);
                var result = _Mapper.Map<List<TicketInfoModel>>(ticketInfos);
                response.Data = result;
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }
            return response;
        }

        // GET: api/TbTicketInfoes
        [HttpGet("GetAllTickets")]
        public async Task<ApiResponse> GetAllTickets(int pageNumber, int pageSize, string filter = "", string value = "")
        {
            try
            {
                var tbTicketInfo = await _iTicketService.GetAllActiveTickets(pageNumber, pageSize, filter.ToLower(), value);
                var result = _Mapper.Map<List<TicketInfoModel>>(tbTicketInfo);

                List<dynamic> Data = new List<dynamic>();

                dynamic obj = new ExpandoObject();
                foreach (var item in result)
                {
                    dynamic obj1 = new ExpandoObject();
                    if (item.DueDate!=null && item.DueDate < DateTime.Now)
                    {
                        item.Slastatus = "SLA Breached";
                    }
                    obj1.data = item;
                    if (item.CreatedBy > 0)
                    {
                        var userProfile = await _CallCenterAgentService.GetAgentById(item.CreatedBy);
                        if (userProfile != null)
                        {
                            obj1.CreatedByName = userProfile.AgentName;
                        }
                        else
                        {
                            obj1.CreatedByName = null;
                        }
                    }
                    else
                    {
                        obj1.CreatedByName = item.Farmer.FarmerName;
                    }
                    if (item.AssignedTo > 0)
                    {
                        long num = (long)item.AssignedTo;
                        var userProfile = await _TsoService.GetTSOById(num);
                        if (userProfile != null)
                        {
                            obj1.AssinedToName = userProfile.Tsoname;
                            obj1.AssinedToCellPhone = userProfile.CellPhone;                            
                        }
                    }

                    Data.Add(obj1);
                }

                obj.data = Data;
                if (String.IsNullOrEmpty(filter))
                {
                    var count = await _iTicketService.GetAllTickets();
                    obj.totalCount = count.Count();
                }
                else
                {
                    var count = await _iTicketService.GetAllTicketsCount(filter.ToLower(), value);
                    obj.totalCount = count.Count();
                }
                response.Data = obj;
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }
            return response;
        }
        [HttpGet("GetAllTicketsByTSOIdPortal")]
        public async Task<ApiResponse> GetAllTicketsByTSOIdPortal(int pageNumber,int pageSize,int tsoID,string filter = "",string value = "") {
            try {
                var tbTicketInfo = await _iTicketService.GetAllTSOTickets(pageNumber,pageSize,tsoID,filter.ToLower(),value);
                var result = _Mapper.Map<List<TicketInfoModel>>(tbTicketInfo);

                List<dynamic> Data = new List<dynamic>();

                dynamic obj = new ExpandoObject();
                foreach (var item in result) {
                    dynamic obj1 = new ExpandoObject();
                    if (item.DueDate!=null && item.DueDate < DateTime.Now) {
                        item.Slastatus = "SLA Breached";
                    }
                    obj1.data = item;
                    if (item.CreatedBy > 0) {
                        var userProfile = await _CallCenterAgentService.GetAgentById(item.CreatedBy);
                        if (userProfile != null) {
                            obj1.CreatedByName = userProfile.AgentName;
                        } else {
                            obj1.CreatedByName = null;
                        }
                    } else {
                        obj1.CreatedByName = item.Farmer.FarmerName;
                    }
                    if (item.AssignedTo > 0) {
                        long num = (long)item.AssignedTo;
                        var userProfile = await _TsoService.GetTSOById(num);
                        if (userProfile != null) {
                            obj1.AssinedToName = userProfile.Tsoname;
                            obj1.AssinedToCellPhone = userProfile.CellPhone;
                        }
                    }

                    Data.Add(obj1);
                }

                obj.data = Data;
                if (String.IsNullOrEmpty(filter)) {
                    var count = await _iTicketService.GetAllTickets();
                    obj.totalCount = count.Count();
                } else {
                    var count = await _iTicketService.GetAllTicketsCount(filter.ToLower(),value);
                    obj.totalCount = count.Count();
                }
                response.Data = obj;
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
            } catch (Exception ex) {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }
            return response;
        }

        [HttpGet("{id}")]
        public async Task<ApiResponse> GetTicket([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not validated.";
                return response;
            }
            try
            {
                var tbTicketInfo = await _iTicketService.GetTicketById(id);

                dynamic obj = new ExpandoObject();
               
                if (tbTicketInfo == null)
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Not Found.";
                    return response;
                }

                if (tbTicketInfo.DueDate != null && tbTicketInfo.DueDate < DateTime.Now )/*&& tbTicketInfo.AssignedTo != nul)*/
                {
                    tbTicketInfo.Slastatus = "SLA Breached";
                }


                if (tbTicketInfo.Farm != null)
                {
                    tbTicketInfo.Farm.TbTicketInfo = null;
                    tbTicketInfo.Farm.Farmer = null;
                    // tbTicketInfo.Farm.DistrictCodeNavigation.ProvinceCodeNavigation = null;
                    
                    tbTicketInfo.Farm.DistrictCodeNavigation = null;                 
                }
                var a = tbTicketInfo.Farm;
                if (tbTicketInfo.Farmer != null)
                {
                    tbTicketInfo.Farmer.TbTicketInfo = null;
                    tbTicketInfo.Farmer.TbFarmInfo = null;
                    tbTicketInfo.Farmer.TbTicketActivity = null;
                    tbTicketInfo.Farmer.TbFarmerMedia = null;
                    tbTicketInfo.Farmer.DistrictCodeNavigation = null;
                    
                    tbTicketInfo.Farmer.TehsilCodeNavigation = null;
                    // tbTicketInfo.Farmer.TbTicketInfo = null;

                }
                if (tbTicketInfo.Farm.TbFarmBlock != null)
                {
                    List<TbFarmBlock> oo = new List<TbFarmBlock>();
                    foreach(var it in tbTicketInfo.Farm.TbFarmBlock)
                    {
                        it.Farm = null;
                        oo.Add(it);
                    }
                    tbTicketInfo.Farm.TbFarmBlock = oo;


                }
                if (tbTicketInfo.TicketType != null)
                {
                    tbTicketInfo.TicketType.TbTicketInfo = null;
                }
                if (tbTicketInfo.Status != null)
                {
                    tbTicketInfo.Status.TbTicketInfo = null;
                }
                    List<TbTicketActivity> ob = new List<TbTicketActivity>();
                if (tbTicketInfo.TbTicketActivity != null)
                {
                        foreach (var it in tbTicketInfo.TbTicketActivity.OrderByDescending(t=>t.CreatedDate))
                        {
                            it.Farmer = null;
                            it.Ticket = null;
                            ob.Add(it);
                        }
                        tbTicketInfo.TbTicketActivity = ob;
                }
                obj.data = tbTicketInfo;

                if (tbTicketInfo.CreatedBy > 0)
                {
                    var userProfile = await _CallCenterAgentService.GetAgentById(tbTicketInfo.CreatedBy);
                    if (userProfile != null)
                    {
                        userProfile.DistrictCodeNavigation = null;
                        userProfile.TehsilCodeNavigation = null;                        
                        
                        obj.CreatedBy = userProfile;
                    }
                }
                if (tbTicketInfo.AssignedTo > 0)
                {
                    long assigned = (long)tbTicketInfo.AssignedTo;
                    var assign = await _TsoService.GetTSOById(assigned);//(Guid.Parse("6D2C5FF5-3D12-45BB-A3C1-CF8A74319206"));
                    if (assign != null)
                    {
                        assign.SalePointCodeNavigation = null;
                        assign.RegionCodeNavigation = null;
                       
                        obj.assignedTo = assign;
                    }
                }

                LocationDataModel LocationData = new LocationDataModel();
                
                if (a.ProvinceCode > 0 && a.ProvinceCode != null)
                {
                    var p = await _addressService.GetProvinceById((long)a.ProvinceCode);
                    LocationData.ProvinceCode = p.ProvinceCode;
                    LocationData.ProvinceName = p.ProvinceName;
                    LocationData.ProvinceNameUrdu = p.ProvinceNameUrdu;
                    p = null;
                }
                if (a.DistrictCode > 0 && a.DistrictCode != null)
                {
                    var d = await _addressService.GetDistrictById((int)a.DistrictCode);

                    LocationData.DistrictCode = d.DistrictCode;
                    LocationData.DistrictName = d.DistrictName;
                    LocationData.DistrictNameUrdu = d.DistrictNameUrdu;
                    d = null;
                }
                if (a.TehsilCode > 0 && a.TehsilCode != null)
                {
                    var t = await _addressService.GetTehsilById((int)a.TehsilCode);
                    LocationData.TehsilCode = t.TehsilCode;
                    LocationData.TehsilName = t.TehsilName;
                    LocationData.TehsilNameUrdu = t.TehsilNameUrdu;

                }
                if (a.RegionId > 0 && a.RegionId != null)
                {
                    var r = await _addressService.GetRegionById((int)a.RegionId);
                    LocationData.RegionCode = r.RegionCode;
                    LocationData.RegionName = r.RegionName;
                    LocationData.RegionNameUrdu = r.RegionNameUrdu;
                    r = null;
                }

                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";


                obj.FarmLocationData = LocationData;

                response.Data = obj;
                
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
                
            }
            return response;
        }
    
     
        [HttpGet("TicketTypes")]
        public async Task<ApiResponse> GetTicketTypes()
        {
            try
            {
                response.Data = await _iTicketService.GetTicketTypes();
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }
            return response;
        }

        [HttpGet("StatusTypes")]
        public async Task<ApiResponse> GetTicketStatuses()
        {
            try
            {
                var result = await _iTicketService.GetTicketStatuses();
                List<TicketStatusModel> Data = new List<TicketStatusModel>();
                foreach (var item in result)
                {
                    TicketStatusModel itm = new TicketStatusModel
                    {
                        StatusId = item.StatusId,
                        StatusName = item.StatusName,
                        StatusDescription = item.StatusDescription
                    };
                    var Ncount = await _iTicketService.GetTicketsStatusesCount(item.StatusId);
                    long count = Ncount.Count();
                    itm.Count = count;
                    Data.Add(itm);
                }
                response.Data = Data;
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }
            return response;
        }

        //id should be of farmer
        [HttpGet("GetCurrentTicketsByFarmer/{id}")]
        public async Task<ApiResponse> GetCurrentTicketsByFarmerId([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State Validation Failed.";
                return response;
            }
            try
            {
                var tbTicketInfo = await _iTicketService.GetCurrentTicketById(id);
                if (tbTicketInfo == null)
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Not Found.";

                    return response;
                }

                List<TbTicketInfo> Data = new List<TbTicketInfo>();
                foreach (var ticket in tbTicketInfo)
                {
                        if (ticket.TicketType != null)
                        {
                            ticket.TicketType.TbTicketInfo = null; 
                        }
                    ticket.TbTicketActivity = null;
                    Data.Add(ticket);
                }

                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = Data;

            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }
            
            return response;
          
        }

        //id should be of farmer
        [HttpGet("GetPreviousTicketsByFarmer/{id}")]
        public async Task<ApiResponse> GetPreviousTicketsByFarmerId([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State Validation Failed.";
                return response;
            }
            try
            {
                var tbTicketInfo = await _iTicketService.GetPreviousTicketById(id);
                if (tbTicketInfo == null)
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Not Found.";

                    return response;
                }

                List<TbTicketInfo> Data = new List<TbTicketInfo>();
                foreach (var ticket in tbTicketInfo)
                {
                    if (ticket.TicketType != null)
                    {
                        ticket.TicketType.TbTicketInfo = null;
                    }
                    ticket.TbTicketActivity = null;
                    Data.Add(ticket);
                }

                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = Data;

            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }

            return response;

        }

        [HttpGet("GetTicketsByTypesOfFarmerId")]
        public async Task<ApiResponse> GetTicketsByTypesOfFarmerId(long farmerid , int tickettypeid)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State Validation Failed.";
                return response;
            }
            try
            {
                var tbTicketInfo = await _iTicketService.GetFarmerTicketsByTicketTypeId(farmerid,tickettypeid);
                if (tbTicketInfo == null)
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Not Found.";

                    return response;
                }

                List<TbTicketInfo> Data = new List<TbTicketInfo>();
                foreach (var ticket in tbTicketInfo)
                {
                    if (ticket.TicketType != null)
                    {
                        ticket.TicketType.TbTicketInfo = null;
                    }
                    ticket.TbTicketActivity = null;
                    Data.Add(ticket);
                }

                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = Data;

            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }

            return response;

        }

        [HttpDelete("{id}")]
        public async Task<ApiResponse> DeleteTicket([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State Validation Failed.";
            }

            var tbTicketInfo = await _iTicketService.GetTicketById(id);
            if (tbTicketInfo == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State Validation Failed.";
            }

            try
            {
                var tbTicket = tbTicketInfo;
                tbTicket.Deleted = true;
                tbTicket.Archive = true;
                await _iTicketService.UpdateTicket(tbTicket);
                             
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = id;
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }
            return response;
        }

        // PUT: api/TbFarmerInfoes/5
        [HttpPut("{id}")]
        public async Task<ApiResponse> PutTicket([FromRoute] long id, [FromBody] TbTicketInfo tbTicketInfo)
        {

            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State Validation Failed.";
                return response;
            }

            if (id != tbTicketInfo.TicketId)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "id is not equal to farmer object id";
                return response;
            }
         
            try
            {
                if (tbTicketInfo.StatusId == 3)
                {
                    tbTicketInfo.ResolvedDate = DateTime.Now;
                    tbTicketInfo.StatusText = "Closed";
                }
                else if (tbTicketInfo.StatusId == 2)
                {
                    tbTicketInfo.UpdatedDate = DateTime.Now;
                    tbTicketInfo.StatusText = "In Progress";
                }
                if (tbTicketInfo.DueDate != null && tbTicketInfo.DueDate < DateTime.Now)
                {
                    tbTicketInfo.Slastatus = "Sla Breached";
                }

                await _iTicketService.UpdateTicket(tbTicketInfo);
                
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                tbTicketInfo = await _iTicketService.GetTicketById(tbTicketInfo.TicketId);
                response.Data = tbTicketInfo;
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                if (!TicketInfoExists(id))
                {

                    response.ResponseMessage = "Ticket doesn't exist.";
                    return response;
                }
                else
                {
                    response.ResponseMessage = "DB ERROR";
                }
            }

            return response;
        }
        [HttpPut("UpdateTicketStatus")]
        public async Task<ApiResponse> UpdateTicketStatus([FromBody] TbTicketInfo tbTicketInfo)
        {
            var ticketInfo = await _iTicketService.GetTicketById(tbTicketInfo.TicketId);

            if (ticketInfo != null)
            {
                try
                {
                   
                    if (tbTicketInfo.StatusId==3)
                        ticketInfo.ResolvedDate = DateTime.Now;

                    ticketInfo.UpdatedDate = DateTime.Now;
                    var tbTicketsStatus = await _iTicketService.GetTicketsStatusByID(Convert.ToInt32(tbTicketInfo.StatusId));
                    ticketInfo.StatusId = tbTicketInfo.StatusId;
                    ticketInfo.StatusText = tbTicketsStatus.StatusName;
                    ticketInfo.FeedbackComments = tbTicketInfo.FeedbackComments;

                    await _iTicketService.UpdateTicket(ticketInfo);

                    response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                    response.ResponseMessage = "Action Completed.";
                }
                catch
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    if (!TicketInfoExists(tbTicketInfo.TicketId))
                    {
                        response.ResponseMessage = "Ticket doesn't exist.";
                    }
                    else
                    {
                        response.ResponseMessage = "DB ERROR";
                    }
                }

            }
            else
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Ticket doesn't exist.";                
            }
           
            return response;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ApiResponse> PostTicket([FromBody] TbTicketInfo tbTicketInfo)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State Validation Failed.";
                return response;
            }
              try
            {
                tbTicketInfo.Slastatus = "Within SLA";
                tbTicketInfo.DueDate = tbTicketInfo.VisitDate.Value.AddDays(10);
                tbTicketInfo.CreatedDate = DateTime.Now;
                tbTicketInfo.StatusId = 1;
                tbTicketInfo.StatusText = "Open";
                await _iTicketService.CreateTicket(tbTicketInfo);

                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Ticket has been scussefully created";
                response.Data = tbTicketInfo;
 
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }
           
            return response;
            
        }

        private bool TicketInfoExists(long id)
        {
            return _iTicketService.GetTicketById(id) == null ? false : true;
        }

        [HttpGet("GetTSOAgents")]
        public async Task<ApiResponse> GetTSOAgents()
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State Validation Failed.";
                return response;
            }
            try
            {
                var tbTsoInfos = await _TsoService.GetAllTSOs();
                if (tbTsoInfos == null)
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Not Found.";

                    return response;
                }

                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = tbTsoInfos;

            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }

            return response;

        }

        [HttpPost("ReplyOnTicket")]
        public async Task<ApiResponse> ReplyOnTicket([FromBody] TbTicketActivity tbTicketActivity)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State Validation Failed.";
                return response;
            }
            try
            {
                tbTicketActivity.CreatedDate = DateTime.Now;
             
                await _iTicketService.CreateTicketActivity(tbTicketActivity);

                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Ticket has been scussefully created";
                response.Data = tbTicketActivity;

            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }

            return response;

        }

        // GET: api/TbTicketInfoes
        [HttpGet("GetAllTicketsByFarmerIdOld")]
        public async Task<ApiResponse> GetAllTicketsByFarmerIdOld(int pageNumber, int pageSize,int farmerid, string filter = "", string value = "")
        {
            try
            {
                var tbTicketInfo = await _iTicketService.GetAllFarmerTickets(pageNumber, pageSize, farmerid, filter.ToLower(), value);
                var result = _Mapper.Map<List<TicketStatusModelForFarmerWeb>>(tbTicketInfo);

                List<dynamic> Data = new List<dynamic>();

                dynamic obj = new ExpandoObject();
                foreach (var item in result)
                {
                    dynamic obj1 = new ExpandoObject();
                    item.Status.TbTicketInfo = null;
                    obj1.data = item;
                    if (item.CreatedBy > 0)
                    {
                        var userProfile = await _CallCenterAgentService.GetAgentById(item.CreatedBy);
                        if (userProfile != null)
                        {
                            obj1.CreatedByName = userProfile.AgentName;
                            obj1.TicketSource = "Portal";
                        }
                        else
                        {
                            obj1.CreatedByName = null;
                        }
                    }
                    else
                    {
                        obj1.TicketSource = "Mobile App";
                        obj1.CreatedByName = item.Farmer.FarmerName;
                    }
                    if (item.AssignedTo > 0)
                    {
                        long num = (long)item.AssignedTo;
                        var userProfile = await _TsoService.GetTSOById(num);
                        if (userProfile != null)
                        {
                            obj1.AssinedToName = userProfile.Tsoname;
                            obj1.AssinedToCellPhone = userProfile.CellPhone;
                        }
                    }
                    Data.Add(obj1);
                }
                obj.data = Data;
                var count = await _iTicketService.GetAllFarmerTicketsCount(farmerid, filter.ToLower(), value);
                obj.totalCount = count.Count();
                response.Data = obj;
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }
            return response;
        }
        [HttpGet("GetAllTicketsByFarmerId")]
        public async Task<ApiResponse> GetAllTicketsByFarmerId(int pageNumber, int pageSize, int farmerid, string filter = "", string value = "")
        {
            try
            {
                var tbTicketInfo = await _iTicketService.GetAllFarmerTickets(pageNumber, pageSize, farmerid, filter.ToLower(), value);
                List<FarmerTicketInfoModel> TSOTickets = new List<FarmerTicketInfoModel>();

                foreach (var item in tbTicketInfo)
                {
                    var TSOTicket = new FarmerTicketInfoModel()
                    {
                        TicketId = item.TicketId,                       
                        TicketTypeName = (item.TicketType != null) ? item.TicketType.TicketTypeName : null,                      
                        StatusName = (item.Status != null) ? item.Status.StatusName : null,
                        Slastatus = item.Slastatus,                      
                        CreatedBy = item.CreatedBy,
                        DueDate = item.DueDate,
                        ResolvedDate = item.ResolvedDate,
                        CreatedDate = item.CreatedDate,
                    };
                    if (item.AssignedTo > 0)
                    {
                        long num = (long)item.AssignedTo;
                        var userProfile = await _TsoService.GetTSOById(num);
                        if (userProfile != null)
                        {
                            TSOTicket.AssignedToName = userProfile.Tsoname;
                            TSOTicket.AssignedToCellPhone = userProfile.CellPhone;
                        }
                    }
                    if (item.CreatedBy > 0)
                    {
                        var userProfile = await _CallCenterAgentService.GetAgentById(item.CreatedBy);
                        if (userProfile != null)
                        {
                            TSOTicket.CreatedByName = userProfile.AgentName;
                            TSOTicket.TicketSource = "Portal";
                        }
                        else
                        {
                            TSOTicket.CreatedByName = null;
                        }
                    }
                    else
                    {
                        TSOTicket.TicketSource = "Mobile App";
                        TSOTicket.CreatedByName = item.Farmer.FarmerName;
                    }

                    TSOTickets.Add(TSOTicket);
                }

                dynamic obj = new ExpandoObject();
                obj.Tickets = TSOTickets;
                var count = await _iTicketService.GetAllFarmerTicketsCount(farmerid, filter.ToLower(), value);
                obj.TotalTickets = count.Count();
                response.Data = obj;
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }
            return response;
        }

        [HttpGet("GetAllTicketsByTSOId")]
        public async Task<ApiResponse> GetAllTicketsByTSOId(int pageNumber, int pageSize, int tsoID, string filter = "", string value = "")
        {
            try
            {
                var tbTicketInfo = await _iTicketService.GetAllTSOTickets(pageNumber, pageSize, tsoID, filter.ToLower(), value);
                List<TSOTicketInfoModel> TSOTickets = new List<TSOTicketInfoModel>();

                foreach (var item in tbTicketInfo)
                {
                    var TSOTicket = new TSOTicketInfoModel()
                    {
                        TicketId = item.TicketId,
                        TicketTypeId = item.TicketTypeId,
                        TicketTypeName = (item.TicketType != null) ? item.TicketType.TicketTypeName : null,
                        TicketTypeNameUrdu = (item.TicketType != null) ? item.TicketType.TicketTypeNameUrdu : null,
                        StatusId = item.StatusId,
                        StatusName = (item.Status != null) ? item.Status.StatusName : null,
                        FarmerId = item.FarmerId,
                        FarmerName = (item.Farmer != null) ? item.Farmer.FarmerName : null,
                        CellPhone = (item.Farmer != null) ? item.Farmer.CellPhone : null,
                        PresentAddress = (item.Farmer != null) ? item.Farmer.PresentAddress : null,
                        PermanentAddress = (item.Farmer != null) ? item.Farmer.PermanentAddress : null,
                        DistrictName = (item.Farmer != null) ? item.Farmer.DistrictCodeNavigation.DistrictName : null,
                        DistrictNameUrdu = (item.Farmer != null) ? item.Farmer.DistrictCodeNavigation.DistrictNameUrdu : null,
                        FeedbackComments = item.FeedbackComments,
                        CreatedDate = item.CreatedDate,
                    }; 

                    TSOTickets.Add(TSOTicket);
                }

                dynamic obj = new ExpandoObject();
                obj.Tickets = TSOTickets;
                var count = await _iTicketService.GetAllTSOTicketsCount(tsoID, filter.ToLower(), value);
                obj.TotalTickets = count.Count();
                response.Data = obj;
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }
            return response;
        }

        [HttpGet("StatusTypesForFarmer")]
        public async Task<ApiResponse> StatusTypesForFarmer(int farmerid)
        {
            try
            {
                var result = await _iTicketService.GetTicketStatuses();
                List<TicketStatusModel> Data = new List<TicketStatusModel>();
                foreach (var item in result)
                {
                    TicketStatusModel itm = new TicketStatusModel
                    {
                        StatusId = item.StatusId,
                        StatusName = item.StatusName,
                        StatusDescription = item.StatusDescription
                    };
                    var Ncount = await _iTicketService.GetTicketsStatusesCountForFarmer(item.StatusId,farmerid);
                    long count = Ncount.Count();
                    itm.Count = count;
                    Data.Add(itm);
                }
                response.Data = Data;
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
            }
            catch (Exception ex)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
            }
            return response;
        }
    }
}

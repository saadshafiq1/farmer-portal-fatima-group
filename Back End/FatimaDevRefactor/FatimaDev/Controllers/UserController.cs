﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Models;
using API.Services;
using Microsoft.AspNetCore.Authorization;
using Infrastructure.OtherModels;
using System.Net.Http;
using Twilio.Rest.Api.V2010.Account;
using Twilio;
using System.Security.Claims;
using Core.Interfaces;
using API.Common;
using AutoMapper;
using Infrastructure.ViewModels;
using System.Dynamic;
using System.Net;
using System.IO;

namespace API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]   
    public class UserController : ControllerBase
    {
        //private readonly FFContext _context;
        private readonly IUserService _userService;
        private readonly IUserLoginService _userLoginService;
        private readonly IFarmerService _farmerService;
        private readonly IMapper _Mapper;
        private readonly ITsoService _tsoService;
        private readonly ICallCenterAgentService _callCenterAgentService;
     

        ApiResponse response = new ApiResponse();
        public UserController(IUserLoginService userLoginService, IUserService userService, IFarmerService farmerService, 
            IMapper _mapper,ITsoService tsoService, ICallCenterAgentService iCallCenterAgentService)
        {
            _userLoginService = userLoginService;
            _userService = userService;
            _farmerService = farmerService;
            _Mapper = _mapper;
            _tsoService = tsoService;
            _callCenterAgentService = iCallCenterAgentService;           
        }

        // GET: api/User
        [HttpGet]
        public async Task<ApiResponse> GetUser()
        {
            response.Data = await _userLoginService.GetAllUserLogin();// _context.TbUserLogin;
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            return response;
        }

        [HttpGet("UserProfile")]
        public async Task<ApiResponse> GetUserProfile()
        {
            if (User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value == "Farmer")
            {
                string cellno = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value.TrimStart('0');
              
                var tbFarmerInfo = await _farmerService.GetFarmerByCellPhone(cellno);
               // var result = _Mapper.Map<UserProfileInfo>(tbFarmerInfo);
                UserProfileInfo userProfile = new UserProfileInfo();

                userProfile.UserId = tbFarmerInfo.FarmerId;
                userProfile.UserName = tbFarmerInfo.FarmerName;
                userProfile.Cnic = tbFarmerInfo.Cnic;
                userProfile.AlternateCellPhone = tbFarmerInfo.AlternateCellPhoneNo;               
                userProfile.Gender = tbFarmerInfo.Gender;
                userProfile.PresentAddress = tbFarmerInfo.PresentAddress;
                userProfile.PermanentAddress = tbFarmerInfo.PermanentAddress;
                userProfile.EducationCode = tbFarmerInfo.EducationCode;
                userProfile.image = tbFarmerInfo.FarmerImage;
                userProfile.UserGuid = tbFarmerInfo.UserGuid;
                userProfile.FatherHusbandName = tbFarmerInfo.FatherHusbandName;
                userProfile.CellPhone = tbFarmerInfo.CellPhone;
                userProfile.MaleDependant = tbFarmerInfo.MaleDependant;
                userProfile.FemaleDependant = tbFarmerInfo.FemaleDependant;
                userProfile.AlternateName = tbFarmerInfo.AlternateName;
                userProfile.AlternateRelationshipwithFarmer = tbFarmerInfo.AlternateRelationshipwithFarmer;
                userProfile.ReferalCode = tbFarmerInfo.ReferalCode;
                userProfile.UnionCouncil = tbFarmerInfo.UnionCouncil;
                userProfile.MozaName = tbFarmerInfo.MozaName;
                userProfile.SmallAnimals = tbFarmerInfo.SmallAnimals;
                userProfile.BigAnimals = tbFarmerInfo.BigAnimals;
                userProfile.Tractor = tbFarmerInfo.Tractor;
                userProfile.ActiveStatus = tbFarmerInfo.ActiveStatus;
                if (tbFarmerInfo.DistrictCodeNavigation != null)
                {
                    userProfile.DistrictCode = tbFarmerInfo.DistrictCodeNavigation.DistrictCode;
                    userProfile.DistrictName = tbFarmerInfo.DistrictCodeNavigation.DistrictName;
                    userProfile.DistrictNameUrdu = tbFarmerInfo.DistrictCodeNavigation.DistrictNameUrdu;
                }
                if (tbFarmerInfo.TehsilCodeNavigation != null)
                {
                    userProfile.TehsilCode = tbFarmerInfo.TehsilCodeNavigation.TehsilCode;
                    userProfile.TehsilName = tbFarmerInfo.TehsilCodeNavigation.TehsilName;
                    userProfile.TehsilNameUrdu = tbFarmerInfo.TehsilCodeNavigation.TehsilNameUrdu;
                }

                userProfile.Role = "Farmer";

                response.Data = userProfile;

                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                return response;
            }
            else if (User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value == "TSO")
            {
                string cellno = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value.TrimStart('0');

            
                var tbTsoInfo = await _tsoService.GetTSOByCellPhone(cellno);
              
                UserProfileInfo userProfile = new UserProfileInfo();

                userProfile.UserId = tbTsoInfo.Tsoid;
                userProfile.UserName = tbTsoInfo.Tsoname;
                userProfile.Cnic = tbTsoInfo.Cnic;
                userProfile.Email = tbTsoInfo.Email;
                userProfile.CellPhone = tbTsoInfo.CellPhone;
                userProfile.AlternateCellPhone = tbTsoInfo.AlternateCellPhone;
                userProfile.Landline = tbTsoInfo.Landline;
                userProfile.Gender = tbTsoInfo.Gender;
                userProfile.PresentAddress = tbTsoInfo.PresentAddress;
                userProfile.PermanentAddress = tbTsoInfo.PermanentAddress;
                userProfile.EducationCode = tbTsoInfo.EducationCode;
                userProfile.image = tbTsoInfo.Tsoimage;
                userProfile.UserGuid = tbTsoInfo.UserGuid;
                userProfile.ActiveStatus = tbTsoInfo.ActiveStatus;
                if (tbTsoInfo.SalePointCodeNavigation != null)
                {
                    userProfile.DistrictCode = tbTsoInfo.SalePointCodeNavigation.SalePointCode;
                    userProfile.DistrictName = tbTsoInfo.SalePointCodeNavigation.SalePointName;
                    userProfile.DistrictNameUrdu = tbTsoInfo.SalePointCodeNavigation.SalePointNameUrdu;
                }               
                userProfile.Role = "TSO";

                response.Data = userProfile;
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                return response;
            }
            else if (User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value == "Call service agent")
            {
                string username = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Anonymous).Value;

                var tbFarmerInfo = await _callCenterAgentService.GetAgentByUserName(Guid.Parse(username));
                var result = _Mapper.Map<CallCenterAgentInfoModel>(tbFarmerInfo);
                response.Data = result;

                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                return response;
            }
            else
            {
                response.Data = null;
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Only Farmer Info Available at the moment.";
                return response;
            }

        }
        [AllowAnonymous]
        [HttpGet("SendPasswordOld")]
        public async Task<ApiResponse> ForgetPasswordOld(string userName)
        {
            var user = await _userLoginService.GetUserByCellPhone(userName); // _context.TbUserLogin.Where(c => c.CellPhone == userName).ToList();

            if (user.Count() != 0)
            {
                var temp = user.FirstOrDefault();
                //HttpClient client = new HttpClient();

                //string uri =String.Format("https://sendpk.com/api/sms.php?username=923238782529&password=7526&sender=Masking&mobile={0}&message={1}",temp.CellPhone , temp.Passkey);
                ////client.DefaultRequestHeaders.Add("Authorization", "token ADD YOUR OAUTH TOKEN");
                //client.DefaultRequestHeaders.Add("User-Agent", "WebAPi");
                //var content = client.GetStringAsync(uri);

                //const string accountSid = "AC511468507e9aebb5e088f3f2e18b6eac";
                //const string authToken = "40f18012c3baad95cb225210bbfc29a0";


                //TwilioClient.Init(accountSid, authToken);

                //var message = MessageResource.Create(
                //    body: "UserName: "+temp.CellPhone+" Password: "+ temp.Passkey,
                //    from: new Twilio.Types.PhoneNumber("+17193565480"),
                //    to: new Twilio.Types.PhoneNumber("+923238782529")
                //);

                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "UserName: " + temp.CellPhone + ", Password:" + temp.Passkey;
                response.Data = "Message will be send when messaging API is available";
                return response;
            }
            else
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "User Name Doesn't exist";
                response.Data = null;
                return response;
            }
        }
        [AllowAnonymous]
        [HttpGet("SendPassword")]
        public async Task<ApiResponse> ForgetPassword(string userName)
        {         
            var users = await _userLoginService.GetUserByCellPhone(userName); 

            if (users.Count() != 0)
            {
                var user = users.FirstOrDefault();

                string httpResponse = _userService.SendOTP(user.CellPhone, user.Passkey);
                if (httpResponse != "0")
                {
                    response.ResponseMessage = httpResponse;
                    response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                }
                else
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Failure sending OTP";
                    response.Data = "UserName: " + user.CellPhone + ", Password:" + user.Passkey;
                }
            }
            else
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "User Name Doesn't exist"; 
            }
            return response;
        }
     
        [AllowAnonymous]
        [HttpGet("SendTSOPassword")]
        public async Task<ApiResponse> ForgetTSOPassword(string userName)
        {
            var user = await _userLoginService.GetUserByUserName(userName); 

            if (user.Count() != 0)
            {
                var tbUser = user.FirstOrDefault();
                if (Common.Common.sendEmail("Test Email", "New Password", "from@email.com", "to@email.com"))
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                    response.ResponseMessage = "Password sent to the provided email address.";
                    response.Data = null;
                }
                else {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "SMTP not provided";
                    response.Data = null;
                }               
            }
            else
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "User Name Doesn't exist";
                response.Data = null;
               
            }
            return response;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public ApiResponse Authenticate([FromBody] TbUserLogin tbUserLogin)
        {
            string strUserName = tbUserLogin.CellPhone;
            string strPassword = tbUserLogin.Passkey;         
            try
            {
                string token = string.Empty;
                if (string.IsNullOrEmpty(strUserName))
                {
                    strUserName = tbUserLogin.UserName;
                    token = _userService.AuthenticateAgent(strUserName, strPassword);
                }
                else
                { token = _userService.AuthenticateFarmerOrTSO(strUserName, strPassword); }

                if (token == null)
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "token is Null.";
                    response.Data = null;
                    return response;

                }
                response.ResponseCode = (int)Common.Common.ResponseCode.Success;
                response.ResponseMessage = "Action Completed.";
                response.Data = token;
                return response;

            }
            catch (Exception ex)
            {

                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = ex.ToString();
                response.Data = null;
                return response;
            }
        }
        [AllowAnonymous]
        [HttpGet("roles")]
        public async Task<ApiResponse> GetRoles()
        {
            response.Data = await _userLoginService.GetAllUserRoleTypeCodeAsync();// _context.TbUserRoleTypeCode;
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            return response;
        }


        // GET: api/User/5
        [HttpGet("{id}")]
        public async Task<ApiResponse> GetUser([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not validated.";
                return response;
            }

            var tbUserLogin = await _userLoginService.GetUserLoginById(id); //_context.TbUserLogin.FindAsync(id);

            if (tbUserLogin == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found.";

                return response;
            }

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbUserLogin;
            return response;
        }

        // PUT: api/User/5
        [HttpPut("{id}")]
        public async Task<ApiResponse> PutUser([FromRoute] string id, [FromBody] TbUserLogin tbUserLogin)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not validated.";
                return response;
            }

            if (id != tbUserLogin.CellPhone)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Id is not equal userlogin object id.";
                return response;
            }

            try
            {
              await  _userLoginService.UpdateUserLogin(tbUserLogin);              
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TbUserLoginExists(id))
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Not Found.";

                    return response;
                }
                else
                {
                    throw;
                }
            }

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbUserLogin;
            return response;
        }

        // POST: api/User
        [AllowAnonymous]
        [HttpPost]
        public async Task<ApiResponse> PostUser([FromBody] TbUserLogin tbUserLogin)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not validated.";
                return response;
            }

            // _context.TbUserLogin.Add(tbUserLogin);
            try
            {
                await _userLoginService.CreateUserLogin(tbUserLogin);
                //  await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TbUserLoginExists(tbUserLogin.CellPhone))
                {
                    response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                    response.ResponseMessage = "Already Exists.";
                    return response;
                }
                else
                {
                    throw;
                }
            }

            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = tbUserLogin;
            return response;
        }

        // DELETE: api/User/5
        [HttpDelete("{id}")]
        public async Task<ApiResponse> DeleteUser([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Model State not validated.";
                return response;
            }

            var tbUserLogin = await _userLoginService.GetUserLoginById(id); //_context.TbUserLogin.FindAsync(id);
            if (tbUserLogin == null)
            {
                response.ResponseCode = (int)Common.Common.ResponseCode.Failure;
                response.ResponseMessage = "Not Found.";

                return response;
            }

            // _context.TbUserLogin.Remove(tbUserLogin);
            //  await _context.SaveChangesAsync();
            await _userLoginService.DeleteUserLogin(tbUserLogin);
            response.ResponseCode = (int)Common.Common.ResponseCode.Success;
            response.ResponseMessage = "Action Completed.";
            response.Data = id;
            return response;
        }


        private bool TbUserLoginExists(string id)
        {
            var user = _userLoginService.GetUserByCellPhone(id);
            return user == null ? false : true;
            // return _context.TbUserLogin.Any(e => e.CellPhone == id);
        }
    }
}
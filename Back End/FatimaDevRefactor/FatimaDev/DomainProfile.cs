﻿using AutoMapper;
using Infrastructure.Models;
using Infrastructure.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Common
{
  
        public class DomainProfile : Profile
        {
            public DomainProfile()
            {
            
            CreateMap<TbFarmInfo, FarmModel>(MemberList.Destination);
            CreateMap<TbFarmBlock, FarmBlockModel>(MemberList.Destination);
        }
   
    }
}

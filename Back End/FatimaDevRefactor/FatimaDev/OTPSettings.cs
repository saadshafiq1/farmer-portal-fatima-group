﻿namespace FatimaDev
{
    public class OTPSettings
    {
        public string username { get; set; }
        public string password { get; set; }
        public string originator { get; set; }
        public string service { get; set; }
        public string message { get; set; }
    }
}
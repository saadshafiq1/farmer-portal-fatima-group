﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Services
{

    public interface IUserService
    {
        string AuthenticateFarmerOrTSO(string username, string password);
        string AuthenticateAgent(string username, string password);
        string SendOTP(string recipient, string passkey);
    }

}

﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using FatimaDev;
using Infrastructure.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace API.Services
{
    public class UserService : IUserService
    {
        private readonly FFContext _context;
        private readonly IConfiguration _configuration;
        private readonly IOptions<OTPSettings> _iconfig;
        public UserService(FFContext context, IConfiguration configuration, IHostingEnvironment env,IOptions<OTPSettings> iconfig)
        {

            var builder = new ConfigurationBuilder()
           .SetBasePath(env.ContentRootPath)
           .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
           .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            builder.AddEnvironmentVariables();
            _configuration = builder.Build();
            // _configuration = configuration;
            _context = context;
            _iconfig = iconfig;
        }
        public string AuthenticateFarmerOrTSO(string username, string password)
        {
            var user = _context.TbUserLogin.Include(a => a.UserRoleCodeNavigation).Where(x => x.CellPhone == username && x.Passkey == password).FirstOrDefault();

            // return null if user not found
            if (user == null)
                return null;


            DateTime expireDateTime;
            if (user.UserRoleCodeNavigation.RoleName == "Farmer")
            {
                expireDateTime = DateTime.UtcNow.AddYears(10);
            }
            else if (user.UserRoleCodeNavigation.RoleName == "TSO")
            {
                expireDateTime = DateTime.UtcNow.AddYears(10);
            }
            else
            {
                expireDateTime = DateTime.UtcNow.AddDays(1);
            }
            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            string strkey = _configuration.GetSection("AppSettings:Secret").Value;
            var key = Encoding.ASCII.GetBytes(strkey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.CellPhone.ToString()),
                    new Claim(ClaimTypes.Role,user.UserRoleCodeNavigation.RoleName)
                }),
                Expires = expireDateTime,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            string tokenString = tokenHandler.WriteToken(token);

            // remove password before returning
            user.Passkey = null;

            return tokenString;
        }

        public string AuthenticateAgent(string username, string password)
        {
            var user = _context.TbUserLogin.Include(a => a.UserRoleCodeNavigation).Where(x => x.UserName == username && x.Passkey == password).FirstOrDefault();

            // return null if user not found
            if (user == null)
                return null;


            DateTime expireDateTime;
            if (user.UserRoleCodeNavigation.RoleName == "Farmer")
            {
                expireDateTime = DateTime.UtcNow.AddYears(10);
            }
            else
            {
                expireDateTime = DateTime.UtcNow.AddDays(1);
            }
            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            string strkey = _configuration.GetSection("AppSettings:Secret").Value;
            var key = Encoding.ASCII.GetBytes(strkey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.CellPhone.ToString()),
                    new Claim(ClaimTypes.Role,user.UserRoleCodeNavigation.RoleName),
                    new Claim(ClaimTypes.Anonymous,user.UserId+"")
                }),
                Expires = expireDateTime,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            string tokenString = tokenHandler.WriteToken(token);

            // remove password before returning
            user.Passkey = null;

            return tokenString;
        }
        public string SendOTP(string recipient, string passkey)
        {
            string strService = _iconfig.Value.service ?? "";
            string strUserName = _iconfig.Value.username ?? "";
            string strPassword = _iconfig.Value.password ?? "";
            string strOriginator = _iconfig.Value.originator ?? "";
            string strMessage = _iconfig.Value.message ?? "";
            string uri = String.Format(strService + "&username={0}&password={1}&recipient={2}&originator={3}&messagedata={4}",
              strUserName, strPassword, recipient, strOriginator, strMessage + passkey);

            return getRequest(uri);
        }
        private string getRequest(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = "Get";
            request.KeepAlive = true;
            request.ContentType = "appication/json";
            request.Headers.Add("Content-Type", "appication/json");

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string myResponse = "";
            using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream()))
            {
                myResponse = sr.ReadToEnd();
            }
            if (response.StatusCode == HttpStatusCode.OK && myResponse.ToLower().Contains("message accepted for delivery"))
                return "OTP Sent Successfully";
            else
                return "0";
        }
    }
}


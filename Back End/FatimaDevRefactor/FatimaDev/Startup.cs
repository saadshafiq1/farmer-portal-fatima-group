﻿using API.Services;
using AutoMapper;
using Infrastructure.Interface;
using Infrastructure.Repository;
using Infrastructure.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Core.Interfaces;
using Core.Concrete;
using Newtonsoft.Json;

namespace FatimaDev
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
           .SetBasePath(env.ContentRootPath)
           .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
           .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: false);

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
            //  Configuration = configuration;

        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddAutoMapper();

            services.Configure<IISOptions>(options =>
            {
                options.AutomaticAuthentication = false;
            });

            services.AddMvc()
                .AddJsonOptions(
            options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore         
                );

            services.AddMvc()
                .AddJsonOptions(
            options => options.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.None
            );
            //services.AddSingleton<IConfiguration>(Configuration);
            services.Configure<IpSettings>(Configuration.GetSection("PublicIp"));
            services.Configure<OTPSettings>(Configuration.GetSection("OTPSettings"));

            services.AddMvcCore().AddJsonOptions(opt => opt.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddMvcCore()
            .AddJsonFormatters(opt => opt.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            string secretKey = Configuration.GetSection("AppSettings:Secret").Value;

            var key = System.Text.Encoding.ASCII.GetBytes(secretKey);
            //var key = System.Text.Encoding.ASCII.GetBytes("this is a great key for the algorithm long and secure");
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            // configure DI for application services
            services.AddScoped<IUserService, UserService>();


            var connection = Configuration.GetConnectionString("DBConnection");

            services.AddDbContext<FFContext>(options => options.UseSqlServer(connection));

            services.AddTransient<IMediaService, MediaService>();
            services.AddTransient<IFarmService, FarmService>();
            services.AddTransient<ICropService, CropService>();
            services.AddTransient<IFarmerService, FarmerService>();
            services.AddTransient<IUserLoginService, UserLoginService>();
            services.AddTransient<IRoleService, RoleService>();
            services.AddTransient<ITsoService, TsoService>();
            services.AddTransient<IAddressService, AddressService>();
            services.AddTransient<IDealerService, DealerService>();
            services.AddTransient<ISeedService, SeedService>();
            services.AddTransient<IFertilizerService, FertilizerService>();
            services.AddTransient<ITicketService, TicketService>();
            services.AddTransient<ICallCenterAgentService, CallCenterAgentService>();
            services.AddTransient<ICommodityPricesService, CommodityPricesService>();
            services.AddTransient<IFarmerReportService, FarmerReportService>();


            services.AddTransient<ICropRepository, CropRepository>();
            services.AddTransient<IBlockCropRepository, BlockCropRepository>();
            services.AddTransient<IFarmerCropPlanRepository, FarmerCropPlanRepository>();
            services.AddTransient<ICropCaseMainRepository, CropCaseMainRepository>();
            services.AddTransient<ICropCalenderMainRepository, CropCalenderMainRepository>();
            services.AddTransient<ICropStepActionRepository, CropStepActionRepository>();
            services.AddTransient<IFarmerCropPlanUnitRepository, FarmerCropPlanUnitRepository>();
            services.AddTransient<IUserRoleTypeCodeRepository, UserRoleTypeCodeRepository>();
            services.AddTransient<IFarmShapeRepository, FarmShapeRepository>();
            services.AddTransient<IUnitShapeRepository, UnitShapeRepository>();
            services.AddTransient<IMediaRepository, MediaRepository>();
            services.AddTransient<IFarmerMediaRepository, FarmerMediaRepository>();
            services.AddTransient<IAddressRepository, AddressRepository>();
            services.AddTransient<IFarmRepository, FarmRepository>();
            services.AddTransient<IFarmererRepository, FarmerRepository>();
            services.AddTransient<IFarmBlockRepository, FarmBlockRepository>();
            services.AddTransient<IFarmTypeRepository, FarmTypeRepository>();
            services.AddTransient<ITicketRepository, TicketRepository>();
            services.AddTransient<ITicketTypeRepository, TicketTypeRepository>();
            services.AddTransient<ITicketTypeStatusRepository, TicketTypeStatusRepository>();
            services.AddTransient<IFarmUnitRepository, FarmUnitRepository>();
            services.AddTransient<IFarmBlockUnitRepository, FarmBlockUnitRepository>();
            services.AddTransient<IUserLoginRepository, UserLoginRepository>();
            services.AddTransient<IRoleRepository, RoleRepository>();
            services.AddTransient<ITsoRepository, TsoRepository>();
            services.AddTransient<ISeedTypesRepository, SeedTypesRepository>();
            services.AddTransient<IFertilizerTypesRepository, FertilizerTypesRepository>();
            services.AddTransient<IDealerInfoRepository, DealerInfoRepository>();
            services.AddTransient<IDealerTypeCodeRepository, DealerTypeCodeRepository>();
            services.AddTransient<IDealerSeedDetailRepository, DealerSeedDetailRepository>();
            services.AddTransient<IDealerFertilizerDetailRepository, DealerFertilizerDetailRepository>();
            services.AddTransient<IDealerStatusRepository, DealerStatusRepository>();
            services.AddTransient<IDealerCategoryRepository, DealerCategoryRepository>();
            services.AddTransient<ICallCenterAgentRepository, CallCenterAgentRepository>();
            services.AddTransient<ITicketActivityRepository, TicketActivityRepository>();
            services.AddTransient<IAIMSCommodityRepository, AIMSCommodityRepository>();
            services.AddTransient<IAIMSCommodityCityRepository, AIMSCommodityCityRepository>();
            services.AddTransient<IFarmerReportsRepository, FarmerReportsRepository>();


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(builder =>
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();
            app.UseMvc();
        }
    }
}

﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface IAIMSCommodityCityRepository
    {
        Task<IEnumerable<TbAimsCommodityCity>> GetAllCommodityCitysAsync();
        Task<TbAimsCommodityCity> GetCommodityCityByIdAsync(int ID);
        Task CreateCommodityCityAsync(TbAimsCommodityCity user);
        Task UpdateCommodityCityAsync(TbAimsCommodityCity user);
        Task DeleteCommodityCityAsync(TbAimsCommodityCity user);
        Task<IEnumerable<TbAimsCommodityCity>> GetCommodityCityByCondition(Expression<Func<TbAimsCommodityCity, bool>> expression);
    }
}

﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface IAIMSCommodityRepository
    {
        Task<IEnumerable<TbAimsCommodity>> GetAllCommoditysAsync();
        Task<TbAimsCommodity> GetCommodityByIdAsync(int ID);
        Task CreateCommodityAsync(TbAimsCommodity user);
        Task UpdateCommodityAsync(TbAimsCommodity user);
        Task DeleteCommodityAsync(TbAimsCommodity user);
        Task<IEnumerable<TbAimsCommodity>> GetCommodityByCondition(Expression<Func<TbAimsCommodity, bool>> expression);
    }
}

﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
   public interface IAddressRepository
    {
        Task<IEnumerable<TbProvinceCode>> GetAllProvincesAsync();
        Task<TbProvinceCode> GetProvinceByIdAsync(long ownerId);
        Task CreateProvinceAsync(TbProvinceCode media);
        Task UpdateProvinceAsync(TbProvinceCode media);
        Task DeleteProvinceAsync(TbProvinceCode media);
        Task<IEnumerable<TbDistrictCode>> GetDistrictsOfProvince(long provinceid);
        Task<IEnumerable<TbDistrictCode>> GetDistrictsByRegion(long regionid);
        Task<IEnumerable<TbTehsilCode>> GetTehsilsOfDistrict(long districtid);
        Task<TbDistrictCode> GetDistrictById(int districtid);
        Task<TbTehsilCode> GetTehsilById(int tehsilid);
        Task<IEnumerable<TbRegionCode>> GetCitiesOfProvince(int provinceid);
        Task<TbRegionCode> GetRegionById(int regionid);
        Task<IEnumerable<TbRegionCode>> GetAllRegion();
        Task<IEnumerable<TbDistrictCode>> GetAllDistricts();
        Task<IEnumerable<TbSalepointCode>> GetAllSalesPoint();


    }
}

﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface IBaseRepository<T>
    {

        Task<IEnumerable<T>> FindAllAsync();
        Task<IEnumerable<T>> FindByConditionAync(Expression<Func<T, bool>> expression, int pageNumber, int pageSize);
        Task<IEnumerable<T>> FindByConditionAync(Expression<Func<T, object>>[] includes, int pageNumber, int pageSize);      
        Task<IEnumerable<T>> FindByConditionAync(Expression<Func<T, bool>> expression, Expression<Func<T, object>>[] includes, int pageNumber, int pageSize);
        Task Create(T entity);
        Task Update(T entity);
        Task Delete(T entity);
        Task SaveAsync();
    }
}

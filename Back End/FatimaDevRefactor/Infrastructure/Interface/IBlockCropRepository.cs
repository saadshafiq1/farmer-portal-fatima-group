﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Infrastructure.Models;

namespace Infrastructure.Interface
{
   public interface IBlockCropRepository
    {
        Task<IEnumerable<TbBlockCrops>> GetAllBlockCropsAsync();       
        Task CreateBlockCropsAsync(TbBlockCrops crop);
        Task UpdateBlockCropsAsync(TbBlockCrops crop);
        Task DeleteBlockCropsAsync(TbBlockCrops crop);        
        Task<IEnumerable<TbBlockCrops>> GetBlockCropByCondition(Expression<Func<TbBlockCrops, bool>> expression);
    }
}

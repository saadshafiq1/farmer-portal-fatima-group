﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface ICallCenterAgentRepository
    {
        Task<TbCallCenterAgentInfo> GetAgentByUserName(Expression<Func<TbCallCenterAgentInfo, bool>> expression, Expression<Func<TbCallCenterAgentInfo, object>>[] includes);


        Task<TbCallCenterAgentInfo> GetAgentById(Expression<Func<TbCallCenterAgentInfo, bool>> expression);

        Task<IEnumerable<TbCallCenterAgentInfo>> GetAllAgentsAsync();
    }
}

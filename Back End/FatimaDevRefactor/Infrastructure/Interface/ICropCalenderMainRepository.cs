﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface ICropCalenderMainRepository
    {
        Task<IEnumerable<TbCropCalenderMain>> GetAllCropCalenderMainAsync();
        Task<TbCropCalenderMain> GetCropCalenderMainByIdAsync(long calenderCode);
        Task CreateCropCalenderMainAsync(TbCropCalenderMain CropCalenderMain);
        Task UpdateCropCalenderMainAsync(TbCropCalenderMain CropCalenderMain);
        Task DeleteCropCalenderMainAsync(TbCropCalenderMain CropCalenderMain);
        Task<IEnumerable<TbCropCalenderMain>> GetCropCalenderMainByCondition(Expression<Func<TbCropCalenderMain, bool>> expression);
        Task<IEnumerable<TbCropCalenderMain>> GetCropCalenderMainByCondition(Expression<Func<TbCropCalenderMain, bool>> expression, Expression<Func<TbCropCalenderMain, object>> []includes);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Infrastructure.Models;

namespace Infrastructure.Interface
{
    public interface ICropCaseMainRepository
    {
        Task<IEnumerable<TbCropCaseMain>> GetAllCropCaseMainAsync();
        Task CreateCropCaseMainAsync(TbCropCaseMain crop);
        Task UpdateCropCaseMainAsync(TbCropCaseMain crop);
        Task DeleteCropCaseMainAsync(TbCropCaseMain crop);
        Task<IEnumerable<TbCropCaseMain>> GetCropCaseMainByCondition(Expression<Func<TbCropCaseMain, bool>> expression);
    }
}

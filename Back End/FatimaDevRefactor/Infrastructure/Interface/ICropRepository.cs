﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface ICropRepository
    {
        Task<IEnumerable<TbCropTypeCode>> GetAllCropsAsync();
        Task<TbCropTypeCode> GetCropByIdAsync(long cropId);
        Task CreateCropAsync(TbCropTypeCode crop);
        Task UpdateCropAsync(TbCropTypeCode crop);
        Task DeleteCropAsync(TbCropTypeCode crop);
        Task<IEnumerable<TbCropTypeCode>> GetCropByCondition(Expression<Func<TbCropTypeCode, bool>> expression);       
    }
}

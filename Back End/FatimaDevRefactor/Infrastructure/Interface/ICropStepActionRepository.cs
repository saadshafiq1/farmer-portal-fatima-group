﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface ICropStepActionRepository
    {
        Task<IEnumerable<TbCropStepAction>> GetAllCropStepActionAsync();
        Task<TbCropStepAction> GetCropStepActionByIdAsync(long cropId);
        Task CreateCropStepActionAsync(TbCropStepAction crop);
        Task UpdateCropStepActionAsync(TbCropStepAction crop);
        Task DeleteCropStepActionAsync(TbCropStepAction crop);
        Task<IEnumerable<TbCropStepAction>> GetCropStepActionByCondition(Expression<Func<TbCropStepAction, bool>> expression);
    }
}
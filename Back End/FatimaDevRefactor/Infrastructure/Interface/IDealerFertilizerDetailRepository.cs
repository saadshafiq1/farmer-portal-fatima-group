﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface IDealerFertilizerDetailRepository
    {
        Task<IEnumerable<TbDealerFertilizerDetails>> GetAllDealerFertilizerDetailAsync();
        Task<TbDealerFertilizerDetails> GetDealerFertilizerDetailByIdAsync(long dealerFertilizerDetailId);
        Task CreateDealerFertilizerDetailAsync(TbDealerFertilizerDetails dealerFertilizerDetail);
        Task UpdateDealerFertilizerDetailAsync(TbDealerFertilizerDetails dealerFertilizerDetail);
        Task DeleteDealerFertilizerDetailAsync(TbDealerFertilizerDetails dealerFertilizerDetail);
        Task<IEnumerable<TbDealerFertilizerDetails>> GetDealerFertilizerDetailByCondition(Expression<Func<TbDealerFertilizerDetails, bool>> expression);
    }
}

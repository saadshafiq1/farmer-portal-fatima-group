﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface IDealerInfoRepository
    {
        Task<IEnumerable<TbDealerInfo>> GetAllDealerInfoAsync();
        Task<IEnumerable<TbDealerInfo>> GetAllDealersCountAsync();
        Task<IEnumerable<TbDealerInfo>> GetAllDealerInfoAsync(int pageNumber, int pageSize, string filter="",string vale="");
        Task<IEnumerable<TbDealerInfo>> GetAllDealersCountAsync(string filter = "", string vale = "");
        Task<TbDealerInfo> GetDealerInfoByIdAsync(long dealerInfoId);
        Task CreateDealerInfoAsync(TbDealerInfo dealerInfo);
        Task UpdateDealerInfoAsync(TbDealerInfo dealerInfo);
        Task DeleteDealerInfoAsync(TbDealerInfo dealerInfo);
        Task<IEnumerable<TbDealerInfo>> GetDealerInfoByCondition(Expression<Func<TbDealerInfo, bool>> expression, int pageNumber, int pageSize);
        Task<IEnumerable<TbDealerInfo>> GetDealerInfoByConditionLatLon(Expression<Func<TbDealerInfo, bool>> expression, int pageNumber, int pageSize, float latitude, float longitude);
    }
}

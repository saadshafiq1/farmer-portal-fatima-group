﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface IDealerSeedDetailRepository
    {
        Task<IEnumerable<TbDealerSeedDetails>> GetAllDealerSeedDetailAsync();
        Task<TbDealerSeedDetails> GetDealerSeedDetailByIdAsync(long dealerSeedDetailId);
        Task CreateDealerSeedDetailAsync(TbDealerSeedDetails dealerSeedDetail);
        Task UpdateDealerSeedDetailAsync(TbDealerSeedDetails dealerSeedDetail);
        Task DeleteDealerSeedDetailAsync(TbDealerSeedDetails dealerSeedDetail);
        Task<IEnumerable<TbDealerSeedDetails>> GetDealerSeedDetailByCondition(Expression<Func<TbDealerSeedDetails, bool>> expression, int pageNumber, int pageSize);
    }
}

﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface IDealerTypeCodeRepository
    {
        Task<IEnumerable<TbDealerTypeCode>> GetAllDealerTypeCodesAsync();
        Task<TbDealerTypeCode> GetDealerTypeCodeByIdAsync(long dealerTypeCodeId);
        Task CreateDealerTypeCodeAsync(TbDealerTypeCode dealerTypeCode);
        Task UpdateDealerTypeCodeAsync(TbDealerTypeCode dealerTypeCode);
        Task DeleteDealerTypeCodeAsync(TbDealerTypeCode dealerTypeCode);
        Task<IEnumerable<TbDealerTypeCode>> GetDealerTypeCodeByCondition(Expression<Func<TbDealerTypeCode, bool>> expression);
    }
}

﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface IFarmBlockRepository
    {
        Task<IEnumerable<TbFarmBlock>> GetAllFarmBlocksAsync();
        Task<TbFarmBlock> GetFarmBlockByIdAsync(long id);
        Task CreateFarmBlockAsync(TbFarmBlock entity);
        Task UpdateFarmBlockAsync(TbFarmBlock entity);
        Task DeleteFarmBlockAsync(TbFarmBlock entity);
        Task<IEnumerable<TbFarmBlock>> GetFarmBlockByCondition(Expression<Func<TbFarmBlock, bool>> expression);
    }
}

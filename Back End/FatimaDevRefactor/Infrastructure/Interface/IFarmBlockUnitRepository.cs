﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface IFarmBlockUnitRepository
    {
        Task<IEnumerable<TbFarmBlockUnit>> GetAllFarmBlockUnitsAsync();
        Task<TbFarmBlockUnit> GetFarmBlockUnitByIdAsync(long id);
        Task CreateFarmBlockUnitAsync(TbFarmBlockUnit entity);
        Task UpdateFarmBlockUnitAsync(TbFarmBlockUnit entity);
        Task DeleteFarmBlockUnitAsync(TbFarmBlockUnit entity);
        Task<IEnumerable<TbFarmBlockUnit>> GetFarmBlockUnitByCondition(Expression<Func<TbFarmBlockUnit, bool>> expression);
    }
}

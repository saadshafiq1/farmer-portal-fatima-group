﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface IFarmRepository
    {
        Task<IEnumerable<TbFarmInfo>> GetAllFarmsAsync();
        Task<TbFarmInfo> GetFarmByIdAsync(int FarmId);
        Task CreateFarmAsync(TbFarmInfo farmInfo);
        Task UpdateFarmAsync(TbFarmInfo farmInfo);
        Task DeleteFarmAsync(TbFarmInfo farmInfo);
        Task<IEnumerable<TbFarmInfo>> GetFarmByCondition(Expression<Func<TbFarmInfo, bool>> expression);
        Task<IEnumerable<TbFarmInfo>> GetFarmsByConditionForWeb(Expression<Func<TbFarmInfo, bool>> expression);
    }
}

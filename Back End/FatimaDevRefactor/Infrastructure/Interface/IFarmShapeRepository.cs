﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface IFarmShapeRepository
    {
        Task<IEnumerable<TbFarmShapes>> GetAllFarmShapesAsync();
        Task<TbFarmShapes> GetFarmShapeByIdAsync(long ownerId);
        Task CreateFarmShapeAsync(TbFarmShapes media);
        Task UpdateFarmShapeAsync(TbFarmShapes owner);
        Task DeleteFarmShapeAsync(TbFarmShapes media);
        Task<IEnumerable<TbFarmShapes>> GetFarmShapeByCondition(Expression<Func<TbFarmShapes, bool>> expression);
    }
}

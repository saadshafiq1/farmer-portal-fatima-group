﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface IFarmTypeRepository
    {
        Task<IEnumerable<TbFarmTypeCode>> GetAllFarmTypesAsync();
        Task<TbFarmTypeCode> GetFarmTypeByIdAsync(long ownerId);
        Task CreateFarmTypeAsync(TbFarmTypeCode media);
        Task UpdateFarmTypeAsync(TbFarmTypeCode owner);
        Task DeleteFarmTypeAsync(TbFarmTypeCode media);
    }
}

﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface IFarmUnitRepository
    {
        Task<IEnumerable<TbFarmUnit>> GetAllFarmUnitsAsync();
        Task<TbFarmUnit> GetFarmUnitByIdAsync(long farmunitid);
        Task CreateFarmUnitAsync(TbFarmUnit farmunitid);
        Task UpdateFarmUnitAsync(TbFarmUnit farmunitid);
        Task DeleteFarmUnitAsync(TbFarmUnit farmunitid);
        Task<IEnumerable<TbFarmUnit>> GetFarmUnitByCondition(Expression<Func<TbFarmUnit, bool>> expression);

        Task<IEnumerable<TbTubewellStatus>> TubeWell();
        Task<IEnumerable<TbTubewellTypeInfo>> TubeWellType();
        Task<IEnumerable<TbWatercourseType>> WaterCourse();
        Task<IEnumerable<TbTunnelTypeInfo>> Tunnel();
        Task<IEnumerable<TbInputPurchaseInfo>> InputPurchase();
        Task<IEnumerable<TbOutputExportInfo>> OutputExport();
    }
}

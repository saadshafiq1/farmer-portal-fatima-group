﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Infrastructure.Models;

namespace Infrastructure.Interface
{
   public interface IFarmerCropPlanRepository
    {
        Task<IEnumerable<TbFarmerCropPlan>> GetAllFarmerCropPlanAsync();
        Task CreateFarmerCropPlanAsync(TbFarmerCropPlan crop);
        Task UpdateFarmerCropPlanAsync(TbFarmerCropPlan crop);
        Task DeleteFarmerCropPlanAsync(TbFarmerCropPlan crop);
        Task<IEnumerable<TbFarmerCropPlan>> GetFarmerCropPlanByCondition(Expression<Func<TbFarmerCropPlan, bool>> expression);
        Task<IEnumerable<TbFarmerCropPlan>> GetFarmerCropPlanByCondition(Expression<Func<TbFarmerCropPlan, bool>> expression, Expression<Func<TbFarmerCropPlan, object>> []includes);
    }
}

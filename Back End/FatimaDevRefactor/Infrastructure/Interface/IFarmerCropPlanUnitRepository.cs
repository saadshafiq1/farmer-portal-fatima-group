﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface IFarmerCropPlanUnitRepository
    {
        Task<IEnumerable<TbFarmerCropPlanUnit>> GetAllFarmerCropPlanUnitAsync();
        Task<TbFarmerCropPlanUnit> GetFarmerCropPlanUnitByIdAsync(long cropId);
        Task CreateFarmerCropPlanUnitAsync(TbFarmerCropPlanUnit crop);
        Task UpdateFarmerCropPlanUnitAsync(TbFarmerCropPlanUnit crop);
        Task DeleteFarmerCropPlanUnitAsync(TbFarmerCropPlanUnit crop);
        Task<IEnumerable<TbFarmerCropPlanUnit>> GetFarmerCropPlanUnitByCondition(Expression<Func<TbFarmerCropPlanUnit, bool>> expression);
    }
}
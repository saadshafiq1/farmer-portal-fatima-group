﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface IFarmerMediaRepository
    {
        Task<IEnumerable<TbFarmerMedia>> GetAllTbMediasAsync();
        Task<TbFarmerMedia> GetMediaByIdAsync(long ownerId);
        Task CreateMediaAsync(TbFarmerMedia media);
        Task UpdateMediaAsync(TbFarmerMedia owner);
        Task DeleteMediaAsync(TbFarmerMedia media);
        Task<IEnumerable<TbFarmerMedia>> GetFarmerMediaByCondition(Expression<Func<TbFarmerMedia, bool>> expression);
    }
}
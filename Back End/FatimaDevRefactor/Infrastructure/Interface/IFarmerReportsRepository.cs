﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface IFarmerReportsRepository
    {
        Task<IEnumerable<TbFarmerReports>> GetAllFarmerReportsAsync(int pageNumber, int pageSize);
        Task<TbFarmerReports> GetFarmerReportByIdAsync(long ReportId);
        Task CreateFarmerReportAsync(TbFarmerReports FarmerReport);
        Task UpdateFarmerReportAsync(TbFarmerReports FarmerReport);
        Task DeleteFarmerReportAsync(TbFarmerReports FarmerReport);
        Task<IEnumerable<TbFarmerReports>> GetFarmerReportByCondition(Expression<Func<TbFarmerReports, bool>> expression);
    }
}

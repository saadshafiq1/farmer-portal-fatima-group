﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface IFarmererRepository
    {

        Task<IEnumerable<TbFarmerInfo>> GetAllFarmersAsync();
        Task<IEnumerable<TbFarmerStatus>> GetFarmerStatuses();
        Task<IEnumerable<TbFarmerInfo>> GetAllFarmerInfoAsync(int pageNumber, int pageSize, string filter = "", string vale = "");
        Task<IEnumerable<TbFarmerInfo>> GetAllFarmersCountAsync(string filter = "", string value = "");
        Task<TbFarmerInfo> GetFarmerByIdAsync(int FarmerID);
        Task CreateFarmerAsync(TbFarmerInfo farmerInfo);
        Task UpdateFarmerAsync(TbFarmerInfo farmerInfo);
        Task DeleteFarmerAsync(TbFarmerInfo farmerInfo);
        Task<IEnumerable<TbFarmerInfo>> GetFarmerByCondition(Expression<Func<TbFarmerInfo, bool>> expression);
        Task<IEnumerable<TbFarmerInfo>> GetFarmerByCondition(Expression<Func<TbFarmerInfo, bool>> expression, Expression<Func<TbFarmerInfo, object>>[] includes);
        Task<TbFarmerInfo> GetFarmerByCellPhone(Expression<Func<TbFarmerInfo, bool>> expression, Expression<Func<TbFarmerInfo, object>> []includes);
    }
}

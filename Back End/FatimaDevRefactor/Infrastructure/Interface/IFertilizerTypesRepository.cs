﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface IFertilizerTypesRepository
    {
        Task<IEnumerable<TbFertilizerTypes>> GetAllFertilizerTypesAsync();
        Task<TbFertilizerTypes> GetFertilizerTypeByIdAsync(long fertilizerTypeId);
        //Task<TbFertilizerType> GetTbFertilizerTypeWithDetailsAsync(Guid ownerId);
        Task CreateFertilizerTypeAsync(TbFertilizerTypes fertilizerType);
        Task UpdateFertilizerTypeAsync(TbFertilizerTypes fertilizerType);
        Task DeleteFertilizerTypeAsync(TbFertilizerTypes fertilizerType);
        Task<IEnumerable<TbFertilizerTypes>> GetFertilizerTypeByCondition(Expression<Func<TbFertilizerTypes, bool>> expression);
    }
}

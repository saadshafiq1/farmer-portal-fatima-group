﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface IMediaRepository
    {
        Task<IEnumerable<TbMedia>> GetAllTbMediasAsync();
        Task<IEnumerable<TbMedia>> GetAllTbMediasAsync(int pageNumber, int pageSize, string filter = "", string value = "");
        Task<IEnumerable<TbMedia>> GetAllTbMediasAsync(string filter = "", string value = "");
        Task<TbMedia> GetMediaByIdAsync(long ownerId);
        Task CreateMediaAsync(TbMedia media);
        Task UpdateMediaAsync(TbMedia owner);
        Task DeleteMediaAsync(TbMedia media);
        Task<IEnumerable<TbMedia>> GetMediaByCondition(Expression<Func<TbMedia, bool>> expression);
    }
}

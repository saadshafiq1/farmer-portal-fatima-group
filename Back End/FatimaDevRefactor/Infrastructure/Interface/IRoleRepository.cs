﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface IRoleRepository
    {
        Task<IEnumerable<TbUserRoleTypeCode>> GetAllRolesAsync();
        Task<TbUserRoleTypeCode> GetRoleByIdAsync(int ownerId);
        Task CreateRoleAsync(TbUserRoleTypeCode user);
        Task UpdateRoleAsync(TbUserRoleTypeCode user);
        Task DeleteRoleAsync(TbUserRoleTypeCode user);
        Task<IEnumerable<TbUserRoleTypeCode>> GetRoleByCondition(Expression<Func<TbUserRoleTypeCode, bool>> expression);
    }
}

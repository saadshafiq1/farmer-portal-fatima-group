﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface ISeedTypesRepository
    {
        Task<IEnumerable<TbSeedTypes>> GetAllSeedTypesAsync();
        Task<TbSeedTypes> GetSeedTypeByIdAsync(long seedTypeId);
        Task CreateSeedTypeAsync(TbSeedTypes SeedType);
        Task UpdateSeedTypeAsync(TbSeedTypes SeedType);
        Task DeleteSeedTypeAsync(TbSeedTypes SeedType);
        Task<IEnumerable<TbSeedTypes>> GetSeedTypeByCondition(Expression<Func<TbSeedTypes, bool>> expression);
    }
}

﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
   public  interface ITicketRepository
    {
        Task<IEnumerable<TbTicketInfo>> GetAllTicketsAsync();
        Task<TbTicketInfo> GetTicketByIdAsync(long ticketId);
        Task<TbTicketInfo> GetTicketInfoById(long id);
        Task CreateTicketAsync(TbTicketInfo ticket);
        Task UpdateTicketAsync(TbTicketInfo ticket);
        Task DeleteTicketAsync(TbTicketInfo ticket);
        Task<IEnumerable<TbTicketInfo>> GetTicketByCondition(Expression<Func<TbTicketInfo, bool>> expression);
        Task<IEnumerable<TbTicketInfo>> GetTicketByCondition(Expression<Func<TbTicketInfo, bool>> expression, Expression<Func<TbTicketInfo, object>>[] includes);
        Task<IEnumerable<TbTicketInfo>> GetAllTicketInfoAsync(int pageNumber, int pageSize, string filter = "", string value = "");       
        Task<IEnumerable<TbTicketInfo>> GetAllTicketCountAsync(string filter = "", string value = "");
        Task<IEnumerable<TbTicketInfo>> GetFarmerTicketInfoAsync(int pageNumber, int pageSize, int farmerid, string filter = "", string value = "");
        Task<IEnumerable<TbTicketInfo>> GetFarmerTicketCountAsync(int farmerid, string filter = "", string value = "");
        Task<IEnumerable<TbTicketInfo>> GetTSOTicketInfoAsync(int pageNumber, int pageSize, int farmerid, string filter = "", string value = "");
        Task<IEnumerable<TbTicketInfo>> GetTSOTicketCountAsync(int farmerid, string filter = "", string value = "");

    }
}

﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface ITsoRepository
    {
        Task<TbTsoInfo> GetTSOById(Expression<Func<TbTsoInfo, bool>> expression);
        Task<IEnumerable<TbTsoInfo>> GetAllTSOsAsync(Expression<Func<TbTsoInfo, object>>[] includes);
        Task<TbTsoInfo> GetTSOByCellPhone(Expression<Func<TbTsoInfo, bool>> expression,Expression<Func<TbTsoInfo, object>>[] includes);
    }
}

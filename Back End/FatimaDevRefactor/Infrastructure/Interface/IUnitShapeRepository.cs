﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface IUnitShapeRepository
    {
        Task<IEnumerable<TbUnitShapes>> GetAllUnitShapesAsync();
        Task<TbUnitShapes> GetUnitShapeByIdAsync(long unitid);
        Task CreateUnitShapeAsync(TbUnitShapes unitshape);
        Task UpdateUnitShapeAsync(TbUnitShapes unitshape);
        Task DeleteUnitShapeAsync(TbUnitShapes unitshape);
        Task<IEnumerable<TbUnitShapes>> GetUnitShapeByCondition(Expression<Func<TbUnitShapes, bool>> expression);
        Task<IEnumerable<TbUnitShapes>> GetUnitShapeByCondition(Expression<Func<TbUnitShapes, bool>> expression, Expression<Func<TbUnitShapes, object>>[] includes);
    }
}

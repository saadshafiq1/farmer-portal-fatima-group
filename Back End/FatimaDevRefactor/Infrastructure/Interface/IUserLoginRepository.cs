﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface IUserLoginRepository
    {
        Task<IEnumerable<TbUserLogin>> GetAllUsersAsync();
        Task<TbUserLogin> GetUserByIdAsync(Guid ownerId);
        Task CreateUserAsync(TbUserLogin user);
        Task UpdateUserAsync(TbUserLogin user);
        Task DeleteUserAsync(TbUserLogin user);
        Task<IEnumerable<TbUserLogin>> GetUserByCondition(Expression<Func<TbUserLogin, bool>> expression);
    }
}
﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interface
{
    public interface IUserRoleTypeCodeRepository
    {
        Task<IEnumerable<TbUserRoleTypeCode>> GetAllUserRoleTypeCodesAsync();
        Task CreateUserRoleTypeCodeAsync(TbUserRoleTypeCode userRoleTypeCode);
        Task UpdateUserRoleTypeCodeAsync(TbUserRoleTypeCode userRoleTypeCode);
        Task DeleteUserRoleTypeCodeAsync(TbUserRoleTypeCode userRoleTypeCode);
        Task<IEnumerable<TbUserRoleTypeCode>> GetUserRoleTypeCodeByCondition(Expression<Func<TbUserRoleTypeCode, bool>> expression);
    }
}
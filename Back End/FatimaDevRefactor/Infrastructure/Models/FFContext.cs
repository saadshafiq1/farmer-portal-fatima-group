﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Infrastructure.Models
{
    public partial class FFContext : DbContext
    {
        public FFContext()
        {
        }

        public FFContext(DbContextOptions<FFContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TbAimsCommodity> TbAimsCommodity { get; set; }
        public virtual DbSet<TbAimsCommodityCity> TbAimsCommodityCity { get; set; }
        public virtual DbSet<TbBlockCrops> TbBlockCrops { get; set; }
        public virtual DbSet<TbCallCenterAgentInfo> TbCallCenterAgentInfo { get; set; }
        public virtual DbSet<TbCropCalenderMain> TbCropCalenderMain { get; set; }
        public virtual DbSet<TbCropCaseMain> TbCropCaseMain { get; set; }
        public virtual DbSet<TbCropStepAction> TbCropStepAction { get; set; }
        public virtual DbSet<TbCropSteps> TbCropSteps { get; set; }
        public virtual DbSet<TbCropTypeCode> TbCropTypeCode { get; set; }
        public virtual DbSet<TbDealerCategory> TbDealerCategory { get; set; }
        public virtual DbSet<TbDealerFertilizerDetails> TbDealerFertilizerDetails { get; set; }
        public virtual DbSet<TbDealerInfo> TbDealerInfo { get; set; }
        public virtual DbSet<TbDealerSeedDetails> TbDealerSeedDetails { get; set; }
        public virtual DbSet<TbDealerStatus> TbDealerStatus { get; set; }
        public virtual DbSet<TbDealerTypeCode> TbDealerTypeCode { get; set; }
        public virtual DbSet<TbDistrictCode> TbDistrictCode { get; set; }
        public virtual DbSet<TbEducationLevelCode> TbEducationLevelCode { get; set; }
        public virtual DbSet<TbFarmBlock> TbFarmBlock { get; set; }
        public virtual DbSet<TbFarmBlockUnit> TbFarmBlockUnit { get; set; }
        public virtual DbSet<TbFarmerCropCalender> TbFarmerCropCalender { get; set; }
        public virtual DbSet<TbFarmerCropCalenderDetails> TbFarmerCropCalenderDetails { get; set; }
        public virtual DbSet<TbFarmerCropPlan> TbFarmerCropPlan { get; set; }
        public virtual DbSet<TbFarmerCropPlanUnit> TbFarmerCropPlanUnit { get; set; }
        public virtual DbSet<TbFarmerInfo> TbFarmerInfo { get; set; }
        public virtual DbSet<TbFarmerMedia> TbFarmerMedia { get; set; }
        public virtual DbSet<TbFarmerReports> TbFarmerReports { get; set; }
        public virtual DbSet<TbFarmerStatus> TbFarmerStatus { get; set; }
        public virtual DbSet<TbFarmInfo> TbFarmInfo { get; set; }
        public virtual DbSet<TbFarmShapes> TbFarmShapes { get; set; }
        public virtual DbSet<TbFarmTypeCode> TbFarmTypeCode { get; set; }
        public virtual DbSet<TbFarmUnit> TbFarmUnit { get; set; }
        public virtual DbSet<TbFertilizerTypeDetails> TbFertilizerTypeDetails { get; set; }
        public virtual DbSet<TbFertilizerTypes> TbFertilizerTypes { get; set; }
        public virtual DbSet<TbInputPurchaseInfo> TbInputPurchaseInfo { get; set; }
        public virtual DbSet<TbMarketInfo> TbMarketInfo { get; set; }
        public virtual DbSet<TbMarketRate> TbMarketRate { get; set; }
        public virtual DbSet<TbMedia> TbMedia { get; set; }
        public virtual DbSet<TbMediaType> TbMediaType { get; set; }
        public virtual DbSet<TbOutputExportInfo> TbOutputExportInfo { get; set; }
        public virtual DbSet<TbPesticideTypes> TbPesticideTypes { get; set; }
        public virtual DbSet<TbPortfolioTypeCode> TbPortfolioTypeCode { get; set; }
        public virtual DbSet<TbProvinceCode> TbProvinceCode { get; set; }
        public virtual DbSet<TbRegionCode> TbRegionCode { get; set; }
        public virtual DbSet<TbRequestDetails> TbRequestDetails { get; set; }
        public virtual DbSet<TbRequestHeader> TbRequestHeader { get; set; }
        public virtual DbSet<TbRequestStatusCode> TbRequestStatusCode { get; set; }
        public virtual DbSet<TbRequestTypeCode> TbRequestTypeCode { get; set; }
        public virtual DbSet<TbSalepointCode> TbSalepointCode { get; set; }
        public virtual DbSet<TbSeedTypes> TbSeedTypes { get; set; }
        public virtual DbSet<TbTehsilCode> TbTehsilCode { get; set; }
        public virtual DbSet<TbTicketActivity> TbTicketActivity { get; set; }
        public virtual DbSet<TbTicketInfo> TbTicketInfo { get; set; }
        public virtual DbSet<TbTicketsStatus> TbTicketsStatus { get; set; }
        public virtual DbSet<TbTicketsTypes> TbTicketsTypes { get; set; }
        public virtual DbSet<TbTsoInfo> TbTsoInfo { get; set; }
        public virtual DbSet<TbTubewellStatus> TbTubewellStatus { get; set; }
        public virtual DbSet<TbTubewellTypeInfo> TbTubewellTypeInfo { get; set; }
        public virtual DbSet<TbTunnelTypeInfo> TbTunnelTypeInfo { get; set; }
        public virtual DbSet<TbUnitShapes> TbUnitShapes { get; set; }
        public virtual DbSet<TbUserLogin> TbUserLogin { get; set; }
        public virtual DbSet<TbUserRoleTypeCode> TbUserRoleTypeCode { get; set; }
        public virtual DbSet<TbWatercourseType> TbWatercourseType { get; set; }
        public virtual DbSet<TbWaterSoilResult> TbWaterSoilResult { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=119.63.133.166\\;Database=FF;Persist Security Info=False;User ID=ff;Password=Musab@1234;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TbAimsCommodity>(entity =>
            {
                entity.ToTable("TB_AIMS_COMMODITY");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CommodityCityId).HasColumnName("CommodityCityID");

                entity.Property(e => e.CommodityId).HasColumnName("CommodityID");

                entity.Property(e => e.CommodityName).HasMaxLength(250);

                entity.Property(e => e.DateInserted)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateUpdated)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Fqp)
                    .HasColumnName("FQP")
                    .HasMaxLength(50);

                entity.Property(e => e.Max)
                    .HasColumnName("MAX")
                    .HasMaxLength(50);

                entity.Property(e => e.Min)
                    .HasColumnName("MIN")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TbAimsCommodityCity>(entity =>
            {
                entity.ToTable("TB_AIMS_COMMODITY_CITY");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.CityId).HasColumnName("CityID");

                entity.Property(e => e.CityName).HasMaxLength(150);

                entity.Property(e => e.DateInserted)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateUpdated)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<TbBlockCrops>(entity =>
            {
                entity.HasKey(e => e.FarmCropId);

                entity.ToTable("TB_BLOCK_CROPS");

                entity.Property(e => e.FarmCropId).HasColumnName("FarmCropID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.CropArea).HasColumnType("decimal(8, 2)");

                entity.Property(e => e.FarmBlockId).HasColumnName("FarmBlockID");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.WaterSoilResultId).HasColumnName("WaterSoilResultID");

                entity.HasOne(d => d.CropCodeNavigation)
                    .WithMany(p => p.TbBlockCrops)
                    .HasForeignKey(d => d.CropCode)
                    .HasConstraintName("FK__TB_BLOCK___CropC__7226EDCC");

                entity.HasOne(d => d.FarmBlock)
                    .WithMany(p => p.TbBlockCrops)
                    .HasForeignKey(d => d.FarmBlockId)
                    .HasConstraintName("FK__TB_BLOCK___FarmB__740F363E");
            });

            modelBuilder.Entity<TbCallCenterAgentInfo>(entity =>
            {
                entity.HasKey(e => e.CallCenterAgentId);

                entity.ToTable("TB_CALL_CENTER_AGENT_INFO");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.AgentName).HasMaxLength(500);

                entity.Property(e => e.AlternateCellPhone).HasMaxLength(20);

                entity.Property(e => e.CellPhone).HasMaxLength(20);

                entity.Property(e => e.Cnic)
                    .HasColumnName("CNIC")
                    .HasMaxLength(20);

                entity.Property(e => e.Email).HasMaxLength(500);

                entity.Property(e => e.Gender).HasMaxLength(20);

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Landline).HasMaxLength(20);

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PermanentAddress).HasMaxLength(1000);

                entity.Property(e => e.Tsoimage).HasColumnName("TSOImage");

                entity.HasOne(d => d.DistrictCodeNavigation)
                    .WithMany(p => p.TbCallCenterAgentInfo)
                    .HasForeignKey(d => d.DistrictCode)
                    .HasConstraintName("FK_TB_CALL_CENTER_AGENT_INFO_TB_DISTRICT_CODE");

                entity.HasOne(d => d.TehsilCodeNavigation)
                    .WithMany(p => p.TbCallCenterAgentInfo)
                    .HasForeignKey(d => d.TehsilCode)
                    .HasConstraintName("FK_TB_CALL_CENTER_AGENT_INFO_TB_TEHSIL_CODE");
            });

            modelBuilder.Entity<TbCropCalenderMain>(entity =>
            {
                entity.HasKey(e => e.CalenderCode);

                entity.ToTable("TB_CROP_CALENDER_MAIN");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDateTime).HasColumnType("datetime");

                entity.Property(e => e.StepEndDate).HasColumnType("datetime");

                entity.Property(e => e.StepStartDate).HasColumnType("datetime");

                entity.HasOne(d => d.CropCaseCodeNavigation)
                    .WithMany(p => p.TbCropCalenderMain)
                    .HasForeignKey(d => d.CropCaseCode)
                    .HasConstraintName("FK__TB_CROP_C__CropC__75F77EB0");

                entity.HasOne(d => d.CropStepCodeNavigation)
                    .WithMany(p => p.TbCropCalenderMain)
                    .HasForeignKey(d => d.CropStepCode)
                    .HasConstraintName("FK__TB_CROP_C__CropS__77DFC722");
            });

            modelBuilder.Entity<TbCropCaseMain>(entity =>
            {
                entity.HasKey(e => e.CropCaseCode);

                entity.ToTable("TB_CROP_CASE_MAIN");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.SowingBeginDate).HasColumnType("datetime");

                entity.Property(e => e.SowingEndDate).HasColumnType("datetime");

                entity.HasOne(d => d.CropCodeNavigation)
                    .WithMany(p => p.TbCropCaseMain)
                    .HasForeignKey(d => d.CropCode)
                    .HasConstraintName("FK__TB_CROP_C__CropC__79C80F94");
            });

            modelBuilder.Entity<TbCropStepAction>(entity =>
            {
                entity.HasKey(e => e.CropStepActionCode);

                entity.ToTable("TB_CROP_STEP_ACTION");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.Detail).HasMaxLength(200);

                entity.Property(e => e.InsertionDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.HasOne(d => d.CropStepCodeNavigation)
                    .WithMany(p => p.TbCropStepAction)
                    .HasForeignKey(d => d.CropStepCode)
                    .HasConstraintName("FK__TB_CROP_S__CropS__7BB05806");
            });

            modelBuilder.Entity<TbCropSteps>(entity =>
            {
                entity.HasKey(e => e.CropStepCode);

                entity.ToTable("TB_CROP_STEPS");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.AudioUrl).HasMaxLength(100);

                entity.Property(e => e.InsertionDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.StepName).HasMaxLength(50);

                entity.Property(e => e.StepNameUrder).HasMaxLength(50);

                entity.Property(e => e.VideoUrl).HasMaxLength(100);

                entity.HasOne(d => d.CropCodeNavigation)
                    .WithMany(p => p.TbCropSteps)
                    .HasForeignKey(d => d.CropCode)
                    .HasConstraintName("FK__TB_CROP_S__CropC__7D98A078");
            });

            modelBuilder.Entity<TbCropTypeCode>(entity =>
            {
                entity.HasKey(e => e.CropCode);

                entity.ToTable("TB_CROP_TYPE_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.CropName).HasMaxLength(500);

                entity.Property(e => e.CropNameUrdu).HasMaxLength(500);

                entity.Property(e => e.CropSeason).HasMaxLength(20);

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<TbDealerCategory>(entity =>
            {
                entity.HasKey(e => e.DealerCategoryId);

                entity.ToTable("TB_DEALER_CATEGORY");

                entity.Property(e => e.DealerCategoryId).HasColumnName("DealerCategoryID");

                entity.Property(e => e.CategoryName).HasMaxLength(100);
            });

            modelBuilder.Entity<TbDealerFertilizerDetails>(entity =>
            {
                entity.HasKey(e => e.DealerDetailId);

                entity.ToTable("TB_DEALER_FERTILIZER_DETAILS");

                entity.Property(e => e.DealerDetailId).HasColumnName("DealerDetailID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.AvailableFertilizerId).HasColumnName("AvailableFertilizerID");

                entity.Property(e => e.DealerId).HasColumnName("DealerID");

                entity.Property(e => e.DealerTypeCodeId).HasColumnName("DealerTypeCodeID");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.AvailableFertilizer)
                    .WithMany(p => p.TbDealerFertilizerDetails)
                    .HasForeignKey(d => d.AvailableFertilizerId)
                    .HasConstraintName("FK_TB_DEALER_FERTILIZER_DETAILS_TB_FERTILIZER_TYPES");
            });

            modelBuilder.Entity<TbDealerInfo>(entity =>
            {
                entity.HasKey(e => e.DealerId);

                entity.ToTable("TB_DEALER_INFO");

                entity.Property(e => e.DealerId).HasColumnName("DealerID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.Address).HasMaxLength(1000);

                entity.Property(e => e.Ccnumber).HasColumnName("CCNumber");

                entity.Property(e => e.CellPhone).HasMaxLength(20);

                entity.Property(e => e.CityVillage).HasMaxLength(200);

                entity.Property(e => e.Cnic)
                    .HasColumnName("CNIC")
                    .HasMaxLength(20);

                entity.Property(e => e.CurrentStatus).HasMaxLength(20);

                entity.Property(e => e.DealerName).HasMaxLength(500);

                entity.Property(e => e.DealerNameUrdu).HasMaxLength(500);

                entity.Property(e => e.Email).HasMaxLength(150);

                entity.Property(e => e.Gender).HasMaxLength(20);

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Landline).HasMaxLength(20);

                entity.Property(e => e.Latitude).HasMaxLength(200);

                entity.Property(e => e.Longitude).HasMaxLength(200);

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Ntn)
                    .HasColumnName("NTN")
                    .HasMaxLength(20);

                entity.Property(e => e.Proprieter).HasMaxLength(200);

                entity.Property(e => e.SherpCc).HasColumnName("SHERP CC");

                entity.Property(e => e.Strn)
                    .HasColumnName("STRN")
                    .HasMaxLength(20);

                entity.Property(e => e.TaxProfileDate).HasColumnType("datetime");

                entity.HasOne(d => d.DealerCategoryNavigation)
                    .WithMany(p => p.TbDealerInfo)
                    .HasForeignKey(d => d.DealerCategory)
                    .HasConstraintName("FK_TB_DEALER_INFO_TB_DEALER_CATEGORY");

                entity.HasOne(d => d.DealerStatusNavigation)
                    .WithMany(p => p.TbDealerInfo)
                    .HasForeignKey(d => d.DealerStatus)
                    .HasConstraintName("FK_TB_DEALER_INFO_TB_DEALER_STATUS");

                entity.HasOne(d => d.DealerTypeCodeNavigation)
                    .WithMany(p => p.TbDealerInfo)
                    .HasForeignKey(d => d.DealerTypeCode)
                    .HasConstraintName("FK_TB_DEALER_INFO_TB_DEALER_TYPE_CODE");

                entity.HasOne(d => d.DistrictCodeNavigation)
                    .WithMany(p => p.TbDealerInfo)
                    .HasForeignKey(d => d.DistrictCode)
                    .HasConstraintName("FK_TB_DEALER_INFO_TB_DISTRICT_CODE");

                entity.HasOne(d => d.ProvinceNavigation)
                    .WithMany(p => p.TbDealerInfo)
                    .HasForeignKey(d => d.Province)
                    .HasConstraintName("FK_TB_DEALER_INFO_TB_PROVINCE_CODE");

                entity.HasOne(d => d.SalePointNavigation)
                    .WithMany(p => p.TbDealerInfo)
                    .HasForeignKey(d => d.SalePoint)
                    .HasConstraintName("FK_TB_DEALER_INFO_TB_SALEPOINT_CODE");

                entity.HasOne(d => d.SaleRegionNavigation)
                    .WithMany(p => p.TbDealerInfo)
                    .HasForeignKey(d => d.SaleRegion)
                    .HasConstraintName("FK_TB_DEALER_INFO_TB_REGION_CODE");

                entity.HasOne(d => d.TehsilCodeNavigation)
                    .WithMany(p => p.TbDealerInfo)
                    .HasForeignKey(d => d.TehsilCode)
                    .HasConstraintName("FK_TB_DEALER_INFO_TB_TEHSIL_CODE");
            });

            modelBuilder.Entity<TbDealerSeedDetails>(entity =>
            {
                entity.HasKey(e => e.DealerDetailId);

                entity.ToTable("TB_DEALER_SEED_DETAILS");

                entity.Property(e => e.DealerDetailId).HasColumnName("DealerDetailID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.AvailableSeedId).HasColumnName("AvailableSeedID");

                entity.Property(e => e.DealerId).HasColumnName("DealerID");

                entity.Property(e => e.DealerTypeCodeId).HasColumnName("DealerTypeCodeID");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.AvailableSeed)
                    .WithMany(p => p.TbDealerSeedDetails)
                    .HasForeignKey(d => d.AvailableSeedId)
                    .HasConstraintName("FK_TB_DEALER_SEED_DETAILS_TB_SEED_TYPES");
            });

            modelBuilder.Entity<TbDealerStatus>(entity =>
            {
                entity.HasKey(e => e.DealerStatusId);

                entity.ToTable("TB_DEALER_STATUS");

                entity.Property(e => e.DealerStatusId).HasColumnName("DealerStatusID");

                entity.Property(e => e.Status).HasMaxLength(100);
            });

            modelBuilder.Entity<TbDealerTypeCode>(entity =>
            {
                entity.HasKey(e => e.DealerTypeCodeId);

                entity.ToTable("TB_DEALER_TYPE_CODE");

                entity.Property(e => e.DealerTypeCodeId).HasColumnName("DealerTypeCodeID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.TypeName).HasMaxLength(500);

                entity.Property(e => e.TypeNameUrdu).HasMaxLength(500);
            });

            modelBuilder.Entity<TbDistrictCode>(entity =>
            {
                entity.HasKey(e => e.DistrictCode);

                entity.ToTable("TB_DISTRICT_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.DistrictName).HasMaxLength(500);

                entity.Property(e => e.DistrictNameUrdu).HasMaxLength(500);

                entity.Property(e => e.GovernmentDistrict)
                    .HasMaxLength(1)
                    .HasDefaultValueSql("('Y')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.SaleDistrict)
                    .HasMaxLength(1)
                    .HasDefaultValueSql("('Y')");

                entity.HasOne(d => d.ProvinceCodeNavigation)
                    .WithMany(p => p.TbDistrictCode)
                    .HasForeignKey(d => d.ProvinceCode)
                    .HasConstraintName("FK__TB_DISTRI__Provi__7F80E8EA");
            });

            modelBuilder.Entity<TbEducationLevelCode>(entity =>
            {
                entity.HasKey(e => e.EducationCode);

                entity.ToTable("TB_EDUCATION_LEVEL_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.EducationLevelName).HasMaxLength(500);

                entity.Property(e => e.EducationLevelNameUrdu).HasMaxLength(500);

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<TbFarmBlock>(entity =>
            {
                entity.HasKey(e => e.FarmBlockId);

                entity.ToTable("TB_FARM_BLOCK");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.BlockArea).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BlockName).HasMaxLength(100);

                entity.Property(e => e.FarmGis).HasColumnName("FarmGIS");

                entity.Property(e => e.InsertionDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDateTime).HasColumnType("datetime");

                entity.Property(e => e.WaterSoilResultId).HasColumnName("WaterSoilResultID");

                entity.HasOne(d => d.Farm)
                    .WithMany(p => p.TbFarmBlock)
                    .HasForeignKey(d => d.FarmId)
                    .HasConstraintName("FK__TB_FARM_B__FarmI__7720AD13");

                entity.HasOne(d => d.WaterSoilResult)
                    .WithMany(p => p.TbFarmBlock)
                    .HasForeignKey(d => d.WaterSoilResultId)
                    .HasConstraintName("FK__TB_FARM_B__Water__7814D14C");
            });

            modelBuilder.Entity<TbFarmBlockUnit>(entity =>
            {
                entity.HasKey(e => e.FarmBlockUnitId);

                entity.ToTable("TB_FARM_BLOCK_UNIT");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDateTime).HasColumnType("datetime");

                entity.HasOne(d => d.FarmBlock)
                    .WithMany(p => p.TbFarmBlockUnit)
                    .HasForeignKey(d => d.FarmBlockId)
                    .HasConstraintName("FK__TB_FARM_B__FarmB__7908F585");

                entity.HasOne(d => d.FarmUnit)
                    .WithMany(p => p.TbFarmBlockUnit)
                    .HasForeignKey(d => d.FarmUnitId)
                    .HasConstraintName("FK__TB_FARM_B__FarmU__7D63964E");
            });

            modelBuilder.Entity<TbFarmerCropCalender>(entity =>
            {
                entity.HasKey(e => e.FarmerCropCalenderCode);

                entity.ToTable("TB_FARMER_CROP_CALENDER");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.FarmId).HasColumnName("FarmID");

                entity.Property(e => e.FarmerId).HasColumnName("FarmerID");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.SowingDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TbFarmerCropCalenderDetails>(entity =>
            {
                entity.HasKey(e => e.FarmerCropCalenderId);

                entity.ToTable("TB_FARMER_CROP_CALENDER_DETAILS");

                entity.Property(e => e.FarmerCropCalenderId).HasColumnName("FarmerCropCalenderID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.FarmId).HasColumnName("FarmID");

                entity.Property(e => e.FarmerId).HasColumnName("FarmerID");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PhaseDetails).HasMaxLength(1000);

                entity.Property(e => e.PhaseName).HasMaxLength(500);
            });

            modelBuilder.Entity<TbFarmerCropPlan>(entity =>
            {
                entity.HasKey(e => e.FarmerCropPlanId);

                entity.ToTable("TB_FARMER_CROP_PLAN");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.SowingDate).HasColumnType("datetime");

                entity.HasOne(d => d.CropCaseCodeNavigation)
                    .WithMany(p => p.TbFarmerCropPlan)
                    .HasForeignKey(d => d.CropCaseCode)
                    .HasConstraintName("FK_TB_FARMER_CROP_PLAN_TB_CROP_CASE_MAIN");
            });

            modelBuilder.Entity<TbFarmerCropPlanUnit>(entity =>
            {
                entity.HasKey(e => e.FarmerCropPlanUnitId);

                entity.ToTable("TB_FARMER_CROP_PLAN_UNIT");

                entity.Property(e => e.ActiveStatus)
                    .HasColumnName("ActiveSTatus")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.HasOne(d => d.FarmUnit)
                    .WithMany(p => p.TbFarmerCropPlanUnit)
                    .HasForeignKey(d => d.FarmUnitId)
                    .HasConstraintName("FK_TB_FARMER_CROP_PLAN_UNIT_TB_FARM_UNIT");

                entity.HasOne(d => d.FarmerCropPlan)
                    .WithMany(p => p.TbFarmerCropPlanUnit)
                    .HasForeignKey(d => d.FarmerCropPlanId)
                    .HasConstraintName("FK_TB_FARMER_CROP_PLAN_UNIT_TB_FARMER_CROP_PLAN");
            });

            modelBuilder.Entity<TbFarmerInfo>(entity =>
            {
                entity.HasKey(e => e.FarmerId);

                entity.ToTable("TB_FARMER_INFO");

                entity.Property(e => e.FarmerId).HasColumnName("FarmerID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.AdjacentFarmerFriendCellPhone).HasMaxLength(200);

                entity.Property(e => e.AdjacentFarmerFriendName).HasMaxLength(20);

                entity.Property(e => e.AlternateCellPhoneNo).HasMaxLength(20);

                entity.Property(e => e.AlternateName).HasMaxLength(500);

                entity.Property(e => e.AlternateRelationshipwithFarmer).HasMaxLength(500);

                entity.Property(e => e.CellPhone).HasMaxLength(20);

                entity.Property(e => e.Cnic)
                    .HasColumnName("CNIC")
                    .HasMaxLength(20);

                entity.Property(e => e.DateOfBirth).HasColumnType("date");

                entity.Property(e => e.FarmerName).HasMaxLength(500);

                entity.Property(e => e.FatherHusbandName).HasMaxLength(500);

                entity.Property(e => e.Gender).HasMaxLength(20);

                entity.Property(e => e.Hobbies).HasMaxLength(500);

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LandLine)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MozaName).HasMaxLength(500);

                entity.Property(e => e.OtherImplement).HasDefaultValueSql("((0))");

                entity.Property(e => e.PermanentAddress).HasMaxLength(1000);

                entity.Property(e => e.PresentAddress).HasMaxLength(1000);

                entity.Property(e => e.PresentUnionConcil).HasMaxLength(500);

                entity.Property(e => e.Tractor).HasDefaultValueSql("((0))");

                entity.Property(e => e.UnionCouncil).HasMaxLength(500);

                entity.HasOne(d => d.DistrictCodeNavigation)
                    .WithMany(p => p.TbFarmerInfoDistrictCodeNavigation)
                    .HasForeignKey(d => d.DistrictCode)
                    .HasConstraintName("FK_TB_FARMER_INFO_TB_DISTRICT_CODE");

                entity.HasOne(d => d.FarmerStatus)
                    .WithMany(p => p.TbFarmerInfo)
                    .HasForeignKey(d => d.FarmerStatusId)
                    .HasConstraintName("FK_TB_FARMER_INFO_TB_FARMER_STATUS");

                entity.HasOne(d => d.PresentDistrictCodeNavigation)
                    .WithMany(p => p.TbFarmerInfoPresentDistrictCodeNavigation)
                    .HasForeignKey(d => d.PresentDistrictCode)
                    .HasConstraintName("FK_TB_FARMER_INFO_TB_DISTRICT_CODE1");

                entity.HasOne(d => d.PresentProvinceCodeNavigation)
                    .WithMany(p => p.TbFarmerInfoPresentProvinceCodeNavigation)
                    .HasForeignKey(d => d.PresentProvinceCode)
                    .HasConstraintName("FK_TB_FARMER_INFO_TB_PROVINCE_CODE1");

                entity.HasOne(d => d.PresentRegionCodeNavigation)
                    .WithMany(p => p.TbFarmerInfoPresentRegionCodeNavigation)
                    .HasForeignKey(d => d.PresentRegionCode)
                    .HasConstraintName("FK_TB_FARMER_INFO_TB_REGION_CODE");

                entity.HasOne(d => d.PresentTehsilCodeNavigation)
                    .WithMany(p => p.TbFarmerInfoPresentTehsilCodeNavigation)
                    .HasForeignKey(d => d.PresentTehsilCode)
                    .HasConstraintName("FK_TB_FARMER_INFO_TB_TEHSIL_CODE");

                entity.HasOne(d => d.ProvinceCodeNavigation)
                    .WithMany(p => p.TbFarmerInfoProvinceCodeNavigation)
                    .HasForeignKey(d => d.ProvinceCode)
                    .HasConstraintName("FK_TB_FARMER_INFO_TB_PROVINCE_CODE");

                entity.HasOne(d => d.RegionCodeNavigation)
                    .WithMany(p => p.TbFarmerInfoRegionCodeNavigation)
                    .HasForeignKey(d => d.RegionCode)
                    .HasConstraintName("FK_TB_FARMER_INFO_TB_REGION_CODE1");

                entity.HasOne(d => d.TehsilCodeNavigation)
                    .WithMany(p => p.TbFarmerInfoTehsilCodeNavigation)
                    .HasForeignKey(d => d.TehsilCode)
                    .HasConstraintName("FK__TB_FARMER__Tehsi__7CD98669");
            });

            modelBuilder.Entity<TbFarmerMedia>(entity =>
            {
                entity.HasKey(e => e.FavouriteId);

                entity.ToTable("TB_FARMER_MEDIA");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Farmer)
                    .WithMany(p => p.TbFarmerMedia)
                    .HasForeignKey(d => d.FarmerId)
                    .HasConstraintName("FK_TB_FARMER_MEDIA_TB_FARMER_INFO");

                entity.HasOne(d => d.Media)
                    .WithMany(p => p.TbFarmerMedia)
                    .HasForeignKey(d => d.MediaId)
                    .HasConstraintName("FK_TB_FARMER_MEDIA_TB_MEDIA");
            });

            modelBuilder.Entity<TbFarmerReports>(entity =>
            {
                entity.HasKey(e => e.ReportId);

                entity.ToTable("TB_FARMER_REPORTS");

                entity.Property(e => e.ReportId).HasColumnName("ReportID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.FarmerId).HasColumnName("FarmerID");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ReportDate).HasColumnType("datetime");

                entity.Property(e => e.ReportName).HasMaxLength(150);

                entity.Property(e => e.Url)
                    .HasColumnName("URL")
                    .HasMaxLength(500);

                entity.HasOne(d => d.Farmer)
                    .WithMany(p => p.TbFarmerReports)
                    .HasForeignKey(d => d.FarmerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_FARMER_REPORTS_TB_FARMER_INFO");
            });

            modelBuilder.Entity<TbFarmerStatus>(entity =>
            {
                entity.HasKey(e => e.FarmerStatusId);

                entity.ToTable("TB_FARMER_STATUS");

                entity.Property(e => e.Description).HasMaxLength(100);

                entity.Property(e => e.FarmerStatueName)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<TbFarmInfo>(entity =>
            {
                entity.HasKey(e => e.FarmId);

                entity.ToTable("TB_FARM_INFO");

                entity.Property(e => e.FarmId)
                    .HasColumnName("FarmID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.AreaonHeis)
                    .HasColumnName("AreaonHEIS")
                    .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.ExportProduct).HasMaxLength(100);

                entity.Property(e => e.FarmAddress).HasMaxLength(1000);

                entity.Property(e => e.FarmGis).HasColumnName("FarmGIS");

                entity.Property(e => e.FarmName).HasMaxLength(500);

                entity.Property(e => e.FarmerId).HasColumnName("FarmerID");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IrrigationArea).HasMaxLength(20);

                entity.Property(e => e.IrrigationType).HasMaxLength(200);

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RegionId).HasColumnName("RegionID");

                entity.Property(e => e.TotalAreaAcres).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.Transport1).HasMaxLength(200);

                entity.Property(e => e.Transport2).HasMaxLength(200);

                entity.Property(e => e.Transport3).HasMaxLength(200);

                entity.HasOne(d => d.DistrictCodeNavigation)
                    .WithMany(p => p.TbFarmInfo)
                    .HasForeignKey(d => d.DistrictCode)
                    .HasConstraintName("FK_TB_FARM_INFO_TB_DISTRICT_CODE");

                entity.HasOne(d => d.Farm)
                    .WithOne(p => p.InverseFarm)
                    .HasForeignKey<TbFarmInfo>(d => d.FarmId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_FARM_INFO_TB_FARM_INFO");

                entity.HasOne(d => d.FarmTypeCodeNavigation)
                    .WithMany(p => p.TbFarmInfo)
                    .HasForeignKey(d => d.FarmTypeCode)
                    .HasConstraintName("FK__TB_FARM_I__FarmT__7BE56230");

                entity.HasOne(d => d.Farmer)
                    .WithMany(p => p.TbFarmInfo)
                    .HasForeignKey(d => d.FarmerId)
                    .HasConstraintName("FK__TB_FARM_I__Farme__7F4BDEC0");

                entity.HasOne(d => d.InputPurchaseNavigation)
                    .WithMany(p => p.TbFarmInfo)
                    .HasForeignKey(d => d.InputPurchase)
                    .HasConstraintName("FK_TB_FARM_INFO_TB_INPUT_PURCHASE_INFO");

                entity.HasOne(d => d.OutputExportNavigation)
                    .WithMany(p => p.TbFarmInfo)
                    .HasForeignKey(d => d.OutputExport)
                    .HasConstraintName("FK_TB_FARM_INFO_TB_OUTPUT_EXPORT_INFO");

                entity.HasOne(d => d.TubeWellStatusNavigation)
                    .WithMany(p => p.TbFarmInfo)
                    .HasForeignKey(d => d.TubeWellStatus)
                    .HasConstraintName("FK_TB_FARM_INFO_TB_TUBEWELL_STATUS");

                entity.HasOne(d => d.TubeWellTypeNavigation)
                    .WithMany(p => p.TbFarmInfo)
                    .HasForeignKey(d => d.TubeWellType)
                    .HasConstraintName("FK_TB_FARM_INFO_TB_TUBEWELL_TYPE_INFO");

                entity.HasOne(d => d.TunnelTypeNavigation)
                    .WithMany(p => p.TbFarmInfo)
                    .HasForeignKey(d => d.TunnelType)
                    .HasConstraintName("FK_TB_FARM_INFO_TB_TUNNEL_TYPE_INFO");

                entity.HasOne(d => d.WaterCourseNavigation)
                    .WithMany(p => p.TbFarmInfo)
                    .HasForeignKey(d => d.WaterCourse)
                    .HasConstraintName("FK_TB_FARM_INFO_TB_WATERCOURSE_TYPE");
            });

            modelBuilder.Entity<TbFarmShapes>(entity =>
            {
                entity.HasKey(e => e.FarmShapeId);

                entity.ToTable("TB_FARM_SHAPES");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDateTime).HasColumnType("datetime");

                entity.HasOne(d => d.Farm)
                    .WithMany(p => p.TbFarmShapes)
                    .HasForeignKey(d => d.FarmId)
                    .HasConstraintName("FK_TB_FARM_SHAPES_TB_FARM_INFO");
            });

            modelBuilder.Entity<TbFarmTypeCode>(entity =>
            {
                entity.HasKey(e => e.FarmTypeCode);

                entity.ToTable("TB_FARM_TYPE_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.TypeName).HasMaxLength(500);

                entity.Property(e => e.TypeNameUrdu).HasMaxLength(500);
            });

            modelBuilder.Entity<TbFarmUnit>(entity =>
            {
                entity.HasKey(e => e.FarmUnitId);

                entity.ToTable("TB_FARM_UNIT");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.Gis).HasColumnName("GIS");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UnitName).HasMaxLength(50);

                entity.HasOne(d => d.Farm)
                    .WithMany(p => p.TbFarmUnit)
                    .HasForeignKey(d => d.FarmId)
                    .HasConstraintName("FK_TB_FARM_UNIT_TB_FARM_INFO");
            });

            modelBuilder.Entity<TbFertilizerTypeDetails>(entity =>
            {
                entity.HasKey(e => e.DetailId);

                entity.ToTable("TB_FERTILIZER_TYPE_DETAILS");

                entity.Property(e => e.DetailId).HasColumnName("DetailID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.FertilizerId).HasColumnName("FertilizerID");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Fertilizer)
                    .WithMany(p => p.TbFertilizerTypeDetails)
                    .HasForeignKey(d => d.FertilizerId)
                    .HasConstraintName("FK_TB_FERTILIZER_TYPE_DETAILS_TB_FERTILIZER_TYPES");
            });

            modelBuilder.Entity<TbFertilizerTypes>(entity =>
            {
                entity.HasKey(e => e.FertilizerId);

                entity.ToTable("TB_FERTILIZER_TYPES");

                entity.Property(e => e.FertilizerId).HasColumnName("FertilizerID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.FertilizerName).HasMaxLength(500);

                entity.Property(e => e.FertilizerNameUrdu).HasMaxLength(500);

                entity.Property(e => e.FertilizerSeason).HasMaxLength(20);

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<TbInputPurchaseInfo>(entity =>
            {
                entity.HasKey(e => e.InputPurchaseId);

                entity.ToTable("TB_INPUT_PURCHASE_INFO");

                entity.Property(e => e.InputPurchaseId).ValueGeneratedNever();

                entity.Property(e => e.InputPurchaseDescription).HasMaxLength(200);

                entity.Property(e => e.InputPurchaseName)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<TbMarketInfo>(entity =>
            {
                entity.HasKey(e => e.MarketId);

                entity.ToTable("TB_MARKET_INFO");

                entity.Property(e => e.MarketId).HasColumnName("MarketID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MarketName).HasMaxLength(500);

                entity.Property(e => e.MarketNameUrdu).HasMaxLength(500);

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RoleName).HasMaxLength(500);

                entity.Property(e => e.RoleNameUrdu).HasMaxLength(500);
            });

            modelBuilder.Entity<TbMarketRate>(entity =>
            {
                entity.HasKey(e => e.MarketRateId);

                entity.ToTable("TB_MARKET_RATE");

                entity.Property(e => e.MarketRateId).HasColumnName("MarketRateID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.CropId).HasColumnName("CropID");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MarketId).HasColumnName("MarketID");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Rate).HasColumnType("decimal(7, 2)");

                entity.Property(e => e.RateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TbMedia>(entity =>
            {
                entity.HasKey(e => e.MediaId);

                entity.ToTable("TB_MEDIA");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.Description).HasMaxLength(300);

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Url)
                    .HasColumnName("URL")
                    .HasMaxLength(300);

                entity.HasOne(d => d.DistrictCodeNavigation)
                    .WithMany(p => p.TbMedia)
                    .HasForeignKey(d => d.DistrictCode)
                    .HasConstraintName("FK_TB_MEDIA_TB_DISTRICT_CODE");

                entity.HasOne(d => d.MediaType)
                    .WithMany(p => p.TbMedia)
                    .HasForeignKey(d => d.MediaTypeId)
                    .HasConstraintName("FK_TB_MEDIA_TB_MEDIA_TYPE");
            });

            modelBuilder.Entity<TbMediaType>(entity =>
            {
                entity.HasKey(e => e.MediaTypeId);

                entity.ToTable("TB_MEDIA_TYPE");

                entity.Property(e => e.Description).HasMaxLength(50);

                entity.Property(e => e.MediaType).HasMaxLength(50);
            });

            modelBuilder.Entity<TbOutputExportInfo>(entity =>
            {
                entity.HasKey(e => e.OutputExportId);

                entity.ToTable("TB_OUTPUT_EXPORT_INFO");

                entity.Property(e => e.OutputExportDescription).HasMaxLength(100);

                entity.Property(e => e.OutputExportName)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<TbPesticideTypes>(entity =>
            {
                entity.HasKey(e => e.PesticideId);

                entity.ToTable("TB_PESTICIDE_TYPES");

                entity.Property(e => e.PesticideId).HasColumnName("PesticideID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PesticideName).HasMaxLength(500);

                entity.Property(e => e.PesticideNameUrdu).HasMaxLength(500);

                entity.Property(e => e.PesticideSeason).HasMaxLength(20);
            });

            modelBuilder.Entity<TbPortfolioTypeCode>(entity =>
            {
                entity.HasKey(e => e.PortfolioCode);

                entity.ToTable("TB_PORTFOLIO_TYPE_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PortfolioName).HasMaxLength(500);

                entity.Property(e => e.PortfolioNameUrdu).HasMaxLength(500);
            });

            modelBuilder.Entity<TbProvinceCode>(entity =>
            {
                entity.HasKey(e => e.ProvinceCode);

                entity.ToTable("TB_PROVINCE_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ProvinceName).HasMaxLength(500);

                entity.Property(e => e.ProvinceNameUrdu).HasMaxLength(500);
            });

            modelBuilder.Entity<TbRegionCode>(entity =>
            {
                entity.HasKey(e => e.RegionCode);

                entity.ToTable("TB_REGION_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RegionName).HasMaxLength(500);

                entity.Property(e => e.RegionNameUrdu).HasMaxLength(500);

                entity.HasOne(d => d.ProvinceCodeNavigation)
                    .WithMany(p => p.TbRegionCode)
                    .HasForeignKey(d => d.ProvinceCode)
                    .HasConstraintName("FK_TB_REGION_CODE_TB_PROVINCE_CODE");
            });

            modelBuilder.Entity<TbRequestDetails>(entity =>
            {
                entity.HasKey(e => e.RequestId);

                entity.ToTable("TB_REQUEST_DETAILS");

                entity.Property(e => e.RequestId)
                    .HasColumnName("RequestID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RequestDetail).HasMaxLength(500);
            });

            modelBuilder.Entity<TbRequestHeader>(entity =>
            {
                entity.HasKey(e => e.RequestId);

                entity.ToTable("TB_REQUEST_HEADER");

                entity.Property(e => e.RequestId).HasColumnName("RequestID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.Details).HasMaxLength(1000);

                entity.Property(e => e.FarmId).HasColumnName("FarmID");

                entity.Property(e => e.FarmerId).HasColumnName("FarmerID");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<TbRequestStatusCode>(entity =>
            {
                entity.HasKey(e => e.RequestStatusCode);

                entity.ToTable("TB_REQUEST_STATUS_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RequestStatusName).HasMaxLength(500);

                entity.Property(e => e.RequestStatusNameUrdu).HasMaxLength(500);
            });

            modelBuilder.Entity<TbRequestTypeCode>(entity =>
            {
                entity.HasKey(e => e.RequestTypeCode);

                entity.ToTable("TB_REQUEST_TYPE_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RequestTypeName).HasMaxLength(500);

                entity.Property(e => e.RequestTypeNameUrdu).HasMaxLength(500);
            });

            modelBuilder.Entity<TbSalepointCode>(entity =>
            {
                entity.HasKey(e => e.SalePointCode);

                entity.ToTable("TB_SALEPOINT_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.SalePointName).HasMaxLength(500);

                entity.Property(e => e.SalePointNameUrdu).HasMaxLength(500);

                entity.HasOne(d => d.TehsilCodeNavigation)
                    .WithMany(p => p.TbSalepointCode)
                    .HasForeignKey(d => d.TehsilCode)
                    .HasConstraintName("FK_TB_SALEPOINT_CODE_TB_TEHSIL_CODE");
            });

            modelBuilder.Entity<TbSeedTypes>(entity =>
            {
                entity.HasKey(e => e.SeedId);

                entity.ToTable("TB_SEED_TYPES");

                entity.Property(e => e.SeedId).HasColumnName("SeedID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.SeedName).HasMaxLength(500);

                entity.Property(e => e.SeedNameUrdu).HasMaxLength(500);

                entity.Property(e => e.SeedSeason).HasMaxLength(20);
            });

            modelBuilder.Entity<TbTehsilCode>(entity =>
            {
                entity.HasKey(e => e.TehsilCode);

                entity.ToTable("TB_TEHSIL_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.TehsilName).HasMaxLength(500);

                entity.Property(e => e.TehsilNameUrdu).HasMaxLength(500);

                entity.HasOne(d => d.DistrictCodeNavigation)
                    .WithMany(p => p.TbTehsilCode)
                    .HasForeignKey(d => d.DistrictCode)
                    .HasConstraintName("FK_TB_TEHSIL_CODE_TB_DISTRICT_CODE");
            });

            modelBuilder.Entity<TbTicketActivity>(entity =>
            {
                entity.HasKey(e => e.ActivityId);

                entity.ToTable("TB_TICKET_ACTIVITY");

                entity.Property(e => e.AgentId).HasColumnName("AgentID");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Tsoid).HasColumnName("TSOId");

                entity.HasOne(d => d.Agent)
                    .WithMany(p => p.TbTicketActivity)
                    .HasForeignKey(d => d.AgentId)
                    .HasConstraintName("FK_TB_TICKET_ACTIVITY_TB_TICKET_ACTIVITY");

                entity.HasOne(d => d.Farmer)
                    .WithMany(p => p.TbTicketActivity)
                    .HasForeignKey(d => d.FarmerId)
                    .HasConstraintName("FK_TicketFarmer_Table_1");

                entity.HasOne(d => d.Ticket)
                    .WithMany(p => p.TbTicketActivity)
                    .HasForeignKey(d => d.TicketId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TicketTable_Table_1");

                entity.HasOne(d => d.Tso)
                    .WithMany(p => p.TbTicketActivity)
                    .HasForeignKey(d => d.Tsoid)
                    .HasConstraintName("FK_TB_TICKET_ACTIVITY_TB_TICKET_ACTIVITY1");
            });

            modelBuilder.Entity<TbTicketInfo>(entity =>
            {
                entity.HasKey(e => e.TicketId);

                entity.ToTable("TB_TICKET_INFO");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DueDate).HasColumnType("datetime");

                entity.Property(e => e.FeedbackAudioUrl).HasMaxLength(500);

                entity.Property(e => e.ResolvedDate).HasColumnType("datetime");

                entity.Property(e => e.Slastatus)
                    .HasColumnName("SLAStatus")
                    .HasMaxLength(200);

                entity.Property(e => e.StatusText).HasMaxLength(200);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.VisitDate).HasColumnType("date");

                entity.HasOne(d => d.Farm)
                    .WithMany(p => p.TbTicketInfo)
                    .HasForeignKey(d => d.FarmId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Farm_Table_1");

                entity.HasOne(d => d.Farmer)
                    .WithMany(p => p.TbTicketInfo)
                    .HasForeignKey(d => d.FarmerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Farmer_Table_1");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.TbTicketInfo)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_Status_Table_1");

                entity.HasOne(d => d.TicketType)
                    .WithMany(p => p.TbTicketInfo)
                    .HasForeignKey(d => d.TicketTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TB_TICKET_INFO_TB_TICKETS_TYPES");
            });

            modelBuilder.Entity<TbTicketsStatus>(entity =>
            {
                entity.HasKey(e => e.StatusId);

                entity.ToTable("TB_TICKETS_STATUS");

                entity.Property(e => e.StatusId).ValueGeneratedNever();

                entity.Property(e => e.StatusDescription).HasMaxLength(10);

                entity.Property(e => e.StatusName)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<TbTicketsTypes>(entity =>
            {
                entity.HasKey(e => e.TicketTypeId);

                entity.ToTable("TB_TICKETS_TYPES");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.TicketTypeDescription).HasMaxLength(200);

                entity.Property(e => e.TicketTypeName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.TicketTypeNameUrdu).HasMaxLength(500);
            });

            modelBuilder.Entity<TbTsoInfo>(entity =>
            {
                entity.HasKey(e => e.Tsoid);

                entity.ToTable("TB_TSO_INFO");

                entity.Property(e => e.Tsoid).HasColumnName("TSOID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.AlternateCellPhone).HasMaxLength(20);

                entity.Property(e => e.CellPhone).HasMaxLength(20);

                entity.Property(e => e.Cnic)
                    .HasColumnName("CNIC")
                    .HasMaxLength(20);

                entity.Property(e => e.Email).HasMaxLength(500);

                entity.Property(e => e.Gender).HasMaxLength(20);

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Landline).HasMaxLength(20);

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PermanentAddress).HasMaxLength(1000);

                entity.Property(e => e.PresentAddress).HasMaxLength(1000);

                entity.Property(e => e.Tsoimage).HasColumnName("TSOImage");

                entity.Property(e => e.Tsoname)
                    .HasColumnName("TSOName")
                    .HasMaxLength(500);

                entity.HasOne(d => d.RegionCodeNavigation)
                    .WithMany(p => p.TbTsoInfo)
                    .HasForeignKey(d => d.RegionCode)
                    .HasConstraintName("FK_TB_TSO_INFO_TB_REGION_CODE");

                entity.HasOne(d => d.RegionCode1)
                    .WithMany(p => p.TbTsoInfo)
                    .HasForeignKey(d => d.RegionCode)
                    .HasConstraintName("FK__TB_TSO_IN__Tehsi__290D0E62");

                entity.HasOne(d => d.SalePointCodeNavigation)
                    .WithMany(p => p.TbTsoInfo)
                    .HasForeignKey(d => d.SalePointCode)
                    .HasConstraintName("FK_TB_TSO_INFO_TB_SALEPOINT_CODE");
            });

            modelBuilder.Entity<TbTubewellStatus>(entity =>
            {
                entity.HasKey(e => e.TubeWellStatusId);

                entity.ToTable("TB_TUBEWELL_STATUS");

                entity.Property(e => e.TubeWellStatusDescription).HasMaxLength(200);

                entity.Property(e => e.TubeWellStatusName)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<TbTubewellTypeInfo>(entity =>
            {
                entity.HasKey(e => e.TubeWellTypeId);

                entity.ToTable("TB_TUBEWELL_TYPE_INFO");

                entity.Property(e => e.TubeWellTypeDesciption).HasMaxLength(200);

                entity.Property(e => e.TubeWellTypeName)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<TbTunnelTypeInfo>(entity =>
            {
                entity.HasKey(e => e.TunnelId);

                entity.ToTable("TB_TUNNEL_TYPE_INFO");

                entity.Property(e => e.TunnelId).ValueGeneratedNever();

                entity.Property(e => e.TunnelTypeDescription).HasMaxLength(200);

                entity.Property(e => e.TunnelTypeName)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<TbUnitShapes>(entity =>
            {
                entity.HasKey(e => e.UnitShapeId);

                entity.ToTable("TB_UNIT_SHAPES");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDateTime).HasColumnType("datetime");

                entity.HasOne(d => d.Unit)
                    .WithMany(p => p.TbUnitShapes)
                    .HasForeignKey(d => d.UnitId)
                    .HasConstraintName("FK_TB_UNIT_SHAPES_TB_FARM_UNIT");
            });

            modelBuilder.Entity<TbUserLogin>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("TB_USER_LOGIN");

                entity.Property(e => e.UserId).ValueGeneratedNever();

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.CellPhone)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.InsertionDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedDateTime).HasColumnType("datetime");

                entity.Property(e => e.Passkey)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.UserName).HasMaxLength(100);

                entity.HasOne(d => d.UserRoleCodeNavigation)
                    .WithMany(p => p.TbUserLogin)
                    .HasForeignKey(d => d.UserRoleCode)
                    .HasConstraintName("FK__TB_USER_L__UserR__7DCDAAA2");
            });

            modelBuilder.Entity<TbUserRoleTypeCode>(entity =>
            {
                entity.HasKey(e => e.UserRoleCode);

                entity.ToTable("TB_USER_ROLE_TYPE_CODE");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RoleName).HasMaxLength(500);

                entity.Property(e => e.RoleNameUrdu).HasMaxLength(500);
            });

            modelBuilder.Entity<TbWatercourseType>(entity =>
            {
                entity.HasKey(e => e.WaterCourseTypeId);

                entity.ToTable("TB_WATERCOURSE_TYPE");

                entity.Property(e => e.WaterCourseDescription).HasMaxLength(200);

                entity.Property(e => e.WaterCourseName)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<TbWaterSoilResult>(entity =>
            {
                entity.HasKey(e => e.WaterSoilResultId);

                entity.ToTable("TB_WATER_SOIL_RESULT");

                entity.Property(e => e.WaterSoilResultId).HasColumnName("WaterSoilResultID");

                entity.Property(e => e.ActiveStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('A')");

                entity.Property(e => e.Details).HasMaxLength(1000);

                entity.Property(e => e.FarmCropId).HasColumnName("FarmCropID");

                entity.Property(e => e.InsertionDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ResultDate).HasColumnType("datetime");

                entity.Property(e => e.TestDate).HasColumnType("datetime");
            });
        }
    }
}

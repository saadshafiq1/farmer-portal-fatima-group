﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbAimsCommodity
    {
        public int Id { get; set; }
        public int? CommodityId { get; set; }
        public int? CommodityCityId { get; set; }
        public string CommodityName { get; set; }
        public string Min { get; set; }
        public string Max { get; set; }
        public string Fqp { get; set; }
        public DateTime? DateInserted { get; set; }
        public DateTime? DateUpdated { get; set; }
    }
}

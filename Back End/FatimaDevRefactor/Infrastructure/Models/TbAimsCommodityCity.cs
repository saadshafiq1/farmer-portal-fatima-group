﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbAimsCommodityCity
    {
        public int Id { get; set; }
        public int? CityId { get; set; }
        public string CityName { get; set; }
        public DateTime? DateInserted { get; set; }
        public DateTime? DateUpdated { get; set; }
        public string ActiveStatus { get; set; }
    }
}

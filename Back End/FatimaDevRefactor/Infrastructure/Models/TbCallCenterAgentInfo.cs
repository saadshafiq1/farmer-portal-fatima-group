﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbCallCenterAgentInfo
    {
        public TbCallCenterAgentInfo()
        {
            TbTicketActivity = new HashSet<TbTicketActivity>();
        }

        public int CallCenterAgentId { get; set; }
        public string AgentName { get; set; }
        public string Cnic { get; set; }
        public string Email { get; set; }
        public string CellPhone { get; set; }
        public string AlternateCellPhone { get; set; }
        public string Landline { get; set; }
        public string Gender { get; set; }
        public string PermanentAddress { get; set; }
        public int? EducationCode { get; set; }
        public int? DistrictCode { get; set; }
        public int? TehsilCode { get; set; }
        public string Tsoimage { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
        public Guid? UserGuid { get; set; }

        public TbDistrictCode DistrictCodeNavigation { get; set; }
        public TbTehsilCode TehsilCodeNavigation { get; set; }
        public ICollection<TbTicketActivity> TbTicketActivity { get; set; }
    }
}

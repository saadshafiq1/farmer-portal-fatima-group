﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbCropStepAction
    {
        public int CropStepActionCode { get; set; }
        public int? CropStepCode { get; set; }
        public bool? IsAlert { get; set; }
        public string Detail { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public TbCropSteps CropStepCodeNavigation { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbCropTypeCode
    {
        public TbCropTypeCode()
        {
            TbBlockCrops = new HashSet<TbBlockCrops>();
            TbCropCaseMain = new HashSet<TbCropCaseMain>();
            TbCropSteps = new HashSet<TbCropSteps>();
        }

        public int CropCode { get; set; }
        public string CropName { get; set; }
        public string CropNameUrdu { get; set; }
        public string CropSeason { get; set; }
        public int? MainCropCode { get; set; }
        public string CropLogo { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public ICollection<TbBlockCrops> TbBlockCrops { get; set; }
        public ICollection<TbCropCaseMain> TbCropCaseMain { get; set; }
        public ICollection<TbCropSteps> TbCropSteps { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbDealerCategory
    {
        public TbDealerCategory()
        {
            TbDealerInfo = new HashSet<TbDealerInfo>();
        }

        public int DealerCategoryId { get; set; }
        public string CategoryName { get; set; }

        public ICollection<TbDealerInfo> TbDealerInfo { get; set; }
    }
}

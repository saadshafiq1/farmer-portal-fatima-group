﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbDealerCategoryCode
    {
        public int DealerCategoryCode { get; set; }
        public string DealerName { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
    }
}

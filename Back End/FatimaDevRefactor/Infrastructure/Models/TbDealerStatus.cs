﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbDealerStatus
    {
        public TbDealerStatus()
        {
            TbDealerInfo = new HashSet<TbDealerInfo>();
        }

        public int DealerStatusId { get; set; }
        public string Status { get; set; }

        public ICollection<TbDealerInfo> TbDealerInfo { get; set; }
    }
}

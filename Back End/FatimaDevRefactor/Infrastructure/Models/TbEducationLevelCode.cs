﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbEducationLevelCode
    {
        public int EducationCode { get; set; }
        public string EducationLevelName { get; set; }
        public string EducationLevelNameUrdu { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
    }
}

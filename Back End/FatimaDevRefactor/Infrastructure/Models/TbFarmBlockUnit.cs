﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbFarmBlockUnit
    {
        public long FarmBlockUnitId { get; set; }
        public long? FarmUnitId { get; set; }
        public long? FarmBlockId { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public TbFarmBlock FarmBlock { get; set; }
        public TbFarmUnit FarmUnit { get; set; }
    }
}

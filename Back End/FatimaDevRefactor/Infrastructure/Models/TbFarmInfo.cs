﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbFarmInfo
    {
        public TbFarmInfo()
        {
            TbFarmBlock = new HashSet<TbFarmBlock>();
            TbFarmShapes = new HashSet<TbFarmShapes>();
            TbFarmUnit = new HashSet<TbFarmUnit>();
            TbTicketInfo = new HashSet<TbTicketInfo>();
        }

        public int FarmId { get; set; }
        public int? FarmerId { get; set; }
        public int? FarmTypeCode { get; set; }
        public decimal? AreaonHeis { get; set; }
        public decimal? TotalAreaAcres { get; set; }
        public string FarmName { get; set; }
        public string FarmAddress { get; set; }
        public long? FarmGis { get; set; }
        public int? ProvinceCode { get; set; }
        public int? TehsilCode { get; set; }
        public int? DistrictCode { get; set; }
        public long? RegionId { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
        public string Transport1 { get; set; }
        public string Transport2 { get; set; }
        public string Transport3 { get; set; }
        public int? TubeWellStatus { get; set; }
        public int? TubeWellType { get; set; }
        public string IrrigationType { get; set; }
        public string IrrigationArea { get; set; }
        public int? WaterCourse { get; set; }
        public int? TunnelType { get; set; }
        public int? InputPurchase { get; set; }
        public string ExportProduct { get; set; }
        public int? OutputExport { get; set; }

        public TbDistrictCode DistrictCodeNavigation { get; set; }
        public TbFarmInfo Farm { get; set; }
        public TbFarmTypeCode FarmTypeCodeNavigation { get; set; }
        public TbFarmerInfo Farmer { get; set; }
        public TbInputPurchaseInfo InputPurchaseNavigation { get; set; }
        public TbOutputExportInfo OutputExportNavigation { get; set; }
        public TbTubewellStatus TubeWellStatusNavigation { get; set; }
        public TbTubewellTypeInfo TubeWellTypeNavigation { get; set; }
        public TbTunnelTypeInfo TunnelTypeNavigation { get; set; }
        public TbWatercourseType WaterCourseNavigation { get; set; }
        public TbFarmInfo InverseFarm { get; set; }
        public ICollection<TbFarmBlock> TbFarmBlock { get; set; }
        public ICollection<TbFarmShapes> TbFarmShapes { get; set; }
        public ICollection<TbFarmUnit> TbFarmUnit { get; set; }
        public ICollection<TbTicketInfo> TbTicketInfo { get; set; }
    }
}

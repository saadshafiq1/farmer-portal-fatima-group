﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbFarmShapes
    {
        public long FarmShapeId { get; set; }
        public int? FarmId { get; set; }
        public string Geometry { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public TbFarmInfo Farm { get; set; }
    }
}

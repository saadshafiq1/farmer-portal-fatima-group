﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbFarmTypeCode
    {
        public TbFarmTypeCode()
        {
            TbFarmInfo = new HashSet<TbFarmInfo>();
        }

        public int FarmTypeCode { get; set; }
        public string TypeName { get; set; }
        public string TypeNameUrdu { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public ICollection<TbFarmInfo> TbFarmInfo { get; set; }
    }
}

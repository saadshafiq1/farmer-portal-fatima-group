﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbFarmerInfo
    {
        public TbFarmerInfo()
        {
            TbFarmInfo = new HashSet<TbFarmInfo>();
            TbFarmerMedia = new HashSet<TbFarmerMedia>();
            TbFarmerReports = new HashSet<TbFarmerReports>();
            TbTicketActivity = new HashSet<TbTicketActivity>();
            TbTicketInfo = new HashSet<TbTicketInfo>();
        }

        public int FarmerId { get; set; }
        public string FarmerName { get; set; }
        public string FatherHusbandName { get; set; }
        public string Cnic { get; set; }
        public int? DistrictCode { get; set; }
        public int? TehsilCode { get; set; }
        public string Gender { get; set; }
        public string CellPhone { get; set; }
        public string PresentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public int? EducationCode { get; set; }
        public int? MaleDependant { get; set; }
        public int? FemaleDependant { get; set; }
        public string AlternateName { get; set; }
        public string AlternateCellPhoneNo { get; set; }
        public string AlternateRelationshipwithFarmer { get; set; }
        public int? ReferalCode { get; set; }
        public string FarmerImage { get; set; }
        public string UnionCouncil { get; set; }
        public string MozaName { get; set; }
        public int? SmallAnimals { get; set; }
        public int? BigAnimals { get; set; }
        public int? Tractor { get; set; }
        public int? OtherImplement { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
        public Guid? UserGuid { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int? ProvinceCode { get; set; }
        public string Hobbies { get; set; }
        public int? PresentProvinceCode { get; set; }
        public int? PresentDistrictCode { get; set; }
        public int? PresentRegionCode { get; set; }
        public int? PresentTehsilCode { get; set; }
        public string PresentUnionConcil { get; set; }
        public string LandLine { get; set; }
        public string AdjacentFarmerFriendName { get; set; }
        public string AdjacentFarmerFriendCellPhone { get; set; }
        public int? RegionCode { get; set; }
        public int? FarmerStatusId { get; set; }

        public TbDistrictCode DistrictCodeNavigation { get; set; }
        public TbFarmerStatus FarmerStatus { get; set; }
        public TbDistrictCode PresentDistrictCodeNavigation { get; set; }
        public TbProvinceCode PresentProvinceCodeNavigation { get; set; }
        public TbRegionCode PresentRegionCodeNavigation { get; set; }
        public TbTehsilCode PresentTehsilCodeNavigation { get; set; }
        public TbProvinceCode ProvinceCodeNavigation { get; set; }
        public TbRegionCode RegionCodeNavigation { get; set; }
        public TbTehsilCode TehsilCodeNavigation { get; set; }
        public ICollection<TbFarmInfo> TbFarmInfo { get; set; }
        public ICollection<TbFarmerMedia> TbFarmerMedia { get; set; }
        public ICollection<TbFarmerReports> TbFarmerReports { get; set; }
        public ICollection<TbTicketActivity> TbTicketActivity { get; set; }
        public ICollection<TbTicketInfo> TbTicketInfo { get; set; }
    }
}

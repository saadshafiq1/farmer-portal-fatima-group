﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbFarmerStatus
    {
        public TbFarmerStatus()
        {
            TbFarmerInfo = new HashSet<TbFarmerInfo>();
        }

        public int FarmerStatusId { get; set; }
        public string FarmerStatueName { get; set; }
        public string Description { get; set; }

        public ICollection<TbFarmerInfo> TbFarmerInfo { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbFertilizerTypeDetails
    {
        public int DetailId { get; set; }
        public int? FertilizerId { get; set; }
        public string DetailDescriptionUrdu { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public TbFertilizerTypes Fertilizer { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbInputPurchaseInfo
    {
        public TbInputPurchaseInfo()
        {
            TbFarmInfo = new HashSet<TbFarmInfo>();
        }

        public int InputPurchaseId { get; set; }
        public string InputPurchaseName { get; set; }
        public string InputPurchaseDescription { get; set; }

        public ICollection<TbFarmInfo> TbFarmInfo { get; set; }
    }
}

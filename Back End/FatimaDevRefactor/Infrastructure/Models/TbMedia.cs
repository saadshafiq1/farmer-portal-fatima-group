﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbMedia
    {
        public TbMedia()
        {
            TbFarmerMedia = new HashSet<TbFarmerMedia>();
        }

        public long MediaId { get; set; }
        public long? MediaTypeId { get; set; }
        public string Description { get; set; }
        public int? DistrictCode { get; set; }
        public string Url { get; set; }
        public string Header { get; set; }
        public string Body { get; set; }
        public string Footer { get; set; }
        public string LeftContent { get; set; }
        public string RightContent { get; set; }
        public string Image { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
        public bool? NationalNews { get; set; }
        public int? ProvinceId { get; set; }

        public TbDistrictCode DistrictCodeNavigation { get; set; }
        public TbMediaType MediaType { get; set; }
        public ICollection<TbFarmerMedia> TbFarmerMedia { get; set; }
    }
}

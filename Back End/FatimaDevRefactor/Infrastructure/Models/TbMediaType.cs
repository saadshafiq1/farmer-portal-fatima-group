﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbMediaType
    {
        public TbMediaType()
        {
            TbMedia = new HashSet<TbMedia>();
        }

        public long MediaTypeId { get; set; }
        public string MediaType { get; set; }
        public string Description { get; set; }

        public ICollection<TbMedia> TbMedia { get; set; }
    }
}

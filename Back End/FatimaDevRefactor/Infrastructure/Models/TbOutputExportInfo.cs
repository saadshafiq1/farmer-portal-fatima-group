﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbOutputExportInfo
    {
        public TbOutputExportInfo()
        {
            TbFarmInfo = new HashSet<TbFarmInfo>();
        }

        public int OutputExportId { get; set; }
        public string OutputExportName { get; set; }
        public string OutputExportDescription { get; set; }

        public ICollection<TbFarmInfo> TbFarmInfo { get; set; }
    }
}

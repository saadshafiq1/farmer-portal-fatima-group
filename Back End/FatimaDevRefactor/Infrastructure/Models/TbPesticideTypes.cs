﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbPesticideTypes
    {
        public int PesticideId { get; set; }
        public string PesticideName { get; set; }
        public string PesticideNameUrdu { get; set; }
        public string PesticideDetailUrdu { get; set; }
        public string PesticideSeason { get; set; }
        public string PesticideLogo { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
    }
}

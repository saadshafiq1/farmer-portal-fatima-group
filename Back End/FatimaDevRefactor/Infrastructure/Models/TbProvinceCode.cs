﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbProvinceCode
    {
        public TbProvinceCode()
        {
            TbDealerInfo = new HashSet<TbDealerInfo>();
            TbDistrictCode = new HashSet<TbDistrictCode>();
            TbFarmerInfoPresentProvinceCodeNavigation = new HashSet<TbFarmerInfo>();
            TbFarmerInfoProvinceCodeNavigation = new HashSet<TbFarmerInfo>();
            TbRegionCode = new HashSet<TbRegionCode>();
        }

        public int ProvinceCode { get; set; }
        public string ProvinceName { get; set; }
        public string ProvinceNameUrdu { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public ICollection<TbDealerInfo> TbDealerInfo { get; set; }
        public ICollection<TbDistrictCode> TbDistrictCode { get; set; }
        public ICollection<TbFarmerInfo> TbFarmerInfoPresentProvinceCodeNavigation { get; set; }
        public ICollection<TbFarmerInfo> TbFarmerInfoProvinceCodeNavigation { get; set; }
        public ICollection<TbRegionCode> TbRegionCode { get; set; }
    }
}

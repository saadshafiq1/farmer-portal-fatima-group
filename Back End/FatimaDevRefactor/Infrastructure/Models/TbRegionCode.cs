﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbRegionCode
    {
        public TbRegionCode()
        {
            TbDealerInfo = new HashSet<TbDealerInfo>();
            TbFarmerInfoPresentRegionCodeNavigation = new HashSet<TbFarmerInfo>();
            TbFarmerInfoRegionCodeNavigation = new HashSet<TbFarmerInfo>();
            TbTsoInfo = new HashSet<TbTsoInfo>();
        }

        public int RegionCode { get; set; }
        public string RegionName { get; set; }
        public string RegionNameUrdu { get; set; }
        public int? ProvinceCode { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public TbProvinceCode ProvinceCodeNavigation { get; set; }
        public ICollection<TbDealerInfo> TbDealerInfo { get; set; }
        public ICollection<TbFarmerInfo> TbFarmerInfoPresentRegionCodeNavigation { get; set; }
        public ICollection<TbFarmerInfo> TbFarmerInfoRegionCodeNavigation { get; set; }
        public ICollection<TbTsoInfo> TbTsoInfo { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbRequestDetails
    {
        public long RequestId { get; set; }
        public int? RequestStatusCode { get; set; }
        public string RequestDetail { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
    }
}

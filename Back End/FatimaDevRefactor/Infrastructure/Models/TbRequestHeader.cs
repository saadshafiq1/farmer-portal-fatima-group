﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbRequestHeader
    {
        public long RequestId { get; set; }
        public int? RequestTypeCode { get; set; }
        public int? RequestStatusCode { get; set; }
        public long? SubmittedBy { get; set; }
        public long? FarmerId { get; set; }
        public long? FarmId { get; set; }
        public string Details { get; set; }
        public long? AssignedTo { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
    }
}

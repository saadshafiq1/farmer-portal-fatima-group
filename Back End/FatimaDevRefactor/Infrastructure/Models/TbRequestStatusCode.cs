﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbRequestStatusCode
    {
        public int RequestStatusCode { get; set; }
        public string RequestStatusName { get; set; }
        public string RequestStatusNameUrdu { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
    }
}

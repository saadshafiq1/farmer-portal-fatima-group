﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbSalepointCode
    {
        public TbSalepointCode()
        {
            TbDealerInfo = new HashSet<TbDealerInfo>();
            TbTsoInfo = new HashSet<TbTsoInfo>();
        }

        public int SalePointCode { get; set; }
        public string SalePointName { get; set; }
        public string SalePointNameUrdu { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
        public int? TehsilCode { get; set; }

        public TbTehsilCode TehsilCodeNavigation { get; set; }
        public ICollection<TbDealerInfo> TbDealerInfo { get; set; }
        public ICollection<TbTsoInfo> TbTsoInfo { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbSeedTypes
    {
        public TbSeedTypes()
        {
            TbDealerSeedDetails = new HashSet<TbDealerSeedDetails>();
        }

        public int SeedId { get; set; }
        public string SeedName { get; set; }
        public string SeedNameUrdu { get; set; }
        public string SeedSeason { get; set; }
        public string SeedLogo { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public ICollection<TbDealerSeedDetails> TbDealerSeedDetails { get; set; }
    }
}

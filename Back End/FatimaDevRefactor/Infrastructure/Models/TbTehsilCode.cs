﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbTehsilCode
    {
        public TbTehsilCode()
        {
            TbCallCenterAgentInfo = new HashSet<TbCallCenterAgentInfo>();
            TbDealerInfo = new HashSet<TbDealerInfo>();
            TbFarmerInfoPresentTehsilCodeNavigation = new HashSet<TbFarmerInfo>();
            TbFarmerInfoTehsilCodeNavigation = new HashSet<TbFarmerInfo>();
            TbSalepointCode = new HashSet<TbSalepointCode>();
            TbTsoInfo = new HashSet<TbTsoInfo>();
        }

        public int TehsilCode { get; set; }
        public string TehsilName { get; set; }
        public string TehsilNameUrdu { get; set; }
        public int? DistrictCode { get; set; }
        public int? RegionCode { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public TbDistrictCode DistrictCodeNavigation { get; set; }
        public ICollection<TbCallCenterAgentInfo> TbCallCenterAgentInfo { get; set; }
        public ICollection<TbDealerInfo> TbDealerInfo { get; set; }
        public ICollection<TbFarmerInfo> TbFarmerInfoPresentTehsilCodeNavigation { get; set; }
        public ICollection<TbFarmerInfo> TbFarmerInfoTehsilCodeNavigation { get; set; }
        public ICollection<TbSalepointCode> TbSalepointCode { get; set; }
        public ICollection<TbTsoInfo> TbTsoInfo { get; set; }
    }
}

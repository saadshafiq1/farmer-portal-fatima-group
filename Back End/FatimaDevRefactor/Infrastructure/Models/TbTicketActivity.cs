﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbTicketActivity
    {
        public long ActivityId { get; set; }
        public int TicketId { get; set; }
        public string Description { get; set; }
        public int? FarmerId { get; set; }
        public long? Tsoid { get; set; }
        public int? AgentId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool? ActiveStatus { get; set; }

        public TbCallCenterAgentInfo Agent { get; set; }
        public TbFarmerInfo Farmer { get; set; }
        public TbTicketInfo Ticket { get; set; }
        public TbTsoInfo Tso { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbTicketsStatus
    {
        public TbTicketsStatus()
        {
            TbTicketInfo = new HashSet<TbTicketInfo>();
        }

        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public string StatusDescription { get; set; }

        public ICollection<TbTicketInfo> TbTicketInfo { get; set; }
    }
}

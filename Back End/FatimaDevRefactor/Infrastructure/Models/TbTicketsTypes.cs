﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbTicketsTypes
    {
        public TbTicketsTypes()
        {
            TbTicketInfo = new HashSet<TbTicketInfo>();
        }

        public int TicketTypeId { get; set; }
        public string TicketTypeName { get; set; }
        public string TicketTypeDescription { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? Status { get; set; }
        public string TicketTypeNameUrdu { get; set; }

        public ICollection<TbTicketInfo> TbTicketInfo { get; set; }
    }
}

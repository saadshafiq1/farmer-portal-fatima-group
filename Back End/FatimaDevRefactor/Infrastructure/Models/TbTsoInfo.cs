﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbTsoInfo
    {
        public TbTsoInfo()
        {
            TbTicketActivity = new HashSet<TbTicketActivity>();
        }

        public long Tsoid { get; set; }
        public string Tsoname { get; set; }
        public string Cnic { get; set; }
        public string Email { get; set; }
        public string CellPhone { get; set; }
        public string AlternateCellPhone { get; set; }
        public string Landline { get; set; }
        public string Gender { get; set; }
        public string PresentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public int? EducationCode { get; set; }
        public int? SalePointCode { get; set; }
        public int? RegionCode { get; set; }
        public string Tsoimage { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
        public Guid? UserGuid { get; set; }

        public TbTehsilCode RegionCode1 { get; set; }
        public TbRegionCode RegionCodeNavigation { get; set; }
        public TbSalepointCode SalePointCodeNavigation { get; set; }
        public ICollection<TbTicketActivity> TbTicketActivity { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbTubewellStatus
    {
        public TbTubewellStatus()
        {
            TbFarmInfo = new HashSet<TbFarmInfo>();
        }

        public int TubeWellStatusId { get; set; }
        public string TubeWellStatusName { get; set; }
        public string TubeWellStatusDescription { get; set; }

        public ICollection<TbFarmInfo> TbFarmInfo { get; set; }
    }
}

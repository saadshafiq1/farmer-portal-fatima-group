﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbTubewellTypeInfo
    {
        public TbTubewellTypeInfo()
        {
            TbFarmInfo = new HashSet<TbFarmInfo>();
        }

        public int TubeWellTypeId { get; set; }
        public string TubeWellTypeName { get; set; }
        public string TubeWellTypeDesciption { get; set; }

        public ICollection<TbFarmInfo> TbFarmInfo { get; set; }
    }
}

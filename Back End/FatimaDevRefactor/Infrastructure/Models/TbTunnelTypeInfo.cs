﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbTunnelTypeInfo
    {
        public TbTunnelTypeInfo()
        {
            TbFarmInfo = new HashSet<TbFarmInfo>();
        }

        public int TunnelId { get; set; }
        public string TunnelTypeName { get; set; }
        public string TunnelTypeDescription { get; set; }

        public ICollection<TbFarmInfo> TbFarmInfo { get; set; }
    }
}

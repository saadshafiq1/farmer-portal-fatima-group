﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbUserLogin
    {
        public Guid UserId { get; set; }
        public string CellPhone { get; set; }
        public string UserName { get; set; }
        public string Passkey { get; set; }
        public int? UserRoleCode { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public TbUserRoleTypeCode UserRoleCodeNavigation { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TbWatercourseType
    {
        public TbWatercourseType()
        {
            TbFarmInfo = new HashSet<TbFarmInfo>();
        }

        public int WaterCourseTypeId { get; set; }
        public string WaterCourseName { get; set; }
        public string WaterCourseDescription { get; set; }

        public ICollection<TbFarmInfo> TbFarmInfo { get; set; }
    }
}

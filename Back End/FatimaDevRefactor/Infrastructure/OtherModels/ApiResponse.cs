﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.OtherModels
{
    public class ApiResponse
    {
        public int ResponseCode { get; set; }
        public object ResponseMessage { get; set; }
        public object Data { get; set; }
    }
}

﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
  public  class AIMSCommodityCityRepository : BaseRepository<TbAimsCommodityCity>, IAIMSCommodityCityRepository
    {

        public AIMSCommodityCityRepository(FFContext repositoryContext)
             : base(repositoryContext)
        {
        }
       
        public async Task<IEnumerable<TbAimsCommodityCity>> GetAllCommodityCitysAsync()
        {
            return await FindAllAsync();
        }
        public async Task<TbAimsCommodityCity> GetCommodityCityByIdAsync(int ID)
        {
            var CommodityCity = await FindByConditionAync(C => C.Id == ID);
            return CommodityCity.FirstOrDefault();
        }
      
        public async Task CreateCommodityCityAsync(TbAimsCommodityCity CommodityCity)
        {
            await Create(CommodityCity);
        }
      
        public async Task UpdateCommodityCityAsync(TbAimsCommodityCity CommodityCity)
        {
            await Update(CommodityCity);          
        }
          public async Task DeleteCommodityCityAsync(TbAimsCommodityCity CommodityCity)
        {
            await Delete(CommodityCity);
        }
        public async Task<IEnumerable<TbAimsCommodityCity>> GetCommodityCityByCondition(Expression<Func<TbAimsCommodityCity, bool>> expression)
        {
            return await FindByConditionAync(expression);
        }
    }
}

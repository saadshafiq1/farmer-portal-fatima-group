﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
  public  class AIMSCommodityRepository : BaseRepository<TbAimsCommodity>, IAIMSCommodityRepository
    {

        public AIMSCommodityRepository(FFContext repositoryContext)
             : base(repositoryContext)
        {
        }
       
        public async Task<IEnumerable<TbAimsCommodity>> GetAllCommoditysAsync()
        {
            return await FindAllAsync();
        }
        public async Task<TbAimsCommodity> GetCommodityByIdAsync(int ID)
        {
            var commodity = await FindByConditionAync(C => C.Id == ID);
            return commodity.FirstOrDefault();
        }
      
        public async Task CreateCommodityAsync(TbAimsCommodity commodity)
        {
            await Create(commodity);
        }
      
        public async Task UpdateCommodityAsync(TbAimsCommodity commodity)
        {
            await Update(commodity);          
        }
          public async Task DeleteCommodityAsync(TbAimsCommodity commodity)
        {
            await Delete(commodity);
        }
        public async Task<IEnumerable<TbAimsCommodity>> GetCommodityByCondition(Expression<Func<TbAimsCommodity, bool>> expression)
        {
            return await FindByConditionAync(expression,0,5000);
        }
    }
}

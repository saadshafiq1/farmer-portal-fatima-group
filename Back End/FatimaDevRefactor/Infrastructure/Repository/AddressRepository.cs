﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class AddressRepository : BaseRepository<TbProvinceCode>, IAddressRepository
    {
        public AddressRepository(FFContext repositoryContext) : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TbProvinceCode>> GetAllProvincesAsync()
        {
            return await FindAllAsync();           
        }        
        public async Task<TbProvinceCode> GetProvinceByIdAsync(long ownerId)
        {
            var pc = await FindByConditionAync(o => o.ProvinceCode == ownerId);
            return pc.FirstOrDefault();
        }
        public async Task CreateProvinceAsync(TbProvinceCode media)
        {         
            await Create(media);           
        }
        public async Task UpdateProvinceAsync(TbProvinceCode owner)
        {
            await Update(owner);
        }
        public async Task DeleteProvinceAsync(TbProvinceCode media)
        {
            await Delete(media);
        }
        public async Task<IEnumerable<TbDistrictCode>> GetDistrictsOfProvince(long provinceid)
        {
            return await this.RepositoryContext.TbDistrictCode.Where(i => i.ProvinceCode == provinceid).ToAsyncEnumerable().ToList();
        }
        
              public async Task<IEnumerable<TbDistrictCode>> GetDistrictsByRegion(long regionid)
        {
            return await this.RepositoryContext.TbDistrictCode.Where(i => i.RegionCode == regionid).ToAsyncEnumerable().ToList();
        }
        public async Task<IEnumerable<TbTehsilCode>> GetTehsilsOfDistrict(long districtid)
        {
            return await this.RepositoryContext.TbTehsilCode.Where(i => i.DistrictCode == districtid).ToAsyncEnumerable().ToList();
        }
        public async Task<TbDistrictCode> GetDistrictById(int districtid)
        {
            return await this.RepositoryContext.TbDistrictCode.FindAsync(districtid);
        }
        public async Task<TbTehsilCode> GetTehsilById(int tehsilid)
        {
            return await this.RepositoryContext.TbTehsilCode.FindAsync(tehsilid);
        }
        
        public async Task<IEnumerable<TbRegionCode>> GetCitiesOfProvince(int provinceid)
        {
            return await this.RepositoryContext.TbRegionCode.Where(i => i.ProvinceCode == provinceid).ToAsyncEnumerable().ToList();
        }
        
        public async Task<TbRegionCode> GetRegionById(int regionid)
        {
            return await this.RepositoryContext.TbRegionCode.FindAsync(regionid);
        }
        public async Task<IEnumerable<TbRegionCode>> GetAllRegion()
        {
            return await this.RepositoryContext.TbRegionCode.ToAsyncEnumerable().ToList();
        }
        
         public async Task<IEnumerable<TbDistrictCode>> GetAllDistricts()
        {
            return await this.RepositoryContext.TbDistrictCode.ToAsyncEnumerable().ToList();
        }

        public async Task<IEnumerable<TbSalepointCode>> GetAllSalesPoint()
        {
            return await this.RepositoryContext.TbSalepointCode.ToAsyncEnumerable().ToList();
        }

    }
}

﻿using Infrastructure.Interface;
using Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        protected FFContext RepositoryContext { get; set; }
        public BaseRepository(FFContext repositoryContext)
        {
            this.RepositoryContext = repositoryContext;
        }
        public async Task<IEnumerable<T>> FindAllAsync()
        {
            return await this.RepositoryContext.Set<T>().ToListAsync();
        }
        public async Task<IEnumerable<T>> FindByConditionAync(Expression<Func<T, bool>> expression, int pageNumber=0, int pageSize=100)
        {
            return await this.RepositoryContext.Set<T>().Where(expression).Skip(pageNumber* pageSize).Take(pageSize).ToListAsync();
        }
        public async Task<IEnumerable<T>> FindByConditionAync(Expression<Func<T, object>>[] includes, int pageNumber = 0, int pageSize = 100)
        {
            IQueryable<T> query = this.RepositoryContext.Set<T>().Skip(pageNumber * pageSize).Take(pageSize);
            foreach (var include in includes)
            {
                query = query.Include(include);
            }
            return await query.ToListAsync();
        }     
        public async Task<IEnumerable<T>> FindByConditionAync(Expression<Func<T, bool>> expression, Expression<Func<T, object>>[] includes, int pageNumber = 0, int pageSize = 100)
        {
            IQueryable<T> query = this.RepositoryContext.Set<T>().Where(expression).Skip(pageNumber * pageSize).Take(pageSize);
            
            foreach (var include in includes)
            {
                query = query.Include(include);
            }

            return await query.ToListAsync();
        }
        public async Task<IEnumerable<T>> FindByConditionAyncForCount(Expression<Func<T, bool>> expression, Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> query = this.RepositoryContext.Set<T>().Where(expression);

            foreach (var include in includes)
            {
                query = query.Include(include);
            }

            return await query.ToListAsync();
        }
        public async Task Create(T entity)
        {
            this.RepositoryContext.Set<T>().Add(entity);
            await this.RepositoryContext.SaveChangesAsync();
        }
        public async Task Update(T entity)
        {
            this.RepositoryContext.Entry(entity).State = EntityState.Modified;
            await this.RepositoryContext.SaveChangesAsync();
        }
        public async Task Delete(T entity)
        {
            this.RepositoryContext.Set<T>().Remove(entity);
            await this.RepositoryContext.SaveChangesAsync();
        }
        public async Task SaveAsync()
        {
            await this.RepositoryContext.SaveChangesAsync();
        }
    

       
    }
}

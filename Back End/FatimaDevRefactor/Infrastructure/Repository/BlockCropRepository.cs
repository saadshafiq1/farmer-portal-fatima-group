﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Infrastructure.Models;
using Infrastructure.Interface;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class BlockCropRepository : BaseRepository<TbBlockCrops>, IBlockCropRepository
    {
        public BlockCropRepository(FFContext repositoryContext)
        : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TbBlockCrops>> GetAllBlockCropsAsync()
        {
            return await FindAllAsync();
        }
        public async Task CreateBlockCropsAsync(TbBlockCrops blockCrop)
        {
            await Create(blockCrop);
        }
        public async Task DeleteBlockCropsAsync(TbBlockCrops blockCrop)
        {
            await Delete(blockCrop);
        }
        public async Task UpdateBlockCropsAsync(TbBlockCrops blockCrop)
        {
            await Update(blockCrop);
        }
        public async Task<IEnumerable<TbBlockCrops>> GetBlockCropByCondition(Expression<Func<TbBlockCrops, bool>> expression)
        {
            return await FindByConditionAync(expression);
        }
    }
}
﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class CallCenterAgentRepository : BaseRepository<TbCallCenterAgentInfo>, ICallCenterAgentRepository
    {
        public CallCenterAgentRepository(FFContext repositoryContext)
         : base(repositoryContext)
        {
        }
        public async Task<TbCallCenterAgentInfo> GetAgentByUserName(Expression<Func<TbCallCenterAgentInfo, bool>> expression, Expression<Func<TbCallCenterAgentInfo, object>>[] includes)
        {
            var agent = await FindByConditionAync(expression, includes);
            
            return agent.FirstOrDefault();
        }

        public async Task<TbCallCenterAgentInfo> GetAgentById(Expression<Func<TbCallCenterAgentInfo, bool>> expression)
        {
             var result =await FindByConditionAync(expression);

             return result.FirstOrDefault();
        }

        public async Task<IEnumerable<TbCallCenterAgentInfo>> GetAllAgentsAsync()
        {
            return await FindAllAsync();
        }
    }
}

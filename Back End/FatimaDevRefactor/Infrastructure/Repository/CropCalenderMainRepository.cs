﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace Infrastructure.Repository
{
    public class CropCalenderMainRepository : BaseRepository<TbCropCalenderMain>, ICropCalenderMainRepository
    {
        public CropCalenderMainRepository(FFContext repositoryContext)
         : base(repositoryContext)
        {
        }
        public async Task CreateCropCalenderMainAsync(TbCropCalenderMain crop)
        {
            await Create(crop);           
        }
        public async Task DeleteCropCalenderMainAsync(TbCropCalenderMain cropCalenderMain)
        {
            await Delete(cropCalenderMain);            
        }
        public async Task<IEnumerable<TbCropCalenderMain>> GetAllCropCalenderMainAsync()
        {
            return await FindAllAsync();
        }
        public async Task<IEnumerable<TbCropCalenderMain>> GetCropCalenderMainByCondition(Expression<Func<TbCropCalenderMain, bool>> expression)
        {
            return await FindByConditionAync(expression);
        }
        public async Task<IEnumerable<TbCropCalenderMain>> GetCropCalenderMainByCondition(Expression<Func<TbCropCalenderMain, bool>> expression, Expression<Func<TbCropCalenderMain, object>> [] includes)
        {   
            return await FindByConditionAync(expression, includes);
        }
        public async Task<TbCropCalenderMain> GetCropCalenderMainByIdAsync(long calenderCode)
        {
            var owner = await FindByConditionAync(o => o.CalenderCode == calenderCode);
            return owner.FirstOrDefault();
        }     
        public async Task UpdateCropCalenderMainAsync(TbCropCalenderMain cropCalenderMain)
        {
            await Update(cropCalenderMain);           
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Infrastructure.Models;
using Infrastructure.Interface;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class CropCaseMainRepository : BaseRepository<TbCropCaseMain>, ICropCaseMainRepository
    {
        public CropCaseMainRepository(FFContext repositoryContext)
        : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TbCropCaseMain>> GetAllCropCaseMainAsync()
        {
            return await FindAllAsync();
        }
        public async Task CreateCropCaseMainAsync(TbCropCaseMain CropCaseMain)
        {
            await Create(CropCaseMain);
        }
        public async Task DeleteCropCaseMainAsync(TbCropCaseMain CropCaseMain)
        {
            await Delete(CropCaseMain);
        }
        public async Task UpdateCropCaseMainAsync(TbCropCaseMain CropCaseMain)
        {
            await Update(CropCaseMain);
        }
        public async Task<IEnumerable<TbCropCaseMain>> GetCropCaseMainByCondition(Expression<Func<TbCropCaseMain, bool>> expression)
        {
            return await FindByConditionAync(expression);
        }
    }
}
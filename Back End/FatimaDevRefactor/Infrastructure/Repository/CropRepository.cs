﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace Infrastructure.Repository
{
    public class CropRepository:BaseRepository<TbCropTypeCode>,ICropRepository
    {
        public CropRepository(FFContext repositoryContext)
         : base(repositoryContext)
        {
        }
        public async Task CreateCropAsync(TbCropTypeCode crop)
        {
            await Create(crop);           
        }
        public async Task DeleteCropAsync(TbCropTypeCode crop)
        {
            await Delete(crop);           
        }
        public async Task<IEnumerable<TbCropTypeCode>> GetAllCropsAsync()
        {
            return await FindAllAsync();
        }
        public async Task<IEnumerable<TbCropTypeCode>> GetCropByCondition(Expression<Func<TbCropTypeCode, bool>> expression)
        {
            return await FindByConditionAync(expression);
        }        
        public async Task<TbCropTypeCode> GetCropByIdAsync(long cropCode)
        {
            var cropTypeCodes = await FindByConditionAync(o => o.CropCode == cropCode);
            return cropTypeCodes.FirstOrDefault();
        }     
        public async Task UpdateCropAsync(TbCropTypeCode cropCode)
        {
            await Update(cropCode);         
        }
    }
}

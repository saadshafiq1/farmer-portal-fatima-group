﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace Infrastructure.Repository
{
    public class CropStepActionRepository : BaseRepository<TbCropStepAction>, ICropStepActionRepository
    {
        public CropStepActionRepository(FFContext repositoryContext)
         : base(repositoryContext)
        {
        }
        public async Task CreateCropStepActionAsync(TbCropStepAction crop)
        {
            await Create(crop);
        }
        public async Task DeleteCropStepActionAsync(TbCropStepAction CropStepAction)
        {
            await Delete(CropStepAction);
        }
        public async Task<IEnumerable<TbCropStepAction>> GetAllCropStepActionAsync()
        {
            return await FindAllAsync();
        }
        public async Task<IEnumerable<TbCropStepAction>> GetCropStepActionByCondition(Expression<Func<TbCropStepAction, bool>> expression)
        {
            return await FindByConditionAync(expression);
        }
        public async Task<TbCropStepAction> GetCropStepActionByIdAsync(long cropStepActionCode)
        {
            var owner = await FindByConditionAync(o => o.CropStepActionCode == cropStepActionCode);
            return owner.FirstOrDefault();
        }
        public async Task UpdateCropStepActionAsync(TbCropStepAction CropStepAction)
        {
            await Update(CropStepAction);
        }
    }
}

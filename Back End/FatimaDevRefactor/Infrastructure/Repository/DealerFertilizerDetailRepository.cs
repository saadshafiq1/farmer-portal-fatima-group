﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace Infrastructure.Repository
{
    public class DealerFertilizerDetailRepository : BaseRepository<TbDealerFertilizerDetails>, IDealerFertilizerDetailRepository
    {
        public DealerFertilizerDetailRepository(FFContext repositoryContext)
         : base(repositoryContext)
        {
        }
        public async Task CreateDealerFertilizerDetailAsync(TbDealerFertilizerDetails dealerFertilizerDetail)
        {
            await Create(dealerFertilizerDetail);
        }
        public async Task DeleteDealerFertilizerDetailAsync(TbDealerFertilizerDetails dealerFertilizerDetail)
        {
            await Delete(dealerFertilizerDetail);
        }
        public async Task<IEnumerable<TbDealerFertilizerDetails>> GetAllDealerFertilizerDetailAsync()
        {
            return await FindAllAsync();
        }
        public async Task<IEnumerable<TbDealerFertilizerDetails>> GetDealerFertilizerDetailByCondition(Expression<Func<TbDealerFertilizerDetails, bool>> expression)
        {
            return await FindByConditionAync(expression);
        }
        public async Task<TbDealerFertilizerDetails> GetDealerFertilizerDetailByIdAsync(long dealerFertilizerDetailId)
        {
            var owner = await FindByConditionAync(o => o.DealerId == dealerFertilizerDetailId);
            return owner.FirstOrDefault();
        }
        public async Task UpdateDealerFertilizerDetailAsync(TbDealerFertilizerDetails dealerFertilizerDetail)
        {
            await Update(dealerFertilizerDetail);
        }
    }
}

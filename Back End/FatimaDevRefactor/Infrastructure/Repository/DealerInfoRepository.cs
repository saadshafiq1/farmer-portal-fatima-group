﻿using GeoCoordinatePortable;
using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;


namespace Infrastructure.Repository
{
    public class DealerInfoRepository : BaseRepository<TbDealerInfo>, IDealerInfoRepository
    {
        public DealerInfoRepository(FFContext repositoryContext)
         : base(repositoryContext)
        {
        }
        public async Task CreateDealerInfoAsync(TbDealerInfo dealerInfo)
        {
            await Create(dealerInfo);
        }
        public async Task DeleteDealerInfoAsync(TbDealerInfo dealerInfo)
        {
            await Delete(dealerInfo);
        }
        public async Task<IEnumerable<TbDealerInfo>> GetAllDealersCountAsync()
        {
            return await FindAllAsync();
        }
        public async Task<IEnumerable<TbDealerInfo>> GetAllDealerInfoAsync()
        {
            var includes = new Expression<Func<TbDealerInfo, object>>[1];
            includes[0] = d => d.DistrictCodeNavigation;

            return await FindByConditionAync(includes);
        }
        public async Task<IEnumerable<TbDealerInfo>> GetAllDealersCountAsync(string filter = "", string value = "")
        {
            var includes = new Expression<Func<TbDealerInfo, object>>[2];
            includes[0] = d => d.DistrictCodeNavigation;
            includes[1] = d => d.TehsilCodeNavigation;

            Expression<Func<TbDealerInfo, bool>> expression;

            if (!string.IsNullOrEmpty(filter) && !string.IsNullOrEmpty(value))
            {
                if ("Dealer".ToLower().Equals(filter))
                    expression = d => d.DealerName.Contains(value);
                else if ("Address".ToLower().Equals(filter))
                    expression = d => d.Address.Contains(value);
                else if ("Phone".ToLower().Equals(filter))
                    expression = d => d.CellPhone.Contains(value);
                else if ("Cnic".ToLower().Equals(filter))
                    expression = d => d.Cnic.Contains(value);
                else if ("District".ToLower().Equals(filter))
                    expression = d => d.DistrictCodeNavigation.DistrictName.Contains(value);
                else if ("Tehsil".ToLower().Equals(filter))
                    expression = d => d.TehsilCodeNavigation.TehsilName.Contains(value);
                else if ("Province".ToLower().Equals(filter))
                    expression = d => d.Province == Int32.Parse(value);
                else
                    expression = d => d.DealerName != "";
            }
            else
                expression = d => d.DealerName != "";

            return await FindByConditionAyncForCount(expression, includes);
        }
        public async Task<IEnumerable<TbDealerInfo>> GetAllDealerInfoAsync(int pageNumber, int pageSize, string filter = "", string value = "")
        {
            var includes = new Expression<Func<TbDealerInfo, object>>[7];
            includes[0] = d => d.DistrictCodeNavigation;
            includes[1] = d => d.TehsilCodeNavigation;
            includes[2] = d => d.DealerStatusNavigation;
            includes[3] = d => d.DealerCategoryNavigation;
            includes[4] = d => d.DealerTypeCodeNavigation;
            includes[5] = d => d.SaleRegionNavigation;
            includes[6] = d => d.SalePointNavigation;

            Expression<Func<TbDealerInfo, bool>> expression = d => d.DealerName != "";

            if (!string.IsNullOrEmpty(filter) && !string.IsNullOrEmpty(value))
            {

                switch (filter.ToLower())
                {
                    case "dealer":
                        expression = d => d.DealerName.Contains(value);
                        break;
                    case "address":
                        expression = d => d.Address.Contains(value);
                        break;
                    case "phone":
                        expression = d => d.CellPhone.Contains(value);
                        break;
                    case "cnic":
                        expression = d => d.Cnic.Contains(value);
                        break;
                    case "district":
                        expression = d => d.DistrictCodeNavigation.DistrictCode == Int32.Parse(value);
                        break;
                    case "tehsil":
                        expression = d => d.TehsilCodeNavigation.TehsilName.Contains(value);
                        break;
                    case "province":
                        expression = d => d.Province == Int32.Parse(value);
                        break;
                    case "region":
                        expression = d => d.SaleRegionNavigation.RegionCode == Int32.Parse(value);
                        break;
                    case "salepoint":
                        expression = d => d.SalePointNavigation.SalePointCode == Int32.Parse(value);
                        break;
                }                
            }           

            return await FindByConditionAync(expression, includes, pageNumber, pageSize);
        }
        public async Task<IEnumerable<TbDealerInfo>> GetDealerInfoByCondition(Expression<Func<TbDealerInfo, bool>> expression, int pageNumber, int pageSize)
        {
            var includes = new Expression<Func<TbDealerInfo, object>>[3];
            includes[0] = d => d.DistrictCodeNavigation;
            includes[1] = d => d.TehsilCodeNavigation;
            includes[2] = d => d.DealerTypeCodeNavigation;

            return await FindByConditionAync(expression, includes, pageNumber, pageSize);
        }
        public async Task<IEnumerable<TbDealerInfo>> GetDealerInfoByConditionLatLon(Expression<Func<TbDealerInfo, bool>> expression, int pageNumber, int pageSize, float latitude, float longitude)
        {           
            var coords = new GeoCoordinate(latitude, longitude);
          
            var nearest = this.RepositoryContext.TbDealerInfo
                .Include(d=>d.DistrictCodeNavigation).Include(d=>d.TehsilCodeNavigation).Include(d=>d.DealerTypeCodeNavigation)
                .Where(expression)                
                .Select(x => new DealerLocation
                {
                    DealerInfo = x,
                    coord = new GeoCoordinate { Latitude =Convert.ToDouble(x.Latitude), Longitude = Convert.ToDouble(x.Longitude) }
                })
                .OrderBy(x => x.coord.GetDistanceTo(coords))  
                .Skip(pageNumber * pageSize).Take(pageSize);          

            var result = nearest.ToList().Select(d=>d.DealerInfo);           
            return result;
        }
        public async Task<TbDealerInfo> GetDealerInfoByIdAsync(long dealerInfoId)
        {
            var includes = new Expression<Func<TbDealerInfo, object>>[3];
            includes[0] = d => d.DistrictCodeNavigation;
            includes[1] = d => d.TehsilCodeNavigation;
            includes[2] = d => d.DealerTypeCodeNavigation;

            var dealer = await FindByConditionAync(d => d.DealerId == dealerInfoId, includes);
            return dealer.FirstOrDefault();
        }
        public async Task UpdateDealerInfoAsync(TbDealerInfo dealerInfo)
        {
            await Update(dealerInfo);
        }
    }


    public class DealerStatusRepository : BaseRepository<TbDealerStatus>, IDealerStatusRepository
    {
        public DealerStatusRepository(FFContext repositoryContext)
       : base(repositoryContext)
        {
        }

        public async Task<IEnumerable<TbDealerStatus>> GetDealerStatus()
        {
            return await FindAllAsync();
        }
    }

    public interface IDealerStatusRepository
    {

        Task<IEnumerable<TbDealerStatus>> GetDealerStatus();
    }


    public class  DealerCategoryRepository : BaseRepository<TbDealerCategory>, IDealerCategoryRepository
    {
        public DealerCategoryRepository(FFContext repositoryContext)
        : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TbDealerCategory>> GetAllDealerCategoryAsync()
        {
            return await FindAllAsync();
        }
    }


    public interface IDealerCategoryRepository
    {
        Task<IEnumerable<TbDealerCategory>> GetAllDealerCategoryAsync();
    }
}

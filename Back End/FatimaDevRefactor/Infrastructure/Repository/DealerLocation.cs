﻿using GeoCoordinatePortable;
using Infrastructure.Models;

namespace Infrastructure.Repository
{
    internal class DealerLocation
    {
        public TbDealerInfo DealerInfo { get; set; }
        public GeoCoordinate coord { get; set; }
    }
}
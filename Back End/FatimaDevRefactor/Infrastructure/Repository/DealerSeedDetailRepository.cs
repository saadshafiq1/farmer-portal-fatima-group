﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace Infrastructure.Repository
{
    public class DealerSeedDetailRepository : BaseRepository<TbDealerSeedDetails>, IDealerSeedDetailRepository
    {
        public DealerSeedDetailRepository(FFContext repositoryContext)
         : base(repositoryContext)
        {
        }
        public async Task CreateDealerSeedDetailAsync(TbDealerSeedDetails dealerSeedDetail)
        {
            await Create(dealerSeedDetail);
        }
        public async Task DeleteDealerSeedDetailAsync(TbDealerSeedDetails dealerSeedDetail)
        {
            await Delete(dealerSeedDetail);
        }
        public async Task<IEnumerable<TbDealerSeedDetails>> GetAllDealerSeedDetailAsync()
        {
            return await FindAllAsync();
        }
        public async Task<IEnumerable<TbDealerSeedDetails>> GetDealerSeedDetailByCondition(Expression<Func<TbDealerSeedDetails, bool>> expression,int pageNumber, int pageSize)
        {
            return await FindByConditionAync(expression,  pageNumber,  pageSize);
        }
        public async Task<TbDealerSeedDetails> GetDealerSeedDetailByIdAsync(long dealerSeedDetailId)
        {
            var owner = await FindByConditionAync(o => o.DealerId == dealerSeedDetailId);
            return owner.FirstOrDefault();
        }
        public async Task UpdateDealerSeedDetailAsync(TbDealerSeedDetails dealerSeedDetail)
        {
            await Update(dealerSeedDetail);
        }
    }
}

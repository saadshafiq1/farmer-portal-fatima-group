﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace Infrastructure.Repository
{
    public class DealerTypeCodeRepository : BaseRepository<TbDealerTypeCode>, IDealerTypeCodeRepository
    {
        public DealerTypeCodeRepository(FFContext repositoryContext)
         : base(repositoryContext)
        {
        }
        public async Task CreateDealerTypeCodeAsync(TbDealerTypeCode dealerTypeCode)
        {
            await Create(dealerTypeCode);
        }
        public async Task DeleteDealerTypeCodeAsync(TbDealerTypeCode dealerTypeCode)
        {
            await Delete(dealerTypeCode);
        }
        public async Task<IEnumerable<TbDealerTypeCode>> GetAllDealerTypeCodesAsync()
        {
            return await FindAllAsync();
        }
        public async Task<IEnumerable<TbDealerTypeCode>> GetDealerTypeCodeByCondition(Expression<Func<TbDealerTypeCode, bool>> expression)
        {
            return await FindByConditionAync(expression);
        }
        public async Task<TbDealerTypeCode> GetDealerTypeCodeByIdAsync(long dealerTypeCodeId)
        {
            var owner = await FindByConditionAync(o => o.DealerTypeCodeId == dealerTypeCodeId);
            return owner.FirstOrDefault();
        }
        public async Task UpdateDealerTypeCodeAsync(TbDealerTypeCode dealerTypeCode)
        {
            await Update(dealerTypeCode);
        }
    }
}

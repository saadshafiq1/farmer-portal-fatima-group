﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class FarmBlockRepository : BaseRepository<TbFarmBlock>, IFarmBlockRepository
    {
        public FarmBlockRepository(FFContext repositoryContext) : base(repositoryContext)
        {
        }
        public async Task CreateFarmBlockAsync(TbFarmBlock farmtype)
        {
            await Create(farmtype);
        }
        public async Task DeleteFarmBlockAsync(TbFarmBlock farmtype)
        {
            await Delete(farmtype);
        }
        public async Task<IEnumerable<TbFarmBlock>> GetAllFarmBlocksAsync()
        {
            var owners = await FindAllAsync();
            return owners;
        }
        public async Task<IEnumerable<TbFarmBlock>> GetFarmBlockByCondition(Expression<Func<TbFarmBlock, bool>> expression)
        {
            return await FindByConditionAync(expression);
        }
        public async Task<TbFarmBlock> GetFarmBlockByIdAsync(long id)
        {
            var owner = await FindByConditionAync(o => o.FarmBlockId == id);
            return owner.FirstOrDefault();
        }
        public async Task UpdateFarmBlockAsync(TbFarmBlock farmtype)
        {
            await Update(farmtype);
        }
    }
}

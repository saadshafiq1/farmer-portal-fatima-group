﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class FarmBlockUnitRepository : BaseRepository<TbFarmBlockUnit>, IFarmBlockUnitRepository
    {
        public FarmBlockUnitRepository(FFContext repositoryContext) : base(repositoryContext)
        {
        }
        public async Task CreateFarmBlockUnitAsync(TbFarmBlockUnit farmtype)
        {
            await Create(farmtype);
        }
        public async Task DeleteFarmBlockUnitAsync(TbFarmBlockUnit farmtype)
        {
            await Delete(farmtype);
        }
        public Task DeleteFarmBlocUnitkAsync(TbFarmBlockUnit entity)
        {
            throw new NotImplementedException();
        }
        public async Task<IEnumerable<TbFarmBlockUnit>> GetAllFarmBlockUnitsAsync()
        {
            var owners = await FindAllAsync();
            return owners;
        }
        public async Task<IEnumerable<TbFarmBlockUnit>> GetFarmBlockUnitByCondition(Expression<Func<TbFarmBlockUnit, bool>> expression)
        {
            return await FindByConditionAync(expression);
        }
        public async Task<TbFarmBlockUnit> GetFarmBlockUnitByIdAsync(long id)
        {
            var owner = await FindByConditionAync(o => o.FarmBlockUnitId == id);
            return owner.FirstOrDefault();
        }
        public async Task UpdateFarmBlockUnitAsync(TbFarmBlockUnit farmtype)
        {
            await Update(farmtype);
        }
    }
}

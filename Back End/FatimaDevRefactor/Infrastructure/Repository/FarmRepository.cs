﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
namespace Infrastructure.Repository
{
    public class FarmRepository : BaseRepository<TbFarmInfo>, IFarmRepository
    {
        public FarmRepository(FFContext repositoryContext)
           : base(repositoryContext)
        {
        }
        public async Task CreateFarmAsync(TbFarmInfo farmInfo)
        {
            await Create(farmInfo);
        }
        public async Task DeleteFarmAsync(TbFarmInfo farmInfo)
        {
            await Delete(farmInfo);
        }
        public async Task<IEnumerable<TbFarmInfo>> GetAllFarmsAsync()
        {
            return await FindAllAsync();
        }
        public async Task<IEnumerable<TbFarmInfo>> GetFarmByCondition(Expression<Func<TbFarmInfo, bool>> expression)
        {
            return await FindByConditionAync(expression);
        }
        public async Task<IEnumerable<TbFarmInfo>> GetFarmsByConditionForWeb(Expression<Func<TbFarmInfo, bool>> expression)
        {
            return await FindByConditionAync(expression);
        }
        public async Task<TbFarmInfo> GetFarmByIdAsync(int FarmId)
        {
            var owner = await FindByConditionAync(o => o.FarmId == FarmId);
            return owner.FirstOrDefault();
        }
        public async Task UpdateFarmAsync(TbFarmInfo farmInfo)
        {
            await Update(farmInfo);
        }
    }
}

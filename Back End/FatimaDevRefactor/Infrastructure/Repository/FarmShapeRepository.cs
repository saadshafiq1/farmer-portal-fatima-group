﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class FarmShapeRepository : BaseRepository<TbFarmShapes>, IFarmShapeRepository
    {
        public FarmShapeRepository(FFContext repositoryContext) : base(repositoryContext)
        {
        }
        public async Task CreateFarmShapeAsync(TbFarmShapes media)
        {
            await Create(media);
        }
        public async Task DeleteFarmShapeAsync(TbFarmShapes media)
        {
            await Delete(media);
        }
        public async Task<IEnumerable<TbFarmShapes>> GetAllFarmShapesAsync()
        {
            return await FindAllAsync();
        }
        public async Task<IEnumerable<TbFarmShapes>> GetFarmShapeByCondition(Expression<Func<TbFarmShapes, bool>> expression)
        {
            return await FindByConditionAync(expression);
        }
        public async Task<TbFarmShapes> GetFarmShapeByIdAsync(long ownerId)
        {
            var a = await FindByConditionAync(c => c.FarmShapeId == ownerId);
            return a.FirstOrDefault();
        }
        public async Task UpdateFarmShapeAsync(TbFarmShapes owner)
        {
            await Update(owner);
        }
    }
}

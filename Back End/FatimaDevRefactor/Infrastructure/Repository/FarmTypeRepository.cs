﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class FarmTypeRepository : BaseRepository<TbFarmTypeCode>, IFarmTypeRepository
    {
        public FarmTypeRepository(FFContext repositoryContext) : base(repositoryContext)
        {
        }
        public async Task CreateFarmTypeAsync(TbFarmTypeCode farmtype)
        {
            await Create(farmtype);          
        }
        public async Task DeleteFarmTypeAsync(TbFarmTypeCode farmtype)
        {
            await Delete(farmtype);           
        }
        public async Task<IEnumerable<TbFarmTypeCode>> GetAllFarmTypesAsync()
        {
            var owners = await FindAllAsync();
            return owners;
        }
        public async Task<TbFarmTypeCode> GetFarmTypeByIdAsync(long id)
        {
            var owner = await FindByConditionAync(o => o.FarmTypeCode == id);
            return owner.FirstOrDefault();
        }
        public async Task UpdateFarmTypeAsync(TbFarmTypeCode farmtype)
        {
            await Update(farmtype);
        }
    }
}

﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class FarmUnitRepository : BaseRepository<TbFarmUnit>, IFarmUnitRepository
    {
        public FarmUnitRepository(FFContext repositoryContext) : base(repositoryContext)
        {
        }
        public async Task CreateFarmUnitAsync(TbFarmUnit farmtype)
        {
            await Create(farmtype);
        }
        public async Task DeleteFarmUnitAsync(TbFarmUnit farmtype)
        {
            await Delete(farmtype);
        }
        public async Task<IEnumerable<TbFarmUnit>> GetAllFarmUnitsAsync()
        {
            var owners = await FindAllAsync();
            return owners;
        }
        public async Task<IEnumerable<TbFarmUnit>> GetFarmUnitByCondition(Expression<Func<TbFarmUnit, bool>> expression)
        {
            return await FindByConditionAync(expression);

        }
        public async Task<TbFarmUnit> GetFarmUnitByIdAsync(long id)
        {
            var owner = await FindByConditionAync(o => o.FarmUnitId == id);
            return owner.FirstOrDefault();
        }
        public async Task UpdateFarmUnitAsync(TbFarmUnit farmtype)
        {
            await Update(farmtype);
        }

        public async Task<IEnumerable<TbTubewellStatus>> TubeWell()
        {
            return await this.RepositoryContext.TbTubewellStatus.ToAsyncEnumerable().ToList();
        }

        public async Task<IEnumerable<TbTubewellTypeInfo>> TubeWellType()
        {
            return await this.RepositoryContext.TbTubewellTypeInfo.ToAsyncEnumerable().ToList();
        }

        public async Task<IEnumerable<TbWatercourseType>> WaterCourse()
        {
            return await this.RepositoryContext.TbWatercourseType.ToAsyncEnumerable().ToList();
        }

        public async Task<IEnumerable<TbTunnelTypeInfo>> Tunnel()
        {
            return await this.RepositoryContext.TbTunnelTypeInfo.ToAsyncEnumerable().ToList();
        }

        public async Task<IEnumerable<TbInputPurchaseInfo>> InputPurchase()
        {
            return await this.RepositoryContext.TbInputPurchaseInfo.ToAsyncEnumerable().ToList();
        }

        public async Task<IEnumerable<TbOutputExportInfo>> OutputExport()
        {
            return await this.RepositoryContext.TbOutputExportInfo.ToAsyncEnumerable().ToList();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Infrastructure.Models;
using Infrastructure.Interface;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class FarmerCropPlanRepository : BaseRepository<TbFarmerCropPlan>, IFarmerCropPlanRepository
    {
        public FarmerCropPlanRepository(FFContext repositoryContext)
        : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TbFarmerCropPlan>> GetAllFarmerCropPlanAsync()
        {
            return await FindAllAsync();
        }
        public async Task CreateFarmerCropPlanAsync(TbFarmerCropPlan farmerCropPlan)
        {
            await Create(farmerCropPlan);
        }
        public async Task DeleteFarmerCropPlanAsync(TbFarmerCropPlan farmerCropPlan)
        {
            await Delete(farmerCropPlan);
        }
        public async Task UpdateFarmerCropPlanAsync(TbFarmerCropPlan farmerCropPlan)
        {
            await Update(farmerCropPlan);
        }
        public async Task<IEnumerable<TbFarmerCropPlan>> GetFarmerCropPlanByCondition(Expression<Func<TbFarmerCropPlan, bool>> expression)
        {
            return await FindByConditionAync(expression);
        }
        public async Task<IEnumerable<TbFarmerCropPlan>> GetFarmerCropPlanByCondition(Expression<Func<TbFarmerCropPlan, bool>> expression, Expression<Func<TbFarmerCropPlan, object>> []includes)
        {
            return await FindByConditionAync(expression,includes);
        }
    }
}
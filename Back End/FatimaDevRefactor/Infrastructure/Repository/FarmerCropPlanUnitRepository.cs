﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace Infrastructure.Repository
{
    public class FarmerCropPlanUnitRepository : BaseRepository<TbFarmerCropPlanUnit>, IFarmerCropPlanUnitRepository
    {
        public FarmerCropPlanUnitRepository(FFContext repositoryContext)
         : base(repositoryContext)
        {
        }
        public async Task CreateFarmerCropPlanUnitAsync(TbFarmerCropPlanUnit crop)
        {
            await Create(crop);
        }
        public async Task DeleteFarmerCropPlanUnitAsync(TbFarmerCropPlanUnit FarmerCropPlanUnit)
        {
            await Delete(FarmerCropPlanUnit);
        }
        public async Task<IEnumerable<TbFarmerCropPlanUnit>> GetAllFarmerCropPlanUnitAsync()
        {
            return await FindAllAsync();
        }
        public async Task<IEnumerable<TbFarmerCropPlanUnit>> GetFarmerCropPlanUnitByCondition(Expression<Func<TbFarmerCropPlanUnit, bool>> expression)
        {
            return await FindByConditionAync(expression);
        }
        public async Task<TbFarmerCropPlanUnit> GetFarmerCropPlanUnitByIdAsync(long farmerCropPlanId)
        {
            var owner = await FindByConditionAync(o => o.FarmerCropPlanId == farmerCropPlanId);
            return owner.FirstOrDefault();
        }
        public async Task UpdateFarmerCropPlanUnitAsync(TbFarmerCropPlanUnit FarmerCropPlanUnit)
        {
            await Update(FarmerCropPlanUnit);
        }
    }
}

﻿using Infrastructure.Interface;
using Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class FarmerMediaRepository : BaseRepository<TbFarmerMedia>, IFarmerMediaRepository
    {
        public FarmerMediaRepository(FFContext repositoryContext)
          : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TbFarmerMedia>> GetAllTbMediasAsync()
        {
            return await FindAllAsync();
        }
        public async Task<TbFarmerMedia> GetMediaByIdAsync(long ownerId)
        {
            var owner = await FindByConditionAync(o => o.MediaId.Equals(ownerId));
            return owner.DefaultIfEmpty(new TbFarmerMedia())
                    .FirstOrDefault();
        }
        public async Task CreateMediaAsync(TbFarmerMedia media)
        {
            await Create(media);
            await SaveAsync();
        }
        public async Task UpdateMediaAsync(TbFarmerMedia owner)
        {
            await Update(owner);
        }
        public async Task DeleteMediaAsync(TbFarmerMedia media)
        {
            await Delete(media);
        }
        public async Task<IEnumerable<TbFarmerMedia>> GetFarmerMediaByCondition(Expression<Func<TbFarmerMedia, bool>> expression)
        {
            return await FindByConditionAync(expression);
        }
    }
}

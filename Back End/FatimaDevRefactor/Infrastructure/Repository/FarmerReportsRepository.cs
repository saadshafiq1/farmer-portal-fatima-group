﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace Infrastructure.Repository
{
    public class FarmerReportsRepository : BaseRepository<TbFarmerReports>, IFarmerReportsRepository
    {
        public FarmerReportsRepository(FFContext repositoryContext)
         : base(repositoryContext)
        {
        }
        public async Task CreateFarmerReportAsync(TbFarmerReports FarmerReport)
        {
            await Create(FarmerReport);
        }
        public async Task DeleteFarmerReportAsync(TbFarmerReports FarmerReport)
        {
            await Delete(FarmerReport);
        }
        public async Task<IEnumerable<TbFarmerReports>> GetAllFarmerReportsAsync(int pageNumber, int pageSize)
        {
            var includes = new Expression<Func<TbFarmerReports, object>>[1];
            includes[0] = f => f.Farmer;
            return await FindByConditionAync( includes, pageNumber, pageSize);
        }
        public async Task<IEnumerable<TbFarmerReports>> GetFarmerReportByCondition(Expression<Func<TbFarmerReports, bool>> expression)
        {
            var includes = new Expression<Func<TbFarmerReports, object>>[1];
            includes[0] = f => f.Farmer;
            return await FindByConditionAync(expression, includes);
        }
        public async Task<TbFarmerReports> GetFarmerReportByIdAsync(long ReportId)
        {
            var includes = new Expression<Func<TbFarmerReports, object>>[1];
            includes[0] = f => f.Farmer;
            var tbFarmerReports = await FindByConditionAync(r => r.ReportId == ReportId, includes);
            return tbFarmerReports.FirstOrDefault();
        }
        public async Task UpdateFarmerReportAsync(TbFarmerReports FarmerReport)
        {
            await Update(FarmerReport);
        }
    }
}

﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class FarmerRepository : BaseRepository<TbFarmerInfo>, IFarmererRepository
    {
        public FarmerRepository(FFContext repositoryContext)
           : base(repositoryContext)
        {
        }
        public async Task CreateFarmerAsync(TbFarmerInfo farmerInfo)
        {
            await Create(farmerInfo);
        }
        public async Task DeleteFarmerAsync(TbFarmerInfo farmerInfo)
        {
            await Delete(farmerInfo);
        }
        public async Task<IEnumerable<TbFarmerInfo>> GetAllFarmersAsync()
        {
            return await FindAllAsync();
        }
        public async Task<IEnumerable<TbFarmerInfo>> GetAllFarmerInfoAsync(int pageNumber, int pageSize, string filter = "", string value = "")
        {
            var includes = new Expression<Func<TbFarmerInfo, object>>[4];
            includes[0] = d => d.DistrictCodeNavigation;
            includes[1] = d => d.TehsilCodeNavigation;
            includes[2] = d => d.FarmerStatus;
            includes[3] = d => d.TbFarmInfo;


            Expression<Func<TbFarmerInfo, bool>> expression;

            if (!string.IsNullOrEmpty(filter) && !string.IsNullOrEmpty(value))
            {
                if ("Farmer".ToLower().Equals(filter))
                    expression = d => d.FarmerName.Contains(value) && d.ActiveStatus == "A";
                else if ("Address".ToLower().Equals(filter))
                    expression = d => d.PermanentAddress.Contains(value) || d.PresentAddress.Contains(value) && d.ActiveStatus == "A";
                else if ("Phone".ToLower().Equals(filter))
                    expression = d => d.CellPhone.Contains(value) && d.ActiveStatus == "A";
                else if ("Cnic".ToLower().Equals(filter))
                    expression = d => d.Cnic.Contains(value) && d.ActiveStatus == "A";
                else if ("District".ToLower().Equals(filter))
                    expression = d => d.DistrictCodeNavigation.DistrictName.Contains(value) && d.ActiveStatus == "A";
                else if ("Tehsil".ToLower().Equals(filter))
                    expression = d => d.TehsilCodeNavigation.TehsilName.Contains(value) && d.ActiveStatus == "A";
                else if ("Province".ToLower().Equals(filter))
                    expression = d => d.ProvinceCode == Int32.Parse(value) && d.ActiveStatus == "A";
                else
                    expression = d => d.FarmerName != "" && d.ActiveStatus == "A";
            }
            else
                expression = d => d.FarmerName != "" && d.ActiveStatus == "A";

            return await FindByConditionAync(expression, includes, pageNumber, pageSize);
        }
        public async Task<IEnumerable<TbFarmerInfo>> GetAllFarmersCountAsync(string filter = "", string value = "")
        {
            var includes = new Expression<Func<TbFarmerInfo, object>>[1];
            includes[0] = d => d.DistrictCodeNavigation;

            Expression<Func<TbFarmerInfo, bool>> expression;

            if (!string.IsNullOrEmpty(filter) && !string.IsNullOrEmpty(value))
            {
                if ("Farmer".ToLower().Equals(filter))
                    expression = d => d.FarmerName.Contains(value) && d.ActiveStatus == "A";
                else if ("Address".ToLower().Equals(filter))
                    expression = d => d.PermanentAddress.Contains(value) || d.PresentAddress.Contains(value) && d.ActiveStatus == "A";
                else if ("Phone".ToLower().Equals(filter))
                    expression = d => d.CellPhone.Contains(value) && d.ActiveStatus == "A";
                else if ("Cnic".ToLower().Equals(filter))
                    expression = d => d.Cnic.Contains(value) && d.ActiveStatus == "A";
                else if ("District".ToLower().Equals(filter))
                    expression = d => d.DistrictCodeNavigation.DistrictName.Contains(value) && d.ActiveStatus == "A";
                else if ("Tehsil".ToLower().Equals(filter))
                    expression = d => d.TehsilCodeNavigation.TehsilName.Contains(value) && d.ActiveStatus == "A";
                else if ("Province".ToLower().Equals(filter))
                    expression = d => d.ProvinceCode == Int32.Parse(value) && d.ActiveStatus == "A";
                else
                    expression = d => d.FarmerName != "" && d.ActiveStatus == "A";
            }
            else
                expression = d => d.FarmerName != "" && d.ActiveStatus == "A";

            return await FindByConditionAync(expression, includes);
        }
        public async Task<IEnumerable<TbFarmerInfo>> GetFarmerByCondition(Expression<Func<TbFarmerInfo, bool>> expression)
        {
            var includes = new Expression<Func<TbFarmerInfo, object>>[8];
            includes[0] = f => f.DistrictCodeNavigation;
            includes[1] = f => f.TehsilCodeNavigation;
            includes[2] = f => f.ProvinceCodeNavigation;
            includes[3] = f => f.RegionCodeNavigation;
            includes[4] = f => f.PresentDistrictCodeNavigation;
            includes[5] = f => f.PresentProvinceCodeNavigation;
            includes[6] = f => f.PresentTehsilCodeNavigation;
            includes[7] = f => f.PresentRegionCodeNavigation;

            return await FindByConditionAync(expression, includes);
        }
        public async Task<IEnumerable<TbFarmerInfo>> GetFarmerByCondition(Expression<Func<TbFarmerInfo, bool>> expression, Expression<Func<TbFarmerInfo, object>>[] includes)
        {
            return await FindByConditionAync(expression, includes);
        }
        public async Task<IEnumerable<TbFarmerStatus>> GetFarmerStatuses()
        {
            return await this.RepositoryContext.TbFarmerStatus.ToAsyncEnumerable().ToList();
        }
        public async Task<TbFarmerInfo> GetFarmerByCellPhone(Expression<Func<TbFarmerInfo, bool>> expression, Expression<Func<TbFarmerInfo, object>>[] includes)
        {
            var farmer = await FindByConditionAync(expression, includes);
            return farmer.FirstOrDefault();
        }
        public async Task<TbFarmerInfo> GetFarmerByIdAsync(int FarmerID)
        {
            var owner = await FindByConditionAync(o => o.FarmerId == FarmerID);
            return owner.FirstOrDefault();
        }
        public async Task UpdateFarmerAsync(TbFarmerInfo farmerInfo)
        {
            await Update(farmerInfo);
        }
    }
}

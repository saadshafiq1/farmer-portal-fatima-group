﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace Infrastructure.Repository
{
    public class FertilizerTypesRepository : BaseRepository<TbFertilizerTypes>, IFertilizerTypesRepository
    {
        public FertilizerTypesRepository(FFContext repositoryContext)
         : base(repositoryContext)
        {
        }
        public async Task CreateFertilizerTypeAsync(TbFertilizerTypes fertilizerType)
        {
            await Create(fertilizerType);
        }
        public async Task DeleteFertilizerTypeAsync(TbFertilizerTypes fertilizerType)
        {
            await Delete(fertilizerType);
        }
        public async Task<IEnumerable<TbFertilizerTypes>> GetAllFertilizerTypesAsync()
        {
            var includes = new Expression<Func<TbFertilizerTypes, object>>[1];
            includes[0] = f => f.TbFertilizerTypeDetails;

            return await FindByConditionAync(includes);
        }
        public async Task<IEnumerable<TbFertilizerTypes>> GetFertilizerTypeByCondition(Expression<Func<TbFertilizerTypes, bool>> expression)
        {
            var includes = new Expression<Func<TbFertilizerTypes, object>>[1];
            includes[0] = f => f.TbFertilizerTypeDetails;

            return await FindByConditionAync(expression, includes);
        }
        public async Task<TbFertilizerTypes> GetFertilizerTypeByIdAsync(long fertilizerTypeId)
        {
            var includes = new Expression<Func<TbFertilizerTypes, object>>[1];
            includes[0] = f => f.TbFertilizerTypeDetails;

            var fertilizer= await FindByConditionAync(f => f.FertilizerId == fertilizerTypeId, includes);         
            return fertilizer.FirstOrDefault();
        }
        public async Task UpdateFertilizerTypeAsync(TbFertilizerTypes fertilizerType)
        {
            await Update(fertilizerType);
        }
    }
}

﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class MediaRepository : BaseRepository<TbMedia>, IMediaRepository
    {
        public MediaRepository(FFContext repositoryContext)
            : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TbMedia>> GetAllTbMediasAsync()
        {
            return await FindAllAsync();
        }
        public async Task<IEnumerable<TbMedia>> GetAllTbMediasAsync(int pageNumber, int pageSize, string filter = "", string value = "")
        {
            var includes = new Expression<Func<TbMedia, object>>[2];
            includes[0] = d => d.DistrictCodeNavigation;
            includes[1] = d => d.MediaType;

            Expression<Func<TbMedia, bool>> expression = m => m.MediaType.MediaType == "News";


            if (!string.IsNullOrEmpty(filter) && !string.IsNullOrEmpty(value))
            {
                if ("District".ToLower().Equals(filter))
                    expression = m => m.DistrictCodeNavigation.DistrictName.Contains(value) && m.MediaType.MediaType == "News";
                else if ("Status".ToLower().Equals(filter))
                    expression = m => m.ActiveStatus.Contains(value) && m.MediaType.MediaType == "News";
            }

            return await FindByConditionAync(expression, includes, pageNumber, pageSize);
        }
        public async Task<IEnumerable<TbMedia>> GetAllTbMediasAsync(string filter = "", string value = "")
        {
            var includes = new Expression<Func<TbMedia, object>>[2];
            includes[0] = d => d.DistrictCodeNavigation;
            includes[1] = d => d.MediaType;

            Expression<Func<TbMedia, bool>> expression = m => m.MediaType.MediaType == "News";


            if (!string.IsNullOrEmpty(filter) && !string.IsNullOrEmpty(value))
            {
                if ("District".ToLower().Equals(filter))
                    expression = m => m.DistrictCodeNavigation.DistrictName.Contains(value) && m.MediaType.MediaType == "News";
                else if ("Status".ToLower().Equals(filter))
                    expression = m => m.ActiveStatus.Contains(value) && m.MediaType.MediaType == "News";
            }

            return await FindByConditionAync(expression, includes);
        }
        public async Task<TbMedia> GetMediaByIdAsync(long ownerId)
        {
            var owner = await FindByConditionAync(o => o.MediaId == ownerId);
            return owner.FirstOrDefault();
        }
        public async Task CreateMediaAsync(TbMedia media)
        {
            await Create(media);
        }
        public async Task UpdateMediaAsync(TbMedia owner)
        {
            await Update(owner);
        }
        public async Task DeleteMediaAsync(TbMedia media)
        {
            await Delete(media);
        }
        public async Task<IEnumerable<TbMedia>> GetMediaByCondition(Expression<Func<TbMedia, bool>> expression)
        {
            return await FindByConditionAync(expression);
        }
    }
}

﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
  public  class RoleRepository:BaseRepository<TbUserRoleTypeCode>,IRoleRepository
    {

        public RoleRepository(FFContext repositoryContext)
             : base(repositoryContext)
        {
        }
        public async Task CreateRoleAsync(TbUserRoleTypeCode User)
        {
            await Create(User);           
        }
        public async Task DeleteRoleAsync(TbUserRoleTypeCode User)
        {
            await Delete(User);          
        }
        public async Task<IEnumerable<TbUserRoleTypeCode>> GetAllRolesAsync()
        {
            return await FindAllAsync();
        }
        public async Task<IEnumerable<TbUserRoleTypeCode>> GetRoleByCondition(Expression<Func<TbUserRoleTypeCode, bool>> expression)
        {
            return await FindByConditionAync(expression);
        }
        public async Task<TbUserRoleTypeCode> GetRoleByIdAsync(int ownerId)
        {
            var owner = await FindByConditionAync(o => o.UserRoleCode == ownerId);
            return owner.FirstOrDefault();
        }
        public async Task UpdateRoleAsync(TbUserRoleTypeCode owner)
        {
            await Update(owner);          
        }
    }
}

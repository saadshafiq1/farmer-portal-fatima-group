﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace Infrastructure.Repository
{
    public class SeedTypesRepository : BaseRepository<TbSeedTypes>, ISeedTypesRepository
    {
        public SeedTypesRepository(FFContext repositoryContext)
         : base(repositoryContext)
        {
        }
        public async Task CreateSeedTypeAsync(TbSeedTypes SeedType)
        {
            await Create(SeedType);
        }
        public async Task DeleteSeedTypeAsync(TbSeedTypes SeedType)
        {
            await Delete(SeedType);
        }
        public async Task<IEnumerable<TbSeedTypes>> GetAllSeedTypesAsync()
        {
            return await FindAllAsync();
        }
        public async Task<IEnumerable<TbSeedTypes>> GetSeedTypeByCondition(Expression<Func<TbSeedTypes, bool>> expression)
        {
            return await FindByConditionAync(expression);
        }
        public async Task<TbSeedTypes> GetSeedTypeByIdAsync(long seedTypeId)
        {
            var owner = await FindByConditionAync(o => o.SeedId == seedTypeId);
            return owner.FirstOrDefault();
        }
        public async Task UpdateSeedTypeAsync(TbSeedTypes seedType)
        {
            await Update(seedType);
        }
    }
}

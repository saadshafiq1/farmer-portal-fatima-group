﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class TicketRepository : BaseRepository<TbTicketInfo>, ITicketRepository
    {
        public TicketRepository(FFContext repositoryContext)
        : base(repositoryContext)
        {            
        }
        public async Task<IEnumerable<TbTicketInfo>> GetAllTicketsAsync()
        {
            return await FindAllAsync();
        }
        public async Task CreateTicketAsync(TbTicketInfo ticket)
        {
            await Create(ticket);
        }
        public async Task DeleteTicketAsync(TbTicketInfo ticket)
        {
            await Delete(ticket);
        }
        public async Task<TbTicketInfo> GetTicketInfoById(long id)
        {

            var includes = new Expression<Func<TbTicketInfo, object>>[7];
            includes[0] = f => f.TbTicketActivity;
            includes[1] = f => f.Farm;
            includes[2] = f => f.Farmer;
            includes[3] = f => f.Status;
            includes[4] = f => f.TicketType;
            includes[5] = f => f.Farm.TbFarmBlock;
            includes[6] = f => f.Farm.DistrictCodeNavigation;
            
                      
            var ticket = await FindByConditionAync(d => d.TicketId == id, includes);
            return ticket.FirstOrDefault();
        }
        public async Task<IEnumerable<TbTicketInfo>> GetTicketByCondition(Expression<Func<TbTicketInfo, bool>> expression)
        {
            return await FindByConditionAync(expression);
        }
        public async Task<IEnumerable<TbTicketInfo>> GetTicketByCondition(Expression<Func<TbTicketInfo, bool>> expression, Expression<Func<TbTicketInfo, object>>[] includes)
        {
            return await FindByConditionAync(expression, includes);
        }
        public async Task<TbTicketInfo> GetTicketByIdAsync(long ticketId)
        {
            var owner = await FindByConditionAync(o => o.TicketId == ticketId);
            return owner.FirstOrDefault();
        }
        public async Task UpdateTicketAsync(TbTicketInfo ticket)
        {
            await Update(ticket);
        }
        public async Task<IEnumerable<TbTicketInfo>> GetAllTicketInfoAsync(int pageNumber, int pageSize, string filter = "", string value = "")
        {
            var includes = new Expression<Func<TbTicketInfo, object>>[5];
            includes[0] = d => d.Farmer;
            includes[1] = d => d.Farm;
            includes[2] = d => d.TicketType;
            includes[3] = d => d.TicketType;
            includes[4] = d => d.Status;

            Expression<Func<TbTicketInfo, bool>> expression;

            if (!string.IsNullOrEmpty(filter) && !string.IsNullOrEmpty(value))
            {
                if ("Status".ToLower().Equals(filter))
                    expression = d => d.StatusId == Int32.Parse(value);
                else if ("Phone".ToLower().Equals(filter))
                    expression = d => d.Farmer.CellPhone.Contains(value) || d.Farmer.LandLine.Contains(value);
                else if ("cnic".ToLower().Equals(filter))
                    expression = d => d.Farmer.Cnic.Contains(value);
                else if ("ticketId".ToLower().Equals(filter))
                    expression = d => d.TicketId == Int32.Parse(value);
                else
                    expression = d => d.TicketId > 0;
            }
            else
                expression = d => d.TicketId > 0;

            return await FindByConditionAync(expression, includes, pageNumber, pageSize);
        }
        public async Task<IEnumerable<TbTicketInfo>> GetAllTicketCountAsync(string filter = "", string value = "")
        {
            var includes = new Expression<Func<TbTicketInfo, object>>[2];
            includes[0] = d => d.Farmer;
            includes[1] = d => d.Farm;

            Expression<Func<TbTicketInfo, bool>> expression;

            if (!string.IsNullOrEmpty(filter) && !string.IsNullOrEmpty(value))
            {
                if ("Status".ToLower().Equals(filter))
                    expression = d => d.StatusId == Int32.Parse(value);
                else if ("Phone".ToLower().Equals(filter))
                    expression = d => d.Farmer.CellPhone.Contains(value) || d.Farmer.LandLine.Contains(value);
                else if ("cnic".ToLower().Equals(filter))
                    expression = d => d.Farmer.Cnic.Contains(value);
                else if ("ticketId".ToLower().Equals(filter))
                    expression = d => d.TicketId == Int32.Parse(value);
                else
                    expression = d => d.TicketId > 0;
            }
            else
                expression = d => d.TicketId > 0;

            return await FindByConditionAyncForCount(expression, includes);
        }
        public async Task<IEnumerable<TbTicketInfo>> GetFarmerTicketInfoAsync(int pageNumber, int pageSize, int farmerid, string filter = "", string value = "")
        {
            var includes = new Expression<Func<TbTicketInfo, object>>[4];
            includes[0] = d => d.Farmer;
            includes[1] = d => d.Farm;
            includes[2] = d => d.TicketType;
            includes[3] = d => d.Status;
            

            Expression<Func<TbTicketInfo, bool>> expression;

            if (!string.IsNullOrEmpty(filter) && !string.IsNullOrEmpty(value))
            {
                if ("Status".ToLower().Equals(filter))
                    expression = d => d.StatusId == Int32.Parse(value) && d.FarmerId == farmerid;
                else if ("Phone".ToLower().Equals(filter))
                    expression = d => (d.Farmer.CellPhone.Contains(value) || d.Farmer.LandLine.Contains(value)) && d.FarmerId == farmerid;
                else if ("cnic".ToLower().Equals(filter))
                    expression = d => d.Farmer.Cnic.Contains(value) && d.FarmerId == farmerid;
                else if ("ticketId".ToLower().Equals(filter))
                    expression = d => d.TicketId == Int32.Parse(value) && d.FarmerId == farmerid;
                else
                    expression = d => d.FarmerId == farmerid;
            }
            else
                expression = d => d.FarmerId == farmerid;

            return await FindByConditionAync(expression, includes, pageNumber, pageSize);
        }
        public async Task<IEnumerable<TbTicketInfo>> GetFarmerTicketCountAsync(int farmerid ,string filter = "", string value = "")
        {
            var includes = new Expression<Func<TbTicketInfo, object>>[2];
            includes[0] = d => d.Farmer;
            includes[1] = d => d.Farm;

            Expression<Func<TbTicketInfo, bool>> expression;

            if (!string.IsNullOrEmpty(filter) && !string.IsNullOrEmpty(value))
            {
                    if ("Status".ToLower().Equals(filter))
                        expression = d => d.StatusId == Int32.Parse(value) && d.FarmerId == farmerid;
                    else if ("Phone".ToLower().Equals(filter))
                        expression = d =>( d.Farmer.CellPhone.Contains(value) || d.Farmer.LandLine.Contains(value) )&& d.FarmerId == farmerid;
                    else if ("cnic".ToLower().Equals(filter))
                        expression = d => d.Farmer.Cnic.Contains(value) && d.FarmerId == farmerid;
                    else if ("ticketId".ToLower().Equals(filter))
                        expression = d => d.TicketId == Int32.Parse(value) && d.FarmerId == farmerid;
                    else
                        expression = d => d.FarmerId == farmerid;
            }
            else
                expression = d =>  d.FarmerId == farmerid;

            return await FindByConditionAyncForCount(expression, includes);
        }
        public async Task<IEnumerable<TbTicketInfo>> GetTSOTicketInfoAsync(int pageNumber, int pageSize, int tsoID, string filter = "", string value = "")
        {
            var includes = new Expression<Func<TbTicketInfo, object>>[5];
            includes[0] = d => d.Farmer;
            includes[1] = d => d.Farm;
            includes[2] = d => d.TicketType;
            includes[3] = d => d.Status;
            includes[4] = d => d.Farmer.DistrictCodeNavigation;


            Expression<Func<TbTicketInfo, bool>> expression;

            if (!string.IsNullOrEmpty(filter) && !string.IsNullOrEmpty(value))
            {
                if ("Status".ToLower().Equals(filter))
                    expression = d => d.StatusId == Int32.Parse(value) && d.AssignedTo == tsoID;
                else if ("Phone".ToLower().Equals(filter))
                    expression = d => (d.Farmer.CellPhone.Contains(value) || d.Farmer.LandLine.Contains(value)) && d.AssignedTo == tsoID;
                else if ("cnic".ToLower().Equals(filter))
                    expression = d => d.Farmer.Cnic.Contains(value) && d.AssignedTo == tsoID;
                else if ("ticketId".ToLower().Equals(filter))
                    expression = d => d.TicketId == Int32.Parse(value) && d.AssignedTo == tsoID;
                else
                    expression = d => d.AssignedTo == tsoID;
            }
            else
                expression = d => d.AssignedTo == tsoID;

            return await FindByConditionAync(expression, includes, pageNumber, pageSize);
        }
        public async Task<IEnumerable<TbTicketInfo>> GetTSOTicketCountAsync(int tsoID, string filter = "", string value = "")
        {
            var includes = new Expression<Func<TbTicketInfo, object>>[2];
            includes[0] = d => d.Farmer;
            includes[1] = d => d.Farm;

            Expression<Func<TbTicketInfo, bool>> expression;

            if (!string.IsNullOrEmpty(filter) && !string.IsNullOrEmpty(value))
            {
                if ("Status".ToLower().Equals(filter))
                    expression = d => d.StatusId == Int32.Parse(value) && d.AssignedTo == tsoID;
                else if ("Phone".ToLower().Equals(filter))
                    expression = d => (d.Farmer.CellPhone.Contains(value) || d.Farmer.LandLine.Contains(value)) && d.AssignedTo == tsoID;
                else if ("cnic".ToLower().Equals(filter))
                    expression = d => d.Farmer.Cnic.Contains(value) && d.AssignedTo == tsoID;
                else if ("ticketId".ToLower().Equals(filter))
                    expression = d => d.TicketId == Int32.Parse(value) && d.AssignedTo == tsoID;
                else
                    expression = d => d.AssignedTo == tsoID;
            }
            else
                expression = d => d.AssignedTo == tsoID;

            return await FindByConditionAyncForCount(expression, includes);
        }
    }

    public  class TicketTypeRepository: BaseRepository<TbTicketsTypes>, ITicketTypeRepository
    {
        public TicketTypeRepository(FFContext repositoryContext)
       : base(repositoryContext)
        {
        }

        public async Task<IEnumerable<TbTicketsTypes>> GetTicketTypes()
        {
            return await FindAllAsync();
        }
    }
   
    public interface ITicketTypeRepository
    {
        Task<IEnumerable<TbTicketsTypes>> GetTicketTypes();
    }

    public class TicketTypeStatusRepository : BaseRepository<TbTicketsStatus>, ITicketTypeStatusRepository
    {
        public TicketTypeStatusRepository(FFContext repositoryContext)
        : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TbTicketsStatus>> GetAllTicketsStautsAsync()
        {
            return await FindAllAsync();
        }
        public async Task<TbTicketsStatus> GetTicketsStatusByID(int ID)
        {
            var ticket = await FindByConditionAync(d => d.StatusId == ID);
            return ticket.FirstOrDefault();
        }
    }
   
    public interface ITicketTypeStatusRepository
        {
        Task<IEnumerable<TbTicketsStatus>> GetAllTicketsStautsAsync();
        Task<TbTicketsStatus> GetTicketsStatusByID(int ID);
    }

    public class TicketActivityRepository : BaseRepository<TbTicketActivity>, ITicketActivityRepository
    {
        public TicketActivityRepository(FFContext repositoryContext)
        : base(repositoryContext)
        {
        }
        public async Task CreateTicketAsync(TbTicketActivity ticket)
        {
            await Create(ticket);
        }
    }   

    public interface ITicketActivityRepository
        {
        Task CreateTicketAsync(TbTicketActivity ticket);
    }

}
﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class TsoRepository : BaseRepository<TbTsoInfo>, ITsoRepository
    {
        public TsoRepository(FFContext repositoryContext)
          : base(repositoryContext)
        {
        }
        public async Task<TbTsoInfo> GetTSOById(Expression<Func<TbTsoInfo, bool>> expression)
        {
            var result = await FindByConditionAync(expression);

            return result.FirstOrDefault();
        }
        public async Task<IEnumerable<TbTsoInfo>> GetAllTSOsAsync(Expression<Func<TbTsoInfo, object>>[] includes)
        {
            return await FindByConditionAync(includes);
        }
        public async Task<TbTsoInfo> GetTSOByCellPhone(Expression<Func<TbTsoInfo, bool>> expression, Expression<Func<TbTsoInfo, object>>[] includes)
        {
            var tso = await FindByConditionAync(expression, includes);
            return tso.FirstOrDefault();
        }
    }
}

﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class UnitShapeRepository : BaseRepository<TbUnitShapes>, IUnitShapeRepository
    {
        public UnitShapeRepository(FFContext repositoryContext) : base(repositoryContext)
        {
        }
        public async Task CreateUnitShapeAsync(TbUnitShapes media)
        {
            await Create(media);
        }
        public async Task DeleteUnitShapeAsync(TbUnitShapes media)
        {
            await Delete(media);
        }
        public async Task<IEnumerable<TbUnitShapes>> GetAllUnitShapesAsync()
        {
            return await FindAllAsync();
        }
        public async Task<IEnumerable<TbUnitShapes>> GetUnitShapeByCondition(Expression<Func<TbUnitShapes, bool>> expression)
        {
            return await FindByConditionAync(expression);
        }
        public async Task<IEnumerable<TbUnitShapes>> GetUnitShapeByCondition(Expression<Func<TbUnitShapes, bool>> expression, Expression<Func<TbUnitShapes, object>>[] includes)
        {
            return await FindByConditionAync(expression, includes);
        }
        public async Task<TbUnitShapes> GetUnitShapeByIdAsync(long ownerId)
        {
            var a = await FindByConditionAync(c => c.UnitId == ownerId);
            return a.FirstOrDefault();
        }
        public async Task UpdateUnitShapeAsync(TbUnitShapes owner)
        {
            await Update(owner);
        }
    }
}

﻿using Infrastructure.Interface;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class UserLoginRepository : BaseRepository<TbUserLogin>, IUserLoginRepository
    {

        public UserLoginRepository(FFContext repositoryContext)
           : base(repositoryContext)
        {
        }
        public async Task CreateUserAsync(TbUserLogin User)
        {
            await Create(User);
        }
        public async Task DeleteUserAsync(TbUserLogin User)
        {
            await Delete(User);
        }
        public async Task<IEnumerable<TbUserLogin>> GetAllUsersAsync()
        {
            return await FindAllAsync();
        }
        public async Task<IEnumerable<TbUserLogin>> GetUserByCondition(Expression<Func<TbUserLogin, bool>> expression)
        {
            return await FindByConditionAync(expression);
        }
        public async Task<TbUserLogin> GetUserByIdAsync(Guid ownerId)
        {
            var owner = await FindByConditionAync(o => o.UserId == ownerId);
            return owner.FirstOrDefault();
        }
        public async Task UpdateUserAsync(TbUserLogin owner)
        {
            await Update(owner);
        }
    }
}

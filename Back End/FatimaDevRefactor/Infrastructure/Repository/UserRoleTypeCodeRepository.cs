﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Infrastructure.Models;
using Infrastructure.Interface;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class UserRoleTypeCodeRepository : BaseRepository<TbUserRoleTypeCode>, IUserRoleTypeCodeRepository
    {
        public UserRoleTypeCodeRepository(FFContext repositoryContext)
        : base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TbUserRoleTypeCode>> GetAllUserRoleTypeCodesAsync()
        {
            return await FindAllAsync();
        }
        public async Task CreateUserRoleTypeCodeAsync(TbUserRoleTypeCode userRoleTypeCode)
        {
            await Create(userRoleTypeCode);
        }
        public async Task DeleteUserRoleTypeCodeAsync(TbUserRoleTypeCode userRoleTypeCode)
        {
            await Delete(userRoleTypeCode);
        }
        public async Task UpdateUserRoleTypeCodeAsync(TbUserRoleTypeCode userRoleTypeCode)
        {
            await Update(userRoleTypeCode);
        }
        public async Task<IEnumerable<TbUserRoleTypeCode>> GetUserRoleTypeCodeByCondition(Expression<Func<TbUserRoleTypeCode, bool>> expression)
        {
            return await FindByConditionAync(expression);
        }
    }
}
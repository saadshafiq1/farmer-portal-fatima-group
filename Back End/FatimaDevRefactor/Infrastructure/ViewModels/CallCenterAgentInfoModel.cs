﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
   public class CallCenterAgentInfoModel
    {
        public int CallCenterAgentId { get; set; }
        public string AgentName { get; set; }
        public string Cnic { get; set; }
        public string Email { get; set; }
        public string CellPhone { get; set; }
        public string AlternateCellPhone { get; set; }
        public string Landline { get; set; }
        public string Gender { get; set; }       
        public Guid? UserGuid { get; set; }
    }
}

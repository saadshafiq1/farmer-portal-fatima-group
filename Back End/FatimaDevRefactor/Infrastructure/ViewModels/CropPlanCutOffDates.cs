﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class CropPlanCutOffDates
    {
        public int CropID{ get; set; }
        public DateTime MinDate { get; set; }
        public DateTime MaxDate { get; set; }
        
    }
}

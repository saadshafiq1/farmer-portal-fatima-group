﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
    public class DealaerCategoryModel
    {
        public int DealerCategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}

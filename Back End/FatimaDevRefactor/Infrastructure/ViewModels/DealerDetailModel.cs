﻿using System;
using System.Collections.Generic;
using System.Text;
using Infrastructure.Models;

namespace Infrastructure.ViewModels
{
    public class DealerDetailModel
    {
        public int DealerId { get; set; }
        public string DealerName { get; set; }
        public string DealerNameUrdu { get; set; }
        public string Cnic { get; set; }
        public string Ntn { get; set; }
        public string Strn { get; set; }
        public string DealerCategory { get; set; }
        public int? DealerStatus { get; set; }
        public DateTime? TaxProfileDate { get; set; }
        public string CurrentStatus { get; set; }
        public int? Ccnumber { get; set; }
        public int? SherpCc { get; set; }
        public string Proprieter { get; set; }
        public string Address { get; set; }
        public int? Province { get; set; }      
        public int? DistrictCode { get; set; }
        public string CityVillage { get; set; }
        public int? TehsilCode { get; set; }
        public int? SaleRegion { get; set; }
        public int? SalePoint { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string CellPhone { get; set; }
        public string Landline { get; set; }
        public string Email { get; set; }
        public string DealerImage { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
        public DistrictCodeModel DistrictCodeNavigation { get; set; }
        public TehsilCodeModel TehsilCodeNavigation { get; set; }
        public DealerStatusModel DealerStatusNavigation { get; set; }
        public DealaerCategoryModel DealerCategoryNavigation { get; set; }

    }

    
}

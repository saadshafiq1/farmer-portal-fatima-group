﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
    public class DealerStatusModel
    {
        public int DealerStatusId { get; set; }
        public string Status { get; set; }
    }
}

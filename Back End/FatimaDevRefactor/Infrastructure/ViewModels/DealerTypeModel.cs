﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
   public class DealerTypeModel
    {
        public int DealerTypeCodeId { get; set; }
        public string TypeName { get; set; }
        public string TypeNameUrdu { get; set; }

    }
}

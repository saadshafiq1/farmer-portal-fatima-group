﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
   public class FarmBlockModel
    {
        public long FarmBlockId { get; set; }
        public long? FarmId { get; set; }
        public string BlockName { get; set; }
        public decimal? BlockArea { get; set; }
        public string ActiveStatus { get; set; }
     
    }
}

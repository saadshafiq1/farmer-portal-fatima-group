﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
    public class FarmModel
    {
        //public FarmModel()
        //{
        //   // TbBlockCrops = new HashSet<TbBlockCrops>();
        //    //TbFarmBlockUnit = new HashSet<TbFarmBlockUnit>();
        //}
        public ICollection<FarmBlockModel> TbFarmBlock { get; set; }
        //public long FarmBlockId { get; set; }
        public long? FarmId { get; set; }
        public int? FarmTypeCode { get; set; }
        public string FarmType { get; set; }
       // public string Canal { get; set; }
       // public string OwnerTubeWell { get; set; }
        public decimal? AreaonHeis { get; set; }
       // public string WaterCourseType { get; set; }
        public decimal? TotalAreaAcres { get; set; }
        public string FarmName { get; set; }
        public string FarmAddress { get; set; }
        public long? FarmGis { get; set; }
        public int? ProvinceCode { get; set; }
        public int? TehsilCode { get; set; }
        public int? DistrictCode { get; set; }
        public long? RegionId { get; set; }
        public long? TotalUnits { get; set; }
        public long? WaterSoilResultId { get; set; }
        public int? ModifiedBy { get; set; }
        public string BlockName { get; set; }
        public decimal? BlockArea { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
       // public TbFarmTypeCode FarmTypeCodeNavigation { get; set; }
        // public TbFarmInfo Farm { get; set; }
        // public TbWaterSoilResult WaterSoilResult { get; set; }
        // public ICollection<TbBlockCrops> TbBlockCrops { get; set; }
        // public ICollection<TbFarmBlockUnit> TbFarmBlockUnit { get; set; }
    }
}

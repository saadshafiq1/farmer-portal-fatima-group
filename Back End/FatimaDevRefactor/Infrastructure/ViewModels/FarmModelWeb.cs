﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
    public class FarmModelWeb
    {
      public FarmModel FarmModel { get; set; }

        public int DistrictCode { get; set; }
        public string DistrictName { get; set; }
        public string DistrictNameUrdu { get; set; }

        public int TehsilCode { get; set; }
        public string TehsilName { get; set; }
        public string TehsilNameUrdu { get; set; }

        public int ProvinceCode { get; set; }
        public string ProvinceName { get; set; }
        public string ProvinceNameUrdu { get; set; }

        public int RegionCode { get; set; }
        public string RegionName { get; set; }
        public string RegionNameUrdu { get; set; }

        //public TbDistrictCode DistrictCodeNavigation { get; set; }
        //public TbTehsilCode TehsilCodeNavigation { get; set; }
        //public TbProvinceCode ProvinceCodeNavigation { get; set; }
        //public TbRegionCode RegionCodeNavigation { get; set; }

    }
}

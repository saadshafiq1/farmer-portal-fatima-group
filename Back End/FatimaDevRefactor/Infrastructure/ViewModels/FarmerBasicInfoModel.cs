﻿using Infrastructure.ViewModels;
using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class FarmerBasicInfoModel
    {
        public long FarmerId { get; set; }
        public string FarmerName { get; set; }
        public string FatherHusbandName { get; set; }
        public string Cnic { get; set; }
        public string Gender { get; set; }
        public string CellPhone { get; set; }               
    }
}

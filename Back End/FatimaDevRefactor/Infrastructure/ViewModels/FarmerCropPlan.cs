﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
   public class FarmerCropPlan
    {
        public List<GeneralCropPlan> Plan { get; set; }
         public int[] UnitIds { get; set; }

        public long? CropPlanId { get; set; }
        public int FarmId { get; set; }
    }
}

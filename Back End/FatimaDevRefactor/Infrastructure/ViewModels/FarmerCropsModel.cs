﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
    public class FarmerCropsModel
    {
        public long CropId { get; set; }
        public string CropName { get; set; }
        public string CropNameUrdu { get; set; }
        public long FarmerCropPlanId { get; set; }
        public string CropImage { get; set; }
        public int CropPlanNumber { get; set; }
        public DateTime? SowingDate { get; set; }


    }
}

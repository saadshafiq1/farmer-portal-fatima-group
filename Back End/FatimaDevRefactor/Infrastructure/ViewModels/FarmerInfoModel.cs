﻿using Infrastructure.ViewModels;
using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class FarmerInfoModel
    {
        public long FarmerId { get; set; }
        public string FarmerName { get; set; }
        public string FatherHusbandName { get; set; }
        public string Cnic { get; set; }
        public string Gender { get; set; }
        public string CellPhone { get; set; }
        public string PresentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public int? EducationCode { get; set; }
        public int? MaleDependant { get; set; }
        public int? FemaleDependant { get; set; }
        public string AlternateName { get; set; }
        public string AlternateCellPhoneNo { get; set; }
        public string AlternateRelationshipwithFarmer { get; set; }
        public int? ReferalCode { get; set; }
        public string FarmerImage { get; set; }
        public string UnionCouncil { get; set; }
        public string MozaName { get; set; }
        public int? SmallAnimals { get; set; }
        public int? BigAnimals { get; set; }
        public int? Tractor { get; set; }
        public Guid? UserGuid { get; set; }
        public DateTime? DateOfBirth { get; set; }

        public DistrictCodeModel DistrictCodeNavigation { get; set; }
        public TehsilCodeModel TehsilCodeNavigation { get; set; }
        public FarmerStatusModelnfo FarmerStatus { get; set; }
    }
}

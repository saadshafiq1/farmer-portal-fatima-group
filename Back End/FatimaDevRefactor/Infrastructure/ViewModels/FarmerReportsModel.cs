﻿using Infrastructure.ViewModels;
using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class FarmerReportsModel
    {
        public int ReportId { get; set; }
        public int FarmerId { get; set; }
        public string ReportName { get; set; }
        public DateTime? ReportDate { get; set; }
        public string Url { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }

        public FarmerBasicInfoModel Farmer { get; set; }
    }
}

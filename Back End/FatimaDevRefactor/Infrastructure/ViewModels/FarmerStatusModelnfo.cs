﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
    public class FarmerStatusModelnfo
    {
        public int FarmerStatusId { get; set; }
        public string FarmerStatueName { get; set; }
    }
}

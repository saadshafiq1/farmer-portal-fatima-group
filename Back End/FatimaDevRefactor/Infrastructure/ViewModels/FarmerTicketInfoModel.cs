﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
    public class FarmerTicketInfoModel
    {
        public int TicketId { get; set; }     
        public string StatusName { get; set; }  
        public string TicketTypeName { get; set; }      
        public string Slastatus { get; set; }
        public long CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public DateTime? CreatedDate { get; set; }     
        public string AssignedToName { get; set; }
        public string AssignedToCellPhone { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? ResolvedDate { get; set; }
        public string TicketSource { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
    public class LocationDataModel
    {
        public int DistrictCode { get; set; }
        public string DistrictName { get; set; }
        public string DistrictNameUrdu { get; set; }

        public int TehsilCode { get; set; }
        public string TehsilName { get; set; }
        public string TehsilNameUrdu { get; set; }

        public int ProvinceCode { get; set; }
        public string ProvinceName { get; set; }
        public string ProvinceNameUrdu { get; set; }

        public int RegionCode { get; set; }
        public string RegionName { get; set; }
        public string RegionNameUrdu { get; set; }
    }
}

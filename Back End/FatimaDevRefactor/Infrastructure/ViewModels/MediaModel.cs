﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
    public class MediaModel
    {
        public long MediaId { get; set; }      
        public string Description { get; set; }       
        public string Url { get; set; }
        public string Header { get; set; }
        public string Body { get; set; }
        public string Footer { get; set; }
        public string LeftContent { get; set; }
        public string RightContent { get; set; }
        public string Image { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
        public DistrictCodeModel DistrictCodeNavigation { get; set; }
    }
}

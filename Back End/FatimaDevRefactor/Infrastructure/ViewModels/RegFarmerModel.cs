﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
    public class RegFarmerModel
    {
        public int farmId { get; set; }
        public int farmerId { get; set; }
        public int cropId { get; set; }
       public DateTime sowingDate { get; set; }
        public int[] farmUnitIds { get; set; }
    }
}

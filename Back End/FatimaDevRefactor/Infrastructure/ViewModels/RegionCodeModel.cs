﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
    public class RegionCodeModel
    {

        public int RegionCode { get; set; }
        public string RegionName { get; set; }
        public string RegionNameUrdu { get; set; }
    }
}

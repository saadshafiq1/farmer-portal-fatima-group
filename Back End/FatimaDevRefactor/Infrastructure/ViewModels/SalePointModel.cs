﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
    public class SalePointModel
    {
        public int SalePointCode { get; set; }
        public string SalePointName { get; set; }
        public string SalePointNameUrdu { get; set; }
    }
}

﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
    public class TSOTicketInfoModel
    {
        public int TicketId { get; set; }
        public int? StatusId { get; set; }
        public string StatusName { get; set; }      
        public int TicketTypeId { get; set; }
        public string TicketTypeName { get; set; }
        public string TicketTypeNameUrdu { get; set; }
        public long FarmerId { get; set; }
        public string FarmerName { get; set; }
        public string CellPhone { get; set; }
        public string PresentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string DistrictName { get; set; }
        public string DistrictNameUrdu { get; set; }
        public string FeedbackComments { get; set; }
        public DateTime? CreatedDate { get; set; }

    }
}

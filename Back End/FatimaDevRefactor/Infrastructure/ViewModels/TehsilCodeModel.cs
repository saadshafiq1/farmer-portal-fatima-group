﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public partial class TehsilCodeModel
    {
        public int TehsilCode { get; set; }
        public string TehsilName { get; set; }
        public string TehsilNameUrdu { get; set; }       
    }
}

﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
    public class TicketInfoModel
    {
        public int TicketId { get; set; }
        public int TicketTypeId { get; set; }
        public DateTime? VisitDate { get; set; }
        public TimeSpan? VisitTime { get; set; }
        public string Description { get; set; }
        public int? StatusId { get; set; }
        public string StatusText { get; set; }
        public string Slastatus { get; set; }
        public int? Feedback { get; set; }
        public string FeedbackComments { get; set; }
        public string FeedbackAudioUrl { get; set; }
        public long FarmerId { get; set; }
        public long FarmId { get; set; }
        public long? AssignedTo { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? ResolvedDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DueDate { get; set; }
        public bool? Archive { get; set; }
        public bool? Deleted { get; set; }

        public FarmModel Farm { get; set; }
        public FarmerInfoModel Farmer { get; set; }
        public ICollection<TbTicketActivity> TbTicketActivity { get; set; }

        public TicketTypeModel TicketType { get; set; }

    }
}

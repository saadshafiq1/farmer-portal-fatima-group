﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
    public class TicketStatusModelForFarmerWeb
    {
        public int TicketId { get; set; }
        public int TicketTypeId { get; set; }
        public int? StatusId { get; set; }
        public string Slastatus { get; set; }
        public long? AssignedTo { get; set; }
        public long CreatedBy { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? ResolvedDate { get; set; }

        public FarmerInfoModel Farmer { get; set; }
        public TicketTypeModel TicketType { get; set; }
        public TbTicketsStatus Status { get; set; }
    }
}

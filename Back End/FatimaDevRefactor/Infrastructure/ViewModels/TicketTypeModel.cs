﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
    public class TicketTypeModel
    {

        public int TicketTypeId { get; set; }
        public string TicketTypeName { get; set; }
        public string TicketTypeDescription { get; set; }
        public string TicketTypeNameUrdu { get; set; }
    }
}

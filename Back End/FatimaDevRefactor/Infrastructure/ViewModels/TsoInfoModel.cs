﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
    public class TsoInfoModel
    {
        public long Tsoid { get; set; }
        public string Tsoname { get; set; }
        public string Cnic { get; set; }
        public string Email { get; set; }
        public string CellPhone { get; set; }
        public string AlternateCellPhone { get; set; }   
        public string ActiveStatus { get; set; }
        public Guid? UserGuid { get; set; }
    }
}

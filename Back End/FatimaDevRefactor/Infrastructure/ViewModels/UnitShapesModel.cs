﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
   public class UnitShapesModel
    {
        public long UnitShapeId { get; set; }
        public long? UnitId { get; set; }
        public string Geometry { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }      
    }
}

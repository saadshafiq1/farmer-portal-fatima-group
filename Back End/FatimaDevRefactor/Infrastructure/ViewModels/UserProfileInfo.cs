﻿using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels
{
    public class UserProfileInfo
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        public string Cnic { get; set; }
        public string Email { get; set; }
        public string AlternateCellPhone { get; set; }
        public string Landline { get; set; }
        public string Gender { get; set; }
        public string PresentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public int? EducationCode { get; set; }
        public string image { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public DateTime? InsertionDate { get; set; }
        public string ActiveStatus { get; set; }
        public Guid? UserGuid { get; set; }

        public string FatherHusbandName { get; set; }
        public string CellPhone { get; set; }
        public int? MaleDependant { get; set; }
        public int? FemaleDependant { get; set; }
        public string AlternateName { get; set; }
        public string AlternateRelationshipwithFarmer { get; set; }
        public int? ReferalCode { get; set; }

        public string UnionCouncil { get; set; }
        public string MozaName { get; set; }
        public int? SmallAnimals { get; set; }
        public int? BigAnimals { get; set; }
        public int? Tractor { get; set; }

        public string Role { get; set; }

        //public DistrictCodeModel DistrictCodeNavigation { get; set; }
        //public TehsilCodeModel TehsilCodeNavigation { get; set; }
        public int DistrictCode { get; set; }
        public string DistrictName { get; set; }
        public string DistrictNameUrdu { get; set; }
        public int TehsilCode { get; set; }
        public string TehsilName { get; set; }
        public string TehsilNameUrdu { get; set; }

    }
}

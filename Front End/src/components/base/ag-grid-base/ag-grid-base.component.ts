import { Component, OnInit, Input, EventEmitter, Output, OnChanges } from '@angular/core';
import { GridOptions } from 'ag-grid';
import {ExcelExportParams} from 'ag-grid';

import * as selectListEditorModule from './ag-grid-components/select-list.editor';
import * as selectListRendererModule from './ag-grid-components/select-list.renderer';
import * as agAdditioanlActionsRendererModule from './ag-grid-components/ag-additional-action.renderer';
import * as agActionsRendererModule from './ag-grid-components/ag-action.renderer';
import * as agStatusRendererModule from './ag-grid-components/ag-status.renderer';
import { EventEmitterService } from '../../../services/event-emitter.service';



@Component({
    selector: 'app-ag-grid-base',
    templateUrl: './ag-grid-base.component.html',
    styleUrls: ['./ag-grid-base.component.css']
})
export class AgGridBaseComponent implements OnInit, OnChanges {
    frameworkComponents: {};
    @Input() rowData: any;
    @Input() columnDefs: any;
    @Input() gridOptions: any;
    @Input() gridParamters: any;
    @Input() headerHeight: any;
    @Input() agGridRowHeight: number;
    @Input() pagination: boolean = false;
    @Input() paginationAutoPageSize: boolean = false;
    @Output() onRowSelected: any = new EventEmitter<any>();

    public gridColumnApi;


    components: {};
    public rowSelection;
    editType: string;
    
    private gridApi;
    public gridOptionsExcel: any;
    constructor( private eventEmitterService: EventEmitterService ) {
        this.gridOptionsExcel = {
            pagination: true,
           
            enableSorting: true
            
        }
        this.rowSelection = "multiple";
    }

    ngOnInit() {
        //this.sizeToFit();
       console.log("Ag grid");
        this.editType = 'fullRow';
        this.frameworkComponents = {
            AgStatusActionsRenderer : agStatusRendererModule.AgStatusActionsRenderer,
            AgGridCellSelectEditor: selectListEditorModule.AgGridCellSelectEditor,
            AgGridCellSelectRenderer: selectListRendererModule.AgGridCellSelectRenderer,
            AgGridActionsRenderer: agActionsRendererModule.AgActionsRenderer,
            AgGridAdditionalActionsRenderer : agAdditioanlActionsRendererModule.AgAdditionalActionsRenderer
        };

        if (this.eventEmitterService.subsVar==undefined) {
            this.eventEmitterService.subsVar = this.eventEmitterService.    
            removeRowEmitter.subscribe((name:string) => {
              this.removeRow();
            });
          }
        this.gridOptions = <GridOptions>{
            context: {
                validateSheetContext: this
            },
            enableFilter: true,
            enableSorting: true,
            defaultColDef: {
                // editable: true
                // stopEditingWhenGridLosesFocus=true,
        },
            onRowEditingStopped: this.onRowUpdateComplete,
            pagination: true
        };
    }



    removeRow() {
        alert("remove called")
        var selectedData = this.gridApi.getSelectedRows();
        this.gridApi.updateRowData({ remove: selectedData });
      }


    convertToCsv () {
        var csv: ExcelExportParams = <ExcelExportParams>{};
        csv.allColumns = true;
        csv.skipGroups = true;
        csv.suppressQuotes = true;
        csv.fileName = 'Report';
        csv.onlySelected=true;
        //var BOM = "\uFEFF"; 
        this.gridOptionsExcel.api.exportDataAsCsv(csv);
        console.log("Convert to csv");
    }

    ngOnChanges(changes: any) {
        const agGridRowHeight = changes['agGridRowHeight'];
        if (agGridRowHeight === undefined) {
            this.agGridRowHeight = 37;
        } else {
            this.gridOptions.rowHeight = agGridRowHeight;
        }
    }

      sizeToFit() {
        this.gridApi.sizeColumnsToFit();
      }

      onGridReady1() {
          this.gridApi.sizeColumnsToFit();
      }

    onGridReady(params) {
        this.gridApi = params.api;
        //this.gridApi.sizeColumnsToFit();
        //this.gridApi.autoSizeAllColumns();
        this.gridColumnApi = params.columnApi;

        if (this.agGridRowHeight === undefined) {
            this.agGridRowHeight = 37;
        } else {
            this.gridOptions.rowHeight = this.agGridRowHeight;
        }

        setTimeout(function() {
            params.api.resetRowHeights();
        }, 500);
    }

    onSelectionChanged(event) {
        if (this.gridApi.getSelectedRows().length === 0) {
            return;
        }
        this.onRowSelected.emit(this.gridApi.getSelectedRows());
    }

    selectEditorValueChange($event) {
        console.log('value rec: ' + $event);
    }

    onRowUpdateComplete(event) {
        console.log(event);
    }
}

function getBooleanValue(cssSelector) {
    return document.querySelector(cssSelector).checked === true;
  }
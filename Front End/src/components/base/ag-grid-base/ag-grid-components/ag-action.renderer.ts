import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
// import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { BaseHttpService } from '../../../../services/BaseHttpService';
import { AppConfig } from '../../../../constants/app-config';
import { EventEmitterService } from '../../../../services/event-emitter.service';
@Component({
    selector: 'child-cell',
    template: `
    <div class="dealer-actions">
                            <a title='View' href="javascript:void(0)" (click)="view($event)" >
                                <i class="icon-view" ></i>
                            </a>
                            <a title='Edit' href="javascript:void(0)" (click)="edit($event)" >
                                <i class="icon-edit0"></i>
                            </a>
                           
</div>
    `

     // <a href="javascript:void(0)" (click)="delete($event)" >
                            //     <i class="icon-delete"></i>
                            // </a>
    // ,
    // styles: [
    //     `
    //         .div {
    //             height: 100px;
    //         }
    //     `
    // ]
})
export class AgActionsRenderer implements ICellRendererAngularComp {
    public params: any;
    // bordereauModel: Bordereau;
    dialogConfiguration = {
        width: '2000px',
        height: '900px',
        data: null,
        disableClose: true
    };

    constructor( private router: Router, private baseService:BaseHttpService, private eventEmitterService: EventEmitterService ) {}

    firstComponentFunction(){    
      }
    agInit(params: any): void {
        this.params = params;
        // this.bordereauModel = this.params.data as Bordereau;
    }

    refresh(): boolean {
        return false;
    }

  
    delete($event: any) {
        this.params && this.params.data ? this.mapDelete(this.params.data) : alert("id not found");
    }
    edit($event: any) {
        this.params && this.params.data ? this.mapNavigation(this.params.data) : alert("id not found");
    }

    view($event:any){
        this.params && this.params.data ? this.mapViewNavigation(this.params.data) : alert("id not found");
        
    }

    mapDelete(data:any){
        data.dealerId && this.baseService.delete(AppConfig.URL_Dealers+data.dealerId).subscribe(data=>{
            !data.responseCode && this.eventEmitterService.removeRowFunction();
        },error =>{ 
            alert("error"+JSON.stringify(error))
        });
        // data.farmerId && this.router.navigate([`farmers-profile/${data.farmerId}`]);
    }

    // onRemoveSelected() {
    //     var selectedData = this.gridApi.getSelectedRows();
    //     var res = this.gridApi.updateRowData({ remove: selectedData });
    //     printResult(res);
    //   }
    mapNavigation(data:any){
        data.dealerId && this.router.navigate([`dealers/${data.dealerId}`]);
        //data.farmerId && this.router.navigate([`farmers-profile/${data.farmerId}`]);
        data.farmerId && this.router.navigate([`add-farmer/${data.farmerId}`]);
        data.data && data.data.ticketId && this.router.navigate([`tickets-detail/${data.data.ticketId}`]);
        data.mediaId && this.router.navigate([`news/${data.mediaId}`]);
    }
    mapViewNavigation(data:any){
        data.dealerId && this.router.navigate([`dealers/${data.dealerId}`], { queryParams: { view: true } });
        data.farmerId && this.router.navigate([`add-farmer/${data.farmerId}`],{ queryParams: { view: true } });
         data.mediaId && this.router.navigate([`news/${data.mediaId}`],{ queryParams: { view: true } });   
         data.data && data.data.ticketId && this.router.navigate([`tickets-detail/${data.data.ticketId}`],{ queryParams: { view: true } });
    }
    
}

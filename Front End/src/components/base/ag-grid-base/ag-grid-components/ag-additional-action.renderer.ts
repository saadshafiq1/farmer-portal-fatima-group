import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { Router } from '@angular/router';
@Component({
    selector: 'child-cell',
    template: `
    <div class="assign-button-actions">

                            <a href="javascript:void(0)" (click)="assign($event)" >
                             <button class="button" >Assign</button>
                            </a>
                           
</div>
    `
    ,
    styles: [
        `
            .button {
                  border-radius: 2px;
                  background-color: #006B39;
                  border: none;
                  color: #FFFFFF;
                  font-size: 14px;
                  line-height: 18px;
                  letter-spacing: 0.21px;
                  padding: 0 15px;
            }
        `
    ]
})
export class AgAdditionalActionsRenderer implements ICellRendererAngularComp {
    public params: any;
    dialogConfiguration = {
        width: '2000px',
        height: '900px',
        data: null,
        disableClose: true
    };

    constructor( private router: Router ) {}

    agInit(params: any): void {
        this.params = params;
    }

    refresh(): boolean {
        return false;
    }
    assign($event: any) {
        this.params && this.params.data ? this.mapNavigation(this.params.data) : alert("id not found");
    }

    mapNavigation(data:any){
       if(data.data.statusId!=3)
        {
        data.data && data.data.ticketId && this.router.navigate([`assign-ticket/${data.data.ticketId}`]);
        } 

    }
    
}

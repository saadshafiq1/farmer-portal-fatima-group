import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { Router } from '@angular/router';
@Component({
    selector: 'child-cell',
    template: `
        <div class="dealer-status">
        <i title='Closed'  [ngClass]="{'icon-Complete-Done': params && params.data && params.data.data && params.data.data.statusId == 3}" class="icon-Complete-Default"></i>
        <i title='In Progress' [ngClass]="{'icon-In-progress-Done': params && params.data && params.data.data && params.data.data.statusId == 2}" class="icon-In-Progress-Default"></i>
        <i title='Overdue' [ngClass]="{'icon-Alarm-Done': params && params.data && params.data.data && params.data.data.statusId == 4}" class="icon-Alarm-Default"></i>
        <i title='On hold' [ngClass]="{'icon-onhold-Done': params && params.data && params.data.data && params.data.data.statusId == 5}" class="icon-on-hold-gray"></i>
    </div>
    `

})


export class AgStatusActionsRenderer implements ICellRendererAngularComp {
    public params: any;
    constructor( private router: Router ) {}
    agInit(params: any): void {
        this.params = params;
    }
    refresh(): boolean {
        return false;
    }
}

import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { Router } from '@angular/router';
import { BaseHttpService } from '../../../../services/BaseHttpService';
import { AppConfig } from '../../../../constants/app-config';
import { EventEmitterService } from '../../../../services/event-emitter.service';
@Component({
    selector: 'child-cell',
    template: `
    <div class="dealer-actions">
                            <a href="javascript:void(0)" (click)="edit($event)" >
                                <i class="icon-edit0"></i>
                            </a>
                           
</div>
    `

})
export class AgTicketActionRenderer implements ICellRendererAngularComp {
    public params: any;
    dialogConfiguration = {
        width: '2000px',
        height: '900px',
        data: null,
        disableClose: true
    };

    constructor( private router: Router, private baseService:BaseHttpService, private eventEmitterService: EventEmitterService ) {}

    firstComponentFunction(){    
      }
    agInit(params: any): void {
        this.params = params;
    }

    refresh(): boolean {
        return false;
    }

    edit($event: any) {
        this.params && this.params.data ? this.mapEditNavigation(this.params.data) : alert("id not found");
    }

    mapEditNavigation(data:any){
        data.data && data.data.ticketId && this.router.navigate([`tickets-detail/${data.data.ticketId}`]);
    }

    
}

import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
// import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { BaseHttpService } from '../../../../services/BaseHttpService';
import { ActivatedRoute } from '@angular/router';
import { AppConfig } from '../../../../constants/app-config';
import { EventEmitterService } from '../../../../services/event-emitter.service';
import { ReportsService } from '../../../reports/reports.service';
@Component({
    selector: 'child-cell',
    template: `<div class="dealer-actions">
    <a title='Delete' href="javascript:void(0)" (click)="delete($event)">
        <i class="icon-delete" ></i>
    </a>
   
</div>`,
    styles: [
        ``
    ]
})
export class ButtonRendererComponent implements ICellRendererAngularComp {
    public id: number;
    
    public params: any;
    public repId;
    private gridApi;
    
    public api;
    rowData: any;
    // bordereauModel: Bordereau;
    dialogConfiguration = {
        width: '80px',
        height: '900px',
        data: null,
        disableClose: true
    };

    constructor(private router: Router, private baseService:BaseHttpService, private eventEmitterService: EventEmitterService,private reportsSerevice: ReportsService , private route: ActivatedRoute) {}
    ngOnInit() {
        this.id = +this.route.snapshot.paramMap.get('id');
    }
    firstComponentFunction(){    
      }
    agInit(params: any): void {
        this.params = params;
        this.gridApi = params.api; 
        // this.bordereauModel = this.params.data as Bordereau;
    }

    refresh(): boolean {
        return false;
    }

  
    delete($event: any) {
        console.log("Delete Method", this.params.data.reportId)
        this.params && this.params.data ? this.mapDelete(this.params) : alert("id not found");
    }
    edit($event: any) {
        this.params && this.params.data ? this.mapNavigation(this.params.data) : alert("id not found");
    }

    view($event:any){
        this.params && this.params.data ? this.mapViewNavigation(this.params.data) : alert("id not found");
        
    }

    mapDelete(data:any){
        console.log("csjk",this.params.data.reportId);
        this.repId=this.params.data.reportId;
        this.reportsSerevice.deleteReport(this.repId).subscribe(x => {
            this.reportsSerevice.getReport(this.id).subscribe(x => {
            this.rowData=x.data});
            });
            this.gridApi.setRowData(this.rowData)
    }
    mapNavigation(data:any){
        data.dealerId && this.router.navigate([`dealers/${data.dealerId}`]);
        //data.farmerId && this.router.navigate([`farmers-profile/${data.farmerId}`]);
        data.farmerId && this.router.navigate([`add-farmer/${data.farmerId}`]);
        data.data && data.data.ticketId && this.router.navigate([`tickets-detail/${data.data.ticketId}`]);
        data.mediaId && this.router.navigate([`news/${data.mediaId}`]);
    }
    mapViewNavigation(data:any){
        data.dealerId && this.router.navigate([`dealers/${data.dealerId}`], { queryParams: { view: true } });
        data.farmerId && this.router.navigate([`add-farmer/${data.farmerId}`],{ queryParams: { view: true } });
         data.mediaId && this.router.navigate([`news/${data.mediaId}`],{ queryParams: { view: true } });   
         data.data && data.data.ticketId && this.router.navigate([`tickets-detail/${data.data.ticketId}`],{ queryParams: { view: true } });
    }
}
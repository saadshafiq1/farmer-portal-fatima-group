import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
// import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { BaseHttpService } from '../../../../services/BaseHttpService';
import { AppConfig } from '../../../../constants/app-config';
import { EventEmitterService } from '../../../../services/event-emitter.service';

@Component({
    selector: 'child-cell',
    template: `<div class="dealer-actions">
    <a title='View' href="javascript:void(0)" (click)="view($event)">
        <i class="icon-view" ></i>
    </a>
   
</div>`,
    styles: [
        ``
    ]
})
export class ButtonRenderer1Component implements ICellRendererAngularComp {
    repUrl="";
    public params: any;
    // bordereauModel: Bordereau;
    dialogConfiguration = {
        width: '2000px',
        height: '900px',
        data: null,
        disableClose: true
    };

    constructor( private router: Router, private baseService:BaseHttpService, private eventEmitterService: EventEmitterService ) {}

    firstComponentFunction(){    
      }
    agInit(params: any): void {
        this.params = params;
        // this.bordereauModel = this.params.data as Bordereau;
    }

    refresh(): boolean {
        return false;
    }

  
    delete($event: any) {
        this.params && this.params.data ? this.mapDelete(this.params.data) : alert("id not found");
    }
    edit($event: any) {
        this.params && this.params.data ? this.mapNavigation(this.params.data) : alert("id not found");
    }

    view($event:any){
        console.log("avskbv",this.params)
        this.params && this.params.data ? this.mapViewNavigation(this.params) : alert("id not found");
        
    }

    mapDelete(data:any){
        data.dealerId && this.baseService.delete(AppConfig.URL_Dealers+data.dealerId).subscribe(data=>{
            !data.responseCode && this.eventEmitterService.removeRowFunction();
        },error =>{ 
            alert("error"+JSON.stringify(error))
        });
        // data.farmerId && this.router.navigate([`farmers-profile/${data.farmerId}`]);
    }

    // onRemoveSelected() {
    //     var selectedData = this.gridApi.getSelectedRows();
    //     var res = this.gridApi.updateRowData({ remove: selectedData });
    //     printResult(res);
    //   }
    mapNavigation(data:any){
        data.dealerId && this.router.navigate([`dealers/${data.dealerId}`]);
        //data.farmerId && this.router.navigate([`farmers-profile/${data.farmerId}`]);
        data.farmerId && this.router.navigate([`add-farmer/${data.farmerId}`]);
        data.data && data.data.ticketId && this.router.navigate([`tickets-detail/${data.data.ticketId}`]);
        data.mediaId && this.router.navigate([`news/${data.mediaId}`]);
    }
    mapViewNavigation(data:any){
        this.repUrl = this.params.value;

        if (this.repUrl == null){
            alert("PDF was not uploaded for this report  ");
          }
          else
            window.open(this.repUrl, "_blank");
    }
}
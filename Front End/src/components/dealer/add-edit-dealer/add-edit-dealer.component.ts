import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DealerService } from '../dealer.service';
import { Dealer } from '../dealer';
import { Location } from '@angular/common';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';
import 'rxjs/add/operator/filter';
import { ToastrService } from 'ngx-toastr';
import { Province } from '../../../models/province';
import { Region } from '../../../models/region';
import { District } from '../../../models/district';
import { Tehsil } from '../../../models/tehsil';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-add-edit-dealer',
  templateUrl: './add-edit-dealer.component.html',
  styleUrls: ['./add-edit-dealer.component.css']
})
export class AddEditDealerComponent implements OnInit {
  form: FormGroup;
  category: any[];
  status: any[];
  provinces: Province[];
  regions: Region[];
  districts: District[];
  tehsils: Tehsil[];
  salepoints: any[];
  currentstatuses: any[] = [{ label: 'Active', value: '1' }, { label: 'In-Active', value: '2' }];
  dealer: Dealer = new Dealer();
  id: number;
  myDate = new Date();
  constructor(private datePipe: DatePipe, private router: Router, fb: FormBuilder, private route: ActivatedRoute, private dealerService: DealerService,
    private location: Location, private ts: ToastrService) {
    this.form = fb.group({
      "dealerNameUrdu": ["", Validators.required],
      "cnic": ["",],
      "ntn": ["",],
      "strn": ["",],
      "dealerCategory": ["",],
      "dealerStatus": ["",],
      "taxProfileDate": ["",],
      "currentStatus": ["",],
      "ccnumber": ["", Validators.pattern("[0-9]*")],
      "sherpCc": ["", Validators.pattern("[0-9]*")],
      "proprieter": ["",],
      "address": ["", Validators.required],
      "province": ["",],
      "districtCode": ["",],
      "cityVillage": ["",],
      "tehsilCode": ["",],
      "saleRegion": ["",],
      "salePoint": ["",],
      "latitude": ["",],
      "longitude": ["",],
      "cellPhone": ["", Validators.required],
      "landline": ["",],
      "email": ["", Validators.email]
    })
  }
  get f() { return this.form.controls; }
  public readOnly = false;
  isNew: boolean = false;
  ngOnInit() {
    this.getDealer();
  }
  getDealer(): void {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.getDealerStatus(this.id);
    this.route.queryParams
      .filter(params => params.view)
      .subscribe(params => {
        this.readOnly = params.view;
        (this.readOnly) ? this.ReadOnly() : null;
      });
    this.id ? this.dealerService.getDealer(this.id)
      .subscribe(data => {
        this.dealer = data;
      
        this.getRegions(this.dealer.province, this.id);
        this.getDistricts(this.dealer.cityVillage, this.id);
        this.getTehsils(this.dealer.districtCode, this.id);
      }) : this.isNew = true;
 

  }

  getDealerStatus(id) {
    if (id != null) {
      this.dealerService.getDealerStatus().subscribe(data => {
        this.category = data["category"];
        this.status = data["status"];
        if (id == 0) {
          this.dealer.dealerCategory = "1";
          this.dealer.dealerStatus = "1";
          this.dealer.currentStatus = '1';
        }

      });
      this.getProvinces(id);
      this.getSalePoints();
    }
  }
  getProvinces(id) {
    if (id != null) {
      this.dealerService.getProvinces().subscribe(data => {
        this.provinces = data;
      });

      this.districts = null;
      this.tehsils = null;
    }
    this.dealer.province = "Select Province";
    this.dealer.cityVillage = "Select City";
    this.dealer.saleRegion = "Select Sale Region";
    this.dealer.districtCode = "Select District";
    this.dealer.tehsilCode = "Select Tehsil";

  }

  getRegions(id, id2) {
    if (id != null) {
      this.dealerService.getRegions(id).subscribe(data => {
        this.regions = data;
      });

      this.districts = null;
      this.tehsils = null;
      if (id2 == 0) {
        this.dealer.cityVillage = "Select City";
        this.dealer.saleRegion = "Select Sale Region";
        this.dealer.districtCode = "Select District";
        this.dealer.tehsilCode = "Select Tehsil";
      }
    }

  }

  getDistricts(id, id2) {
    if (id != null) {
      this.dealerService.getDistricts(id).subscribe(data => {
        this.districts = data;
      });
      this.tehsils = null;
      if (id2 == 0) {
        this.dealer.districtCode = "Select District";
        this.dealer.tehsilCode = "Select Tehsil";
      }
    }

  }

  getTehsils(id, id2) {
    if (id != null) {
      this.dealerService.getTehsils(id).subscribe(data => {
        this.tehsils = data;
      });
      if (id2 == 0) {
        this.dealer.tehsilCode = "Select Tehsil";
      }
    }

  }

  check() {

    if (this.dealer.province == "Select Province") {
      this.dealer.province = null;
    }
    if (this.dealer.cityVillage == "Select City") {
      this.dealer.cityVillage = null;
    }
    if (this.dealer.saleRegion == "Select Sale Region") {
      this.dealer.saleRegion = null;
    }
    if (this.dealer.districtCode == "Select District") {
      this.dealer.districtCode = null;
    }
    if (this.dealer.tehsilCode == "Select Tehsil") {
      this.dealer.tehsilCode = null;
    }

  }
  goBack(): void {
    this.location.back();
  }
  save(): void {
    let dealerId = this.dealer.dealerId;
    this.dealer = this.form.value;
    this.dealer.dealerId = dealerId;
    this.check();
    console.log(this.dealer);
    this.isNew ? this.dealerService.addDealer(this.dealer)
      .subscribe(dealerD => {
        this.dealer = dealerD;
        this.dealer && this.dealer.dealerId && this.router.navigate(['dealers/' + dealerD.dealerId])
        this.ts.success("Operation Performed Successfully");
      }) : this.dealerService.updateDealer(this.dealer)
        .subscribe();

    this.location.back();
  }

  delete(): void {
    if (this.id != 0) {
      // this.heroes = this.heroes.filter(h => h !== hero);
      this.dealerService.deleteDealer(this.dealer).subscribe(dealerD => {
        dealerD && this.router.navigate(['dealers']);
      });
    }
  }

  getSalePoints() {
    this.dealerService.getSalePoints().subscribe(data => {
      this.salepoints = data;
    });

  }

  ReadOnly() {
    this.form.controls['dealerNameUrdu'].disable();
    this.form.controls['cnic'].disable();
    this.form.controls['ntn'].disable();
    this.form.controls['strn'].disable();
    this.form.controls['dealerCategory'].disable();
    this.form.controls['taxProfileDate'].disable();
    this.form.controls['currentStatus'].disable();
    this.form.controls['ccnumber'].disable();
    this.form.controls['sherpCc'].disable();
    this.form.controls['proprieter'].disable();
    this.form.controls['address'].disable();
    this.form.controls['province'].disable();
    this.form.controls['districtCode'].disable();
    this.form.controls['cityVillage'].disable();
    this.form.controls['tehsilCode'].disable();
    this.form.controls['saleRegion'].disable();
    this.form.controls['salePoint'].disable();
    this.form.controls['latitude'].disable();
    this.form.controls['longitude'].disable();
    this.form.controls['cellPhone'].disable();
    this.form.controls['landline'].disable();
    this.form.controls['email'].disable();
    this.form.controls['dealerStatus'].disable();
  }
  onSubmit() {
    console.log("Form Values: ", this.form.value);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from '../../constants/app-config';
import { Dealer } from './dealer';
import { Province } from '../../models/province';
import { Region } from '../../models/region';
import { District } from '../../models/district';
import { Tehsil } from '../../models/tehsil';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { TSO } from '../../models/tso';

@Injectable({
  providedIn: 'root'
})
export class DealerService {

  constructor(private http: HttpClient, private ts: ToastrService) {
  }

  addDealer(dealer: Dealer): Observable<Dealer> {
    return this.http.post<Dealer>(`${AppConfig.URL_Dealers}`, dealer).pipe(
      map(x => x["data"]),
      tap((newDealer: Dealer) => console.log(`added dealer w/ id=${newDealer.dealerId}`)),
      catchError(this.handleError<Dealer>('addDealer'))
    );
  }

  getDealers(params: string) {
    console.log("params", params)
    return this.http.get<Dealer[]>(`${AppConfig.URL_Dealers}AllDealers?` + params)
      .pipe(
      map(x => x["data"]),
      tap(_ => console.log('fetched dealers')),
      catchError(this.handleError('getDealers', []))
      );
  }
  getDealer(id) {
    return this.http.get<Dealer>(`${AppConfig.URL_Dealers}${id}`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`updated dealer id=${id}`)),
      catchError(this.handleError<any>('getdealer'))
    );
  }

  getProvinces() {
    return this.http.get<Province>(`${AppConfig.URL_Lookup}Province`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`getProvinces`)),
      catchError(this.handleError<any>('getProvinces'))
    );
  }

  getRegions(id) {
    return this.http.get<Region>(`${AppConfig.URL_Lookup}CitiesOfProvince/${id}`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`getRegions`)),
      catchError(this.handleError<any>('getRegions'))
    );
  }
  getRegion() {
    return this.http.get<Region>(`${AppConfig.URL_Lookup}GetAllCities`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`getRegions`)),
      catchError(this.handleError<any>('getRegions'))
    );
  }

  getDistricts(id) {
    return this.http.get<District>(`${AppConfig.URL_Lookup}GetDistrictsByRegion/${id}`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`getDistricts`)),
      catchError(this.handleError<any>('getDistricts'))
    );
  }
  getDistrict() {
    return this.http.get<District>(`${AppConfig.URL_Lookup}GetAllDistricts`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`getDistricts`)),
      catchError(this.handleError<any>('getDistricts'))
    );
  }

  getTehsils(id) {
    return this.http.get<Tehsil>(`${AppConfig.URL_Lookup}TehsilsOfDistrict/${id}`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`getTehsils`)),
      catchError(this.handleError<any>('getTehsils'))
    );
  }

  getSalePoints() {
    return this.http.get<Province>(`${AppConfig.URL_Lookup}GetAllSalesPoint`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`getSalePoints`)),
      catchError(this.handleError<any>('getSalePoints'))
    );

  }

  getTSO() {
    return this.http.get<TSO>(`${AppConfig.URL_AppBase}ticket/GetTSOAgents`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`getTSO`)),
      catchError(this.handleError<any>('getTSO'))
    );
  }


  deleteDealer(dealer: Dealer | number): Observable<Dealer> {
    const id = typeof dealer === 'number' ? dealer : dealer.dealerId;
    const url = `${AppConfig.URL_Dealers}${id}`;
    return this.http.delete<Dealer>(url).pipe(
      map(x => x["data"]),
      tap(_ => this.log(`deleted dealer id=${id}`)),
      catchError(this.handleError<Dealer>('deleteDealer'))
    );
  }

  updateDealer(dealer: Dealer): Observable<any> {
    return this.http.put(`${AppConfig.URL_Dealers}${dealer["dealerId"]}`, dealer).pipe(
      tap(_ => this.log(`updated dealer id=${dealer["dealerId"]}`)),
      catchError(this.handleError<any>('updatedealer'))
    );
  }

  getDealerStatus() {
    return this.http.get<Dealer[]>(`${AppConfig.URL_Dealers}/GetDealerStatusesAndCategory`)
      .pipe(
      map(x => x["data"]),
      tap(_ => console.log('fetched dealers status')),
      catchError(this.handleError('getDealerStatus', []))
      );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this.log(`${operation} failed: ${error.message}`);
      this.ts.error("Failed to Perform Operation");
      return of(result as T);
    };
  }

  private log(message: string) {
    this.ts.success("Operation Performed Successfully");
  }

}

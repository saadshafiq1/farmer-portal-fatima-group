import { Component, OnInit } from '@angular/core';
import { MockData } from '../../../constants/mock-data';
import { DealerService } from '../dealer.service';
import { Dealer } from '../dealer';
import { Province } from '../../../models/province';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Region } from 'src/models/region';
import { District } from 'src/models/district';
import { Salespoint } from '../../../models/salespoint';
import { TSO } from '../../../models/tso';

@Component({
    selector: 'app-list-dealer',
    templateUrl: './list-dealer.component.html',
    styleUrls: ['./list-dealer.component.css']
})
export class ListDealerComponent implements OnInit {
    workFlowViewcolumnDefs = [];
    workFlowViewrowData = [];
    dealers: Dealer[];
    selectedValue;
    selectedRegion: string ="0";
    selectedDistrict: string ="0";
    selectedTSO: string = "0";
    selectedSalespoint: string = "0" ;
    searchvalue;
    public disval;
    public breadcrumbUrl;
    allFlag;
    
    provinceFlag;
    categ= ['Region'];
    categories = ['Phone', 'CNIC'];
    provinces: Province[];
    regions: Region[];
    salespoints:Salespoint[];
    tso: TSO[];
    districts:District[];
    private searchedValue: string = "";
    private params: string;
    private currentPage: number = 0;
    currentpage;
    private searchedcat: string;
    private pages: number = 1;
    public recordsPerPage: number = 15;
    public totalrecords: number = 0;
    showBoundaryLinks = true;
    
    
    constructor(public dealersService: DealerService, private router: Router) {
        
    }
    ngOnInit() {        
        this.selectedValue = 'CNIC';
       
        this.dealersService.getDealers('pageNumber=' + this.currentPage + '&pageSize=' + this.recordsPerPage).pipe(first()).subscribe(data => {
            this.dealers = data['data'];
            this.totalrecords = data['totalCount'];
            this.getWfGridData();
        });
        this.dealersService.getProvinces().pipe(first()).subscribe(data => {
        this.provinces = data;           
        });
        this.dealersService.getSalePoints().pipe(first()).subscribe(data => {
            console.log("Salespoint Data", data)
            this.salespoints= data;
        });
        this.dealersService.getTSO().pipe(first()).subscribe(data => {
            console.log("TSO Data", data)
            this.tso = data;
        });
        this.dealersService.getRegion().pipe(first()).subscribe(data => {
        this.regions = data;
       
        });
        this.dealersService.getDistrict().pipe(first()).subscribe(data => {
        this.districts = data;
      
        });

    }
    addedit(id) {
        this.router.navigate(['/dealers/' + id]);
    }
    pageChanged(event: any) {
        this.pages = event.page;
        this.pages = this.pages - 1;
        if (this.searchedValue == "") {
            this.params = 'pageNumber=' + this.pages + '&pageSize=' + this.recordsPerPage;
        }
        else {
            this.params = 'pageNumber=' + this.pages + '&pageSize=' + this.recordsPerPage + '&filter=' + this.searchedcat + '&value=' + this.searchedValue;
        }
        this.dealersService.getDealers(this.params).pipe(first()).subscribe(data => {
            this.dealers = data['data'];
            this.getWfGridData();
        });
        
    }
    
    search(searchcat: string, searchval: string) {
        
        this.searchedcat = searchcat;
        this.searchedValue = searchval; 
     
      
        if (searchcat == "Province") {
            this.searchvalue = "";
            this.selectedRegion="0";
            this.selectedDistrict ="0";
            this.selectedSalespoint = "0" ;
            this.selectedTSO = "0";
            this.params = 'pageNumber=' + this.currentPage + '&pageSize=' + this.recordsPerPage + '&filter=' + this.searchedcat + '&value=' + this.searchedValue;
            this.allFlag = 0;
            this.provinceFlag = 1;
             this.breadcrumbUrl = searchval;
             
         
            

        }
        if (searchcat == "Region") {
            this.searchvalue = "";
            this.selectedDistrict ="0";
            this.selectedSalespoint = "0" ;
            this.selectedTSO = "0";
            this.params = 'pageNumber=' + this.currentPage + '&pageSize=' + this.recordsPerPage + '&filter=' + this.searchedcat + '&value=' + this.searchedValue;
            this.allFlag = 0;
            this.provinceFlag = 1;
            this.breadcrumbUrl = 'All';
           
        
        }
       
        if (searchcat == "District") {
            this.searchvalue = "";
            this.selectedRegion="0";
            this.selectedSalespoint = "0" ;
            this.selectedTSO = "0";
            this.params = 'pageNumber=' + this.currentPage + '&pageSize=' + this.recordsPerPage + '&filter=' + this.searchedcat + '&value=' + this.searchedValue;
            this.allFlag = 0;
            this.provinceFlag = 1;
            this.breadcrumbUrl = 'All';
           
        }

        if (searchcat == "salepoint") {
            this.searchvalue = "";
            this.selectedRegion="0";
            this.selectedDistrict ="0";
            this.selectedTSO = "0";
            this.params = 'pageNumber=' + this.currentPage + '&pageSize=' + this.recordsPerPage + '&filter=' + this.searchedcat + '&value=' + this.searchedValue;
            this.allFlag = 0;
            this.provinceFlag = 1;
            this.breadcrumbUrl = 'All';
          
            }
        if (searchcat == "tso") {
            this.searchvalue = "";
            this.selectedRegion="0";
            this.selectedDistrict ="0";
            this.selectedSalespoint = "0";
            this.params = 'pageNumber=' + this.currentPage + '&pageSize=' + this.recordsPerPage + '&filter=' + this.searchedcat + '&value=' + this.searchedValue;
            this.allFlag = 0;
            this.provinceFlag = 1;
            this.breadcrumbUrl = 'All';
            
            }
        
        if (this.searchedValue == "") {

            this.params = 'pageNumber=' + this.currentPage + '&pageSize=' + this.recordsPerPage;
            this.allFlag = 1;
            this.provinceFlag = 0;
            this.breadcrumbUrl = 'All';
           
        }
        
        this.dealersService.getDealers(this.params).pipe(first()).subscribe(data => {
            this.dealers = data['data'];
            console.log("data filter", data)
            this.totalrecords = data['totalCount'];
            console.log("Total Records:",this.totalrecords);
            this.getWfGridData();
        });
        this.currentpage = 1;
    }
    
    getWfGridData() {
        const colDef: any[] = MockData.agGridColumnDefs_Dealers;
        console.log("Column Def", MockData.agGridColumnDefs_Dealers)
        this.workFlowViewcolumnDefs = colDef;
        this.workFlowViewrowData = this.dealers;
        console.log(this.dealers);
        console.log("ag grid");
        console.log("isse" + colDef);
    }
    
    onGridReady(params) {
        params.api.sizeColumnsToFit();
    }
   
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFarmerFarmsComponent } from './add-farmer-farms.component';

describe('AddFarmerFarmsComponent', () => {
  let component: AddFarmerFarmsComponent;
  let fixture: ComponentFixture<AddFarmerFarmsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFarmerFarmsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFarmerFarmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Farmers } from '../farmers';
import { FarmersService } from '../farmers.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Location } from '@angular/common';
import { Province } from '../../../models/province';
import { Region } from '../../../models/region';
import { District } from '../../../models/district';
import { Tehsil } from '../../../models/tehsil';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-add-farmer',
  templateUrl: './add-farmer.component.html',
  styleUrls: ['./add-farmer.component.css']
})
export class AddFarmerComponent implements OnInit {
  form: FormGroup;
  public readOnly = false;
  isNew: boolean = false;
  farmer: Farmers = new Farmers();
  id: number;
  provinces: Province[];
  presentprovinces: Province[];
  regions: Region[];
  presentregions: Region[];
  districts: District[];
  presentdistricts: District[];
  tehsils: Tehsil[];
  presenttehsils: Tehsil[];
  farmerStatus: any[];
  checkflag: boolean;
  myDate = new Date();
  categories = ['Male', 'Female'];
  constructor(private datePipe: DatePipe, private router: Router, fb: FormBuilder, private route: ActivatedRoute, private farmerService: FarmersService,
    private location: Location, private ts: ToastrService) {
    this.form = fb.group({
      "farmerName": ["", Validators.required],
      // "cnic": ["",Validators.pattern("^[0-9+]{5}-[0-9+]{7}-[0-9]{1}$")],
      "cnic": ["",],
      "gender": ["",],
      "fatherHusbandName": ["",],
      "maleDependant": ["",],
      "femaleDependant": ["",],
      "permanentAddress": ["",],
      "unionCouncil": ["",],
      "presentAddress": ["",],
      "cellPhone": ["", Validators.required],
      "referalCode": ["",],
      "farmerStatusId": ["",],
      "alternateName": ["",],
      "alternateCellPhoneNo": ["",],
      "userGuid": ["",],
      "dateOfBirth": ["",],
      "educationCode": ["",],
      "hobbies": ["",],
      "provinceCode": ["",],
      "districtCode": ["",],
      //"regionCode": ["", ],
      "tehsilCode": ["",],
      "presentProvinceCode": ["",],
      "presentDistrictCode": ["",],
      "presentRegionCode": ["",],
      "presentTehsilCode": ["",],
      "presentUnionConcil": ["",],
      "landLine": ["",],
      "adjacentFarmerFriendName": ["",],
      "adjacentFarmerFriendCellPhone": ["",],
      "check": ["",],
    })
    this.checkflag = true;
  }

  ngOnInit() {
    this.getFarmer();
  }
  get f() { return this.form.controls; }
  getFarmer(): void {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.farmer.gender = 'Male';
    this.getProvinces(this.id);
    this.getPresentProvinces(this.id);
    this.getFarmerStatus(this.id);
    if (this.id != 0) {
      this.form.controls['cellPhone'].disable();
    }
    this.route.queryParams
      .filter(params => params.view)
      .subscribe(params => {
        this.readOnly = params.view;
        (this.readOnly) ? this.ReadOnly() : null;
      });
    this.id ? this.farmerService.getFarmer(this.id)
      .subscribe(data => {
        this.farmer = data;
        // this.farmer.dateOfBirth=this.datePipe.transform(this.farmer.dateOfBirth, 'yyyy-MM-dd');
        this.getDistricts(this.farmer.provinceCode, this.id);
        this.getPresentDistricts(this.farmer.presentProvinceCode, this.id);
        this.getTehsils(this.farmer.districtCode, this.id);
        this.getPresentTehsils(this.farmer.presentDistrictCode, this.id);
      }) : this.isNew = true;
  }
  save(): void {
    console.log(this.farmer);
    this.check();
    this.isNew ? this.farmerService.addFarmer(this.farmer)
      .subscribe(farmerD => {
        this.farmer = farmerD;
        this.farmer && this.farmer.farmerId && this.router.navigate(['/add-farmer/' + farmerD.farmerId])
        this.ts.success("Operation Performed Successfully");
      }) : this.farmerService.updateFarmer(this.farmer).subscribe(

      );
    this.location.back();
  }
  goBack() {
    this.location.back();
  }

  delete(): void {
    if (this.id != 0) {
      console.log(this.farmer);
      this.farmerService.deleteFarmer(this.farmer).subscribe(farmerD => {
        farmerD && this.router.navigate(['farmers']);
      });
    }
  }


  addFarms() {
    if (this.id != 0) {
      if (this.readOnly) {
        this.router.navigate(['/farmers-add-farms/' + this.id], { queryParams: { view: true } });
      }
      else {
        this.router.navigate(['/farmers-add-farms/' + this.id]);
      }
    }

  }
  addReports() {
    if (this.id != 0) {
      if (this.readOnly) {
        this.router.navigate(['/reports/' + this.id], { queryParams: { view: true } });
      }
      else {
        this.router.navigate(['/reports/' + this.id]);
      }
    }

  }
  

  addTickets() {
    if (this.id != 0) {
      if (this.readOnly) {
        this.router.navigate(['/farmer-list-ticket/' + this.id], { queryParams: { view: true } });
      }
    }

  }

  addContracts() {
    if (this.id != 0) {
      if (this.readOnly) {

        this.router.navigate(['/farmers-contract/' + this.id], { queryParams: { view: true } });
      }
    }

  }

  addLoans() {
    if (this.id != 0) {
      if (this.readOnly) {
        this.router.navigate(['/farmers-loan/' + this.id], { queryParams: { view: true } });
      }
    }

  }
 
  getFarmerStatus(id) {
    this.farmerService.getFarmerStatus().subscribe(data => {
      this.farmerStatus = data;
    });
    if (id == 0) {
      this.farmer.farmerStatusId = 1;
    }
  }

  getProvinces(id) {
    if (id != null) {
      this.farmerService.getProvinces().subscribe(data => {
        this.provinces = data;
      });
      this.districts = null;
      this.tehsils = null;
    }
    this.farmer.provinceCode = "Select Province";
    this.farmer.districtCode = "Select District";
    this.farmer.tehsilCode = "Select Tehsil";

  }

  getPresentProvinces(id) {
    if (id != null) {
      this.farmerService.getProvinces().subscribe(data => {
        this.presentprovinces = data;
      });
      this.presentdistricts = null;
      this.presenttehsils = null;
    }
    this.farmer.presentProvinceCode = "Select Province";
    this.farmer.presentDistrictCode = "Select District";
    this.farmer.presentTehsilCode = "Select Tehsil";
  }


  getDistricts(id, id2) {
    if (id != null) {
      this.farmerService.getDistricts(id).subscribe(data => {
        this.districts = data;
      });
      this.tehsils = null;
      if (id2 == 0) {
        this.farmer.districtCode = "Select District";
        this.farmer.tehsilCode = "Select Tehsil";
      }
    }

  }

  getPresentDistricts(id, id2) {
    if (id != null && id != "Select District") {
      this.farmerService.getDistricts(id).subscribe(data => {
        this.presentdistricts = data;
      });
      this.presenttehsils = null;
      if (id2 == 0) {
        this.farmer.presentDistrictCode = "Select District";
        this.farmer.presentTehsilCode = "Select Tehsil";
      }
    }

  }


  getTehsils(id, id2) {
    if (id != null) {
      this.farmerService.getTehsils(id).subscribe(data => {
        this.tehsils = data;
      });
      if (id2 == 0) {
        this.farmer.tehsilCode = "Select Tehsil";
      }
    }

  }

  getPresentTehsils(id, id2) {
    if (id != null) {
      this.farmerService.getTehsils(id).subscribe(data => {
        this.presenttehsils = data;
      });
      if (id2 == 0) {
        this.farmer.presentTehsilCode = "Select Tehsil";
      }
    }

  }

  check() {
    if (this.farmer.provinceCode == "Select Province") {

      this.farmer.provinceCode = null;
    }
    if (this.farmer.presentProvinceCode == "Select Province") {
      this.farmer.presentProvinceCode = null;
    }
    if (this.farmer.districtCode == "Select District") {
      this.farmer.districtCode = null;
    }
    if (this.farmer.presentDistrictCode == "Select District") {
      this.farmer.presentDistrictCode = null;
    }
    if (this.farmer.tehsilCode == "Select Tehsil") {
      this.farmer.tehsilCode = null;
    }
    if (this.farmer.presentTehsilCode == "Select Tehsil") {
      this.farmer.presentTehsilCode = null;
    }

  }

  ReadOnly() {
    this.form.controls['farmerName'].disable();
    this.form.controls['cnic'].disable();
    this.form.controls['gender'].disable();
    this.form.controls['fatherHusbandName'].disable();
    this.form.controls['maleDependant'].disable();
    this.form.controls['femaleDependant'].disable();
    this.form.controls['permanentAddress'].disable();
    this.form.controls['presentAddress'].disable();
    this.form.controls['cellPhone'].disable();
    this.form.controls['alternateName'].disable();
    this.form.controls['alternateCellPhoneNo'].disable();
    this.form.controls['unionCouncil'].disable();
    this.form.controls['dateOfBirth'].disable();
    this.form.controls['educationCode'].disable();
    this.form.controls['hobbies'].disable();
    this.form.controls['provinceCode'].disable();
    this.form.controls['districtCode'].disable();
    // this.form.controls['regionCode'].disable();
    this.form.controls['tehsilCode'].disable();
    this.form.controls['presentProvinceCode'].disable();
    this.form.controls['presentDistrictCode'].disable();
    this.form.controls['presentRegionCode'].disable();
    this.form.controls['presentTehsilCode'].disable();
    this.form.controls['presentUnionConcil'].disable();
    this.form.controls['landLine'].disable();
    this.form.controls['adjacentFarmerFriendName'].disable();
    this.form.controls['adjacentFarmerFriendCellPhone'].disable();
    this.form.controls['referalCode'].disable();
    this.form.controls['farmerStatusId'].disable();
    this.form.controls['check'].disable();
  }
  onSubmit() {
    console.log("Form Values: ", this.form.value);
  }

  getPresentAdress(event: any) {

    if (event == false) {
      this.checkflag = true;
    }
    else {
      this.checkflag = false;
    }
    if (event == true) {
      this.farmer.presentAddress = this.farmer.permanentAddress;
      this.farmer.presentProvinceCode = this.farmer.provinceCode;
      if (this.farmer.presentProvinceCode != "Select Province")
        this.getPresentDistricts(this.farmer.presentProvinceCode, 0);
      this.farmer.presentDistrictCode = this.farmer.districtCode;
      if (this.farmer.presentDistrictCode != "Select District")
        this.getPresentTehsils(this.farmer.presentDistrictCode, 0);
      this.farmer.presentTehsilCode = this.farmer.tehsilCode;
      this.farmer.presentUnionConcil = this.farmer.unionCouncil;
    }
    else {
      this.farmer.presentAddress = null;
      this.farmer.presentProvinceCode = "Select Province";
      this.farmer.presentDistrictCode = "Select District";
      this.farmer.presentTehsilCode = "Select Tehsil";
      this.farmer.presentUnionConcil = null;
      this.presentdistricts = null;
      this.presenttehsils = null;
    }

  }

}

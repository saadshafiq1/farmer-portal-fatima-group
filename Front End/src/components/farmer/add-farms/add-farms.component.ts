import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { FarmersService } from '../farmers.service';
import { Farms } from '../farms';
import { Blocks } from '../blocks';
import { Crops } from '../crops';
import { first } from 'rxjs/operators';
import { Province } from '../../../models/province';
import { Region } from '../../../models/region';
import { District } from '../../../models/district';
import { Tehsil } from '../../../models/tehsil';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-add-farms',
  templateUrl: './add-farms.component.html',
  styleUrls: ['./add-farms.component.css']
})
export class AddFarmsComponent implements OnInit {
  public notification;
  form: FormGroup;
  public readOnly = false;
  isNew: boolean = false;
  farms: Farms[];
  blocks: Blocks[];
  crops: Crops[];
  tubeWell: any[];
  tubeWellType: any[];
  waterCourse: any[];
  tunnel: any[];
  InputPurchase: any[];
  OutputExport: any[];
  provinces: Province[];
  regions: Region[];
  districts: District[];
  tehsils: Tehsil[];
  check;
  id: number;
  farminfo: Farms = new Farms();
  checkflag: boolean;
  constructor(private router: Router, fb: FormBuilder, private route: ActivatedRoute, private farmerService: FarmersService,
    private location: Location, private ts: ToastrService) {
    this.form = fb.group({
      "farmAddress": ["",],
      "tehsilCode": ["",],
      "districtCode": ["",],
      "provinceCode": ["",],
      "regionId": ["",],
      "tubeWellStatus": ["",],
      "waterCourse": ["",],
      "tubeWellType": ["",],
      "irrigationType": ["",],
      "irrigationArea": ["",],
      "tunnelType": ["",],
      "transport1": ["",],
      "transport2": ["",],
      "transport3": ["",],
      "inputPurchase": ["",],
      "exportProduct": ["",],
      "outputExport": ["",],
    })

  }

  ngOnInit() {
    this.getfarms();
  }

  getfarms(): void {
    this.checkflag = false;
    this.id = +this.route.snapshot.paramMap.get('id');
    this.route.queryParams
      .filter(params => params.view)
      .subscribe(params => {
        this.readOnly = params.view;
        (this.readOnly) ? this.ReadOnly() : null;
      });
    this.farmerService.getFarms(this.id).pipe(first()).subscribe(data => {
      this.farms = data
      if (this.farms.length == 0) {
        this.ts.info("No Farms Found");
        this.checkflag = true;
        this.location.back();
      }
    });

  }


  getfarminfo(id: string) {
    this.provinces = null;
    this.regions = null;
    this.districts = null;
    this.tehsils = null;
    this.tubeWell = null;
    this.tubeWellType = null;
    this.waterCourse = null;
    this.tunnel = null;
    this.InputPurchase = null;
    this.OutputExport = null;
    console.log(id);
    this.farmerService.getFarmInfo(id).pipe(first()).subscribe(data => {
      this.farminfo = data;
      this.getProvinces(id);
      this.getDistricts(this.farminfo.provinceCode, 1);
      this.getTehsils(this.farminfo.districtCode, 1);
    });

    this.farmerService.getBlocks(id).pipe(first()).subscribe(data => {
      this.blocks = data;
    });
    this.getfarmdetail();
  }

  getfarmdetail() {

    this.farmerService.getFarmDetail().pipe(first()).subscribe(data => {
      this.tubeWell = data["tubewell"];
      this.tubeWellType = data["tubewelltype"];
      this.waterCourse = data["watercourse"];
      this.tunnel = data["tunnel"];
      this.InputPurchase = data["inputPurchase"];
      this.OutputExport = data["outputExport"];
    });

  }

  getProvinces(id) {
    if (id != null) {
      this.farmerService.getProvinces().subscribe(data => {
        this.provinces = data;
      });

      this.districts = null;
      this.tehsils = null;
    }
  }


  getDistricts(id, id2) {
    if (id != null && id != "Select City") {
      this.farmerService.getDistricts(id).subscribe(data => {
        this.districts = data;
      });
      this.tehsils = null;
      if (id2 == 0) {
        this.farminfo.districtCode = "Select District";
        this.farminfo.tehsilCode = "Select Tehsil";
      }
    }
  }

  getTehsils(id, id2) {
    if (id != null && id != "Select District") {
      this.farmerService.getTehsils(id).subscribe(data => {
        this.tehsils = data;
      });
      if (id2 == 0) {
        this.farminfo.tehsilCode = "Select Tehsil";
      }
    }
  }


  getCrops(id) {
    this.farmerService.getCrops(id).pipe(first()).subscribe(cropdata => {
      this.crops = cropdata;
    });

  }


  save(): void {
    this.checkval();
    console.log(this.farminfo.farmerId);
    if (this.farminfo.farmId != null) {
      this.farmerService.updateFarm(this.farminfo).pipe(first()).subscribe(data => {

      });
      this.goBack();
    }

  }

  viewNotification() {
    this.notification = true;
    console.log("this.notification: ", this.notification);
  }

  notificationClose() {
    this.notification = false;
    console.log("this.notification: ", this.notification);
  }

  ReadOnly() {
    this.form.controls['farmAddress'].disable();
    this.form.controls['tehsilCode'].disable();
    this.form.controls['districtCode'].disable();
    this.form.controls['provinceCode'].disable();
    this.form.controls['tubeWellStatus'].disable();
    this.form.controls['waterCourse'].disable();
    this.form.controls['tubeWellType'].disable();
    this.form.controls['irrigationType'].disable();
    this.form.controls['irrigationArea'].disable();
    this.form.controls['tunnelType'].disable();
    this.form.controls['transport1'].disable();
    this.form.controls['transport2'].disable();
    this.form.controls['transport3'].disable();
    this.form.controls['inputPurchase'].disable();
    this.form.controls['exportProduct'].disable();
    this.form.controls['outputExport'].disable();
    this.form.controls['regionId'].disable();
  }

  checkval(): void {
    if (this.farminfo.provinceCode == "Select Province") {
      this.farminfo.provinceCode = null;
    }

    if (this.farminfo.regionId == "Select City") {
      this.farminfo.regionId = null;
    }
    if (this.farminfo.districtCode == "Select District") {
      this.farminfo.districtCode = null;
    }
    if (this.farminfo.tehsilCode == "Select Tehsil") {
      this.farminfo.tehsilCode = null;
    }
  }

  addTickets() {

    if (this.id != 0) {
      if (this.readOnly) {
        this.router.navigate(['/farmer-list-ticket/' + this.id], { queryParams: { view: true } });
      }
    }
  }

  addFarmer() {
    if (this.id != 0) {

      if (this.readOnly) {
        this.router.navigate(['/add-farmer/' + this.id], { queryParams: { view: true } });
      }
      else {
        this.router.navigate(['/add-farmer/' + this.id]);
      }
    }

  }
  addReports() {
    if (this.id != 0) {

      if (this.readOnly) {
        this.router.navigate(['/reports/' + this.id], { queryParams: { view: true } });
      }
      else {
        this.router.navigate(['/reports/' + this.id]);
      }
    }

  }

  addContracts() {
    if (this.id != 0) {
      if (this.readOnly) {
        this.router.navigate(['/farmers-contract/' + this.id], { queryParams: { view: true } });
      }
    }
  }
  addLoans() {
    if (this.id != 0) {
      if (this.readOnly) {
        this.router.navigate(['/farmers-loan/' + this.id], { queryParams: { view: true } });
      }
    }

  }
  goBack() {
    this.location.back();
  }

}
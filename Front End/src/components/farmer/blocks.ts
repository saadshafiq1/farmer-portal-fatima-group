import { Injectable } from '@angular/core';
export class Blocks {
    "farmBlockId"?: number;
    "farmId"?: number;
    "waterSoilResultId"?: number;
    "modifiedBy"?: number;
    "farmGis"?: number;
    "blockName"?: string;
    "blockArea"?: string;
    "modifiedDateTime"?: Date;
    "insertionDate"?: Date;
   "activeStatus"?: string;
}


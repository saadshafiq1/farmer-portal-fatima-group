import { Injectable } from '@angular/core';
export class Crops {
  "farmCropId"?: number;
  "farmBlockId"?: number;
  "cropCode"?: number;
  "cropArea"?: string;
  "waterSoilResultId"?: number;
  "modifiedBy"?: number;
  "modifiedDateTime"?: Date;
  "insertionDate"?: Date;
  "activeStatus"?: string;
}


import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmersContractComponent } from './farmers-contract.component';

describe('FarmersContractComponent', () => {
  let component: FarmersContractComponent;
  let fixture: ComponentFixture<FarmersContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmersContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmersContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

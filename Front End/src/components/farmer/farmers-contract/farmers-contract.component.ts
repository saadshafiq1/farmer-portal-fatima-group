import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'app-farmers-contract',
  templateUrl: './farmers-contract.component.html',
  styleUrls: ['./farmers-contract.component.css']
})
export class FarmersContractComponent implements OnInit {
  id: number;
  constructor(private location: Location,private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
  }

  addTickets() {

    if (this.id != 0) {

      this.router.navigate(['/farmer-list-ticket/' + this.id], { queryParams: { view: true } });

    }
  }

  addFarms() {
    if (this.id != 0) {

      this.router.navigate(['/farmers-add-farms/' + this.id], { queryParams: { view: true } });
    }

  }

  addFarmer() {
    if (this.id != 0) {


      this.router.navigate(['/add-farmer/' + this.id], { queryParams: { view: true } });
    }
    else {
      this.router.navigate(['/add-farmer/' + this.id]);
    }


  }
  addLoans() {
    if (this.id != 0) {

      this.router.navigate(['/farmers-loan/' + this.id], { queryParams: { view: true } });
    }

  }
  goBack()
  {
     this.location.back();
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmersDetailComponent } from './farmers-detail.component';

describe('FarmersDetailComponent', () => {
  let component: FarmersDetailComponent;
  let fixture: ComponentFixture<FarmersDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmersDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmersDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { MockData } from '../../../constants/mock-data';
import { Router } from '@angular/router';
import { FarmersService } from '../farmers.service';
import { first } from 'rxjs/operators';
import { Province } from '../../../models/province';
import { Farmers } from '../farmers';
@Component({
    selector: 'app-farmers-detail',
    templateUrl: './farmers-detail.component.html',
    styleUrls: ['./farmers-detail.component.css']
})
export class FarmersDetailComponent implements OnInit {
    farmersViewcolumnDefs = [];
    farmersViewrowData = [];
    categories = ['Phone', 'CNIC',];
    private pages: number = 1;
    private params: string;
    public recordsPerPage: number = 15;
    public totalrecords: number = 0;
    showBoundaryLinks = true;
    private currentPage: number = 0;
    public breadcrumbUrl;
    searchvalue;
    allFlag;
    provinceFlag;
    selectedValue;
    currentpage;
    provinces: Province[];
    private searchedcat: string;
    private searchedValue: string = "";
    farmers: Farmers[];
    constructor(private router: Router, public farmersService: FarmersService) {
    }

    ngOnInit() {
        this.selectedValue = 'CNIC';
        this.farmersService.getFarmers('pageNumber=' + this.currentPage + '&pageSize=' + this.recordsPerPage).pipe(first()).subscribe(data => {
            this.farmers = data['data'];
            this.totalrecords = data['totalCount'];
            console.log("Farmers", this.totalrecords);
            this.getWfGridData();
        });
        console.log("called")
        this.farmersService.getProvinces().pipe(first()).subscribe(data => {
            this.provinces = data;
        });
    }

    getWfGridData() {
        const colDef: any[] = MockData.agGridColumnDefs_Farmers;

        colDef.map(column => {
            column.headerName = column.headerName;
        });

        this.farmersViewcolumnDefs = colDef;
        this.farmersViewrowData = this.farmers;
    }

    addFarmer(id) {
        this.router.navigate(['/add-farmer/' + id]);
    }
    pageChanged(event: any) {
        //this.toastr.success('Hello world!', 'Page Changed');
        this.pages = event.page;
        this.pages = this.pages - 1;
        if (this.searchedValue == "") {
            this.params = 'pageNumber=' + this.pages + '&pageSize=' + this.recordsPerPage;
        }
        else {
            this.params = 'pageNumber=' + this.pages + '&pageSize=' + this.recordsPerPage + '&filter=' + this.searchedcat + '&value=' + this.searchedValue;
        }
        this.farmersService.getFarmers(this.params).pipe(first()).subscribe(data => {
            this.farmers = data['data'];
            this.getWfGridData();
        });
    }

    search(searchcat: string, searchval: string) {
        console.log(searchcat);
        console.log(searchval);
        this.searchedcat = searchcat;
        this.searchedValue = searchval;
        console.log(this.searchedValue);
        if (searchcat == "Province") {
            this.searchvalue = "";
        }
        if (this.searchedValue == "") {

            this.params = 'pageNumber=' + this.currentPage + '&pageSize=' + this.recordsPerPage;
            this.allFlag = 1;
            this.provinceFlag = 0;
            this.breadcrumbUrl = 'All';
        }
        else {
            this.params = 'pageNumber=' + this.currentPage + '&pageSize=' + this.recordsPerPage + '&filter=' + this.searchedcat + '&value=' + this.searchedValue;
            this.allFlag = 0;
            this.provinceFlag = 1;
            this.breadcrumbUrl = searchval;

        }
        this.farmersService.getFarmers(this.params).pipe(first()).subscribe(data => {
            this.farmers = data['data'];
            this.totalrecords = data['totalCount'];
            console.log(this.totalrecords);
            this.getWfGridData();
        });
        this.currentpage = 1;
    }
    ResetVal(val) {
        if (val == 'Phone') {
            this.searchvalue = "92";
        }
        else {
            this.searchvalue = "";
        }
    }
     onGridReady(params) {
        params.api.sizeColumnsToFit();
    }
}
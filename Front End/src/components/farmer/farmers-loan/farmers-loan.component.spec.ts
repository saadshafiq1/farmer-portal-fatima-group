import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmersLoanComponent } from './farmers-loan.component';

describe('FarmersLoanComponent', () => {
  let component: FarmersLoanComponent;
  let fixture: ComponentFixture<FarmersLoanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmersLoanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmersLoanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

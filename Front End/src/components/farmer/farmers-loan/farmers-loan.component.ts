import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'app-farmers-loan',
  templateUrl: './farmers-loan.component.html',
  styleUrls: ['./farmers-loan.component.css']
})
export class FarmersLoanComponent implements OnInit {
  id: number;
  constructor(private location: Location,private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
  }

  addTickets() {

    if (this.id != 0) {

      this.router.navigate(['/farmer-list-ticket/' + this.id], { queryParams: { view: true } });

    }
  }

  addFarms() {
    if (this.id != 0) {

      this.router.navigate(['/farmers-add-farms/' + this.id], { queryParams: { view: true } });
    }

  }

  addFarmer() {
    if (this.id != 0) {


      this.router.navigate(['/add-farmer/' + this.id], { queryParams: { view: true } });
    }
    else {
      this.router.navigate(['/add-farmer/' + this.id]);
    }


  }
  addContracts() {
    if (this.id != 0) {

      this.router.navigate(['/farmers-contract/' + this.id], { queryParams: { view: true } });
    }

  }
  goBack()
  {
     this.location.back();
  }

}



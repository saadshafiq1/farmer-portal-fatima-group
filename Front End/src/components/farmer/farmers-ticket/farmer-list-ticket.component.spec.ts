import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmerListTicketComponent } from './farmer-list-ticket.component';

describe('ListDealerComponent', () => {
  let component: FarmerListTicketComponent;
  let fixture: ComponentFixture<FarmerListTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FarmerListTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FarmerListTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

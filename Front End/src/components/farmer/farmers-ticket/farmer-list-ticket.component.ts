import { Component, OnInit } from '@angular/core';
import { MockData } from '../../../constants/mock-data';
import { FarmersService } from '../farmers.service';
import { FarmerTicket } from '../FarmerTicket';
import { Province } from '../../../models/province';
import { TicketStatus } from '../../../models/ticketstatus';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
@Component({
    selector: 'app-farmer-list-ticket',
    templateUrl: './farmer-list-ticket.component.html',
    styleUrls: ['./farmer-list-ticket.component.css']
})
export class FarmerListTicketComponent implements OnInit {
    workFlowViewcolumnDefs = [];
    workFlowViewrowData = [];
    tickets: FarmerTicket[];
    selectedValue;
    searchvalue;
    public breadcrumbUrl;
    allFlag;
    id: number;
    provinceFlag;
    categories = ['TicketId', 'TicketType', 'SLA STATUS', 'Ticket Status'];
    provinces: Province[];
    ticketstatus: TicketStatus[];
    private searchedValue: string = "";
    private params: string;
    private currentPage: number = 0;
    currentpage;
    private searchedcat: string;
    private pages: number = 1;
    public recordsPerPage: number = 15;
    public totalrecords: number = 0;
    showBoundaryLinks = true;
    constructor(private location: Location, public ticketsService: FarmersService, private router: Router, private route: ActivatedRoute) {

    }
    ngOnInit() {
        this.selectedValue = 'TicketId';        
        this.id = +this.route.snapshot.paramMap.get('id');
        this.ticketsService.getTickets('pageNumber=' + this.currentPage + '&pageSize=' + this.recordsPerPage + '&farmerid=' + this.id).subscribe(data => {
            this.tickets = data['Tickets'];
            console.log(this.tickets);
            this.totalrecords = data.TotalTickets;
            console.log(this.totalrecords);
            this.getWfGridData();
        });

        this.ticketsService.getTicketsStatus(this.id).pipe(first()).subscribe(data => {
            this.ticketstatus = data;
            console.log(this.ticketstatus);
        });

    }
    pageChanged(event: any) {
        this.pages = event.page;
        this.pages = this.pages - 1;
        this.params = 'pageNumber=' + this.pages + '&pageSize=' + this.recordsPerPage + '&farmerid=' + this.id;
        console.log(this.params);
        this.ticketsService.getTickets(this.params).pipe(first()).subscribe(data => {
            console.log("data", data)
            this.tickets = data['Tickets'];
            this.getWfGridData();
        });
    }
    search(searchcat: string, searchval: string) {
        console.log(searchcat);
        console.log(searchval);
        this.searchedcat = searchcat;
        this.searchedValue = searchval;
        console.log(this.searchedValue);
        if (searchcat == "status") {
            this.searchvalue = "";
        }
        if (this.searchedValue == "") {
            this.params = 'pageNumber=' + this.currentPage + '&pageSize=' + this.recordsPerPage + '&farmerid=' + this.id;
            this.breadcrumbUrl = 'All';
        }
        else {
            this.params = 'pageNumber=' + this.currentPage + '&pageSize=' + this.recordsPerPage + '&filter=' + this.searchedcat + '&value=' + this.searchedValue + '&farmerid=' + this.id;
            this.breadcrumbUrl = searchval;
        }
        this.ticketsService.getTickets(this.params).pipe(first()).subscribe(data => {
            this.tickets = data['Tickets'];
            this.totalrecords = data.TotalTickets;
            console.log(this.totalrecords);
            this.getWfGridData();
        });
        this.currentpage = 1;
    }

    getWfGridData() {
        const colDef: any[] = MockData.agGridColumnDefs_FarmerTickets;
        this.workFlowViewcolumnDefs = colDef;
        this.workFlowViewrowData = this.tickets;

    }

    addFarmer() {
        this.router.navigate(['/add-farmer/' + this.id], { queryParams: { view: true } });
    }


    addFarms() {
        this.router.navigate(['/farmers-add-farms/' + this.id], { queryParams: { view: true } });
    }

    addContracts() {
        this.router.navigate(['/farmers-contract/' + this.id], { queryParams: { view: true } });
    }

    addLoans() {
        this.router.navigate(['/farmers-loan/' + this.id], { queryParams: { view: true } });
    }
    goBack() {
        this.location.back();
    }
    addReports() {
        if (this.id != 0) {
    
          this.router.navigate(['/reports/' + this.id], { queryParams: { view: true } });
        }
    
    }
  onGridReady(params) {
        params.api.sizeColumnsToFit();
    }
    

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from '../../constants/app-config';
import { Farmers } from './farmers';
import { Farms } from './farms';
import { Crops } from './crops';
import { FarmerTicket } from './FarmerTicket';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Province } from '../../models/province';
import { ToastrService } from 'ngx-toastr';
import { Region } from '../../models/region';
import { District } from '../../models/district';
import { Tehsil } from '../../models/tehsil';
@Injectable({
  providedIn: 'root'
})
export class FarmersService {

  constructor(private http: HttpClient, private ts: ToastrService) {

  }

  addFarmer(farmer: Farmers): Observable<Farmers> {
    console.log(farmer);
    return this.http.post<Farmers>(`${AppConfig.URL_Farmers}`, farmer).pipe(
      map(x => x["data"]),
      tap((newFarmer: Farmers) => console.log(`added Farmer w/ id=${newFarmer.farmerId}`)),
      catchError(this.handleError<Farmers>('addFarmer'))
    );
  }

  getFarmers(params: string) {
    console.log("service called")
    console.log(`${AppConfig.URL_Farmers}GetAllFarmers?` + params);

    return this.http.get<Farmers[]>(`${AppConfig.URL_Farmers}GetAllFarmers?` + params)
      .pipe(
      map(x => x["data"]),
      tap(_ => console.log('fetched dealers')),
      catchError(this.handleError('getFarmers'))
      );
  }

  getFarms(id) {
    return this.http.get<Farms>(`${AppConfig.URL_Farms}FarmsOfFarmer/${id}`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`fetched farms id=${id}`)),
      catchError(this.handleError<any>('getFarms'))
    );
  }

  getBlocks(id) {
    return this.http.get<Farms>(`${AppConfig.URL_Farms}blocksoffarms/${id}`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`fetched block id=${id}`)),
      catchError(this.handleError<any>('getBlocks'))
    );
  }

  getCrops(id) {
    console.log(id);
    return this.http.get<Crops>(`${AppConfig.URL_Crops}cropsofblock/${id}`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`fetched crop id=${id}`)),
      catchError(this.handleError<any>('getCrops'))
    );
  }

  getFarmInfo(id) {
    return this.http.get<Farms>(`${AppConfig.URL_Farms}/${id}`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`fetched farms id=${id}`)),
      catchError(this.handleError<any>('getFarmerFarms'))
    );
  }

  getTickets(params: string) {
    console.log(`${AppConfig.URL_Tickets}GetAllTicketsByFarmerId?` + params);
    return this.http.get<FarmerTicket>(`${AppConfig.URL_Tickets}GetAllTicketsByFarmerId?` + params).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`getTickets`)),
      catchError(this.handleError<any>('getTickets'))
    );
  }

  deleteFarmer(farmer: Farmers | number): Observable<Farmers> {
    const id = typeof farmer === 'number' ? farmer : farmer.farmerId;
    const url = `${AppConfig.URL_Farmers}${id}`;
    return this.http.delete<Farmers>(url).pipe(
      map(x => x["data"]),
      tap(_ => this.log(`deleted farmer id=${id}`)),
      catchError(this.handleError<Farmers>('deleteFarmer'))
    );
  }

  getTicketsStatus(id) {
    return this.http.get(`${AppConfig.URL_Tickets}StatusTypesForFarmer?FarmerId=` + id).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`getTicketsStatus`)),
      catchError(this.handleError<any>('getTicketsStatus'))
    );
  }

  getFarmer(id) {
    return this.http.get<Farmers>(`${AppConfig.URL_Farmers}${id}`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`updated farmers id=${id}`)),
      catchError(this.handleError<any>('getfarmers'))
    );
  }

  updateFarmer(farmer: Farmers): Observable<any> {
    console.log(`${AppConfig.URL_Farmers}${farmer["farmerId"]}`, farmer);
    return this.http.put(`${AppConfig.URL_Farmers}${farmer["farmerId"]}`, farmer).pipe(
      tap(_ => this.log(`updated farmer id=${farmer["farmerId"]}`)),
      catchError(this.handleError<any>('updatefarmer'))
    );

  }

  updateFarm(farm: Farms): Observable<any> {
    console.log(`${AppConfig.URL_Farms}${farm["farmId"]}`);
    return this.http.put(`${AppConfig.URL_Farms}${farm["farmId"]}`, farm).pipe(
      tap(_ => this.log(`updated farm id=${farm["farmId"]}`)),
      catchError(this.handleError<any>('updateFarm'))
    );
  }

  getFarmDetail() {
    return this.http.get(`${AppConfig.URL_Farms}GetFarmRelevantDropDowns`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`getFarmDetail`)),
      catchError(this.handleError<any>('getFarmDetail'))
    );

  }

  getProvinces() {
    return this.http.get<Province>(`${AppConfig.URL_Lookup}Province`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`getProvinces`)),
      catchError(this.handleError<any>('getProvinces'))
    );
  }

  getRegions(id) {
    return this.http.get<Region>(`${AppConfig.URL_Lookup}CitiesOfProvince/${id}`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`getRegions`)),
      catchError(this.handleError<any>('getRegions'))
    );
  }

  getDistricts(id) {
    return this.http.get<Region>(`${AppConfig.URL_Lookup}DistrictsOfProvince/${id}`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`getDistricts`)),
      catchError(this.handleError<any>('getDistricts'))
    );
  }

  getTehsils(id) {

    return this.http.get<Region>(`${AppConfig.URL_Lookup}TehsilsOfDistrict/${id}`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`getTehsils`)),
      catchError(this.handleError<any>('getTehsils'))
    );
  }

  getFarmerStatus() {
    return this.http.get<Province>(`${AppConfig.URL_Farmers}GetFarmerStatuses`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`getFarmerStatus`)),
      catchError(this.handleError<any>('getFarmerStatus'))
    );
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this.log(`${operation} failed: ${error.message}`);
      this.ts.error("Failed to Perform Operation");
      return of(result as T);
    };
  }


  private log(message: string) {
    this.ts.success("Operation Performed Successfully");
  }
}

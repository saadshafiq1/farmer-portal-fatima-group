import { Injectable } from '@angular/core';
export class FarmerTicket {
      "ticketId"?: number;
      "ticketTypeId "?: number;
      "visitDate "?: Date;
      "visitTime"?: Date;
      "description"?: string;
      "statusId "?: number;
      "statusText"?: string;
      "slastatus"?: string;
      "feedback "?: number;
      "feedbackComments"?: string;
      "feedbackAudioUrl"?: string;
      "farmerId"?: number;
      "farmId"?: number;
      "assignedTo"?: number;
      "createdBy"?: number;
      "CreatedByName"?: string;
      "resolvedDate"?: Date;
      "createdDate"?: Date;
      "updatedDate "?: Date;
      "modifiedDate  "?: Date;
      "archive"?: boolean;
      "deleted"?: boolean;
}

import { Injectable } from '@angular/core';
export class Farms {
    "tmFarmBlock"?: any[];
    "farmType"?: string;
    "waterSoilResultId"?: number;
    "blockName"?: string;
    "blockArea"?: number;
    "totalUnits"?: number;
    "farmId"?: number;
    "farmerId"?: number;
    "farmTypeCode"?: number;
    "canal"?: string;
    "ownerTubeWell"?: string;
    "areaonHeis"?: number;
    "waterCourseType"?: string;
    "totalAreaAcres"?: number;
    "farmName"?: string;
    "farmAddress"?: string;
    "farmGis"?: number;
    "provinceCode"?: string;
    "tehsilCode"?: string;
    "districtCode"?: string;
    "regionId"?: string;
    "modifiedBy"?: number;
    "modifiedDateTime"?: Date;
    "insertionDate"?: Date;
    "activeStatus"?: string;
    "transport1"?: string;
    "transport2"?: string;
    "transport3"?: string;
    "tubeWellStatus"?: string;
    "tubeWellType"?: string;
    "irrigationType"?: string;
    "irrigationArea"?: string;
    "waterCourse"?: string;
    "tunnelType"?: string;
    "inputPurchase"?: string;
    "exportProduct"?: string;
    "outputExport"?: string;
}

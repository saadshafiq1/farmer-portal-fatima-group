import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthService } from '../../services/auth.service';
import { first } from 'rxjs/operators';
import { User } from '../../models/user';

import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading: boolean;
  //submitted = false;
  returnUrl: string;
  error = '';
  invalidLogin: boolean = false;
  errorMessage: string;
  isSubmitted = true;
  user: User = new User();
  constructor(private readonly authservice: AuthService,
    private route: ActivatedRoute,
    private readonly router: Router, private ts: ToastrService) {

  }

  ngOnInit() {
    this.authservice.logout();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/tickets';

  }

  Submit() {
    // this.authservice.authenticateLogin(this.user)
    // .pipe(first())
    //         .subscribe(
    //             data => {
    //                 this.router.navigate([this.returnUrl]);
    //             },
    //             error => {
    //                 this.error = error;
    //                 this.loading = false;
    //             });

    this.authservice.authenticateLogin(this.user)

      .pipe(first())
      .subscribe(
      data => {
        this.router.navigate([this.returnUrl]);

      },

      error => {
        this.error = error;
        this.loading = false;

      });
  }

}

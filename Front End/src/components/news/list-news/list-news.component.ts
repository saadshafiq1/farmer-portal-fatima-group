import { Component, OnInit } from '@angular/core';
import { MockData } from '../../../constants/mock-data';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NewsService } from '../News.service';
import { News } from '../News';
@Component({
    selector: 'app-list-news',
    templateUrl: './list-news.component.html',
    styleUrls: ['./list-news.component.css']
})
export class ListNewsComponent implements OnInit {
    public workFlowViewcolumnDefs: any[];
    public workFlowViewrowData: any[];
    news: News[];
    private currentPage: number = 0;
    public recordsPerPage: number = 15;
    public totalrecords: number = 0;
    private params: string;
    currentpage;
    private searchedcat: string;
    searchvalue;
    allFlag;
    provinceFlag;
    categories = ['District', 'Status'];
    newsstatus: any[] = [{ label: 'Active', value: 'A' }, { label: 'InActive', value: 'I' }];
    selectedValue;
    breadcrumbUrl;
    private searchedValue: string = "";
    private pages: number = 1;
    showBoundaryLinks = true;
    public colDef: any[];
    constructor(public newsService: NewsService, private router: Router) {
    }
    ngOnInit() {
        this.selectedValue = 'District';
        this.newsService.getAllNews('pageNumber=' + this.currentPage + '&pageSize=' + this.recordsPerPage).pipe(first()).subscribe(data => {
            this.news = data['data'];
            
            this.totalrecords = data['totalCount'];
            this.getWfGridData();
        });
    }
    pageChanged(event: any) {
        this.pages = event.page;
        this.pages = this.pages - 1;
        if (this.searchedValue == "") {
            this.params = 'pageNumber=' + this.pages + '&pageSize=' + this.recordsPerPage;
        }
        else {
            this.params = 'pageNumber=' + this.pages + '&pageSize=' + this.recordsPerPage + '&filter=' + this.searchedcat + '&value=' + this.searchedValue;
        }
        this.newsService.getAllNews(this.params).pipe(first()).subscribe(data => {
            this.news = data['data'];
            this.getWfGridData();
        });
    }
    search(searchcat: string, searchval: string) {
        console.log(searchcat);
        console.log(searchval);
        this.searchedcat = searchcat;
        this.searchedValue = searchval;
        console.log(this.searchedValue);
        if (searchcat == "Province") {
            this.searchvalue = "";
        }
        if (this.searchedValue == "") {

            this.params = 'pageNumber=' + this.currentPage + '&pageSize=' + this.recordsPerPage;
            this.breadcrumbUrl = 'All';
        }
        else {
            this.params = 'pageNumber=' + this.currentPage + '&pageSize=' + this.recordsPerPage + '&filter=' + this.searchedcat + '&value=' + this.searchedValue;
            this.breadcrumbUrl = searchval;
        }
        this.newsService.getAllNews(this.params).pipe(first()).subscribe(data => {
            this.news = data['data'];
            this.totalrecords = data['totalCount'];
            this.getWfGridData();
        });
        this.currentpage = 1;
    }
    getWfGridData() {

        this.colDef = MockData.agGridColumnDefs_News;
        //const rowDef: any[] = MockData.agGridRowDefs_News;
        this.workFlowViewcolumnDefs = this.colDef;
        console.log(this.workFlowViewcolumnDefs);
        this.workFlowViewrowData = this.news;
        console.log(this.workFlowViewrowData);
    }
    addedit(id) {
        this.router.navigate(['/news/' + id]);
    }
     onGridReady(params) {
        params.api.sizeColumnsToFit();
    }

}

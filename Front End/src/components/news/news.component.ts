import { Component, OnInit } from '@angular/core';
import { NewsService } from './News.service';
import { News } from './News';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { District } from '../../models/district';
import { Location } from '@angular/common';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  news: News = {};
  fileToUpload: File = null;
  form: FormGroup;
  public readOnly = false;
  isNew: boolean = false;
  id: number;
  imgsrc;
  name: string = "";
  districts: District[];
  Region: any[] = [{ label: 'All Districts', value: '1' }, { label: 'One District', value: '2' }];
  categories: any[] = [{ label: 'Active', value: 'A' }, { label: 'InActive', value: 'I' }];
  myDate = new Date();
  national;
  base64textString: string;
  constructor(private location: Location, private newsService: NewsService, private route: ActivatedRoute, private router: Router, private ts: ToastrService, fb: FormBuilder) {
    this.form = fb.group({
      "header": ["", Validators.required],
      "body": ["", Validators.required],
      "districtCode": ["",],
      "activeStatus": ["",],
      "national": ["",],
    })

  }
  ngOnInit() {
    this.getNews();
  }
  save(): void {
    this.news.mediaTypeId = 1;
    this.isNew ? this.newsService.addNews(this.imgsrc, JSON.stringify(this.news)).subscribe(news => {
      this.news = news;
      this.news && this.news.mediaId && this.router.navigate(['news/' + news.mediaId]);
      this.ts.success("Operation Performed Successfully");
    }) : this.newsService.updateNews(this.imgsrc, JSON.stringify(this.news)).subscribe(


    );
    this.location.back();
  }

  handleFileInput(files: FileList, event) {
    this.fileToUpload = files.item(0);
    if (this.fileToUpload.size > 1000000) {
      this.ts.error("File Size Exceeds 1Mb Please Upload Image Again");
    }
    else {
      var file = event.target.files;
      var file1 = files[0];
      if (files && file1) {
        var reader = new FileReader();
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsBinaryString(file1);
      }
    }
  }

  getNews(): void {
    this.news.activeStatus = "A";
    this.national = "2";
    this.id = +this.route.snapshot.paramMap.get('id');
    this.loadDistricts();
    this.route.queryParams
      .filter(params => params.view)
      .subscribe(params => {
        this.readOnly = params.view;
        (this.readOnly) ? this.ReadOnly() : null;
      });
    this.id ? this.newsService.getNews(this.id)
      .subscribe(news => {
        this.news = news;
        this.imgsrc = this.news.url;
        if (this.news.nationalNews == true) {
          this.national = "1";
          this.form.controls['districtCode'].disable();
        }
        else {
          this.national = "2";
        }
      }
      ) : this.isNew = true;


  }


  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    this.imgsrc = "data:image/jpg;base64," + this.base64textString;
  }

  delete(): void {
    if (this.id != 0) {
      this.newsService.deleteNews(this.news).subscribe(news => {
        this.router.navigate(['news/']);
      });
    }
  }
  goBack(): void {
    this.location.back();
  }

  ReadOnly() {
    this.form.controls['header'].disable();
    this.form.controls['body'].disable();
    this.form.controls['districtCode'].disable();
    this.form.controls['activeStatus'].disable();
    this.form.controls['national'].disable();
  }

  getValues(value, id) {
    if (value == 2) {
      this.news.nationalNews = false;
      this.form.controls['districtCode'].enable();

    }
    else {

      this.news.nationalNews = true;
      if (id != 0) {
        this.news.districtCode = null;
      }
      this.news.districtCode = null;
      this.form.controls['districtCode'].disable();
    }

  }
  loadDistricts() {
    this.newsService.getDistricts().subscribe(data => {
      this.districts = data;
      // console.log("saad");
      // console.log(data);
    });
  }

  editorConfig = {
    editable: true,
    height: '15rem',
    minHeight: '5rem',
    toolbar: [
      ["bold", "italic", "underline", "link"],
      ["orderedList", "unorderedList"],
    ]
  };

}

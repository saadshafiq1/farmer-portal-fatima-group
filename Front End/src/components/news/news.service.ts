import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { AppConfig } from '../../constants/app-config';
import { News } from './News';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Province} from '../../models/province';
import { ToastrService } from 'ngx-toastr';
import { District } from '../../models/district';

@Injectable({
  providedIn: 'root'
})

export class NewsService  
{
  constructor(private http: HttpClient,private ts: ToastrService) 
  {
    
  }

addNews (img,news): Observable<News>  
{
 var body = {
      image: img, tbMedia:news
    }
  return this.http.post<News>(`${AppConfig.URL_News}`, body)
  .pipe(map(x => x["data"] ),
    tap((news: News) => console.log(`added  w/ id=${news.mediaId}`)),
    catchError(this.handleError('addNews'))
  );
}

updateNews(img,news): Observable<News>  
{
    var body = {
      image: img, tbMedia:news
    }
 var obj=JSON.parse(news);
  return this.http.put<News>(`${AppConfig.URL_News}${obj.mediaId}`, body)
  .pipe(map(x => x["data"] ),
   tap(_ => this.log(`updated News id=${obj.mediaId}`)),
    //tap((news: News) => console.log(`updated  w/ id=${news.mediaId}`)),
    catchError(this.handleError('updateNews'))
  );
}

getAllNews(params:string) 
{
  console.log(`${AppConfig.URL_News}AllMedia?`+params);
  return this.http.get<News []>(`${AppConfig.URL_News}AllMedia?`+params)
  .pipe(
    map(x => x["data"] ),
    tap(_ => console.log('fetched news')),
     catchError(this.handleError('getAllNews', []))
  );
}

getNews(id){
  return this.http.get<News>(`${AppConfig.URL_News}${id}`).pipe(
    map(x => x["data"] ),
    tap(_ => console.log(`updated news id=${id}`)),
    catchError(this.handleError<any>('getNews'))
  );
}

deleteNews (news: News | number): Observable<News> {
  const id = typeof news === 'number' ? news : news.mediaId;
  const url = `${AppConfig.URL_News}${id}`;
  return this.http.delete<News>(url).pipe(
    map(x => x["data"] ),
    tap(_ => this.log(`deleted news id=${id}`)),
    catchError(this.handleError<News>('deleteNews'))
  );
}

  getDistricts(){
  return this.http.get<District>(`${AppConfig.URL_Lookup}GetAllDistricts`).pipe(
    map(x => x["data"] ),
    tap(_ => console.log(`getDistricts`)),
    catchError(this.handleError<any>('getDistricts'))
  );
  }


private handleError<T> (operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {
    this.log(`${operation} failed: ${error.message}`);
    this.ts.error("Failed to Perform Operation");
    return of(result as T);
  };
}

private log(message: string) {
  this.ts.success("Operation Performed Successfully");
}
}

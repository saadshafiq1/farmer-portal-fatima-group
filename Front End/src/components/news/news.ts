import { Injectable } from '@angular/core';
export class News
{
    "mediaId"?: number;
    "mediaTypeId"?: number; 
    "description"?: string; 
    "districtCode"?: number;
    "url"?: string; 
    "header"?: string; 
    "body"?: string; 
    "footer"?: string;
    "leftContent"?: string;
    "rightContent"?: string;
    "image"?: string;
    "modifiedBy"?: number;
    "modifiedDateTime"?: Date;
    "insertionDate"?: Date;
    "activeStatus"?: string;  
    "nationalNews"?:boolean;
}
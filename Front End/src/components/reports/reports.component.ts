import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { error } from '@angular/compiler/src/util';
import { Reports } from './reports';
import { ReportsService } from './reports.service';

import { ReactiveFormsModule } from '@angular/forms';
import { MockData } from 'src/constants/mock-data';
import { HttpClient } from '@angular/common/http';
import { formatDate } from 'ngx-bootstrap';
import { style } from '@angular/animations';
import { ButtonRendererComponent } from 'src/components/base/ag-grid-base/ag-grid-components/button-renderer.component';
import { ButtonRenderer1Component } from 'src/components/base/ag-grid-base/ag-grid-components/button-renderer1.component';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {
title = 'app';
rowData: any;
  columnDefs = [
  { headerName: 'Report Name', field: 'reportName', sort: "asc" ,width: 150},
  
  { headerName: 'Created Date', field: 'insertionDate',width: 150 ,cellRenderer: (data) => {
    return data.value ? (new Date(data.value)).toLocaleDateString() : '';

}},
  { headerName: 'Report Date', field: 'reportDate' ,width: 150,cellRenderer: (data) => {
    return data.value ? (new Date(data.value)).toLocaleDateString() : '';

}},
  
    
    {headerName: 'Delete',width: 80, field: 'status',  cellRenderer: 'buttonRenderer',cellRendererParams: {onClick: this.onBtnClick1.bind(this.rowData),label: 'Delete',}},
    {headerName: 'View',width: 80, field: 'url', cellRenderer: 'buttonRenderer1',cellRendererParams: {onClick1: this.onBtnClick2.bind(this.rowData), label: 'View', }}
    
  

];
  repUrl="";
  
  form: FormGroup;
  public id: number;
  public readOnly = false;
  base64textString: string;
  isreport: boolean = false;
  fileData: File = null;
  public FormObject :any;
  public  currentpage;
  imgsrc;
  frameworkComponents: any;
  frameworkComponents1: any;
  click='';
  public recordsPerPage: number = 15;
  public filename:string;
  public datepicker:any;
  public fileupload:any;
  reports:Reports[];
  reportdata:Reports=new Reports();
  a:Date;
  public dat:any;
  public repId;
  public dates:string[]=[];
  workFlowViewrowData = [];
  workFlowViewcolumnDefs = [];
  constructor( private http: HttpClient,private location: Location,fb: FormBuilder,private router:Router, private route: ActivatedRoute, private reportsSerevice: ReportsService) {
    this.form = fb.group({

      "farmAddress": ["",],
      "tehsilCode": ["",],
      "districtCode": ["",],
      "provinceCode": ["",],
      "regionId": ["",],
      "tubeWellStatus": ["",],
      "waterCourse": ["",],
      "tubeWellType": ["",],
      "irrigationType": ["",],
      "irrigationArea": ["",],
      "tunnelType": ["",],
      "transport1": ["",],
      "transport2": ["",],
      "transport3": ["",],
      "inputPurchase": ["",],
      "exportProduct": ["",],
      "outputExport": ["",],
      "farmerName": ["",],
    })
    this.frameworkComponents = {
      
     //buttonRenderer1: ButtonRenderer1Component,
    }
    this.frameworkComponents1 = {
      buttonRenderer: ButtonRendererComponent,
     buttonRenderer1: ButtonRenderer1Component,
    }

  }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.form = new FormGroup({
      name: new FormControl(), 
      datepicker:new FormControl()  ,
      fileupload:new FormControl()
    
    });
      this.reportsSerevice.getReport(this.id).subscribe(x => {this.rowData=x.data;
      console.log("report data",x.data);});
  }

  
  addFarmer() {
    if (this.id != 0) {

      if (this.readOnly) {
        this.router.navigate(['/add-farmer/' + this.id], { queryParams: { view: true } });
      }
      else {
        this.router.navigate(['/add-farmer/' + this.id]);
      }
    }
  }

addFarms() {
  if (this.id != 0) {

    if (this.readOnly) {
      this.router.navigate(['/farmers-add-farms/'+ this.id], { queryParams: { view: true } });
    }
    else {
      this.router.navigate(['/farmers-add-farms/'+ this.id]);
    }
  }
}
addReports() {
  if (this.id != 0) {

    if (this.readOnly) {
      this.router.navigate(['/reports/' + this.id], { queryParams: { view: true } });
    }
    else {
      this.router.navigate(['/reports/' + this.id]);
    }
  }

}
  addTickets() {
    if (this.id != 0) {
      if (this.readOnly) {
        this.router.navigate(['/farmer-list-ticket/' + this.id], { queryParams: { view: true } });
      }
    }

  }

  addContracts() {
    if (this.id != 0) {
      if (this.readOnly) {

        this.router.navigate(['/farmers-contract/' + this.id], { queryParams: { view: true } });
      }
    }

  }
  addLoans() {
    if (this.id != 0) {
      if (this.readOnly) {
        this.router.navigate(['/farmers-loan/' + this.id], { queryParams: { view: true } });
      }
    }

  }

handleFileInput(files: FileList, event) {
  this.fileData = files.item(0);
  if (this.fileData.size > 1000000) {
  error("File too big ");
  }
  else {
    var file = event.target.files;
    var file1 = files[0];
    if (files && file1) {
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file1);
    }
  }
}
 
onSubmit() {
  this.filename=this.form.get('name').value
  this.datepicker=this.form.get('datepicker').value

  if (this.filename == null )
  {
    alert("Enter the file name");
  }
  if(this.imgsrc == null)
  {
    alert("Upload PDF file ");
  }
  if(this.datepicker == null)
  {
    alert("Please select the date ");
  }
  if(this.filename !==null && this.imgsrc !==null && this.datepicker !==null)
  {
    this.reportsSerevice.addFarmerReports(this.id,this.filename,this.imgsrc,this.datepicker).subscribe(x => {
    this.reportsSerevice.getReport(this.id).subscribe(x => { this.rowData=x.data
    console.log("data", x.data) 
    })});
    alert("File Uploaded Successfully")
    this.Empty();
  }
}
Empty(){
  this.imgsrc=null;
}
onBtnClick1(e) {

this.repId=e.reportId
this.reportsSerevice.deleteReport(this.repId).subscribe(x => {
this.reportsSerevice.getReport(this.id).subscribe(x => {
  console.log("xskcx",x)
  this.rowData=x.data});});
}
onBtnClick2(e) {
  
  this.repUrl=e.url
  console.log("url",this.repUrl);
  if (this.repUrl == null){
    alert("PDF was not uploaded for this report  ");
  }
  else
    window.open(this.repUrl, "_blank");
  }

_handleReaderLoaded(readerEvt) {
  var binaryString = readerEvt.target.result;
  this.base64textString = btoa(binaryString);
  this.imgsrc = this.base64textString;
}
  goBack() {
    this.location.back();
  }
}

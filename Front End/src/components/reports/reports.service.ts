import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { AppConfig } from '../../constants/app-config';

import { Observable, of, observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Province} from '../../models/province';
import { ToastrService } from 'ngx-toastr';
import { District } from '../../models/district';
import { Reports } from './reports';
@Injectable({
  providedIn: 'root'
})
 

export class ReportsService  
{
  constructor(private http: HttpClient,private ts: ToastrService) 
  { 
  }
addFarmerReports (FarmerId,reportName,Pdf,reportDate): Observable<Reports>  
{
 var body = {
    
    FarmerId:FarmerId,
    reportName:reportName,
    pdf: Pdf,
    reportDate:reportDate
    
    }
    console.log("dateeee",reportDate)
    return this.http.post<Reports>(`${AppConfig.URL_Reports}`, body);
  
}
getReport(id): Observable<any> {
 
  return this.http.get<Reports>(`${AppConfig.URL_GetReports}${id}`);
}
deleteReport(id){

  return this.http.delete<Reports>(`${AppConfig.URL_DeleteReport}${id}`);
}
}

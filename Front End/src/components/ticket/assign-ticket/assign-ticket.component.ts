import { Component, OnInit } from '@angular/core';
import {TicketType} from '../../../models/ticketTypes';
import  {TicketService} from '../ticket.service';
import { Router,ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { Ticket } from '../ticket';
import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-assign-ticket',
  templateUrl: './assign-ticket.component.html',
  styleUrls: ['./assign-ticket.component.css']
})
export class AssignTicketComponent implements OnInit {

 private ticketTypes : TicketType  [];
  public ticketValue = 0;
  public visitDate;
  public cnic;
  public phone;
  public description;
  public tsos : any =[];
  public tso : any ={};
  public FarmLocationData : any ={};

  ticketInformation: boolean = false;
  farmAssign: boolean = false;
  ticket : Ticket = new Ticket();

  constructor( private ts: ToastrService,private location :Location,private ticketsService : TicketService, private router: Router,private route : ActivatedRoute) { }

  ngOnInit() {
  this.getTicket();
  this.getTso();
  }

    getTicket(): void {
      const id = +this.route.snapshot.paramMap.get('id');
      id && this.ticketsService.getTicket(id)
      .subscribe(ticketRes => {
        ticketRes ? this.ticket = ticketRes.data : null ; 
        ticketRes && ticketRes.FarmLocationData ?  this.FarmLocationData = ticketRes.FarmLocationData : null;
        ticketRes && ticketRes.assignedTo ? this.tso= ticketRes.assignedTo : null ;
        console.log("Ticket",this.ticket)
      }  )   
    }

    getTso(){
      this.ticketsService.getTsoAgents('').subscribe(tSo=>{
          this.tsos = tSo;
      }) 
    }

    selectOption(e){
      let selecetdObject=this.tsos.filter(t => {return t.tsoid == e });
      selecetdObject && selecetdObject[0] ? this.tso = selecetdObject[0] : null;
    }

  onChange() { 
    if(this.ticketValue!=null && (this.visitDate!=null || this.visitDate!=undefined)) {
      this.ticketInformation = true;
    }
  }

  farmAssignTo() {
    if(this.cnic!=null && this.phone!=null && this.description!=null) {
      this.farmAssign = true;
    }
  }

  assignTicket(){
    if(this.ticket.assignedTo!=null)
  {
    console.log("Ticket",this.ticket)
    // this.ticket["statusId"]=2;
    this.ticketsService.assignTicket(this.ticket)
    .subscribe(ticketD => {
      console.log("t",ticketD);
    this.router.navigate(['tickets-detail/'+this.ticket.ticketId])
    //this.router.navigate(['tickets-detail/'+ticketD.ticketId])
      // this.ticket=dealerD;
      // this.dealer && this.dealer.dealerId && this.router.navigate(['dealers/'+dealerD.dealerId])
      // this.ts.success("Operation Performed Successfully");
    })
  }
  else{
    this.ts.error("TSO is not Selected");
  }
  }

  goBack()
  {
    this.location.back();
  }
  

}

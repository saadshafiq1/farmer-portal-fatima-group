import { Component, OnInit } from '@angular/core';
import { TicketType } from '../../../models/ticketTypes';
import { Ticket } from '../ticket';
import { Farms } from '../../farmer/farms';
import { Farmers } from '../../farmer/farmers';

import { TicketService } from '../ticket.service';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Location } from '@angular/common';
@Component({
  selector: 'app-create-tickets',
  templateUrl: './create-tickets.component.html',
  styleUrls: ['./create-tickets.component.css']
})
export class CreateTicketsComponent implements OnInit {
  public ticketTypes: TicketType[];
  form: FormGroup;
  myDate = new Date();
  ticket: Ticket = new Ticket();
  farms: Farms[];
  farmer: Farmers;
  CreatedBy;
  public ticketValue;
  public visitDate;
  public cnic;
  public phone;
  data;
  public description;

  ticketInformation: boolean = false;
  farmerInfoProfile: boolean = false;
  constructor(private location: Location, private route: ActivatedRoute, fb: FormBuilder, private ticketsService: TicketService, private router: Router) {


    // this.form = fb.group({
    //   "dealerName": ["", ],
    //   "cnic": ["", ],
    //   "ntn": ["", ],
    //   "strn": ["", ],
    //   "dealerCategory": ["", ],
    //   "dealerStatus": ["", ],
    //   "taxProfileDate": ["", ],
    //   "currentStatus": ["",],
    //   "ccnumber": ["", ],
    //   "sherpCc": ["", ],
    //   "proprieter": ["", ],
    //   "address": ["", ],
    //   "province": ["", ],
    //   "districtCode": ["", ],
    //   "cityVillage": ["", ],
    //   "tehsilCodeNavigation": ["", ],
    //   "saleRegion": ["", ],
    //   "salePoint": ["", ],
    //   "latitude": ["", ],
    //   "longitude": ["", ],
    //   "cellPhone": ["", ],
    //   "landline": ["", ],
    //   "email": ["", ]
    // })

  }

  ngOnInit() {
    this.ticketsService.getTicketType().pipe(first()).subscribe(data => {
      this.ticketTypes = data;
      this.ticketsService.getUserProfile().subscribe(x => {
      
        this.data=x.data;
        this.CreatedBy=this.data.callCenterAgentId
     
      })

    });

  }

  onChange() {
    if (this.ticketValue != null && (this.visitDate != null || this.visitDate != undefined)) {
      this.ticketInformation = true;
    }
  }

  save(): void {
    console.log(this.ticket);
    let TicketObj = this.ticket;
    delete TicketObj.farmer;
    TicketObj.createdBy=this.CreatedBy;
    console.log("ticket", this.ticket);
    console.log("obj", this.ticket);
    this.ticketsService.addTicket(TicketObj)
      .subscribe(ticketD => {
        console.log("succ", ticketD)
        ticketD && this.router.navigate(['assign-ticket/' + ticketD.ticketId])
        // this.ts.success("Operation Performed Successfully");
      });
    // this.location.back();
  }


  getFarmerDetail(key?: string, value?: string) {
    if (key && value) {

      let params = '?' + key + '=' + value ;

      console.log(params)
      // phone=923111111111

      this.ticketsService.getExactFarmer(params).pipe(first()).subscribe(data => {
        if (data && data[0]) {
          this.farmer = data[0];
          console.log("farmer", this.farmer)
          this.ticket.farmerId = data[0].farmerId;
          this.ticketsService.getfarmerFarms(this.ticket.farmerId).pipe(first()).subscribe(farmsArray => {
            this.farms = farmsArray;
            console.log("farms", this.farms)
          })
        }
        else {
          this.ticket.farmId =null;
          this.ticket.farmer = null;
          this.farms = [];
        }
        console.log("t", this.ticket)
      })
    }

  }

  farmSelected(id) {
    this.ticket.farmId = id;
    console.log(this.ticket)
  }

  farmersInfoProfile() {
    if (this.cnic != null && this.phone != null && this.description != null) {
      this.farmerInfoProfile = true;
    }
  }
  goBack() {
    this.location.back();
  }

}

import { Component, OnInit } from '@angular/core';
import { MockData } from '../../../constants/mock-data';
import { TicketService } from '../ticket.service';
import { Ticket } from '../ticket';
import { Province } from '../../../models/province';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { TicketStatus } from '../../../models/ticketstatus';
import { TSO } from '../../../models/tso';
@Component({
    selector: 'app-list-ticket',
    templateUrl: './list-ticket.component.html',
    styleUrls: ['./list-ticket.component.css']
})
export class ListTicketComponent implements OnInit {
    workFlowViewcolumnDefs = [];
    workFlowViewrowData = [];
    tickets: any[];
    ticketstatus: TicketStatus[];
    selectedValue;
    searchvalue;
    selectedTSO: string = "0";
    tso: TSO[];
    public breadcrumbUrl;
    allFlag;
    provinceFlag;
    categories: any[] = [{ label: 'Ticket Id', value: 'ticketId' }, { label: 'Farmer Cnic', value: 'cnic' }, { label: 'Farmer Cell Phone', value: 'Phone' }];
    provinces: Province[];
    private searchedValue: string = "";
    private params: string;
    private currentPage: number = 0;
    currentpage;
    private searchedcat: string;
    private pages: number = 1;
    public recordsPerPage: number = 15;
    public totalrecords: number = 0;
    totalrec: number;
    showBoundaryLinks = true;
    constructor(public ticketsService: TicketService, private router: Router) {
    }

    ngOnInit() {
        this.selectedValue = 'ticketId';
        //this.selectedValue = 'Select Category';
        this.ticketsService.getTickets('pageNumber=' + this.currentPage + '&pageSize=' + this.recordsPerPage).pipe(first()).subscribe(data => {
            this.tickets = data['data'];
            this.totalrecords = data['totalCount'];
            this.totalrec = data['totalCount'];
            console.log("avsvb")
            this.getWfGridData();
        });
        this.ticketsService.getTicketsStatus().pipe(first()).subscribe(data => {
            this.ticketstatus = data;
            console.log("ticketstatus",this.ticketstatus);
        });
        this.ticketsService.getTSO().pipe(first()).subscribe(data => {
            console.log("TSO Data", data)
            this.tso = data;
        });
    }

    delete(ticket: Ticket): void {
        // this.tickets = this.tickets.filter(h => h !== ticket);
        // this.ticketsService.deleteTicket(4).subscribe();
    }
    addedit(id) {
        // this.router.navigate(['/assign-ticket']);
        this.router.navigate(['/create-tickets']);

    }
    pageChanged(event: any) {
        //this.toastr.success('Hello world!', 'Page Changed');
        this.pages = event.page;
        this.pages = this.pages - 1;
        if (this.searchedValue == "") {
            console.log("sdbkvalue",this.searchedValue)
            this.params = 'pageNumber=' + this.pages + '&pageSize=' + this.recordsPerPage;
        }
        else if (this.searchedcat == "tso")
        {
            this.params = 'pageNumber=' + this.pages + '&pageSize=' + this.recordsPerPage + '&tsoID='+ this.searchedValue;
        }
        else {
            this.params = 'pageNumber=' + this.pages + '&pageSize=' + this.recordsPerPage + '&filter=' + this.searchedcat + '&value=' + this.searchedValue;
        }
        if (this.searchedcat != "tso")
        {
            
            this.ticketsService.getTickets(this.params).pipe(first()).subscribe(data => {
                this.tickets = data['data'];
                this.getWfGridData();
            });
        }
        else{
            console.log("PARAMAAAS", this.params)
            this.ticketsService.getTicketsById(this.params).pipe(first()).subscribe(data => {
                console.log("sakjhvb",data);
                this.tickets = data['data'];
                this.getWfGridData();
                
            });
        }
        
    }
    search(searchcat: string, searchval: string) {
        console.log("searchcat",searchcat);
        console.log("searchval",searchval);
        this.searchedcat = searchcat;
        this.searchedValue = searchval;
        console.log(this.searchedValue);
        if (searchcat == "status") {
            this.searchvalue = "";
        }
        if (this.searchedValue == "") {

            this.params = 'pageNumber=' + this.currentPage + '&pageSize=' + this.recordsPerPage;
            this.allFlag = 1;
            this.provinceFlag = 0;
            this.breadcrumbUrl = 'All';
        }
        else {
            if (searchcat == "tso")
            {
                this.params = 'pageNumber=' + this.currentPage + '&pageSize=' + this.recordsPerPage + '&tsoID='+ this.searchedValue;
                this.allFlag = 0;
                this.provinceFlag = 1;
                this.breadcrumbUrl = searchval;
            }
            else
            {
                this.params = 'pageNumber=' + this.currentPage + '&pageSize=' + this.recordsPerPage + '&filter=' + this.searchedcat + '&value=' + this.searchedValue;
                this.allFlag = 0;
                this.provinceFlag = 1;
                this.breadcrumbUrl = searchval;
            }
            
        }
        if (searchcat == "tso")
        {
            console.log("PARAMAAAS", this.params)
            this.ticketsService.getTicketsById(this.params).pipe(first()).subscribe(data => {
                console.log("sakjhvb",data);
                this.tickets = data['data'];
                this.getWfGridData();
                
            });
        }
        else
        {
            this.ticketsService.getTickets(this.params).pipe(first()).subscribe(data => {
                this.tickets = data['data'];
                this.totalrecords = data['totalCount'];
                console.log(this.totalrecords);
                this.getWfGridData();
            });
        }
        
        this.currentpage = 1;
    }

    getWfGridData() {
        const colDef: any[] = MockData.agGridColumnDefs_Tickets;
        this.workFlowViewcolumnDefs = colDef;
        this.workFlowViewrowData = this.tickets;
    }
    ResetVal(val) {
        if (val == 'Phone') {
            this.searchvalue = "92";
        }
        else {
            this.searchvalue = "";
        }
    }
     onGridReady(params) {
        params.api.sizeColumnsToFit();
    }
}

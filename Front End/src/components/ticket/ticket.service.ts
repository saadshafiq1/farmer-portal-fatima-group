import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from '../../constants/app-config';
import { Ticket } from './ticket';
import { Farms } from '../farmer/farms';
import { Province } from '../../models/province';
import { Farmers } from '../farmer/farmers';
import { TicketType } from '../../models/ticketTypes';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { TSO } from '../../models/tso';

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  constructor(private http: HttpClient, private ts: ToastrService) {
  }

  addTicket(ticket: Ticket): Observable<Ticket> {
    debugger
    return this.http.post<Ticket>(`${AppConfig.URL_Tickets}`, ticket).pipe(
      map(x => x["data"]),
      tap((newTicket: Ticket) => console.log(`added ticket w/ id=${newTicket.ticketId}`)),
      catchError(this.handleError<Ticket>('addTicket'))
    );
  }


  getTicketsStatus() {
    return this.http.get(`${AppConfig.URL_Tickets}StatusTypes`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`getTicketsStatus`)),
      catchError(this.handleError<any>('getTicketsStatus'))
    );
  }

  assignTicket(ticket: Ticket): Observable<Ticket> {
    return this.http.put<Ticket>(`${AppConfig.URL_Tickets}${ticket["ticketId"]}`, ticket).pipe(
      map(x => x["data"]),
      tap((newTicket: Ticket) => console.log(`added ticket w/ id=${newTicket.ticketId}`)),
      catchError(this.handleError<Ticket>('addTicket'))
    );
  }

  getTickets(params: string) {
    console.log("params", params)
    return this.http.get<Ticket[]>(`${AppConfig.URL_Tickets}GetAllTickets?` + params)
      .pipe(
      map(x => x["data"]),
      tap(_ => console.log('fetched tickets')),
      catchError(this.handleError('getTickets', []))
      );
  }

  getTicketsById(params: string) {
    return this.http.get<Ticket[]>(`${AppConfig.URL_Tickets}GetAllTicketsByTSOIdPortal?` + params)
      .pipe(
      map(x => x["data"]),
      tap(_ => console.log('fetched tickets')),
      catchError(this.handleError('getTickets', []))
      );
  }
  getTicket(id) {
    return this.http.get<Ticket>(`${AppConfig.URL_Tickets}${id}`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`updated ticket id=${id}`)),
      catchError(this.handleError<any>('getticket'))
    );
  }

  getprovinces() {
    return this.http.get<Province>(`${AppConfig.URL_Lookup}Province`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`get filters`)),
      catchError(this.handleError<any>('get filters'))
    );
  }
  getExactFarmer(params) {
    return this.http.get<Farmers[]>(`${AppConfig.URL_Farmers}/GetFarmerByFilter` + params).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`get filters`)),
      catchError(this.handleError<any>('get filters'))
    );
  }

  getTsoAgents(params) {
    return this.http.get<any[]>(`${AppConfig.URL_TSO}`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`get filters`)),
      catchError(this.handleError<any>('get filters'))
    );
  }
  getTSO() {
    return this.http.get<TSO>(`${AppConfig.URL_AppBase}ticket/GetTSOAgents`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`getTSO`)),
      catchError(this.handleError<any>('getTSO'))
    );
  }
  getfarmerFarms(params) {
    return this.http.get<Farms[]>(`${AppConfig.URL_Farms}/FarmsOfFarmerWeb/` + params).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`get filters`)),
      catchError(this.handleError<any>('get filters'))
    );
  }

  getTicketType() {
    return this.http.get<Ticket[]>(`${AppConfig.URL_Tickets}TicketTypes`).pipe(
      map(x => x["data"]),
      tap(_ => console.log(`get filters`)),
      catchError(this.handleError<any>('get filters'))
    );
  }

  deleteTicket(ticket: Ticket | number): Observable<Ticket> {
    const id = typeof ticket === 'number' ? ticket : ticket.ticketId;
    const url = `${AppConfig.URL_Tickets}${id}`;

    return this.http.delete<Ticket>(url).pipe(
      map(x => x["data"]),
      tap(_ => this.log(`deleted ticket id=${id}`)),
      catchError(this.handleError<Ticket>('deleteTicket'))
    );
  }

  updateTicket(ticket: Ticket): Observable<any> {
    return this.http.put(`${AppConfig.URL_Tickets}${ticket["ticketId"]}`, ticket).pipe(
      tap(_ => this.log(`updated ticket id=${ticket["ticketId"]}`)),
      catchError(this.handleError<any>('updateticket'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this.log(`${operation} failed: ${error.message}`);
      this.ts.error("Failed to Perform Operation");
      return of(result as T);
    };
  }

  private log(message: string) {
    this.ts.success("Operation Performed Successfully");
  }
replyOnTicket (TicketId,Description ,FarmerId ,AgentId): Observable<Comment>  
{
 var body = {
    TicketId:TicketId,
    Description:Description,
    FarmerId:FarmerId,
    AgentId:AgentId
    }
    console.log("URL",AppConfig.URL_Comment);
    return this.http.post<Comment>(`${AppConfig.URL_Comment}`, body);
   
}
getUserProfile(): Observable<Comment> {
 
  return this.http.get<Comment>(`${AppConfig.URL_Profile}`);
}
 
}


import { Injectable } from '@angular/core';
import { Farmers } from '.././farmer/farmers';
import { TicketType } from '../../models/ticketTypes';
import { TicketStatus } from '../../models/ticketstatus';

export class Ticket {
      "ticketId"?: number;
      "ticketTypeId"?: number;
      "visitDate"?: Date;
      "visitTime"?: Date;
      "description"?: string;
      "statusId"?: number;
      "statusText"?: string;
      "slastatus"?: string;
      "feedback"?: number;
      "feedbackComments"?: string;
      "feedbackAudioUrl"?: string;
      "farmerId"?: number;
      "farmId"?: number;
      "assignedTo"?: number;
      "createdBy"?: number;
      "resolvedDate"?: Date;
      "createdDate"?: Date;
      "updatedDate"?: Date;
      "modifiedDate"?: Date;
      "archive"?: boolean;
      "deleted"?: boolean;
      "farmer"?: Farmers;
      "dueDate"?: Date;
      "ticketType"?: TicketType;
      "status"?: TicketStatus;
}

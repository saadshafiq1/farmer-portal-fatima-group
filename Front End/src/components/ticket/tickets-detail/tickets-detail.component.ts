import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TicketService } from '../ticket.service';
import { TicketType } from '../../../models/ticketTypes';
import { first } from 'rxjs/operators';
import { Ticket } from '../ticket';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-tickets-detail',
  templateUrl: './tickets-detail.component.html',
  styleUrls: ['./tickets-detail.component.css']
})
export class TicketsDetailComponent implements OnInit {
  public assignedTo: any = {};
  public FarmLocationData: any = {};
  public createdBy: any = {};
  ticket: Ticket = new Ticket();
  public reply: string = '';
   id:number;
   FarmerId:number;
   Description:string;
   AgentId:number;
   readonly:boolean=false;
   form: FormGroup;
   data:any;
   AgentName:string;
   TicketActivity:string;
  constructor(private ticketsService: TicketService, private location: Location, private router: Router, private route: ActivatedRoute, fb: FormBuilder,) { 
    this.form = fb.group({
      "commentbox": ["", Validators.required], })
  }
  get f() { return this.form.controls; }

  ngOnInit() {
    

    this.getTicket();
    // this.form = new FormGroup({
    //   commentbox : new FormControl() , 
      
    // })
  
    this.ticketsService.getUserProfile().subscribe(x => {
      
      this.data=x.data;
      this.AgentId=this.data.callCenterAgentId
      this.AgentName=this.data.agentName
      console.log("agentID",this.AgentName);
    })
  }


  goBack(): void {
    //this.location.back();
    this.router.navigate(['tickets']);
  }

  getTicket(): void {
      this.route.queryParams
      .filter(params => params.view)
      .subscribe(params => {
          this.readonly=params.view;
      });
     this.id = +this.route.snapshot.paramMap.get('id');
     console.log("id",this.id);
    this.id && this.ticketsService.getTicket(this.id)
      .subscribe(ticketRes => {
        ticketRes ? this.ticket = ticketRes.data : null; 
        ticketRes.FarmLocationData ? this.FarmLocationData = ticketRes.FarmLocationData : null;
        ticketRes.assignedTo ? this.assignedTo = ticketRes.assignedTo : null;
        ticketRes.createdBy ? this.createdBy = ticketRes.farmer : null;
        this.FarmerId=ticketRes.data.farmer.farmerId;
        this.TicketActivity=ticketRes.data.tbTicketActivity;
        

      })
  }
  onSubmit() {
    
    this.Description=this.form.get('commentbox').value
    
    if(this.Description== null){
      alert("Enter a Comment")
    }
    if(this.Description!= null){
      
    console.log("action")
  
    this.ticketsService.replyOnTicket(this.id,this.Description,this.FarmerId,this.AgentId).subscribe(x => {
       this.id && this.ticketsService.getTicket(this.id)
        .subscribe(ticketRes => { this.TicketActivity=ticketRes.data.tbTicketActivity;
        
      })
    
    })
  }
   
  }


  updateStatus(statusID) {
    
  
    this.ticket["statusId"] = statusID;
    this.ticketsService.assignTicket(this.ticket)
      .subscribe(ticketD => {
        console.log("t", ticketD)
        this.ticket = ticketD;
       
      })
    
  }

  replyConvo() {

  }

}

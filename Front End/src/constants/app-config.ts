import { environment } from '../environments/environment';

export class AppConfig {
    public static URL_AppBase: string = environment.appRoot;
    public static URL_Dealers: string = AppConfig.URL_AppBase + 'Dealer/';
    public static URL_Farmers: string = AppConfig.URL_AppBase + 'Farmer/';
    public static URL_Lookup: string = AppConfig.URL_AppBase + 'lookup/';
    public static URL_Tickets: string = AppConfig.URL_AppBase + 'Ticket/';
    public static URL_Farms: string = AppConfig.URL_AppBase + 'Farm/';
    public static URL_Crops: string = AppConfig.URL_AppBase + 'Crop/';
    public static URL_News: string = AppConfig.URL_AppBase + 'Media/';
    public static URL_Reports: string = AppConfig.URL_AppBase + 'Farmer/AddFarmerReport';
    public static URL_GetReports: string = AppConfig.URL_AppBase + 'Farmer/GetFarmerReportByFarmerID/';
    public static URL_DeleteReport: string = AppConfig.URL_AppBase + 'Farmer/DeleteFarmerReport/';
    public static URL_TSO: string = AppConfig.URL_AppBase + 'tso/';
    public static URL_Regions: string = AppConfig.URL_AppBase + 'Regions/';
    public static URL_Districts: string = AppConfig.URL_AppBase + 'Districts/';
    public static URL_Comment: string = AppConfig.URL_AppBase + 'ticket/ReplyOnTicket';
    public static URL_Profile: string = AppConfig.URL_AppBase + 'User/UserProfile';

}

import { Router } from '@angular/router';
import * as moment from 'moment';
const Status_TEMPLATE = `
    <div class="dealer-status">
        <i title='Closed' class="icon-Complete-Done"></i>
        <i title='In Progress' class="icon-In-Progress-Default"></i>
    </div>`;

export class MockData {
    onclick() {
        alert('Working');
        console.log("View");
    }
    static agGridColumnDefs_Farmers: any[] = [
        { headerName: ' ', field: '', width: 60, checkboxSelection: true, headerCheckboxSelection: true },
        { headerName: 'Farmer', field: 'farmerName', width: 150, sortable: true, sort: "asc" },
        { headerName: 'CNIC #', field: 'cnic', width: 120, },
        { headerName: 'Father / Husband Name', field: 'fatherHusbandName', width: 200, },
        { headerName: 'Date of Birth', field: 'dateOfBirth', width: 120, },
        { headerName: 'Phone #', field: 'cellPhone', width: 110, },
        { headerName: 'Farmer Status', field: 'farmerStatus.farmerStatueName', width: 130, },
        { headerName: 'Area (Acres)', field: 'unionCouncil', width: 120, },
        { headerName: 'Status', field: 'activeStatus', width: 100, template: Status_TEMPLATE },
        { headerName: 'Action', field: '', width: 150, cellRenderer: 'AgGridActionsRenderer' }
    ];
    static agGridColumnDefs_Tickets: any[] = [
        { headerName: ' ', field: '', width: 60, checkboxSelection: true, headerCheckboxSelection: true },
        { headerName: 'Ticket ID', field: 'data.ticketId', width: 100, sortable: true },
        { headerName: 'Ticket Type', field: 'data.ticketType.ticketTypeName', width: 170,sortable: true },
        { headerName: 'Created Date', field: 'data.createdDate', width: 170, sort: "desc", valueFormatter: function (params) {
            return (params.value != null? moment(params.value).format('YYYY-MM-DD HH:mm:ss'): '')
        }},
        { headerName: 'Resolved Date', field: 'data.resolvedDate', width: 170, cellRenderer: (data) => {
            return (data.value != null? moment(data.value).format('YYYY-MM-DD HH:mm:ss'): '')
        } },
        { headerName: 'SLA Status', field: 'data.slastatus', width: 110, },
        { headerName: 'Created By', field: 'CreatedByName', width: 110, },
        { headerName: 'Status', field: 'data.activeStatus', width: 120, cellRenderer: 'AgStatusActionsRenderer'},
        { headerName: 'Action', field: '', width: 120, cellRenderer: 'AgGridActionsRenderer' },
        { headerName: 'Assign', field: '', width: 120, cellRenderer: 'AgGridAdditionalActionsRenderer' }
    ];
    static agGridColumnDefs_FarmerTickets: any[] = [
        { headerName: ' ', field: '', width: 60, checkboxSelection: true, headerCheckboxSelection: true },
        { headerName: 'Ticket ID', field: 'ticketId', width: 100, sortable: true },
        { headerName: 'Ticket Type', field: 'ticketTypeName', width: 200, },
        { headerName: 'Created By', field: 'createdByName', width: 120, },
        { headerName: 'Due Date', field: 'dueDate', width: 150, sort: "desc" },
        { headerName: 'Resolved Date', field: 'resolvedDate', width: 150, },
        { headerName: 'Ticket Source', field: 'ticketSource', width: 120, },
        { headerName: 'SLA Status', field: 'slastatus', width: 120, },
        { headerName: 'Ticket Status', field: 'statusName', width: 100, },

    ];

    static agGridColumnDefs_News: any[] = [
        { headerName: ' ', field: '', width: 60, checkboxSelection: true, headerCheckboxSelection: true },
        { headerName: 'District', field: 'districtCodeNavigation.districtName', width: 120, sortable: true },
        { headerName: 'News Title', field: 'header', width: 200, },
        {
            headerName: 'News Body', field: 'body', cellRenderer: (data) => {
                return data.value;
            }, width: 300,
        },
        {
            headerName: 'Created Date', field: 'insertionDate', cellRenderer: (data) => {
                return data.value ? (new Date(data.value)).toLocaleDateString() : '';
            }, width: 200,
        },
        {
            headerName: 'Modified Date', field: 'modifiedDateTime',  cellRenderer: (data) => {
                return data.value ? (new Date(data.value)).toLocaleDateString() : '';
            }, width: 200,
        },
       
        {
            headerName: 'Status', field: 'activeStatus', cellRenderer: (data) => {
                if (data.value == 'A') return 'Active'; else return 'Inactive';
            }, width: 100,
        },
        //{ headerName: 'Modified By', field: 'modifiedBy', width: 100, },
        { headerName: 'Action', field: '', width: 150, cellRenderer: 'AgGridActionsRenderer' }

    ];

    static agGridColumnDefs_Dealers: any[] = [
        { headerName: ' ', field: '', width: 60, checkboxSelection: true, headerCheckboxSelection: true },
        { headerName: 'Dealer', field: 'dealerNameUrdu', width: 120, sort: "asc" },
        { headerName: 'Address', field: 'address', width: 250, },
        { headerName: 'Phone #', field: 'cellPhone', width: 100, },
        { headerName: 'CNIC #', field: 'cnic', width: 120, },
        { headerName: 'Dealer Status', field: 'dealerStatusNavigation.status', width: 125, },
        { headerName: 'District', field: 'districtCodeNavigation.districtNameUrdu', width: 100, },
        { headerName: 'Tehsil', field: 'tehsilCodeNavigation.tehsilNameUrdu', width: 100, },
        { headerName: 'Status', field: 'activeStatus', width: 100, template: Status_TEMPLATE },
        { headerName: 'Action', field: '', width: 150, cellRenderer: 'AgGridActionsRenderer' }
    ];


}

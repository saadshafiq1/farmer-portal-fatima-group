import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service'
@Injectable({
    providedIn: 'root'
})
export class AppMasterGuard implements CanActivate {
    constructor(
        // private myRoute: Router
        private router: Router,
        private authenticationService: AuthService) {
    }
    // canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    //  let url: string = state.url;  
    //     return this.verifyLogin(url);
    // }
    //   verifyLogin(url) : boolean{
    //     if(!this.isLoggedIn()){
    //         this.myRoute.navigate(['/login']);
    //         return false;
    //     }
    //     else if(this.isLoggedIn()){
    //         return true;
    //     }
    // }
    //   public isLoggedIn(): boolean{
    //     let status = false;
    //     if( localStorage.getItem('isLoggedIn') == "true"){
    //       status = true;
    //     }
    //     else{
    //       status = false;
    //     }
    //     return status;
    // }


    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authenticationService.currentUserValue;
        if (currentUser) {
            // logged in so return true
            return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
        return false;
    }
}

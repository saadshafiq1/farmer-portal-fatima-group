export class TicketStatus {
    statusId: number;
    statusName: string;
    statusDescription: string;
    count: number;
}
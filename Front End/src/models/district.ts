export class District {
    districtCode: string;
    districtName: string;
    districtNameUrdu: string;
}
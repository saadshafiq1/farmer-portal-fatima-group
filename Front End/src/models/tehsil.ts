export class Tehsil {
    tehsilCode: string;
    tehsilName: string;
    tehsilNameUrdu: string;
}
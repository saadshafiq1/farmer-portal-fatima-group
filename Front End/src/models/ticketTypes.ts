export class TicketType {
    "ticketTypeId": number;
    "ticketTypeName": string;
    "ticketTypeDescription": string;
    "createdDate": string;
    "modifiedDate": string;
    "status": boolean;
    "ticketTypeNameUrdu": string;
    "tbTicketInfo": any[]
}
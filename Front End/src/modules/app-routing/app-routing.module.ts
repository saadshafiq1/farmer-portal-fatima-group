import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '../../components/login/login.component';
import { ForgetPasswordComponent } from '../../components/forget-password/forget-password.component';
import { FarmersDetailComponent } from '../../components/farmer/farmers-detail/farmers-detail.component';
import { ListDealerComponent } from '../../components/dealer/list-dealer/list-dealer.component';
import { AppMasterGuard } from '../../guards/app-master.guard';
import { AddEditDealerComponent } from '../../components/dealer/add-edit-dealer/add-edit-dealer.component';
import { ListTicketComponent } from '../../components/ticket/list-ticket/list-ticket.component';
import { AddFarmsComponent } from '../../components/farmer/add-farms/add-farms.component';
import { NewsComponent } from '../../components/news/news.component';
import { CreateTicketsComponent } from '../../components/ticket/create-tickets/create-tickets.component';
import { AddFarmerComponent } from '../../components/farmer/add-farmer/add-farmer.component';
import { AddFarmerFarmsComponent } from '../../components/farmer/add-farmer-farms/add-farmer-farms.component';
import { FarmersContractComponent } from '../../components/farmer/farmers-contract/farmers-contract.component';
import { FarmersLoanComponent } from '../../components/farmer/farmers-loan/farmers-loan.component';
import { TicketsDetailComponent } from '../../components/ticket/tickets-detail/tickets-detail.component';
import { AssignTicketComponent } from '../../components/ticket/assign-ticket/assign-ticket.component';
import { FarmerListTicketComponent } from '../../components/farmer/farmers-ticket/farmer-list-ticket.component';
import { ListNewsComponent } from '../../components/news/list-news/list-news.component';
import { ReportsComponent } from 'src/components/reports/reports.component';
const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', component: ListTicketComponent, canActivate: [AppMasterGuard] },
  { path: 'forget-password', component: ForgetPasswordComponent },
  { path: 'farmers', component: FarmersDetailComponent, canActivate: [AppMasterGuard] },
  { path: 'dealers', component: ListDealerComponent, canActivate: [AppMasterGuard] },
  { path: 'dealers/:id', component: AddEditDealerComponent, canActivate: [AppMasterGuard] },
  { path: 'farmers-add-farms/:id', component: AddFarmsComponent },
  { path: 'tickets', component: ListTicketComponent, canActivate: [AppMasterGuard] },
  { path: 'news/:id', component: NewsComponent, canActivate: [AppMasterGuard] },
  { path: 'create-tickets', component: CreateTicketsComponent, canActivate: [AppMasterGuard] },
  { path: 'add-farmer/:id', component: AddFarmerComponent, canActivate: [AppMasterGuard] },
  { path: 'add-farmer-farm', component: AddFarmerFarmsComponent, canActivate: [AppMasterGuard] },
  { path: 'farmers-contract/:id', component: FarmersContractComponent, canActivate: [AppMasterGuard] },
  { path: 'farmers-loan/:id', component: FarmersLoanComponent, canActivate: [AppMasterGuard] },
  { path: 'farmer-list-ticket/:id', component: FarmerListTicketComponent, canActivate: [AppMasterGuard] },
  { path: 'tickets-detail/:id', component: TicketsDetailComponent, canActivate: [AppMasterGuard] },
  { path: 'assign-ticket/:id', component: AssignTicketComponent, canActivate: [AppMasterGuard] },
  { path: 'news', component: ListNewsComponent },
  { path: 'reports/:id', component: ReportsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],

})
export class AppRoutingModule { }
﻿import { Injectable } from '@angular/core';
import { Response, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from '../services/auth.service'
@Injectable()
export class BaseHttpService {
    public token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImltcmFuLmh1c3NhaW4iLCJyb2xlIjoiVFNPIiwibmJmIjoxNTUyNDg2NDYzLCJleHAiOjE1NTI1NzI4NjMsImlhdCI6MTU1MjQ4NjQ2M30.6nPoBED6RgLVTxoc8foiixdcGwy2okjjd0CY284UAB4";

    constructor(private http: HttpClient) { }
    getData(url: string): Observable<any> {
        let myheaders = new HttpHeaders();
        myheaders.set('Authorization', `Bearer ${this.token}`);
        myheaders.append('content-type', 'application/json');
        let options = ({ headers: myheaders });
        return this.http
            .get(url, options)
            .map(this.extractData)
            .catch(this.handleErrorObservable);
    }

    add(model, url: string): Observable<any> {
        const headers = new HttpHeaders({
            'Content-Type': 'application/json; charset=utf-8'
        });
        const options = { headers: headers };
        const body = JSON.stringify(model);
        return this.http
            .post(url, body, options)
            .map(this.extractData)
            .catch(this.handleErrorObservable);
    }

    deleteByBody(model, url: string): Observable<any> {
        const headers = new HttpHeaders({
            'Content-Type': 'application/json; charset=utf-8'
        });
        const options = { headers: headers };

        const body = JSON.parse(model);
        return this.http
            .post(url, body, options)
            .map(this.extractData)
            .catch(this.handleErrorObservable);
    }

    delete(url: string): Observable<any> {
        // const headers = new HttpHeaders({
        //     'Content-Type': 'application/json; charset=utf-8'
        // });
        // const options = { headers: headers };

        return this.http
            .delete(url)
            .map(this.extractData)
            .catch(this.handleErrorObservable);
    }

    edit(model, url: string): Observable<any> {
        const headers = new HttpHeaders({
            'Content-Type': 'application/json; charset=utf-8'
        });
        const options = { headers: headers };
        const body = JSON.stringify(model);
        return this.http
            .put(url, body, options)
            .map(this.extractData)
            .catch(this.handleErrorObservable);
    }

    editByParams(url: string): Observable<any> {
        const headers = new HttpHeaders({
            'Content-Type': 'application/json; charset=utf-8'
        });
        const options = { headers: headers };
        return this.http
            .put(url, '', options)
            .map(this.extractData)
            .catch(this.handleErrorObservable);
    }

    private extractData(res: Response) {
        const body = res;
        return body || {};
    }

    private handleErrorObservable(error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    }
}

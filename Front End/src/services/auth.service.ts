import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { RequestOptions, Headers } from '@angular/http';
import { Http, Response } from '@angular/http';
import { User } from '../models/user';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  private _loginURL = 'https://119.63.133.166:8181/api/user/authenticate';
  isLoggedIn = false;
  public token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImltcmFuLmh1c3NhaW4iLCJyb2xlIjoiVFNPIiwibmJmIjoxNTUyNDg2NDYzLCJleHAiOjE1NTI1NzI4NjMsImlhdCI6MTU1MjQ4NjQ2M30.6nPoBED6RgLVTxoc8foiixdcGwy2okjjd0CY284UAB4";
  constructor(private http: Http, private httpClient: HttpClient, private ts: ToastrService) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  authenticateLogin(login: User) {
   // localStorage.setItem('name', login.UserName);
    return this.httpClient.post<any>(`${this._loginURL}`, login, {})
      .pipe(map(user => {
        if (user && user.responseCode === 0) {
          localStorage.setItem('currentUser', JSON.stringify(user));

          this.currentUserSubject.next(user);
          this.ts.success("Logged In Successfully", '', { timeOut: 1000 });
          this.isLoggedIn = true;
        }
        else {
          this.ts.error("Failed to Logged In", '', { timeOut: 1000 });
        }
        return user;
      })

      );

  }

  errorHandler(error: Response) {
    console.log(error);
    return Observable.throw(error);
  }

  logout(): void {
    localStorage.removeItem('currentUser');
    // localStorage.removeItem('name');
    this.currentUserSubject.next(null);
    this.isLoggedIn = false;

  }

  getAuthorizationToken() {
    return JSON.parse(localStorage.getItem('currentUser'));
  }


}
import { Injectable, EventEmitter } from '@angular/core';    
import { Subscription } from 'rxjs/internal/Subscription';    
    
@Injectable({    
  providedIn: 'root'    
})    
export class EventEmitterService {    
    
  removeRowEmitter = new EventEmitter();    
  subsVar: Subscription;    

  constructor() { }
  removeRowFunction() {
    this.removeRowEmitter.emit();
  }
}
import { Component } from '@angular/core';
import { Router,Routes } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { User } from '../models/user';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'Fatima Group Web Portal';
  public breadcrumbUrl = 'Tickets';
  farmerFlag;
  dealerFlag;
  ticketFlag;
  newsFlag;
  dropDown: boolean = false;
  currentUser: User;
  //loginName:string;
  dropDownData = [
    {
      id: 1,
      name: 'ABC'
    },
    {
      id: 2,
      name: 'DEF'
    },
    {
      id: 3,
      name: 'GHI'
    }
  ];
  farmer() {
    this.farmerFlag = 1;
    this.dealerFlag = 0;
    this.breadcrumbUrl = 'Farmers';
  }

  dealers() {
    this.farmerFlag = 0;
    this.dealerFlag = 1;
    this.breadcrumbUrl = 'Dealers';
  }
  tickets() {
    this.farmerFlag = 0;
    this.dealerFlag = 0;
    this.ticketFlag = 1;
    this.breadcrumbUrl = 'Tickets';
  }

  news()
  {
    this.farmerFlag = 0;
    this.dealerFlag = 0;
    this.ticketFlag = 0;
    this.newsFlag=1;
    this.breadcrumbUrl = 'News';
  }

  openDropDown() {
    console.log("Open dropdown");
    this.dropDown = !this.dropDown;
    console.log("Drop Down value: ", this.dropDown);
  }

  constructor(private router: Router,private  auth: AuthService)
  {
    this.auth.currentUser.subscribe(x => this.currentUser = x);
   // this.loginName=null;
   /// this.loginName=localStorage.getItem('name');
  }

  ngOnInit() 
  {
    //console.log(this.route);
  }
   get isLoggedIn() 
  { 
    return this.auth.isLoggedIn;
  }
  logout(): void {
    console.log("Logout");
   this.auth.logout();
    this.router.navigate(['/login']);
  }
  
  
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AgActionsRenderer } from '../components/base/ag-grid-base/ag-grid-components/ag-action.renderer';
import { AgAdditionalActionsRenderer } from '../components/base/ag-grid-base/ag-grid-components/ag-additional-action.renderer';
import { AgStatusActionsRenderer } from '../components/base/ag-grid-base/ag-grid-components/ag-status.renderer';
import { AgActiveStatusActionsRenderer } from '../components/base/ag-grid-base/ag-grid-components/ag-activestatus.renderer';
import { AgGridCellSelectEditor } from '../components/base/ag-grid-base/ag-grid-components/select-list.editor';
import { AgGridCellSelectRenderer } from '../components/base/ag-grid-base/ag-grid-components/select-list.renderer';
import { AgGridBaseComponent } from '../components/base/ag-grid-base/ag-grid-base.component';
import { AgTicketActionRenderer } from '../components/base/ag-grid-base/ag-grid-components/ag-ticket-action.renerer';
import { AppAgGridModule } from '../modules/app-ag-grid/app-ag-grid.module';
import { AppRoutingModule } from '../modules/app-routing/app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from '../components/login/login.component';
import { ForgetPasswordComponent } from '../components/forget-password/forget-password.component';
import { FarmersDetailComponent } from '../components/farmer/farmers-detail/farmers-detail.component';
import { ReactiveFormsModule,FormControl,FormsModule } from '@angular/forms';
import { AppMasterGuard } from '../guards/app-master.guard';
import { HttpModule } from '@angular/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthService } from '../services/auth.service';
import {  BaseHttpService } from '../services/BaseHttpService';
import { JwtInterceptor, ErrorInterceptor } from '../_helpers';
import {ListDealerComponent}  from '../components/dealer/list-dealer/list-dealer.component';
import { AddEditDealerComponent } from '../components/dealer/add-edit-dealer/add-edit-dealer.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import {EventEmitterService} from '../services/event-emitter.service'
import { AddFarmsComponent } from '../components/farmer/add-farms/add-farms.component';
import { AddFarmerFarmsComponent } from '../components/farmer/add-farmer-farms/add-farmer-farms.component'
import { NewsComponent } from '../components/news/news.component';
import { CreateTicketsComponent } from '../components/ticket/create-tickets/create-tickets.component';
import { AddFarmerComponent } from '../components/farmer/add-farmer/add-farmer.component';
import { FarmersLoanComponent } from '../components/farmer/farmers-loan/farmers-loan.component';
import { FarmersContractComponent } from '../components/farmer/farmers-contract/farmers-contract.component'; 
import { TicketsDetailComponent } from '../components/ticket/tickets-detail/tickets-detail.component';
import { AssignTicketComponent } from '../components/ticket/assign-ticket/assign-ticket.component';
import {ListTicketComponent}  from '../components/ticket/list-ticket/list-ticket.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FarmerListTicketComponent } from '../components/farmer/farmers-ticket/farmer-list-ticket.component';
import {ListNewsComponent}  from '../components/news/list-news/list-news.component';
import { NgxEditorModule } from 'ngx-editor';
import {NgxMaskModule} from 'ngx-mask';
import { DatePipe } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReportsComponent } from '../components/reports/reports.component';
import { HttpClientModule } from '@angular/common/http';
import { AgGridModule } from 'ag-grid-angular';
import {MatDatepickerModule, MatInputModule,MatNativeDateModule} from '@angular/material';
import { ButtonRendererComponent } from 'src/components/base/ag-grid-base/ag-grid-components/button-renderer.component';
import { ButtonRenderer1Component } from 'src/components/base/ag-grid-base/ag-grid-components/button-renderer1.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ForgetPasswordComponent,
    FarmersDetailComponent,
    ListDealerComponent,
    AddEditDealerComponent,
    AgGridBaseComponent,
    AgGridCellSelectEditor,
    AgGridCellSelectRenderer,
    AgActionsRenderer,
    AgAdditionalActionsRenderer,
    AgStatusActionsRenderer,
    ButtonRendererComponent,
    ButtonRenderer1Component,
    AgTicketActionRenderer,
    AgActiveStatusActionsRenderer,
    AddFarmsComponent,
    ListTicketComponent,
    NewsComponent,
    CreateTicketsComponent,
    AddFarmerComponent,
    AddFarmerFarmsComponent,
    FarmersContractComponent,
    FarmersLoanComponent,
    TicketsDetailComponent,
    AssignTicketComponent,
    FarmerListTicketComponent,
    ListNewsComponent,
    ReportsComponent,
    
  ],
  imports: [
    AgGridModule.withComponents([ButtonRendererComponent,ButtonRenderer1Component]),
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    AppAgGridModule,
    PaginationModule.forRoot(),
    NgxEditorModule,
    NgxMaskModule.forRoot(),
    MatDatepickerModule, MatInputModule,MatNativeDateModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot({positionClass: 'toast-bottom-right'},),
    NgbModule.forRoot()
  ],
  providers: [BaseHttpService,DatePipe,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    AppMasterGuard,AuthService,EventEmitterService],

  entryComponents: [AgStatusActionsRenderer,AgGridCellSelectEditor, AgGridCellSelectRenderer,AgActionsRenderer,AgAdditionalActionsRenderer,AgTicketActionRenderer],
  bootstrap: [AppComponent]
})
export class AppModule { }
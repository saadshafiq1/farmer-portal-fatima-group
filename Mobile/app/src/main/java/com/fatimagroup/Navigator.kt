package com.fatimagroup

import android.content.Intent
import com.fatimagroup.base.BaseActivity
import com.fatimagroup.repository.LocalDataSource
import com.fatimagroup.repository.SharedPrefDataSource
import com.fatimagroup.ui.forgot_password.ForgotPasswordActivity
import com.fatimagroup.ui.home.MainActivity
import com.fatimagroup.ui.login.LoginActivity
import com.fatimagroup.ui.signup.SignupActivity
import javax.inject.Inject

/**
 * Created by hunain.liaquat.
 */
class Navigator @Inject constructor(
    var localDataSource: LocalDataSource,
    sharedPrefDataSource: SharedPrefDataSource
) {

    fun gotoMainActivity(activity: BaseActivity) {
        val intent = Intent(activity, MainActivity::class.java)
        startActivity(activity, intent)
    }

    fun gotoLoginActivity(activity: BaseActivity) {
        val intent = Intent(activity, LoginActivity::class.java)
        startActivity(activity, intent)
    }

    fun gotoSignupActivity(activity: BaseActivity) {
        val intent = Intent(activity, SignupActivity::class.java)
        startActivity(activity, intent)
    }

    fun gotoForgotPasswordActivity(activity: BaseActivity) {
        val intent = Intent(activity, ForgotPasswordActivity::class.java)
        startActivity(activity, intent)
    }

    private fun startActivity(activity: BaseActivity, intent: Intent) {
        activity.startActivity(intent)
    }

    private fun startActivityForResult(activity: BaseActivity, intent: Intent, requestCode: Int) {
        activity.startActivityForResult(intent, requestCode)
    }
}
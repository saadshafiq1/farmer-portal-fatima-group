package com.fatimagroup.base

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.fatimagroup.R
import com.fatimagroup.util.LANGUAGE_URDU
import io.github.inflationx.viewpump.ViewPumpContextWrapper
import java.util.*


open class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setLocale(Locale(LANGUAGE_URDU))
    }

    fun replaceFragment(fragment: BaseFragment, addToBackStack: Boolean, frameId: Int, tag: String) {
        if (supportFragmentManager != null) {
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            if (fragment != null)
                fragmentTransaction.replace(frameId, fragment, tag)
            if (addToBackStack)
                fragmentTransaction.addToBackStack(fragment!!.javaClass.simpleName)
            if (!isFinishing) {
                fragmentTransaction.commit()
            }

        }
    }

    private fun setLocale(locale: Locale) {
        val resources = resources
        val configuration = resources.configuration
        val displayMetrics = resources.displayMetrics
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(locale)
        } else {
            configuration.locale = locale
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            applicationContext.createConfigurationContext(configuration)
        } else {
            resources.updateConfiguration(configuration, displayMetrics)
        }
    }

    protected override fun attachBaseContext(base: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(updateBaseContextLocale(base)))
    }

    private fun updateBaseContextLocale(context: Context): Context {
        val language = LANGUAGE_URDU // Helper method to get saved language from SharedPreferences
        val locale = Locale(language)
        Locale.setDefault(locale)

        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            updateResourcesLocale(context, locale)
        } else updateResourcesLocaleLegacy(context, locale)

    }

    @TargetApi(Build.VERSION_CODES.N)
    private fun updateResourcesLocale(context: Context, locale: Locale): Context {
        val configuration = context.getResources().getConfiguration()
        configuration.setLocale(locale)
        return context.createConfigurationContext(configuration)
    }

    private fun updateResourcesLocaleLegacy(context: Context, locale: Locale): Context {
        val resources = context.getResources()
        val configuration = resources.getConfiguration()
        configuration.locale = locale
        resources.updateConfiguration(configuration, resources.getDisplayMetrics())
        return context
    }
}

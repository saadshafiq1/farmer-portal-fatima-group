package com.fatimagroup.base


import androidx.fragment.app.Fragment

open abstract class BaseFragment : Fragment() {

    open abstract fun initUI()


}

package com.fatimagroup.base

open class BaseResponse {

    open var responseCode: Int = 0
    open var responseMessage: String = ""
}
package com.fatimagroup.base

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.fatimagroup.R
import javax.inject.Inject

class FragmentBindingAdapters @Inject constructor(val fragment: Fragment) {
    @BindingAdapter("imageUrl")
    fun bindImage(imageView: ImageView, url: String?) {
        val requestOptions = RequestOptions().fallback(R.drawable.default_image).error(R.drawable.default_image)
            .placeholder(R.drawable.default_image).diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH)
        Glide.with(fragment).setDefaultRequestOptions(requestOptions).load(url).into(imageView)
    }
}
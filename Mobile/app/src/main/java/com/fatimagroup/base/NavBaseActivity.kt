package com.fatimagroup.base

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import com.fatimagroup.R
import com.fatimagroup.databinding.ActivityNavBaseBinding
import com.fatimagroup.ui.weather.WeatherActivity
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_nav_base.*


abstract class NavBaseActivity : BaseActivity() {

    private lateinit var activityNavBaseBinding: ActivityNavBaseBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityNavBaseBinding = DataBindingUtil.setContentView(this, R.layout.activity_nav_base)
//        setContentView(R.layout.activity_nav_base)
//        setToolbarDrawer(activityNavBaseBinding.toolbar)

    }




    /* Override all setContentView methods to put the content view to the FrameLayout view_stub
    * so that, we can make other activity implementations looks like normal activity subclasses.
    */
    override fun setContentView(layoutResID: Int) {
        if (this::activityNavBaseBinding.isInitialized) {
            val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val lp = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            val stubView = inflater.inflate(layoutResID, activityNavBaseBinding.viewStub, false)
            activityNavBaseBinding.viewStub.addView(stubView, lp)
        } else {
            super.setContentView(layoutResID)
        }
    }

    override fun setContentView(view: View) {
        if (this::activityNavBaseBinding.isInitialized) {
            val lp = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            activityNavBaseBinding.viewStub.addView(view, lp)
        }
    }

    override fun setContentView(view: View, params: ViewGroup.LayoutParams) {
        if (this::activityNavBaseBinding.isInitialized) {
            activityNavBaseBinding.viewStub.addView(view, params)
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
//        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }


}

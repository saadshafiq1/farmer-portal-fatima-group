/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fatimagroup.db


import androidx.room.Database
import androidx.room.RoomDatabase
import com.fatimagroup.db.UserDao
import com.fatimagroup.models.Farm
import com.fatimagroup.models.FarmBlock
import com.fatimagroup.models.Farmer
import com.fatimagroup.models.User

/**
 * Main database description.
 */
@Database(
    entities = [
        User::class,
        Farmer::class,
        Farm::class,
        FarmBlock::class],
    version = 1,
    exportSchema = false
)
abstract class LocalDB : RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun farmDao(): FarmDao
    abstract fun farmerDao(): FarmerDao

}

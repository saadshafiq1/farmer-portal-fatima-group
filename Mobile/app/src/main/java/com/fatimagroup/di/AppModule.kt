package com.fatimagroup.di

import android.app.Application
import androidx.room.Room
import com.fatimagroup.db.LocalDB
import com.fatimagroup.db.UserDao
import com.fatimagroup.util.LiveDataCallAdapterFactory
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.fatimagroup.AppExecutors
import com.fatimagroup.Navigator
import com.fatimagroup.db.FarmDao
import com.fatimagroup.db.FarmerDao
import com.fatimagroup.repository.*
import com.fatimagroup.repository.interactors.AuthInteractor
import javax.inject.Singleton

/**
 * Created by hunain.liaquat.
 */
@Module(includes = [ViewModelModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideApiInterface(): ApiInterface {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .build()
            .create(ApiInterface::class.java)
    }

    @Singleton
    @Provides
    fun provideNetworkDataSource(apiInterface: ApiInterface): NetworkDataSource {
        return NetworkDataSource(apiInterface)
    }

    @Singleton
    @Provides
    fun provideLocalDataSource(userDao: UserDao, farmerDao: FarmerDao, farmDao: FarmDao): LocalDataSource {
        return LocalDataSource(userDao, farmerDao, farmDao)
    }

    @Singleton
    @Provides
    fun provideSharedPrefDataSource(): SharedPrefDataSource {
        return SharedPrefDataSource()
    }

    @Provides
    fun provideLoginInteractor(
        appExecutors: AppExecutors,
        networkDataSource: NetworkDataSource,
        localDataSource: LocalDataSource,
        sharedPrefDataSource: SharedPrefDataSource
    ): AuthInteractor {
        return AuthInteractor(
            networkDataSource,
            localDataSource,
            sharedPrefDataSource,
            appExecutors
        )
    }

    @Provides
    fun provideAppExecutor(): AppExecutors {
        return AppExecutors()
    }

    @Singleton
    @Provides
    fun provideDb(app: Application): LocalDB {
        return Room
            .databaseBuilder(app, LocalDB::class.java, "local.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideUserDao(db: LocalDB): UserDao {
        return db.userDao()
    }

    @Singleton
    @Provides
    fun provideFarmerDao(db: LocalDB): FarmerDao {
        return db.farmerDao()
    }

    @Singleton
    @Provides
    fun provideFarmDao(db: LocalDB): FarmDao {
        return db.farmDao()
    }


    @Provides
    fun provideNavigator(

        localDataSource: LocalDataSource,
        sharedPrefDataSource: SharedPrefDataSource
    ): Navigator {
        return Navigator(localDataSource, sharedPrefDataSource)
    }
}
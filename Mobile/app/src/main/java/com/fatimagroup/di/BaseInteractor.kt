package com.fatimagroup.di

import com.fatimagroup.AppExecutors
import com.fatimagroup.repository.LocalDataSource
import com.fatimagroup.repository.NetworkDataSource
import com.fatimagroup.repository.SharedPrefDataSource
import javax.inject.Inject

/**
 * Created by hunain.liaquat.
 */
open class BaseInteractor {
    @Inject
    constructor(
        networkDataSource: NetworkDataSource,
        localDataSource: LocalDataSource,
        sharedPrefDataSource: SharedPrefDataSource,
        appExecutors: AppExecutors
    ) {
        this.networkDataSource = networkDataSource
        this.localDataSource = localDataSource
        this.sharedPrefDataSource = sharedPrefDataSource
        this.appExecutors = appExecutors
    }

    open lateinit var networkDataSource: NetworkDataSource;
    open lateinit var localDataSource: LocalDataSource;
    open lateinit var sharedPrefDataSource: SharedPrefDataSource;
    open lateinit var appExecutors: AppExecutors;
}
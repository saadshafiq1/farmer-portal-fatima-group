package com.fatimagroup.di

import com.fatimagroup.ui.forgot_password.ForgotPasswordFragment
import com.fatimagroup.ui.home.MainFragment
import com.fatimagroup.ui.login.LoginFragment
import com.fatimagroup.ui.otp_verification.OtpVerificationFragment
import com.fatimagroup.ui.signup.SignupFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeLoginFragment(): LoginFragment

    @ContributesAndroidInjector
    abstract fun contributeSignupFragment(): SignupFragment

    @ContributesAndroidInjector
    abstract fun contributeForgotPasswordFragment(): ForgotPasswordFragment

    @ContributesAndroidInjector
    abstract fun contributeMainFragment(): MainFragment

    @ContributesAndroidInjector
    abstract fun contributeOtpVerificationFragment(): OtpVerificationFragment
}

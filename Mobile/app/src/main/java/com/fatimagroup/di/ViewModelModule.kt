/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fatimagroup.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.fatimagroup.viewmodel.MyViewModelFactory
import com.fatimagroup.ui.forgot_password.ForgotPasswordViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import com.fatimagroup.ui.login.LoginViewModel
import com.fatimagroup.ui.otp_verification.OtpVerificationViewModel
import com.fatimagroup.ui.signup.SignupViewModel

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(repoViewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ForgotPasswordViewModel::class)
    abstract fun bindForgotPasswordViewModel(repoViewModel: ForgotPasswordViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SignupViewModel::class)
    abstract fun bindSignupViewModel(repoViewModel: SignupViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OtpVerificationViewModel::class)
    abstract fun bindOtpVerificationViewModel(repoViewModel: OtpVerificationViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: MyViewModelFactory): ViewModelProvider.Factory
}

package com.fatimagroup.models

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.fatimagroup.base.BaseResponse
import com.google.gson.annotations.SerializedName

@Entity(
    tableName = "farm",
    foreignKeys = [ForeignKey(entity = Farmer::class, parentColumns = ["farmerId"], childColumns = ["farmerId"])]
)

data class Farm(
    @PrimaryKey
    @SerializedName("farmId")
    var farmId: Int = 0,
    @SerializedName("activeStatus")
    var activeStatus: String = "",
    @SerializedName("areaonHeis")
    var areaonHeis: String = "",
    @SerializedName("canal")
    var canal: String = "",
    @SerializedName("districtId")
    var districtId: String = "",
    @SerializedName("farmAddress")
    var farmAddress: String = "",
    @SerializedName("farmGis")
    var farmGis: String = "",
    @SerializedName("farmName")
    var farmName: String = "",
    @SerializedName("farmTypeCode")
    var farmTypeCode: String = "",
    @SerializedName("farmTypeCodeNavigation")
    var farmTypeCodeNavigation: Int = 0,
    @SerializedName("farmerId")
    var farmerId: Int = 0,
    @SerializedName("insertionDate")
    var insertionDate: String = "",
    @SerializedName("modifiedBy")
    var modifiedBy: String = "",
    @SerializedName("modifiedDateTime")
    var modifiedDateTime: String = "",
    @SerializedName("ownerTubeWell")
    var ownerTubeWell: String = "",
    @SerializedName("provinceId")
    var provinceId: String = "",
    @SerializedName("regionId")
    var regionId: String = "",
    @SerializedName("tehsilId")
    var tehsilId: String = "",
    @SerializedName("totalAreaAcres")
    var totalAreaAcres: String = "",
    @SerializedName("waterCourseType")
    var waterCourseType: String = ""
) : BaseResponse()
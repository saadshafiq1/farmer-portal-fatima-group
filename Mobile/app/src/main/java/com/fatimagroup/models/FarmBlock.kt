package com.fatimagroup.models

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "farm_block",
    foreignKeys = [ForeignKey(entity = Farm::class, parentColumns = ["farmId"], childColumns = ["farmId"])]
)
data class FarmBlock(
    @PrimaryKey
    var blockId: Int = 0,
    var farmId: Int = 0
)
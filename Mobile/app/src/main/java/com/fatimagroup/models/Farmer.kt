package com.fatimagroup.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation
import com.fatimagroup.base.BaseResponse
import com.google.gson.annotations.SerializedName

@Entity(tableName = "farmer")
data class Farmer(
    @PrimaryKey
    //abc
    //abc
    @SerializedName("farmerId")
    var farmerId: Int = 0,
    @SerializedName("activeStatus")
    var activeStatus: String = "",
    @SerializedName("alternateCellPhoneNo")
    var alternateCellPhoneNo: String = "",
    @SerializedName("alternateName")
    var alternateName: String = "",
    @SerializedName("alternateRelationshipwithFarmer")
    var alternateRelationshipwithFarmer: String = "",
    @SerializedName("bigAnimals")
    var bigAnimals: Int = 0,
    @SerializedName("cellPhone")
    var cellPhone: String = "",
    @SerializedName("cnic")
    var cnic: String = "",
    @SerializedName("districtCode")
    var districtCode: Int = 0,
    @SerializedName("educationCode")
    var educationCode: Int = 0,
    @SerializedName("farmerImage")
    var farmerImage: String = "",
    @SerializedName("farmerName")
    var farmerName: String = "",
    @SerializedName("fatherHusbandName")
    var fatherHusbandName: String = "",
    @SerializedName("femaleDependant")
    var femaleDependant: Int = 0,
    @SerializedName("gender")
    var gender: String = "",
    @SerializedName("insertionDate")
    var insertionDate: String = "",
    @SerializedName("maleDependant")
    var maleDependant: Int = 0,
    @SerializedName("modifiedBy")
    var modifiedBy: Int = 0,
    @SerializedName("modifiedDateTime")
    var modifiedDateTime: String = "",
    @SerializedName("mozaName")
    var mozaName: String = "",
    @SerializedName("otherImplement")
    var otherImplement: Int = 0,
    @SerializedName("permanentAddress")
    var permanentAddress: String = "",
    @SerializedName("presentAddress")
    var presentAddress: String = "",
    @SerializedName("referalCode")
    var referalCode: String = "",
    @SerializedName("smallAnimals")
    var smallAnimals: Int = 0,
    @SerializedName("tehsilCode")
    var tehsilCode: Int = 0,
    @SerializedName("tehsilCodeNavigation")
    var tehsilCodeNavigation: Int = 0,
    @SerializedName("tractor")
    var tractor: Int = 0,
    @SerializedName("unionCouncil")
    var unionCouncil: String = ""
) : BaseResponse()

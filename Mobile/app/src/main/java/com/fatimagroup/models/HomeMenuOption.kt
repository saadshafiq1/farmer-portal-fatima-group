package com.fatimagroup.models

data class HomeMenuOption(
    var name: String,
    var id: String,
    var imageUrl: String
)
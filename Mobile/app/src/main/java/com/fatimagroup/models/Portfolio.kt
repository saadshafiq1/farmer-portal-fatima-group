package com.fatimagroup.models

data class Portfolio(
    var name: String,
    var id: String,
    var imageUrl: String
)
package com.fatimagroup.models

data class Services(
    var name: String,
    var id: String,
    var imageUrl: String
)
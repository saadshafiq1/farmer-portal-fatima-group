package com.fatimagroup.models

import androidx.room.Entity
import com.google.gson.annotations.SerializedName

/**
 * Created by hunain.liaquat.
 */
@Entity(primaryKeys = ["username"])
data class User constructor(
    @field:SerializedName("username")
    val username: String,
    @field:SerializedName("first_name")
    val firstName: String,
    @field:SerializedName("last_name")
    val lastName: String,
    @field:SerializedName("phone_number")
    val phoneNumber: String
)
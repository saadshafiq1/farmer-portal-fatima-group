package com.fatimagroup.repository

import androidx.lifecycle.LiveData
import com.fatimagroup.models.Farmer
import retrofit2.http.POST

/**
 * Created by hunain.liaquat.
 */

const val BASE_URL = "http://10.100.107.102/api/"

const val REGISTER_USER = "TbFarmerInfoes"

interface ApiInterface {


    @POST(REGISTER_USER)
    fun registerFarmer(farmer: Farmer): LiveData<ApiResponse<Farmer>>

}
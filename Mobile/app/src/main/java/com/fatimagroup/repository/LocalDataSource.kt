package com.fatimagroup.repository

import androidx.lifecycle.LiveData
import com.fatimagroup.db.FarmDao
import com.fatimagroup.db.FarmerDao
import com.fatimagroup.db.UserDao
import com.fatimagroup.models.Farmer
import javax.inject.Inject

/**
 * Created by hunain.liaquat.
 */
class LocalDataSource @Inject constructor(var userDao: UserDao, var farmerDao: FarmerDao, var farmDao: FarmDao) {
    fun insertFarmer(farmer: Farmer) {
        farmerDao.insert(farmer)
    }

    fun getFarmerByNumber(mobileNumber: String): LiveData<Farmer> {
        return farmerDao.findByPhoneNumber(mobileNumber)
    }

}
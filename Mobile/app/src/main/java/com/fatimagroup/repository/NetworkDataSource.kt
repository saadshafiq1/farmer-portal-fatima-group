package com.fatimagroup.repository

import androidx.lifecycle.LiveData
import com.fatimagroup.models.Farmer

/**
 * Created by hunain.liaquat.
 */
class NetworkDataSource constructor(var apiInterface: ApiInterface) {

    fun registerFarmer(farmer: Farmer): LiveData<ApiResponse<Farmer>> {
        return apiInterface.registerFarmer(farmer)
    }

}
package com.fatimagroup.repository.interactors

import android.os.Handler
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.fatimagroup.AppExecutors
import com.fatimagroup.models.Farmer
import com.fatimagroup.models.Resource
import com.fatimagroup.models.User
import com.fatimagroup.repository.*
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by hunain.liaquat.
 */
@Singleton
class AuthInteractor @Inject constructor(
    private var networkDataSource: NetworkDataSource,
    private var localDataSource: LocalDataSource,
    private var sharedPrefDataSource: SharedPrefDataSource,
    private var appExecutors: AppExecutors
) {

    fun login(username: String, password: String): LiveData<Resource<User>> {
        return object : NetworkBoundResource<User, User>(appExecutors) {
            override fun saveCallResult(item: User) {
            }

            override fun loadFromDb(): LiveData<User> {
                val liveData = MutableLiveData<User>()
                liveData.value = User("", "", "", "")
                return liveData
            }

            override fun createCall(): LiveData<ApiResponse<User>> {
                val liveData = MutableLiveData<ApiResponse<User>>()
                Handler().postDelayed({
                    liveData.value = ApiResponse.create(Response.success(User("hunain", "Hunain", "Liaquat", "03333")))
                }, 3000)
                return liveData;
            }

            override fun shouldFetch(data: User?): Boolean {
                return true
            }
        }.asLiveData()

    }

    fun forgotPasswordRequest(cnicNumber: String): LiveData<Resource<User>> {
        return object : NetworkBoundResource<User, User>(appExecutors) {
            override fun saveCallResult(item: User) {
            }

            override fun loadFromDb(): LiveData<User> {
                val liveData = MutableLiveData<User>()
                liveData.value = User("", "", "", "")
                return liveData
            }

            override fun createCall(): LiveData<ApiResponse<User>> {
                val liveData = MutableLiveData<ApiResponse<User>>()
                Handler().postDelayed({
                    liveData.value = ApiResponse.create(Response.success(User("hunain", "Hunain", "Liaquat", "03333")))
                }, 3000)
                return liveData;
            }

            override fun shouldFetch(data: User?): Boolean {
                return true
            }
        }.asLiveData()
    }

    fun register(
        cnicNumber: String,
        mobileNumber: String,
        username: String,
        referenceCode: String
    ): LiveData<Resource<Farmer>> {

        return object : NetworkBoundResource<Farmer, Farmer>(appExecutors) {
            override fun saveCallResult(farmer: Farmer) {
                localDataSource.insertFarmer(farmer)
            }

            override fun loadFromDb(): LiveData<Farmer> {
                return localDataSource.getFarmerByNumber(mobileNumber)
            }

            override fun createCall(): LiveData<ApiResponse<Farmer>> {
                val farmer = Farmer(cellPhone = mobileNumber, farmerName = username, cnic = cnicNumber)
                return networkDataSource.registerFarmer(farmer)
            }

            override fun shouldFetch(data: Farmer?): Boolean {
                return true
            }
        }.asLiveData()

    }
}
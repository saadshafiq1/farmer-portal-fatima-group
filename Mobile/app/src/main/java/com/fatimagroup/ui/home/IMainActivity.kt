package com.fatimagroup.ui.home

import com.fatimagroup.models.Portfolio
import com.fatimagroup.models.Services

interface IMainActivity {

    fun onPortfolioSelected(portfolio: Portfolio, position: Int)
    fun onServicesSelected(services: Services, position: Int)
}

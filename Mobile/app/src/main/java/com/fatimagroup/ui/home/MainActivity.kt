package com.fatimagroup.ui.home

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.fatimagroup.R
import com.fatimagroup.base.BaseFragment
import com.fatimagroup.base.NavBaseActivity
import com.fatimagroup.databinding.ActivityMainBinding
import com.fatimagroup.ui.weather.WeatherActivity
import com.google.android.material.navigation.NavigationView
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject


class MainActivity : NavBaseActivity(), NavigationView.OnNavigationItemSelectedListener, HasSupportFragmentInjector {

    private var TAG = "MainFragment"
    private lateinit var activityMainBinding: ActivityMainBinding
    private lateinit var mDrawerToggle: ActionBarDrawerToggle

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = DataBindingUtil.inflate(layoutInflater, R.layout.activity_main, null, false)
        setContentView(activityMainBinding.root)
        setToolbarDrawer(activityMainBinding.toolbar)

        var fragment: Fragment? = supportFragmentManager.findFragmentByTag(TAG)
        if (fragment == null) {
            fragment = MainFragment()
        }
        fragment.arguments = intent.extras
        replaceFragment(fragment as BaseFragment, false, R.id.content_frame, TAG)
    }

    private fun setToolbarDrawer(toolbar: Toolbar) {
        setSupportActionBar(toolbar)
        mDrawerToggle = ActionBarDrawerToggle(
            this,
            activityMainBinding.drawerLayout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        activityMainBinding.drawerLayout.addDrawerListener(mDrawerToggle)
        mDrawerToggle.syncState()

        setSupportActionBar(toolbar)

        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.menu_icon)
        activityMainBinding.navView.setNavigationItemSelectedListener(this)

        mDrawerToggle.isDrawerIndicatorEnabled = false
        toolbar.setNavigationIcon(R.drawable.menu_icon)
        toolbar.setNavigationOnClickListener {
            run {
                if (activityMainBinding.drawerLayout.isDrawerOpen(GravityCompat.START))
                    activityMainBinding.drawerLayout.closeDrawer(GravityCompat.START)
                else
                    activityMainBinding.drawerLayout.openDrawer(GravityCompat.START)
            }
        }
    }

    override fun onBackPressed() {
        if (activityMainBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            1
            activityMainBinding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        if (this::mDrawerToggle.isInitialized)
            mDrawerToggle.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if (this::mDrawerToggle.isInitialized)
            mDrawerToggle.onConfigurationChanged(newConfig)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_weather -> {
                var intent = Intent(this, WeatherActivity::class.java)
                startActivity(intent)
                // Handle the camera action
            }
            R.id.nav_news -> {

            }
            R.id.nav_request_advisor -> {

            }
            R.id.nav_request_test -> {

            }
            R.id.nav_reports -> {

            }
            R.id.nav_tickets -> {

            }
            R.id.nav_information -> {

            }
        }
        activityMainBinding.drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }
}

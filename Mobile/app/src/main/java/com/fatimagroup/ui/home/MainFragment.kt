package com.fatimagroup.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.fatimagroup.AppExecutors
import com.fatimagroup.R
import com.fatimagroup.base.BaseFragment
import com.fatimagroup.base.MyDataBindingComponent
import com.fatimagroup.databinding.FragmentMainBinding
import com.fatimagroup.di.Injectable
import com.fatimagroup.models.Portfolio
import com.fatimagroup.models.Services
import javax.inject.Inject


class MainFragment : BaseFragment(), Injectable, IMainActivity {


    @Inject
    lateinit var appExecutors: AppExecutors
    private lateinit var fragmentMainBinding: FragmentMainBinding

    var dataBindingComponent: DataBindingComponent =
        MyDataBindingComponent(this)

    override fun initUI() {
        val list = ArrayList<Portfolio>()
        list.add(Portfolio("test1", "1", ""))
        list.add(Portfolio("test2", "2", ""))
        list.add(Portfolio("test3", "3", ""))
        list.add(Portfolio("test4", "4", ""))
        val portfolioAdapter = PortfolioAdapter(dataBindingComponent, appExecutors, this)
        fragmentMainBinding.rvPortfolio.layoutManager = GridLayoutManager(activity, 3)
        fragmentMainBinding.rvPortfolio.adapter = portfolioAdapter
        portfolioAdapter.submitList(list)


        val servicesList = ArrayList<Services>()
        servicesList.add(Services("پیداوار کیلکولیٹر", "1", ""))
        servicesList.add(Services("فصل کی منصوبہ بندی", "2", ""))
        servicesList.add(Services("معلومات", "3", ""))
        servicesList.add(Services("ہماری خدمات", "4", ""))
        val servicesAdapter = ServicesAdapter(dataBindingComponent, appExecutors, this)
        fragmentMainBinding.rvServices.layoutManager = GridLayoutManager(activity, 2)
        fragmentMainBinding.rvServices.adapter = servicesAdapter
        servicesAdapter.submitList(servicesList)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragmentMainBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        initUI()
        return fragmentMainBinding.root
    }

    override fun onPortfolioSelected(portfolio: Portfolio, position: Int) {
    }

    override fun onServicesSelected(services: Services, position: Int) {
    }
}

package com.fatimagroup.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.fatimagroup.base.DataBoundListAdapter
import com.fatimagroup.AppExecutors
import com.fatimagroup.R
import com.fatimagroup.databinding.ItemPortfolioHomeBinding
import com.fatimagroup.models.Portfolio

class PortfolioAdapter(
    private val dataBindingComponent: DataBindingComponent,
    appExecutors: AppExecutors,
    private val repoClickCallback: IMainActivity
) : DataBoundListAdapter<Portfolio, ItemPortfolioHomeBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<Portfolio>() {
        override fun areItemsTheSame(oldItem: Portfolio, newItem: Portfolio): Boolean {
            return oldItem.id == newItem.id

        }

        override fun areContentsTheSame(oldItem: Portfolio, newItem: Portfolio): Boolean {
            return oldItem.name == newItem.name

        }
    }
) {

    override fun createBinding(parent: ViewGroup): ItemPortfolioHomeBinding {
        val binding = DataBindingUtil.inflate<ItemPortfolioHomeBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_portfolio_home,
            parent,
            false,
            dataBindingComponent
        )

        return binding
    }

    override fun bind(binding: ItemPortfolioHomeBinding, item: Portfolio, position: Int) {
        binding.portfolio = item
        binding.root.setOnClickListener {
            binding.portfolio?.let { portfolio ->
                repoClickCallback?.onPortfolioSelected(portfolio, position)
            }
        }
    }
}
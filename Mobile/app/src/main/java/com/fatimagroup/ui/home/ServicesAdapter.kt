package com.fatimagroup.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.fatimagroup.base.DataBoundListAdapter
import com.fatimagroup.AppExecutors
import com.fatimagroup.R
import com.fatimagroup.databinding.ItemServicesHomeBinding
import com.fatimagroup.models.Services

class ServicesAdapter(
    private val dataBindingComponent: DataBindingComponent,
    appExecutors: AppExecutors,
    private val repoClickCallback: IMainActivity
) : DataBoundListAdapter<Services, ItemServicesHomeBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<Services>() {
        override fun areItemsTheSame(oldItem: Services, newItem: Services): Boolean {
            return oldItem.id == newItem.id

        }

        override fun areContentsTheSame(oldItem: Services, newItem: Services): Boolean {
            return oldItem.name == newItem.name

        }
    }
) {

    override fun createBinding(parent: ViewGroup): ItemServicesHomeBinding {
        val binding = DataBindingUtil.inflate<ItemServicesHomeBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_services_home,
            parent,
            false,
            dataBindingComponent
        )

        return binding
    }

    override fun bind(binding: ItemServicesHomeBinding, item: Services, position: Int) {
        binding.services = item
        binding.root.setOnClickListener {
            binding.services?.let { service ->
                repoClickCallback?.onServicesSelected(service, position)
            }
        }
    }
}
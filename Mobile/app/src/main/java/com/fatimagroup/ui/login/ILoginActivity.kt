package com.fatimagroup.ui.login

/**
 * Created by hunain.liaquat.
 */
interface ILoginActivity {

    fun onLoginClicked()
    fun onForgotPasswordClicked()
    fun onRegisterClicked()

}
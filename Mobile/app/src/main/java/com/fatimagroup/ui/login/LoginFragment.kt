package com.fatimagroup.ui.login

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.fatimagroup.AppExecutors
import com.fatimagroup.Navigator
import com.fatimagroup.R
import com.fatimagroup.base.BaseActivity

import com.fatimagroup.base.BaseFragment
import com.fatimagroup.databinding.FragmentLoginBinding
import com.fatimagroup.di.Injectable
import com.fatimagroup.models.Status
import com.fatimagroup.repository.ApiSuccessResponse
import com.fatimagroup.util.autoCleared
import javax.inject.Inject


class LoginFragment : BaseFragment(), Injectable, ILoginActivity {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var loginViewModel: LoginViewModel

    @Inject
    lateinit var appExecutors: AppExecutors

    lateinit var mView: View

    @Inject
    lateinit var navigator: Navigator

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        loginViewModel = ViewModelProviders.of(this, viewModelFactory).get(LoginViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        mView = binding.root
        binding.iLoginActivity = this
        initUI()
        return mView

    }

    override fun initUI() {
    }

    private fun validate(): Boolean {
        var cnicNumber: String = binding.etPhoneNumber.text.toString()
        var password: String = binding.etPassword.text.toString()
        binding.tilPhoneNumber.isErrorEnabled = false
        binding.tilPassword.isErrorEnabled = false

        var valid: Boolean = true
        if (cnicNumber.replace(" ", "").replace("x", "").length != 11) {
            valid = false
            binding.tilPhoneNumber.error = getString(R.string.error_mobile_number)
            binding.tilPhoneNumber.isErrorEnabled = true
        }
        if (password.length < 6) {
            valid = false
            binding.tilPassword.isErrorEnabled = true
            binding.tilPassword.error = getString(R.string.error_password)
        }
        return valid

    }

    var binding by autoCleared<FragmentLoginBinding>()


    override fun onLoginClicked() {
        if (validate()) {
            loginViewModel.login(binding.etPhoneNumber.text.toString(), binding.etPassword.text.toString())
                .observe(this,
                    Observer { resource ->
                        run {
                            when (resource.status) {
                                Status.SUCCESS -> {
                                    navigator.gotoMainActivity(activity as BaseActivity)
                                }
                                Status.LOADING -> {
                                    Toast.makeText(activity, "Logging in", Toast.LENGTH_SHORT).show()
                                }
                                Status.ERROR -> {

                                }
                                else -> {
                                }
                            }
                        }
                    })
        }
    }

    override fun onForgotPasswordClicked() {
        navigator.gotoForgotPasswordActivity(activity as BaseActivity)
    }

    override fun onRegisterClicked() {
        navigator.gotoSignupActivity(activity as BaseActivity)
    }
}

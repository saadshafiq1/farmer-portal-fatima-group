package com.fatimagroup.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.fatimagroup.models.Resource
import com.fatimagroup.models.User
import com.fatimagroup.repository.interactors.AuthInteractor
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by hunain.liaquat.
 */
class LoginViewModel @Inject constructor(var authInteractor: AuthInteractor) : ViewModel() {


    fun login(cnicNumber: String, password: String) : LiveData<Resource<User>>{
        Timber.d("Testing loginViewModel")
        return authInteractor.login(cnicNumber, password)

    }
}
package com.fatimagroup.ui.otp_verification

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.fatimagroup.R
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import com.fatimagroup.base.BaseActivity
import com.fatimagroup.base.BaseFragment
import javax.inject.Inject

class OtpVerificationActivity : BaseActivity(), HasSupportFragmentInjector {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector


    var TAG: String = "LoginFragment"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp_verification)
        var fragment: Fragment? = supportFragmentManager.findFragmentByTag(TAG)
        if (fragment == null) {
            fragment = OtpVerificationFragment()
        }
        fragment.arguments = intent.extras
        replaceFragment(fragment as BaseFragment, false, R.id.frame_content, TAG)
    }


}

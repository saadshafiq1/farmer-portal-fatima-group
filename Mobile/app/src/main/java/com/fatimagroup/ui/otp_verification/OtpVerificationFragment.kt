package com.fatimagroup.ui.otp_verification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.fatimagroup.AppExecutors
import com.fatimagroup.Navigator
import com.fatimagroup.R
import com.fatimagroup.base.BaseActivity

import com.fatimagroup.base.BaseFragment
import com.fatimagroup.databinding.FragmentForgotPasswordBinding
import com.fatimagroup.databinding.FragmentOtpVerificationBinding
import com.fatimagroup.di.Injectable
import com.fatimagroup.models.Status
import com.fatimagroup.util.autoCleared
import javax.inject.Inject


class OtpVerificationFragment : BaseFragment(), Injectable, IOtpVerificationActivity {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var otpVerificationViewModel: OtpVerificationViewModel

    @Inject
    lateinit var appExecutors: AppExecutors

    lateinit var mView: View

    @Inject
    lateinit var navigator: Navigator
    var binding by autoCleared<FragmentOtpVerificationBinding>()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        otpVerificationViewModel =
                ViewModelProviders.of(this, viewModelFactory).get(OtpVerificationViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_otp_verification, container, false)
        mView = binding.root
        binding.iOtpVerificationActivity = this
        initUI()
        return mView

    }

    private fun validate(): Boolean {
        var cnicNumber: String = binding.etPhoneNumber.text.toString()
        var valid: Boolean = true
        binding.tilPhoneNumber.isErrorEnabled = false
        if (cnicNumber.replace("-", "").length != 13) {
            valid = false
            binding.tilPhoneNumber.error = getString(R.string.error_cnic)
            binding.tilPhoneNumber.isErrorEnabled = true
        }
        return valid

    }


    override fun initUI() {
    }

    override fun onSubmitClicked() {
        if (validate()) {
            otpVerificationViewModel.forgotPasswordRequest(
                binding.etPhoneNumber.text.toString()
            ).observe(this,
                Observer { resource ->
                    run {
                        when (resource.status) {
                            Status.SUCCESS -> {
                                navigator.gotoMainActivity(activity as BaseActivity)
                            }
                            Status.LOADING -> {
                                Toast.makeText(activity, "Logging in", Toast.LENGTH_SHORT).show()
                            }
                            Status.ERROR -> {

                            }
                            else -> {
                            }
                        }
                    }
                })
        }
    }

}

package com.fatimagroup.ui.otp_verification

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.fatimagroup.models.Resource
import com.fatimagroup.models.User
import com.fatimagroup.repository.interactors.AuthInteractor
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by hunain.liaquat.
 */
class OtpVerificationViewModel @Inject constructor(var authInteractor: AuthInteractor) : ViewModel() {

    fun forgotPasswordRequest(cnicNumber: String): LiveData<Resource<User>> {
        return authInteractor.forgotPasswordRequest(cnicNumber)
    }
}
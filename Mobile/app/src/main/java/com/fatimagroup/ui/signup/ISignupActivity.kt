package com.fatimagroup.ui.signup

/**
 * Created by hunain.liaquat.
 */
interface ISignupActivity {

    fun onRegisterClicked()
    fun onImageSelectClicked()

}
package com.fatimagroup.ui.signup

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.fatimagroup.R
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import com.fatimagroup.base.BaseActivity
import com.fatimagroup.base.BaseFragment
import javax.inject.Inject

class SignupActivity : BaseActivity(), HasSupportFragmentInjector {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingAndroidInjector


    val TAG: String = "SignupActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        var fragment: Fragment? = supportFragmentManager.findFragmentByTag(TAG)
        if (fragment == null) {
            fragment = SignupFragment()
        }
        fragment.arguments = intent.extras
        replaceFragment(fragment as BaseFragment, false, R.id.frame_content, TAG)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}

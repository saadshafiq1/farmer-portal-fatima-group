package com.fatimagroup.ui.signup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.fatimagroup.AppExecutors
import com.fatimagroup.Navigator
import com.fatimagroup.R
import com.fatimagroup.base.BaseActivity

import com.fatimagroup.base.BaseFragment
import com.fatimagroup.databinding.FragmentSignupBinding
import com.fatimagroup.di.Injectable
import com.fatimagroup.models.Status
import com.fatimagroup.util.autoCleared
import com.fxn.pix.Pix
import javax.inject.Inject
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import com.bumptech.glide.Glide
import com.fxn.utility.PermUtil


class SignupFragment : BaseFragment(), Injectable, ISignupActivity {
    private val IMAGE_SELECTION: Int = 1001

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var signupViewModel: SignupViewModel

    @Inject
    lateinit var appExecutors: AppExecutors

    private lateinit var mView: View

    @Inject
    lateinit var navigator: Navigator
    var binding by autoCleared<FragmentSignupBinding>()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        signupViewModel = ViewModelProviders.of(this, viewModelFactory).get(SignupViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_signup, container, false)
        mView = binding.root
        binding.iSignupActivity = this
        initUI()
        return mView

    }

    private fun validate(): Boolean {
        val cnicNumber: String = binding.etPhoneNumber.text.toString()
        val userName: String = binding.etName.text.toString()
        var valid: Boolean = true
        if (cnicNumber.replace("-", "").length != 13) {
            valid = false
        }
        if (userName.length < 6) {
            valid = false
        }
        return valid
    }


    override fun initUI() {
    }


    override fun onRegisterClicked() {
        if (validate())
            signupViewModel.register(
                binding.etCnicNumber.text.toString(), binding.etPhoneNumber.text.toString(),
                binding.etName.text.toString(), binding.etReference.text.toString()
            ).observe(this,
                Observer { resource ->
                    run {
                        when (resource.status) {
                            Status.SUCCESS -> {
                                navigator.gotoMainActivity(activity as BaseActivity)
                            }
                            Status.LOADING -> {
                                Toast.makeText(activity, "Logging in", Toast.LENGTH_SHORT).show()
                            }
                            Status.ERROR -> {

                            }
                            else -> {
                            }
                        }
                    }
                })
    }

    override fun onImageSelectClicked() {
        Pix.start(
            this,
            IMAGE_SELECTION
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_SELECTION) {
            val returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)
            if (returnValue.size > 0) {
                binding.imgCamera.visibility = View.GONE
                binding.imgUserPic.visibility = View.VISIBLE
                Glide.with(this).load(returnValue[0]).into(binding.imgUserPic)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Pix.start(this, IMAGE_SELECTION)
                } else {
//                    Toast.makeText(this@MainActivity, "Approve permissions to open Pix ImagePicker", Toast.LENGTH_LONG)
//                        .show()
                }
                return
            }
        }
    }
}

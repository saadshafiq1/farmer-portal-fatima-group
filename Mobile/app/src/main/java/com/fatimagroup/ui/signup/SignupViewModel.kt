package com.fatimagroup.ui.signup

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.fatimagroup.models.Resource
import com.fatimagroup.models.User
import com.fatimagroup.repository.interactors.AuthInteractor
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by hunain.liaquat.
 */
class SignupViewModel @Inject constructor(var authInteractor: AuthInteractor) : ViewModel() {


    fun register(
        cnicNumber: String,
        mobileNumber: String,
        userName: String,
        referenceCode: String
    ): LiveData<Resource<User>> {
        Timber.d("Testing signupViewModel")
        return authInteractor.login(cnicNumber, mobileNumber)

    }
}
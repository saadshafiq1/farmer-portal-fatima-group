package com.fatimagroup.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.fatimagroup.Navigator
import com.fatimagroup.R
import com.fatimagroup.base.BaseActivity
import com.fatimagroup.ui.login.LoginActivity
import javax.inject.Inject

class SplashActivity : BaseActivity() {

    @Inject
    lateinit var navigator: Navigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler().postDelayed({
            navigator.gotoLoginActivity(this)
            finish()
        }, 2000)
    }
}

package com.fatimagroup.ui.weather

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.fatimagroup.R
import com.fatimagroup.base.NavBaseActivity

class WeatherActivity : NavBaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weather)
    }
}

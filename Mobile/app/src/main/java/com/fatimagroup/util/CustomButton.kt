package com.fatimagroup.util

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatButton

/**
 * Created by hunain.liaquat.
 */
class CustomButton(context: Context?, attributeSet: AttributeSet?) : AppCompatButton(context, attributeSet) {

    constructor(context: Context?) : this(context, null) {

    }
}
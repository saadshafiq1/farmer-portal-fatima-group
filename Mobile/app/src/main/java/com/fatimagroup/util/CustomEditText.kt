package com.fatimagroup.util

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText

/**
 * Created by hunain.liaquat.
 */
class CustomEditText(context: Context?, attributeSet: AttributeSet?) : AppCompatEditText(context, attributeSet) {

    constructor(context: Context?) : this(context, null) {

    }
}
package com.fatimagroup.util

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.textfield.TextInputLayout

/**
 * Created by hunain.liaquat.
 */
class CustomTextInputLayout(context: Context?, attributeSet: AttributeSet?) : TextInputLayout(context, attributeSet) {

    constructor(context: Context?) : this(context, null) {

    }
}
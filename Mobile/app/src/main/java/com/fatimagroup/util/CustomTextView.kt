package com.fatimagroup.util

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView

/**
 * Created by hunain.liaquat.
 */
class CustomTextView(context: Context?, attributeSet: AttributeSet?) : AppCompatTextView(context, attributeSet) {

    constructor(context: Context?) : this(context, null) {

    }
}